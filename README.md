# CRM SHOP PROJECT

## Project structure

- `/bin` - console commands  
- `/config` - configuration files 
- `/public` - root web directory
- `/src` - PHP code
    - `/Application` - application logic classes
    - `/Cli` - console command classes
    - `/Database` - fixtures & migrations and Domain logic classes
    - `/Http` - http protocol response and application logic handling
- `/templates` - twig template files (pages & emails)
- `/tests` - tests
- `/translations` - translations
- `/var` - tmp files
- `/vendor` - composer dependency

## How to set up?

1. Create an empty MySQL / PostgreSQL database
2. Create in the root `.env.local` file and specify `DATABASE_URL` & `MAILER_DSN`
3. Install composer dependencies (composer install)
4. Run doctrine schema updater
    ```bash
    bin/console doctrine:schema:update --force
    ```
5. Create application globals
    ```bash
    bin/console globals:create
    ```
6. Create static site pages
    ```bash
    bin/console site:pages:create
    ```
7. Create admin user
    ```bash
    bin/console user:create:admin [--login] [--password]
    ```
8. Create menu links (--optional)
    ```bash
    bin/console site:menu:create
    ```
9. Configure apache/nginx for index.php file in public directory (configuration examples are stored there)
9. Run the server. (eg. php -S localhost:8080)
    ```bash
    cd public
    ```
    ```bash
    php -S localhost:8080
    ```

Full list of CLI commands
=
Application
* bin/console globals:create [--clear]
* bin/console site:pages:create [--clear]
* bin/console site:menu:create [--clear]

User
* bin/console user:create:admin [--login] [--password]
