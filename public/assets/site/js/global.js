$('p').each(function () {
    $(this).addClass('fw500').addClass('text-dark')
});

$('td').each(function () {
    $(this).addClass('fw500').addClass('text-dark')
});

let maxModuleHeader = 0;
$('.js-module-header').each(function () {
    if ($(this).height() > maxModuleHeader) {
        maxModuleHeader = $(this).height();
    }
}).each(function () {
    $(this).height(maxModuleHeader);
});

let maxModulePrice = 0;
$('.js-module-price').each(function () {
    if ($(this).height() > maxModulePrice) {
        maxModulePrice = $(this).height();
    }
}).each(function () {
    $(this).height(maxModulePrice);
});
