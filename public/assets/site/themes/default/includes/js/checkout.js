$('#order_save').attr('class', 'view-btn form-control text-white');
$('select').addClass('custom-select');
$('#order_comment').replaceWith(`<textarea name="order[comment]" class="form-control" placeholder="${TEXT_COMMENT}"></textarea>`);

$('#js-nova-city').hide();
$('#js-nova-warehouse').hide();
$('#js-nova-price').hide();

$(DELIVERY_TYPE_SELECTOR).on('change', function () {
    if ($(this).val() === KEY_DELIVERY_COMPANY_NOVA) {
        $('#js-nova-city').show();
        $('#js-nova-warehouse').show();
        $('#js-nova-price').show();
        $('#js-city').hide();
        $('#js-address').hide();
        $('#js-courier-price').hide();
    } else {
        $('#js-nova-city').hide();
        $('#js-nova-warehouse').hide();
        $('#js-nova-price').hide();
        $('#js-city').show();
        $('#js-address').show();
        $('#js-courier-price').show();
    }
});


$('#order_nova_city').on('change', function () {
    $.ajax({
        url: WAREHOUSES_URL,
        method: 'GET',
        data: {ref: $('#order_nova_city').val()},
        success: function (res) {
            $('#order_nova_warehouse').html('');
            res.forEach(item => {
                $('#order_nova_warehouse').append(`<option value="${item.Ref}">${item.Description}</option>`);
            })
        }
    })
});
