(function () {
    try {
        $('select[data-type="nice-select"]').niceSelect();

        const urlParams = new URLSearchParams(window.location.search);
        const sortParam = urlParams.get('sort');

        $(document).click(function (e) {
            sortRedirect($(e.target).attr('data-value'));

            if ($(e.target).attr('data-value') === 'sort-filter-desc') {
                sortParamRedirect(sortParam);
            }

            if ($(e.target).attr('data-value') === 'sort-filter-asc') {
                sortParamRedirect(sortParam);
            }

        });

        function sortRedirect(query) {
            if ($(`a[title="${query}"]`).attr('href') !== undefined) {
                window.location.replace($(`a[title=${query}]`).attr('href'));
            }
        }

        function sortParamRedirect(query) {
            if (query === 'p.price') {
                window.location.replace($('a[title=sort-price]').attr('href'));
            }

            if (query === 'p.creationDate') {
                window.location.replace($('a[title=sort-creation-date]').attr('href'));
            }
        }
    } catch (e) {
        //
    }
})(); // Sorting


$('.js-add-to-cart').each(function () {
    $(this).on('click', function (e) {
        let id = $(e.target).attr('data-id');
        $.ajax({
            url: URL_ADD_PRODUCT,
            method: 'POST',
            data: {
                id:        id,
                quantity:  1,
                varieties: []
            },
            success: function () {
                let btn = $('body').find(`button[data-id="${id}"]`);

                btn.html(`<span>${TEXT_IN_CART}</span><span class="lnr lnr-checkmark-circle ml-10"></span>`);
                btn.on('click', toCart);
                let val = $('#js-cart-product-counter').text();
                val++;
                $('#js-cart-product-counter').text(val);
            }
        })
    })
});

function toCart() {
    location.replace(REDIRECT_URL_INDEX);
}

$(window).on('load resize', function () {
    let max = 0;
    $('.js-product-pic').each(function () {
        let parent = $(this).parent().parent();
        let parentHeight = $(parent).height();

        if (parentHeight > max) {
            max = parentHeight;
        }
    }).each(function () {
        $(this).parent().parent().height(max);
    });

    let maxBody = 0;
    $('.js-product-body').each(function () {
        if ($(this).height() > maxBody) {
            maxBody = $(this).height();
        }
    }).each(function () {
        $(this).height(maxBody);
    });

    if ($(window).width() < 650) { // restyling on phones
        $('.js-product-btn').each(function () {
            $(this)
                .removeClass('view-btn')
                .removeClass('color-2')
                .addClass('genric-btn')
                .addClass('info')
                .addClass('small')
                .find('span').css({
                    'font-size': '10px',
                    'font-weight': '700'
                });

            $(this).find('.lnr').hide();
        });

        let maxName = 0;
        $('.js-product-name').each(function () {
            if ($(this).height() > maxName) {
                maxName = $(this).height();
            }
        }).each(function () {
            $(this).height(maxName);
        });

        $('.js-product-pic').css('max-height', '180px');

        $('.js-product-cart').each(function () {
            $(this).css({
                'padding-left': 0,
                'padding-right': 0
            })
        })
    }
});
