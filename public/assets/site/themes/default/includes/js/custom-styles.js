$(document).on('ready',function () {
    $('.custom-content img').each(function () {
        let img = $(this).addClass('img-thumbnail').clone();
        let parent = $(this).parent();

        $(this).remove();

        let div = document.createElement('div');
        $(div).append(img);

        $(parent).append(div);
    });

    $('iframe').each(function () {

        let frame = $(this).clone().addClass('embed-responsive-item');
        let frameParent = $(this).parent();

        $(this).remove();

        let div = document.createElement('div');
        $(div).addClass('embed-responsive').addClass('embed-responsive-16by9').append(frame);

        $(frameParent).append(div);
    });

    $('iframe').each(function () {
        let parent = $(this).parent();

        let frameWidth = parent.width();
        if (parent.width() > 650) {
            frameWidth = 650;
        }

        let frameHeight = parent.height();
        if (parent.height() > 350) {
            frameHeight = 350;
        }

        $(this).width(frameWidth).height(frameHeight);
    });

    $('p').each(function () {
        $(this).addClass('fw500').addClass('text-dark')
    });

    $('td').each(function () {
        $(this).addClass('fw500').addClass('text-dark')
    })
});
