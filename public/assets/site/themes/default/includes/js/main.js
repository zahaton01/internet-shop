let maxModuleHeader = 0;
$('.js-module-header').each(function () {
    let thisHeight = $(this).height();

    if (thisHeight > maxModuleHeader) {
        maxModuleHeader = thisHeight;
    }
});

$('.js-module-header').each(function () {
    $(this).height(maxModuleHeader);
});

let maxModulePrice = 0;
$('.js-module-price').each(function () {
    let thisHeight = $(this).height();

    if (thisHeight > maxModulePrice) {
        maxModulePrice = thisHeight;
    }
});

$('.js-module-price').each(function () {
    $(this).height(maxModulePrice);
})
