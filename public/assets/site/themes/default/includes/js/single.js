$('#addToCart').on('click', function () {
    let varieties = [];

    $('.js-variety').each(function () {
        let variety = {
            variety: $(this).attr('data-id'),
            value:   $(this).val()
        };

        varieties.push(variety);
    });

    $.ajax({
        url: URL_ADD_PRODUCT,
        method: 'POST',
        data: {
            id:        $(this).attr('data-id'),
            quantity:  1,
            varieties: varieties
        },
        success: function () {
            $('#addToCart').html(`<span>${TEXT_IN_CART}</span><span class="lnr lnr-checkmark-circle ml-10"></span>`).on('click', toCart);
            let val = $('#js-cart-product-counter').text();
            val++;
            $('#js-cart-product-counter').text(val);
        }
    })
});

function toCart() {
    location.replace(REDIRECT_URL_CART);
}

function swapElement(a, b) {
    var aNext = $('<div>').insertAfter(a);
    a.insertAfter(b);
    b.insertBefore(aNext);
    aNext.remove();
}

$(document).ready(function(){
    $('#tab-desc').find('ul').addClass('unordered-list');
    $('#tab-desc').find('ol').addClass('ordered-list');

    if ($(window).width() < 1000) {
        swapElement($('#prodDesc'), $('#img-nav'))
    }

    $('.slider-for').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        fade: true,
        asNavFor: '.slider-nav'
    });

    $('.slider-nav').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        asNavFor: '.slider-for',
        autoplay: true,
        arrows: true,
        centerMode: false,
        focusOnSelect: true
    });

    $('.slick-slide:not(.slick-cloned)').each(function () {
        $(this).on('click', function () {
            $(this).magnificPopup({
                items: {
                    src: $(this).find('img').attr('src')
                },
                type: 'image'
            });
        })
    });
});