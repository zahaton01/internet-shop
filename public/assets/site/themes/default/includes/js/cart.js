$(document).ready(function () {
    let value,
        quantity = document.getElementsByClassName('quantity-container');

    function createBindings(quantityContainer) {
        var quantityAmount = quantityContainer.getElementsByClassName('quantity-amount')[0];
        var increase = quantityContainer.getElementsByClassName('increase')[0];
        var decrease = quantityContainer.getElementsByClassName('decrease')[0];
        increase.addEventListener('click', function () {
            increaseValue(quantityAmount);
        });
        decrease.addEventListener('click', function () {
            decreaseValue(quantityAmount);
        });
    }

    function init() {
        for (var i = 0; i < quantity.length; i++ ) {
            createBindings(quantity[i]);
        }
    }

    function increaseValue(quantityAmount) {
        value = parseInt(quantityAmount.value, 10);
        value = isNaN(value) ? 1 : value;
        value++;
        quantityAmount.value = value;
        countPrice(quantityAmount);
    }

    function decreaseValue(quantityAmount) {
        value = parseInt(quantityAmount.value, 10);

        value = isNaN(value) ? 1 : value;
        if (value > 1) value--;

        quantityAmount.value = value;
        countPrice(quantityAmount);
    }

    function isInt(n){
        return Number(n) === n && n % 1 === 0;
    }

    let totalPrice = 0;
    function countPrice(quantityAmount, value) {
        let id = $(quantityAmount).attr('data-id');
        let price = $(quantityAmount).attr('data-price');

        if (!isInt(value)) {
            value = +$(quantityAmount).val();
        }

        price *= value;

        $('#js-total-' + id).text(numeral(price).format('(0.00)') + ` ${CURRENCY}`);
        totalPrice = 0;
        $('.js-total-price').each(function () {
            let price = $(this).text();
            if (!isInt(price)) {
                price = parseFloat(price);
            } else {
                price = parseInt(price);
            }
            totalPrice += price;
        });

        $('#subTotal').text(numeral(totalPrice).format('(0.00)') + ` ${CURRENCY}`)
    }

    $('.quantity-amount').each(function () {
        $(this).on('change', function () {
            countPrice(this);
        })
    });

    init();

});

function setInputFilter(textbox, inputFilter) {
    ["input", "keydown", "keyup", "mousedown", "mouseup", "select", "contextmenu", "drop"].forEach(function(event) {
        textbox.addEventListener(event, function() {
            if (inputFilter(this.value)) {
                this.oldValue = this.value;
                this.oldSelectionStart = this.selectionStart;
                this.oldSelectionEnd = this.selectionEnd;
            } else if (this.hasOwnProperty("oldValue")) {
                this.value = this.oldValue;
                this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
            }
        });
    });
}

$('.quantity-amount').each(function () {
    setInputFilter(this, function(value) {
        return /^\d*$/.test(value) && (parseInt(value) !== 0);
    });
});

$('#finish').on('click', function () {
    let res = [];
    $('.js-total-price').each(function () {
        let product   = {};
        let id        = $(this).attr('data-id');
        let varieties = [];

        $('.js-varieties-ul').each(function () {
           if ($(this).attr('data-id') === id) {
               $(this).find('.js-variety').each(function () {
                   let variety = {
                       variety: $(this).attr('data-id'),
                       value:   $(this).val()
                   };

                   varieties.push(variety);
               })
           }
        });

        product.id        = id;
        product.quantity  = $('body').find('input[data-id='+product.id+']').val();
        product.varieties = varieties;
        res.push(product);
    });

    if (res.length < 1) {
        $('#errHolder').show().text(TEXT_NO_ITEMS_IN_CART);
    } else {
        $.ajax({
            url: REFILL_CART_URL,
            method: 'POST',
            data: { cart: res },
            success: function () {
                location.replace(ORDER_URL)
            }
        })
    }
});
