app.service('utilService', function () {
    return {
        guid: function () {
            return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
                let r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
                return v.toString(16);
            });
        },
        dateNow: function () {
            let date  = new Date();
            let month = date.getMonth() + 1;

            if (month < 10) {
                month = `0${month}`
            }

            return `${date.getDate()}-${month}-${date.getFullYear()}`;
        },
        errorStringFromResponse: function (errors) {
            let msg = '';

            errors.forEach(error => {
                msg += `${error.message}. `;

                if ('' !== error.pointer) {
                    msg += `(${error.pointer})`
                }
            });

            return msg;
        }
    }
});