app.controller("editProductVarietyCtrl", function ($scope, $http, utilService) {
    $scope.allValues      = [];
    $scope.availabilities = [];
    $scope.productVariety = {
        id: VARIETY_ID,
        variety: {},
        values: []
    };

    $scope.changeValues = function () {
        $scope.productVariety.values = [];

        $('.js-value').each(function () {
            if ($(this).is(':checked')) {
                let id = $(this).attr('value-id');

                $scope.productVariety.values.push({
                    id:           id,
                    availability: $(`#availability_${id}`).val(),
                    checked:      true
                });
            }
        });
    };

    $scope.submit = function () {
        $('#error').hide();

        $http({
            method: "PUT",
            url:    URL_PUT_PRODUCT_VARIETY,
            data:   $scope.productVariety
        }).then( () => {
            window.location.replace(REDIRECT_URL_PRODUCT)
        }, errorResponse => {
            $('#error').html(utilService.errorStringFromResponse(errorResponse.data.errors)).show();
        });
    };

    $scope.init = function () {
        $http({
            method: "GET",
            url: URL_GET_AVAILABILITIES,
        }).then(successResult => { $scope.availabilities = successResult.data });

        $http({
            method: "GET",
            url: URL_GET_PRODUCT_VARIETY,
            params: {id: VARIETY_ID}
        }).then( successResult => {
            let data                             = successResult.data;
            $scope.productVariety.variety.id     = data.variety.id;
            $scope.productVariety.variety.name   = data.variety.name;
            $scope.productVariety.values         = data.values;
            $scope.allValues                     = data['all_values'];

            $scope.allValues.forEach((possibleValue, index) => {
               $scope.productVariety.values.forEach(savedValue => {
                  if (possibleValue.id === savedValue.id) {
                      $scope.allValues[index].checked      = true;
                      $scope.allValues[index].availability = savedValue.availability;
                  }
               });
            });
        });
    }
});
