app.controller("createProductVarietyCtrl", function ($scope, $http, utilService) {
    $scope.varieties      = [];
    $scope.values         = [];
    $scope.availabilities = [];
    $scope.productVariety = {
        variety: {
            id: ''
        },
        product: {
            id: PRODUCT_ID
        },
        values: [],
    };

    $scope.changeValues = function () {
        $scope.productVariety.values = [];

        $('.js-value').each(function () {
            if ($(this).is(':checked')) {
                let id = $(this).attr('value-id');

                $scope.productVariety.values.push({
                    id: id,
                    availability: $(`#availability_${id}`).val()
                });
            }
        });
    };

    $scope.getValues = function () {
        $http({
            method: "GET",
            url: URL_GET_VALUES,
            params: {variety: $scope.productVariety.variety.id}
        }).then( successResult => {
            $scope.values                = successResult.data;
            $scope.productVariety.values = [];

            $scope.values.forEach(val => {
                $scope.productVariety.values.push({
                    id: val.id,
                    availability: AVAILABILITY_AVAILABLE
                });
            });
        });
    };

    $scope.submit = function () {
        $('#error').hide();

        $http({
            method: "POST",
            url: URL_POST_VARIETY,
            data: $scope.productVariety
        }).then( () => {
            window.location.replace(REDIRECT_URL_PRODUCT);
        }, errorResponse => {
            $('#error').html(utilService.errorStringFromResponse(errorResponse.data.errors)).show();
        });
    };

    $scope.init = function () {
        $http({
            method: "GET",
            url: URL_GET_VARIETIES,
        }).then( successResult => { $scope.varieties = successResult.data; });

        $http({
            method: "GET",
            url: URL_GET_AVAILABILITIES,
        }).then( successResult => { $scope.availabilities = successResult.data; });
    }
});
