app.controller("productCommentsCtrl", function ($scope, $http, utilService) {
    $scope.comments   = [];
    $scope.page       = 1;
    $scope.product    = PRODUCT;
    $scope.newComment = {
        text: '',
        name: '',
        user: getUser(),
        product: PRODUCT
    };

    $scope.addReply = function (e) {
        let btn         = angular.element(e.currentTarget);
        let comment     = btn.attr('data-id');
        let textElement = $(`#comment_text_${comment}`);
        let nameElement = $(`#comment_name_${comment}`);
        let replyObj = {
            name:    nameElement.val(),
            text:    textElement.val(),
            comment: comment,
            user:    getUser(),
            date:    utilService.dateNow()
        };

        btn.prop('disabled', true);

        $http({
            method: "POST",
            url: URL_ADD_REPLY,
            data: replyObj
        }).then(successResult => {
            btn.prop('disabled', false);

            replyObj.id      = successResult.data.id;
            replyObj.isAdmin = successResult.data.isAdmin;

            let commentIndex = 0;
            for (let i = 0; i < $scope.comments.length; i++) {
                if ($scope.comments[i].id === comment) {
                    $scope.comments[i].replies.push(replyObj);
                    commentIndex = i;
                    break;
                }
            }

            textElement.val('');
            nameElement.val('');

            let commentObj     = $scope.comments[commentIndex];
            let prevReplyIndex = commentObj.replies.length > 0 ? (commentObj.replies.length - 2) : 0;
            document.getElementById(`comment_reply_${commentObj.replies[prevReplyIndex].id}`).scrollIntoView({
                block: "center",
                behavior: "smooth"
            });

            //$(`#comment_${comment}`).collapse('hide');
        });
    };

    $scope.init = function () {
        getComments($scope.page);
    };

    $scope.moreComments = function () {
        getComments(++$scope.page);
    };

    $scope.addComment = function (e) {
        let btn = angular.element(e.currentTarget);
        btn.prop('disabled', true);

        $http({
            method: "POST",
            url: URL_ADD_COMMENT,
            data: $scope.newComment
        }).then(successResult => {
            $('.no-more-comments-msg').remove();

            window.scrollTo({
                left: 0,
                top: ($(`#comments`).offset().top - 100)
            });

            $scope.comments.unshift({
                text:    $scope.newComment.text,
                name:    $scope.newComment.name,
                date:    utilService.dateNow(),
                id:      successResult.data.id,
                replies: [],
                isAdmin: successResult.data.isAdmin
            });

            $scope.newComment.name = '';
            $scope.newComment.text = '';

            btn.prop('disabled', false);
        });
    };

    function getComments(page) {
        $http({
            method: "GET",
            url: URL_GET_COMMENTS,
            params: {
                page:               page,
                'filters[product]': PRODUCT
            }
        }).then(successResult => {
            successResult.data.comments.forEach(comment => {
                $scope.comments.push(comment);
            });

            $('#comments-length').text(`(${successResult.data.total})`);
        }, () => {
            let message = $scope.comments.length === 0 ? TEXT_NO_COMMENTS_YET : TEXT_NO_COMMENTS;

            error(message);
        });
    }

    function error(message) {
        $('#comment-container').append(`<h3 class="no-more-comments-msg text-danger mb-20">${message}</h3>`)
    }

    function getUser() {
        let id = $('#comment-container').data('id');

        if (typeof id !== undefined && id !== false) {
            return id;
        }

        return null;
    }
});
