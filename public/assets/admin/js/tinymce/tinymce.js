(function () {
    function onSubmit() {
        let content = tinymce.get(selector).getContent();

        let div = document.createElement('div');
        div.append(content);

        $(`input[name="${selector}"]`).val(div.outerHTML);
    }

    $(`#${selector}`).parent().parent().parent().on('submit', onSubmit());
})();
