(function () {
    let href = '';
    $('.js-delete-modal').each(function () {
        $(this).on('click', function () {
            $('#delete-modal').modal('show');
            href = $(this).attr('href');
            $('#delete-modal-link').attr('href', href);
            return false;
        });
    });
})();
