// Adding custom-form classes to symfony forms
(function () {
    $('select').each(function () {
        if (!$(this).hasClass('custom-select')) {
            $(this).addClass('custom-select');
        }
    });

    $('input[type="checkbox"]').each(function () { // wrapping checkboxes in custom-checkboxes
        let parent = $(this).parent();

        if (!$(parent).hasClass('custom-control')) {
            $(parent).attr('class', 'custom-control custom-checkbox');
            $(this).addClass('custom-control-input');
            $(parent).find('label').addClass('custom-control-label');
        }
    });

    $('.custom-file-input').each(function () {
        $(this).on('change', function () {
            let parent = $(this).parent();
            $(parent).find('label').text($(this).val());
        });
    });
})();

function readImageInput(e) {
    let inputFiles = this.files;

    if (inputFiles === undefined || inputFiles.length === 0) return;

    let inputFile = inputFiles[0];
    let reader = new FileReader();

    reader.onload = function(event) {
        console.log($(this).attr('id'));
        $('#image-preview').attr("src", event.target.result).show();
    };

    reader.readAsDataURL(inputFile);
}