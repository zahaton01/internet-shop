<?php

namespace App\Database\Domain\Repository\Site;

use App\Database\Domain\Entity\Site\SitePage\SitePage;
use App\Database\Domain\Model\Query\Params\QueryParams;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 */
class SitePageRepository extends EntityRepository
{
    /**
     * @param QueryParams|null $params
     *
     * @return Query
     */
    public function findByQuery(QueryParams $params = null): Query
    {
        $qb = $this->createQueryBuilder('sp');

        return $qb->getQuery();
    }
}
