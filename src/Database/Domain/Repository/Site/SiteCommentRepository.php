<?php

namespace App\Database\Domain\Repository\Site;

use App\Database\Domain\Model\Pagination\Pagination;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Tools\Pagination\Paginator;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 */
class SiteCommentRepository extends EntityRepository
{
    /**
     * @param Pagination $pagination
     *
     * @return Paginator
     */
    public function getComments(Pagination $pagination)
    {
        $qb = $this->createQueryBuilder('c');
        $qb->orderBy('c.creationDate', 'DESC');

        $qb->setFirstResult($pagination->getFirstResult());
        $qb->setMaxResults($pagination->getPageSize());

        return new Paginator($qb->getQuery(), true);
    }
}