<?php

namespace App\Database\Domain\Repository\Commerce\Product;

use App\Database\Domain\Entity\Commerce\Product\Category\ProductCategory;
use App\Database\Domain\Entity\Commerce\Product\SubCategory\ProductSubCategory;
use App\Database\Domain\Model\Query\Params\QueryParams;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 */
class ProductSpecificationRepository extends EntityRepository
{
    /**
     * @param QueryParams $params
     *
     * @return Query
     */
    public function findByQuery(QueryParams $params): Query
    {
        $qb = $this->createQueryBuilder('s');
        $qb
            ->leftJoin('s.productCategory', 'c')
            ->leftJoin('s.productSubCategory', 'sc')
            ->orderBy('s.id', 'DESC');

        if ($params->hasFilter('category')) {
            $qb
                ->andWhere('c.id = :categoryId')
                ->setParameter('categoryId', (string) $params->getFilter('category'));
        }

        if ($params->hasFilter('subCategory')) {
            $qb
                ->andWhere('sc.id = :subCategoryId')
                ->setParameter('subCategoryId', (string) $params->getFilter('subCategory'));
        }

        return $qb->getQuery();
    }

    /**
     * @param ProductCategory|null    $category
     * @param ProductSubCategory|null $subCategory
     *
     * @return mixed
     */
    public function findByCategories(?ProductCategory $category = null, ?ProductSubCategory $subCategory = null)
    {
        $qb = $this->createQueryBuilder('s');
        $qb
            ->leftJoin('s.productCategory', 'c')
            ->leftJoin('s.productSubCategory', 'sc')
            ->andWhere('s.isEnabled = 1');

        if (null !== $category) {
            $qb
                ->andWhere('c.id = :categoryId')
                ->setParameter('categoryId', $category->getId());
        }

        if (null !== $subCategory) {
            $qb
                ->andWhere('sc.id = :subCategoryId')
                ->setParameter('subCategoryId', $subCategory->getId());
        }

        return $qb->getQuery()->getResult();
    }
}
