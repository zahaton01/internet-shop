<?php

namespace App\Database\Domain\Repository\Commerce\Product;

use App\Database\Domain\Model\Pagination\Pagination;
use App\Database\Domain\Model\Query\Params\QueryParams;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Tools\Pagination\Paginator;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 */
class ProductCommentRepository extends EntityRepository
{
    /**
     * @param Pagination  $pagination
     * @param QueryParams $params
     *
     * @return Paginator
     */
    public function getComments(Pagination $pagination, QueryParams $params)
    {
        $qb = $this->createQueryBuilder('c');
        $qb
            ->andWhere('c.product = :id')
            ->setParameter('id', $params->getFilter('product'))
            ->orderBy('c.creationDate', 'DESC');

        $qb->setFirstResult($pagination->getFirstResult());
        $qb->setMaxResults($pagination->getPageSize());

        return new Paginator($qb->getQuery(), true);
    }
}