<?php

namespace App\Database\Domain\Repository\Commerce\Product;

use App\Database\Domain\Model\Query\Product\ProductFilters;
use App\Database\Domain\Model\Query\Params\QueryParams;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 */
class ProductRepository extends EntityRepository
{
    /**
     * @param QueryParams $params
     *
     * @return Query
     */
    public function findByQuery(QueryParams $params = null): Query
    {
        $qb = $this->createQueryBuilder('p');
        $qb
            ->leftJoin('p.productCategory', 'pc')
            ->leftJoin('p.productSubCategory', 'psc')
            ->orderBy('p.creationDate', 'DESC');

        if (null !== $params) {
            if ($params->hasFilter('category')) {
                $qb
                    ->andWhere('pc.id = :categoryId')
                    ->setParameter('categoryId', $params->getFilter('category')->int());
            }

            if ($params->hasFilter('subCategory')) {
                $qb
                    ->andWhere('psc.id = :subCategoryId')
                    ->setParameter('subCategoryId', $params->getFilter('subCategory')->int());
            }

            if ($params->hasFilter('query')) {
                $qb
                    ->andWhere('lower(trim(p.name)) LIKE :query or lower(trim(p.vendorCode)) LIKE :query')
                    ->setParameter('query', "%{$params->getFilter('query')->formatted()}%");
            }
        }

        return $qb->getQuery();
    }

    /**
     * @param ProductFilters $productFilters
     *
     * @return Query
     */
    public function findByProductFilters(ProductFilters $productFilters): Query
    {
        $qb = $this->createQueryBuilder('p');
        $qb
            ->leftJoin('p.productCategory', 'pc')
            ->leftJoin('p.productSubCategory', 'psc')
            ->leftJoin('p.specifications', 'ps')
            ->leftJoin('ps.specification', 'specification')
            ->leftJoin('ps.values', 'values')
            ->andWhere('p.isVisible = 1')
            ->orderBy('p.creationDate', 'DESC');

        if ($productFilters->hasFilter('min_price')) {
            $qb
                ->andWhere('p.price >= :min_price')
                ->setParameter('min_price', $productFilters->getFilter('min_price')->int());
        }

        if ($productFilters->hasFilter('max_price')) {
            $qb
                ->andWhere('p.price <= :max_price')
                ->setParameter('max_price', $productFilters->getFilter('max_price')->int());
        }

        if ($productFilters->hasCategory()) {
            $qb
                ->andWhere('pc.id = :category')
                ->setParameter('category', $productFilters->getCategory()->getId());
        }

        if ($productFilters->hasSubCategory()) {
            $qb
                ->andWhere('psc.id = :subCategory')
                ->setParameter('subCategory', $productFilters->getSubCategory()->getId());
        }

        if ($productFilters->hasSpecifications()) {
            $qb
                ->andWhere('specification.id IN (:specifications) AND values.id IN (:specification_values)')
                ->setParameter('specifications', $productFilters->getSpecificationIds())
                ->setParameter('specification_values', $productFilters->getSpecificationValueIds());
        }

        return $qb->getQuery();
    }

    /**
     * @param array $ids
     *
     * @return mixed
     */
    public function findByIds(array $ids)
    {
        $qb = $this->createQueryBuilder('p');
        $qb
            ->orderBy('p.creationDate', 'DESC')
            ->andWhere('p.id IN (:products)')
            ->setParameter('products', $ids);

        return $qb->getQuery()->getResult();
    }
}
