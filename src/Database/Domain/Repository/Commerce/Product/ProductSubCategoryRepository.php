<?php

namespace App\Database\Domain\Repository\Commerce\Product;

use App\Database\Domain\Model\Query\Params\QueryParams;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 */
class ProductSubCategoryRepository extends EntityRepository
{
    /**
     * @param QueryParams $params
     *
     * @return Query
     */
    public function findByQuery(QueryParams $params = null): Query
    {
        $qb = $this->createQueryBuilder('sc');

        return $qb->getQuery();
    }
}