<?php

namespace App\Database\Domain\Repository\Commerce\Order;

use App\Database\Domain\Model\Query\Params\QueryParams;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 */
class OrderRepository extends EntityRepository
{
    /**
     * @param QueryParams $params
     *
     * @return Query
     */
    public function findByQuery(QueryParams $params = null): Query
    {
        $qb = $this->createQueryBuilder('o');
        $qb->orderBy('o.creationDate', 'DESC');

        if ($params->hasStartDate()) {
            $qb
                ->andWhere('o.creationDate >= :startDate')
                ->setParameter('startDate', $params->getStartDate());
        }

        if ($params->hasEndDate()) {
            $qb
                ->andWhere('o.creationDate <= :endDate')
                ->setParameter('endDate', $params->getEndDate());
        }

        if ($params->hasFilter('query')) {
            $qb
                ->andWhere('lower(trim(o.name)) LIKE :query or lower(trim(o.phone)) LIKE :query or lower(trim(o.id)) LIKE :query')
                ->setParameter('query', "%{$params->getFilter('query')->formatted()}%");
        }

        if ($params->hasFilter('status')) {
            $qb
                ->andWhere('o.status = :status')
                ->setParameter('status', $params->getFilter('status')->getValue());
        }

        if ($params->hasFilter('payment_status')) {
            $qb
                ->andWhere('o.paymentStatus = :payment_status')
                ->setParameter('payment_status', $params->getFilter('payment_status')->getValue());
        }

        return $qb->getQuery();
    }
}
