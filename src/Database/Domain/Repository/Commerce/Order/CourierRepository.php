<?php

namespace App\Database\Domain\Repository\Commerce\Order;

use App\Database\Domain\Model\Query\Params\QueryParams;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 */
class CourierRepository extends EntityRepository
{
    /**
     * @param QueryParams $params
     *
     * @return Query
     */
    public function findByQuery(QueryParams $params = null): Query
    {
        $qb = $this->createQueryBuilder('c');
        $qb->orderBy('c.id', 'DESC');

        return $qb->getQuery();
    }
}