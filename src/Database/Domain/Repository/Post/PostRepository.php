<?php

namespace App\Database\Domain\Repository\Post;

use App\Database\Domain\Model\Query\Params\QueryParams;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 */
class PostRepository extends EntityRepository
{
    /**
     * @param QueryParams $params
     *
     * @return Query
     */
    public function findByQuery(QueryParams $params = null): Query
    {
        $qb = $this->createQueryBuilder('p');
        $qb->orderBy('p.creationDate', 'DESC');

        if (null !== $params) {

            if ($params->hasFilter('is_visible')) {
                $qb
                    ->andWhere('p.isVisible = :is_visible')
                    ->setParameter('is_visible', $params->getFilter('is_visible')->boolToInt());
            }

            if ($params->hasFilter('query')) {
                $qb
                    ->andWhere('lower(trim(p.title)) LIKE :query')
                    ->setParameter('query', "%{$params->getFilter('query')->formatted()}%");
            }
        }

        return $qb->getQuery();
    }

    /**
     * @return Query
     */
    public function search(): Query
    {
        $qb = $this->createQueryBuilder('p');
        $qb
            ->andWhere('p.isVisible = 1')
            ->orderBy('p.creationDate', 'DESC');

        return $qb->getQuery();
    }
}
