<?php

namespace App\Database\Domain\ValueObject\Order;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 */
class NovaPoshtaDeliveryData
{
    /** @var string|null */
    private $city;

    /** @var string|null */
    private $warehouse;

    /** @var string|null */
    private $cityRef;

    /** @var string|null */
    private $warehouseRef;

    /**
     * @return string
     */
    public function getCity(): ?string
    {
        return $this->city;
    }

    /**
     * @param string $city
     *
     * @return self
     */
    public function setCity(?string $city): self
    {
        $this->city = $city;
        return $this;
    }

    /**
     * @return string
     */
    public function getWarehouse(): ?string
    {
        return $this->warehouse;
    }

    /**
     * @param string $warehouse
     *
     * @return self
     */
    public function setWarehouse(?string $warehouse): self
    {
        $this->warehouse = $warehouse;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getCityRef(): ?string
    {
        return $this->cityRef;
    }

    /**
     * @param string|null $cityRef
     *
     * @return self
     */
    public function setCityRef(?string $cityRef): self
    {
        $this->cityRef = $cityRef;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getWarehouseRef(): ?string
    {
        return $this->warehouseRef;
    }

    /**
     * @param string|null $warehouseRef
     *
     * @return self
     */
    public function setWarehouseRef(?string $warehouseRef): self
    {
        $this->warehouseRef = $warehouseRef;
        return $this;
    }
}