<?php

namespace App\Database\Domain\Entity\Commerce\Order;

use App\Database\Domain\Entity\AbstractEntity;
use App\Database\Domain\Entity\Commerce\Order\Courier;
use App\Application\Model\Commerce\Cart\Cart;
use App\Database\Domain\Entity\User\User;
use App\Database\Domain\Traits\CreationDateTrait;
use App\Database\Domain\ValueObject\Order\NovaPoshtaDeliveryData;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 *
 * @ORM\Table("orders")
 * @ORM\Entity(repositoryClass="App\Database\Domain\Repository\Commerce\Order\OrderRepository")
 */
class Order extends AbstractEntity
{
    use CreationDateTrait;

    public const DELIVERY_COURIER                = 'delivery_courier';
    public const DELIVERY_NOVA_POSHTA            = 'delivery_nova';

    public const PAYMENT_TYPE_CASH               = 'cash';
    public const PAYMENT_TYPE_ONLINE_LIQPAY      = 'liqpay';

    public const PAYMENT_STATUS_AWAITING_PAYMENT = 'awaiting_payment';
    public const PAYMENT_STATUS_PAID             = 'paid';

    public const ORDER_STATUS_COMPLETED          = 'completed';
    public const ORDER_STATUS_ON_DELIVER         = 'on_deliver';
    public const ORDER_STATUS_NOT_CONFIRMED      = 'not_confirmed';
    public const ORDER_STATUS_CONFIRMED          = 'confirmed';
    public const ORDER_STATUS_CANCELLED          = 'cancelled';

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=false)
     *
     * @Assert\NotBlank(message="Order delivery type cannot be empty")
     * @Assert\NotNull(message="Order delivery type cannot be null")
     * @Assert\Choice(choices={Order::DELIVERY_COURIER, Order::DELIVERY_NOVA_POSHTA}, message="Invalid delivery type")
     */
    private $deliveryType;

    /**
     * @var Courier[]
     *
     * @ORM\ManyToMany(targetEntity=Courier::class, inversedBy="orders")
     */
    private $couriers;

    /**
     * @var array|null
     *
     * @ORM\Column(type="json", nullable=true)
     */
    private $deliveryData;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=false)
     *
     * @Assert\NotBlank(message="Order payment status cannot be empty")
     * @Assert\NotNull(message="Order payment status cannot be null")
     * @Assert\Choice(choices={Order::PAYMENT_STATUS_AWAITING_PAYMENT, Order::PAYMENT_STATUS_PAID}, message="Invalid order payment status")
     */
    private $paymentStatus;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=false)
     *
     * @Assert\NotBlank(message="Order payment type cannot be empty")
     * @Assert\NotNull(message="Order payment type cannot be empty")
     * @Assert\Choice(choices={Order::PAYMENT_TYPE_CASH, Order::PAYMENT_TYPE_ONLINE_LIQPAY}, message="Invalid order payment type")
     */
    private $paymentType;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=false)
     *
     * @Assert\NotNull(message="Order status cannot be empty")
     * @Assert\NotBlank(message="Order status cannot be null")
     * @Assert\Choice(choices={
     *     Order::ORDER_STATUS_COMPLETED,
     *     Order::ORDER_STATUS_ON_DELIVER,
     *     Order::ORDER_STATUS_CONFIRMED,
     *     Order::ORDER_STATUS_NOT_CONFIRMED,
     *     Order::ORDER_STATUS_CANCELLED
     *     }, message="Invalid order status")
     */
    private $status;

    /**
     * @var Coupon|null
     *
     * @ORM\ManyToOne(targetEntity=Coupon::class)
     * @ORM\JoinColumn(referencedColumnName="id")
     */
    private $coupon;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=false)
     *
     * @Assert\NotBlank(message="Order customer name cannot be empty")
     * @Assert\NotNull(message="Order customer name cannot be null")
     */
    private $name;

    /**
     * @var string|null
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $surname;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=false)
     *
     * @Assert\NotNull(message="Order customer phone cannot be empty")
     * @Assert\NotBlank(message="Order customer phone cannot be null")
     */
    private $phone;

    /**
     * @var float
     *
     * @ORM\Column(type="float", nullable=false)
     *
     * @Assert\NotNull(message="Order shipment price cannot be null")
     */
    private $shipmentPrice;

    /**
     * @var array
     *
     * @ORM\Column(type="json", nullable=false)
     */
    private $cart;

    /**
     * @var string|null
     *
     * @ORM\Column(type="text", nullable=true)
     */
    private $comment;

    /**
     * @var string|null
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $city;

    /**
     * @var string|null
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $address;

    /**
     * @var string|null
     *
     * @ORM\Column(type="string", nullable=true)
     *
     * @Assert\Email(message="Invalid order customer email")
     */
    private $email;

    /**
     * @return bool
     */
    public function isDeliveryCourier(): bool
    {
        return $this->deliveryType === self::DELIVERY_COURIER;
    }

    /**
     * @return bool
     */
    public function isDeliveryNovaPoshta()
    {
        return $this->deliveryType === self::DELIVERY_NOVA_POSHTA;
    }

    /**
     * @return bool
     */
    public function isOnlinePayment(): bool
    {
        return $this->paymentType === self::PAYMENT_TYPE_ONLINE_LIQPAY;
    }

    /**
     * @return bool
     */
    public function isCashPayment(): bool
    {
        return $this->paymentType === self::PAYMENT_TYPE_CASH;
    }

    /**
     * @return array
     */
    public function getCart(): ?array
    {
        return $this->cart;
    }

    /**
     * @param Cart $cart
     *
     * @return self
     */
    public function setCart(?Cart $cart): self
    {
        $data = [];
        foreach ($cart->getCartProducts() as $cartProduct) {
            $varieties = [];
            foreach ($cartProduct->getVarieties() as $cartVariety) {
                $varieties[] = [
                    'variety' => $cartVariety->getRelatedVariety()->getId(),
                    'value'   => $cartVariety->getRelatedValue()->getId()
                ];
            }

            $data[] = [
                'id'        => $cartProduct->getRelatedProduct()->getId(),
                'quantity'  => $cartProduct->getQuantity(),
                'varieties' => $varieties
            ];
        }

        $this->cart = $data;

        return $this;
    }

    /**
     * @return array
     */
    public static function statuses()
    {
        return [
            self::ORDER_STATUS_ON_DELIVER,
            self::ORDER_STATUS_CANCELLED,
            self::ORDER_STATUS_CONFIRMED,
            self::ORDER_STATUS_COMPLETED,
            self::ORDER_STATUS_NOT_CONFIRMED
        ];
    }

    /**
     * @return array
     */
    public static function paymentStatuses()
    {
        return [
            self::PAYMENT_STATUS_PAID,
            self::PAYMENT_STATUS_AWAITING_PAYMENT
        ];
    }

    /**
     * @return string
     */
    public function getDeliveryType(): ?string
    {
        return $this->deliveryType;
    }

    /**
     * @param string $deliveryType
     *
     * @return self
     */
    public function setDeliveryType(?string $deliveryType): self
    {
        $this->deliveryType = $deliveryType;
        return $this;
    }

    /**
     * @return Courier[]|Collection
     */
    public function getCouriers()
    {
        return $this->couriers;
    }

    /**
     * @param Courier[] $couriers
     *
     * @return self
     */
    public function setCouriers(array $couriers): self
    {
        $this->couriers = $couriers;
        return $this;
    }

    public function setDeliveryData($data): self
    {
        if ($data instanceof NovaPoshtaDeliveryData) {
            $this->deliveryData                  = [];
            $this->deliveryData['city']          = $data->getCity();
            $this->deliveryData['warehouse']     = $data->getWarehouse();
            $this->deliveryData['city_ref']      = $data->getCityRef();
            $this->deliveryData['warehouse_ref'] = $data->getWarehouseRef();
        }

        return $this;
    }

    /**
     * @return NovaPoshtaDeliveryData|null
     */
    public function getDeliveryData()
    {
        if ($this->isDeliveryNovaPoshta()) {
            $data = new NovaPoshtaDeliveryData();
            $data
                ->setCity($this->deliveryData['city'])
                ->setWarehouse($this->deliveryData['warehouse'])
                ->setCityRef($this->deliveryData['city_ref'])
                ->setWarehouseRef($this->deliveryData['warehouse_ref']);

            return $data;
        }

        return null;
    }

    /**
     * @return string
     */
    public function getPaymentStatus(): ?string
    {
        return $this->paymentStatus;
    }

    /**
     * @param string $paymentStatus
     *
     * @return self
     */
    public function setPaymentStatus(?string $paymentStatus): self
    {
        $this->paymentStatus = $paymentStatus;
        return $this;
    }

    /**
     * @return string
     */
    public function getPaymentType(): ?string
    {
        return $this->paymentType;
    }

    /**
     * @param string $paymentType
     *
     * @return self
     */
    public function setPaymentType(?string $paymentType): self
    {
        $this->paymentType = $paymentType;
        return $this;
    }

    /**
     * @return string
     */
    public function getStatus(): ?string
    {
        return $this->status;
    }

    /**
     * @param string $status
     *
     * @return self
     */
    public function setStatus(?string $status): self
    {
        $this->status = $status;
        return $this;
    }

    /**
     * @return Coupon|null
     */
    public function getCoupon(): ?Coupon
    {
        return $this->coupon;
    }

    /**
     * @param Coupon|null $coupon
     *
     * @return self
     */
    public function setCoupon(?Coupon $coupon): self
    {
        $this->coupon = $coupon;
        return $this;
    }

    /**
     * @return string
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return self
     */
    public function setName(?string $name): self
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getSurname(): ?string
    {
        return $this->surname;
    }

    /**
     * @param string|null $surname
     *
     * @return self
     */
    public function setSurname(?string $surname): self
    {
        $this->surname = $surname;
        return $this;
    }

    /**
     * @return string
     */
    public function getPhone(): ?string
    {
        return $this->phone;
    }

    /**
     * @param string $phone
     *
     * @return self
     */
    public function setPhone(?string $phone): self
    {
        $this->phone = $phone;
        return $this;
    }

    /**
     * @return float
     */
    public function getShipmentPrice(): ?float
    {
        return $this->shipmentPrice;
    }

    /**
     * @param float $shipmentPrice
     *
     * @return self
     */
    public function setShipmentPrice(?float $shipmentPrice): self
    {
        $this->shipmentPrice = $shipmentPrice;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getComment(): ?string
    {
        return $this->comment;
    }

    /**
     * @param string|null $comment
     *
     * @return self
     */
    public function setComment(?string $comment): self
    {
        $this->comment = $comment;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getCity(): ?string
    {
        return $this->city;
    }

    /**
     * @param string|null $city
     *
     * @return self
     */
    public function setCity(?string $city): self
    {
        $this->city = $city;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getAddress(): ?string
    {
        return $this->address;
    }

    /**
     * @param string|null $address
     *
     * @return self
     */
    public function setAddress(?string $address): self
    {
        $this->address = $address;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * @param string|null $email
     *
     * @return self
     */
    public function setEmail(?string $email): self
    {
        $this->email = $email;
        return $this;
    }

    public function getUser()
    {
    }
}
