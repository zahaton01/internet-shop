<?php

namespace App\Database\Domain\Entity\Commerce\Order;

use App\Database\Domain\Entity\AbstractEntity;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\PersistentCollection;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 *
 * @ORM\Entity(repositoryClass="App\Database\Domain\Repository\Commerce\Order\CourierRepository")
 */
class Courier extends AbstractEntity
{
    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", nullable=false)
     *
     * @Assert\NotBlank(message="Courier name cannot be empty")
     * @Assert\NotNull(message="Courier name cannot be null")
     */
    private $name;

    /**
     * @var PersistentCollection|Order[]
     *
     * @ORM\ManyToMany(targetEntity=Order::class, mappedBy="couriers")
     * @ORM\JoinTable(name="order_couriers",
     *    joinColumns={@ORM\JoinColumn(name="order_id", referencedColumnName="id", onDelete="CASCADE")},
     *    inverseJoinColumns={@ORM\JoinColumn(name="courier_id", referencedColumnName="id", onDelete="CASCADE")}
     * )
     */
    private $orders;

    /**
     * @return string
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return self
     */
    public function setName(?string $name): self
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return PersistentCollection|Order[]
     */
    public function getOrders(): ?PersistentCollection
    {
        return $this->orders;
    }

    /**
     * @param Order $order
     *
     * @return $this
     */
    public function addOrder(Order $order)
    {
        $this->orders[] = $order;

        return $this;
    }
}