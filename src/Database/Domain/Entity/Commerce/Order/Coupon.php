<?php

namespace App\Database\Domain\Entity\Commerce\Order;

use App\Database\Domain\Entity\AbstractEntity;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 *
 * @ORM\Entity(repositoryClass="App\Database\Domain\Repository\Commerce\Order\CouponRepository")
 *
 * @UniqueEntity(fields={"code"}, message="Coupon already exists")
 */
class Coupon extends AbstractEntity
{
    public const TYPE_PERCENT = 'coupon_percent';
    public const TYPE_FIXED   = 'coupon_fixed';

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=false, unique=true)
     *
     * @Assert\NotNull()
     * @Assert\NotBlank()
     */
    private $code;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=false)
     *
     * @Assert\NotBlank(message="Coupon discount type cannot be blank")
     * @Assert\NotNull(message="Coupon discount type cannot be null")
     * @Assert\Choice(choices={Coupon::TYPE_PERCENT, Coupon::TYPE_FIXED}, message="Invalid discount type")
     */
    private $discountType;

    /**
     * @var float
     *
     * @ORM\Column(type="float", nullable=true)
     */
    private $discountPercent;

    /**
     * @var float
     *
     * @ORM\Column(type="float", nullable=true)
     */
    private $discountFixed;

    /**
     * @return string
     */
    public function getCode(): ?string
    {
        return $this->code;
    }

    /**
     * @param string $code
     *
     * @return self
     */
    public function setCode(?string $code): self
    {
        $this->code = $code;
        return $this;
    }

    /**
     * @return string
     */
    public function getDiscountType(): ?string
    {
        return $this->discountType;
    }

    /**
     * @param string $discountType
     *
     * @return self
     */
    public function setDiscountType(?string $discountType): self
    {
        $this->discountType = $discountType;
        return $this;
    }

    /**
     * @return float
     */
    public function getDiscountPercent(): ?float
    {
        return $this->discountPercent;
    }

    /**
     * @param float $discountPercent
     *
     * @return self
     */
    public function setDiscountPercent(?float $discountPercent): self
    {
        $this->discountPercent = $discountPercent;
        return $this;
    }

    /**
     * @return float
     */
    public function getDiscountFixed(): ?float
    {
        return $this->discountFixed;
    }

    /**
     * @param float $discountFixed
     *
     * @return self
     */
    public function setDiscountFixed(?float $discountFixed): self
    {
        $this->discountFixed = $discountFixed;
        return $this;
    }

    /**
     * @return bool
     */
    public function isFixed()
    {
        return $this->discountType === self::TYPE_FIXED;
    }

    /**
     * @return bool
     */
    public function isPercent()
    {
        return $this->discountType === self::TYPE_PERCENT;
    }
}
