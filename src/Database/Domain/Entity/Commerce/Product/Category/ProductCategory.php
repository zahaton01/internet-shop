<?php

namespace App\Database\Domain\Entity\Commerce\Product\Category;

use App\Database\Domain\Entity\Commerce\Product\AbstractProductCategory;
use App\Database\Domain\Entity\Commerce\Product\Category\Meta\ProductCategoryMeta;
use App\Database\Domain\Entity\Commerce\Product\Product;
use App\Database\Domain\Entity\Commerce\Product\Specification\ProductSpecification;
use App\Database\Domain\Entity\Commerce\Product\SubCategory\ProductSubCategory;
use App\Database\Domain\Validation\Constraints\Slug\UniqueSlug;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\PersistentCollection;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 *
 * @ORM\Entity(repositoryClass="App\Database\Domain\Repository\Commerce\Product\ProductCategoryRepository")
 */
class ProductCategory extends AbstractProductCategory
{
    /**
     * @var PersistentCollection|ProductCategoryTranslation[]
     *
     * @ORM\OneToMany(targetEntity=ProductCategoryTranslation::class, mappedBy="productCategory", cascade={"persist", "remove"})
     */
    private $translations;

    /**
     * @var ProductCategoryMeta
     *
     * @ORM\OneToOne(targetEntity=ProductCategoryMeta::class, cascade={"persist", "remove"})
     * @ORM\JoinColumn(name="meta_id", referencedColumnName="id")
     *
     * @Assert\Valid()
     */
    private $meta;

    /**
     * @var PersistentCollection|ProductSubCategory[]
     *
     * @ORM\OneToMany(targetEntity=ProductSubCategory::class, mappedBy="productCategory")
     */
    private $productSubCategories;

    /**
     * @var PersistentCollection|Product[]
     *
     * @ORM\OneToMany(targetEntity=Product::class, mappedBy="productCategory")
     */
    private $products;

    /**
     * @var PersistentCollection|ProductSpecification[]
     *
     * @ORM\OneToMany(targetEntity=ProductSpecification::class, mappedBy="productCategory")
     */
    private $specifications;

    /**
     * @return ProductCategoryTranslation[]|PersistentCollection
     */
    public function getTranslations()
    {
        return $this->translations;
    }

    /**
     * @param ProductCategoryTranslation[]|PersistentCollection $translations
     *
     * @return self
     */
    public function setTranslations($translations): self
    {
        $this->translations = $translations;
        return $this;
    }

    /**
     * @return ProductCategoryMeta
     */
    public function getMeta(): ?ProductCategoryMeta
    {
        return $this->meta;
    }

    /**
     * @param ProductCategoryMeta $meta
     *
     * @return self
     */
    public function setMeta(ProductCategoryMeta $meta): self
    {
        $this->meta = $meta;
        return $this;
    }

    /**
     * @return ProductSubCategory[]|PersistentCollection
     */
    public function getProductSubCategories()
    {
        return $this->productSubCategories;
    }

    /**
     * @param ProductSubCategory[]|PersistentCollection $productSubCategories
     *
     * @return self
     */
    public function setProductSubCategories($productSubCategories): self
    {
        $this->productSubCategories = $productSubCategories;
        return $this;
    }

    /**
     * @return Product[]|PersistentCollection
     */
    public function getProducts()
    {
        return $this->products;
    }

    /**
     * @param Product[]|PersistentCollection $products
     *
     * @return self
     */
    public function setProducts($products): self
    {
        $this->products = $products;
        return $this;
    }

    /**
     * @return ProductSpecification[]|PersistentCollection
     */
    public function getSpecifications()
    {
        return $this->specifications;
    }

    /**
     * @param ProductSpecification[]|PersistentCollection $specifications
     *
     * @return self
     */
    public function setSpecifications($specifications): self
    {
        $this->specifications = $specifications;
        return $this;
    }

    /**
     * @return int
     */
    public function visibleProductsLength()
    {
        $count = 0;
        /** @var Product $product */
        foreach ($this->products as $product) {
            if ($product->isVisible()) {
                $count++;
            }
        }

        return $count;
    }
}
