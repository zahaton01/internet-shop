<?php

namespace App\Database\Domain\Entity\Commerce\Product\Category;

use App\Database\Domain\Entity\Commerce\Product\AbstractProductCategoryTranslation;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 *
 * @ORM\Entity()
 */
class ProductCategoryTranslation extends AbstractProductCategoryTranslation
{
    /**
     * @var ProductCategory
     *
     * @ORM\ManyToOne(targetEntity=ProductCategory::class, inversedBy="translations")
     * @ORM\JoinColumn(name="category_id", referencedColumnName="id")
     *
     * @Assert\Valid()
     */
    private $productCategory;

    /**
     * @return ProductCategory
     */
    public function getProductCategory(): ProductCategory
    {
        return $this->productCategory;
    }

    /**
     * @param ProductCategory $productCategory
     *
     * @return self
     */
    public function setProductCategory(ProductCategory $productCategory): self
    {
        $this->productCategory = $productCategory;
        return $this;
    }
}
