<?php

namespace App\Database\Domain\Entity\Commerce\Product\Category\Meta;

use App\Database\Domain\Entity\AbstractMeta;
use App\Database\Domain\Traits\LocaleTrait;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 *
 * @ORM\Entity()
 */
class ProductCategoryMetaTranslation extends AbstractMeta
{
    use LocaleTrait;

    /**
     * @var ProductCategoryMeta
     *
     * @ORM\ManyToOne(targetEntity=ProductCategoryMeta::class, inversedBy="translations")
     * @ORM\JoinColumn(name="meta_id", referencedColumnName="id")
     */
    private $meta;

    /**
     * @return ProductCategoryMeta
     */
    public function getMeta(): ProductCategoryMeta
    {
        return $this->meta;
    }

    /**
     * @param ProductCategoryMeta $meta
     *
     * @return self
     */
    public function setMeta(ProductCategoryMeta $meta): self
    {
        $this->meta = $meta;
        return $this;
    }
}
