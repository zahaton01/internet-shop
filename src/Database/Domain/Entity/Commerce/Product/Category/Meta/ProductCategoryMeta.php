<?php

namespace App\Database\Domain\Entity\Commerce\Product\Category\Meta;

use App\Database\Domain\Entity\AbstractMeta;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\PersistentCollection;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 *
 * @ORM\Entity()
 */
class ProductCategoryMeta extends AbstractMeta
{
    /**
     * @var PersistentCollection|ProductCategoryMetaTranslation[]
     *
     * @ORM\OneToMany(targetEntity=ProductCategoryMetaTranslation::class, mappedBy="meta", cascade={"persist", "remove"})
     */
    private $translations;

    /**
     * @return ProductCategoryMetaTranslation[]|PersistentCollection
     */
    public function getTranslations()
    {
        return $this->translations;
    }

    /**
     * @param ProductCategoryMetaTranslation[]|PersistentCollection $translations
     *
     * @return self
     */
    public function setTranslations($translations): self
    {
        $this->translations = $translations;
        return $this;
    }
}
