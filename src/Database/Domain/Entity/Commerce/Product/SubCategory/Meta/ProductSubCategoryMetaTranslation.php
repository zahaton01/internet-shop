<?php

namespace App\Database\Domain\Entity\Commerce\Product\SubCategory\Meta;

use App\Database\Domain\Entity\AbstractMeta;
use App\Database\Domain\Traits\LocaleTrait;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 *
 * @ORM\Entity()
 */
class ProductSubCategoryMetaTranslation extends AbstractMeta
{
    use LocaleTrait;

    /**
     * @var ProductSubCategoryMeta
     *
     * @ORM\ManyToOne(targetEntity=ProductSubCategoryMeta::class, inversedBy="translations")
     * @ORM\JoinColumn(name="meta_id", referencedColumnName="id")
     *
     * @Assert\Valid()
     */
    private $meta;

    /**
     * @return ProductSubCategoryMeta|null
     */
    public function getMeta(): ?ProductSubCategoryMeta
    {
        return $this->meta;
    }

    /**
     * @param ProductSubCategoryMeta $meta
     *
     * @return $this
     */
    public function setMeta(ProductSubCategoryMeta $meta): self
    {
        $this->meta = $meta;
        return $this;
    }
}