<?php

namespace App\Database\Domain\Entity\Commerce\Product\SubCategory\Meta;

use App\Database\Domain\Entity\AbstractMeta;
use App\Database\Domain\Traits\LocaleTrait;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\PersistentCollection;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 *
 * @ORM\Entity()
 */
class ProductSubCategoryMeta extends AbstractMeta
{
    /**
     * @var PersistentCollection|ProductSubCategoryMetaTranslation[]
     *
     * @ORM\OneToMany(targetEntity=ProductSubCategoryMetaTranslation::class, mappedBy="meta", cascade={"persist", "remove"})
     */
    private $translations;

    /**
     * @return ProductSubCategoryMetaTranslation[]|PersistentCollection
     */
    public function getTranslations()
    {
        return $this->translations;
    }

    /**
     * @param ProductSubCategoryMetaTranslation[]|PersistentCollection $translations
     *
     * @return self
     */
    public function setTranslations($translations): self
    {
        $this->translations = $translations;
        return $this;
    }
}