<?php

namespace App\Database\Domain\Entity\Commerce\Product\SubCategory;

use App\Database\Domain\Entity\Commerce\Product\AbstractProductCategory;
use App\Database\Domain\Entity\Commerce\Product\Category\ProductCategory;
use App\Database\Domain\Entity\Commerce\Product\Product;
use App\Database\Domain\Entity\Commerce\Product\Specification\ProductSpecification;
use App\Database\Domain\Entity\Commerce\Product\SubCategory\Meta\ProductSubCategoryMeta;
use App\Database\Domain\Validation\Constraints\Slug\UniqueSlug;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\PersistentCollection;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 *
 * @ORM\Entity(repositoryClass="App\Database\Domain\Repository\Commerce\Product\ProductSubCategoryRepository")
 */
class ProductSubCategory extends AbstractProductCategory
{
    /**
     * @var PersistentCollection|ProductSubCategoryTranslation[]
     *
     * @ORM\OneToMany(targetEntity=ProductSubCategoryTranslation::class, mappedBy="productSubCategory", cascade={"persist", "remove"})
     */
    private $translations;

    /**
     * @var ProductSubCategoryMeta
     *
     * @ORM\OneToOne(targetEntity=ProductSubCategoryMeta::class, cascade={"persist", "remove"})
     * @ORM\JoinColumn(name="meta_id", referencedColumnName="id")
     *
     * @Assert\Valid()
     */
    private $meta;

    /**
     * @var ProductCategory
     *
     * @ORM\ManyToOne(targetEntity=ProductCategory::class, inversedBy="productSubCategories")
     * @ORM\JoinColumn(name="category_id", referencedColumnName="id", onDelete="SET NULL")
     *
     * @Assert\Valid()
     */
    private $productCategory;

    /**
     * @var PersistentCollection|Product[]
     *
     * @ORM\OneToMany(targetEntity=Product::class, mappedBy="productSubCategory")
     */
    private $products;

    /**
     * @var PersistentCollection|ProductSpecification[]
     *
     * @ORM\OneToMany(targetEntity=ProductSpecification::class, mappedBy="productSubCategory")
     */
    private $specifications;

    /**
     * @return ProductSubCategoryTranslation[]|PersistentCollection
     */
    public function getTranslations()
    {
        return $this->translations;
    }

    /**
     * @param ProductSubCategoryTranslation[]|PersistentCollection $translations
     *
     * @return self
     */
    public function setTranslations($translations): self
    {
        $this->translations = $translations;
        return $this;
    }

    /**
     * @return ProductSubCategoryMeta
     */
    public function getMeta(): ProductSubCategoryMeta
    {
        return $this->meta;
    }

    /**
     * @param ProductSubCategoryMeta $meta
     *
     * @return self
     */
    public function setMeta(ProductSubCategoryMeta $meta): self
    {
        $this->meta = $meta;
        return $this;
    }

    /**
     * @return ProductCategory|null
     */
    public function getProductCategory(): ?ProductCategory
    {
        return $this->productCategory;
    }

    /**
     * @param ProductCategory $category
     *
     * @return self
     */
    public function setProductCategory(ProductCategory $category): self
    {
        $this->productCategory = $category;
        return $this;
    }

    /**
     * @return Product[]|PersistentCollection
     */
    public function getProducts()
    {
        return $this->products;
    }

    /**
     * @param Product[]|PersistentCollection $products
     *
     * @return self
     */
    public function setProducts($products): self
    {
        $this->products = $products;
        return $this;
    }

    /**
     * @return ProductSpecification[]|PersistentCollection
     */
    public function getSpecifications()
    {
        return $this->specifications;
    }

    /**
     * @param ProductSpecification[]|PersistentCollection $specifications
     *
     * @return self
     */
    public function setSpecifications($specifications): self
    {
        $this->specifications = $specifications;
        return $this;
    }

    /**
     * @return int
     */
    public function visibleProductsLength()
    {
        $count = 0;
        /** @var Product $product */
        foreach ($this->products as $product) {
            if ($product->isVisible()) {
                $count++;
            }
        }

        return $count;
    }
}
