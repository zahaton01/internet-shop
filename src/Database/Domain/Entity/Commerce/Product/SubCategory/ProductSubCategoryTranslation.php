<?php

namespace App\Database\Domain\Entity\Commerce\Product\SubCategory;

use App\Database\Domain\Entity\Commerce\Product\AbstractProductCategoryTranslation;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 *
 * @ORM\Entity()
 */
class ProductSubCategoryTranslation extends AbstractProductCategoryTranslation
{
    /**
     * @var ProductSubCategory
     *
     * @ORM\ManyToOne(targetEntity=ProductSubCategory::class, inversedBy="translations")
     * @ORM\JoinColumn(name="sub_category_id", referencedColumnName="id")
     *
     * @Assert\Valid()
     */
    private $productSubCategory;

    /**
     * @return ProductSubCategory
     */
    public function getProductSubCategory(): ProductSubCategory
    {
        return $this->productSubCategory;
    }

    /**
     * @param ProductSubCategory $productSubCategory
     *
     * @return self
     */
    public function setProductSubCategory(ProductSubCategory $productSubCategory): self
    {
        $this->productSubCategory = $productSubCategory;
        return $this;
    }
}