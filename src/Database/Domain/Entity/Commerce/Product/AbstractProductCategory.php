<?php

namespace App\Database\Domain\Entity\Commerce\Product;

use App\Database\Domain\Entity\AbstractEntity;
use App\Database\Domain\Traits\SlugTrait;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 *
 * @ORM\MappedSuperclass()
 */
abstract class AbstractProductCategory extends AbstractEntity
{
    use SlugTrait;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=false)
     *
     * @Assert\NotBlank(message="Product category name cannot be empty")
     * @Assert\NotNull(message="Product category name cannot be empty")
     */
    protected $name;

    /**
     * @var string
     *
     * @ORM\Column(type="text", nullable=true)
     */
    protected $description;

    /**
     * @return string
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return self
     */
    public function setName(string $name): self
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getDescription(): ?string
    {
        return html_entity_decode($this->description);
    }

    /**
     * @param string $desc
     *
     * @return self
     */
    public function setDescription(?string $desc): self
    {
        $this->description = $desc;
        return $this;
    }
}