<?php

namespace App\Database\Domain\Entity\Commerce\Product\Variety;

use App\Database\Domain\Entity\AbstractEntity;
use App\Database\Domain\Traits\LocaleTrait;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 *
 * @ORM\Entity()
 */
class VarietyTranslation extends AbstractEntity
{
    use LocaleTrait;

    /**
     * @var Variety
     *
     * @ORM\ManyToOne(targetEntity="Variety", inversedBy="translations")
     * @ORM\JoinColumn(name="variety_id", referencedColumnName="id")
     *
     * @Assert\Valid()
     */
    private $variety;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", nullable=true)
     */
    private $name;

    /**
     * @return Variety
     */
    public function getVariety(): Variety
    {
        return $this->variety;
    }

    /**
     * @param Variety $variety
     *
     * @return self
     */
    public function setVariety(Variety $variety): self
    {
        $this->variety = $variety;
        return $this;
    }

    /**
     * @return string
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return self
     */
    public function setName(string $name): self
    {
        $this->name = $name;
        return $this;
    }
}
