<?php

namespace App\Database\Domain\Entity\Commerce\Product\Variety;

use App\Database\Domain\Entity\AbstractEntity;
use App\Database\Domain\Entity\Commerce\Product\Product;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use App\Database\Domain\Validation\Constraints\ProductVariety\UniqueProductVariety;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 *
 * @ORM\Entity()
 *
 * @UniqueProductVariety()
 */
class ProductVariety extends AbstractEntity
{
    /**
     * @var Product
     *
     * @ORM\ManyToOne(targetEntity=Product::class, inversedBy="varieties")
     * @ORM\JoinColumn(name="product_id", referencedColumnName="id", onDelete="CASCADE")
     *
     * @Assert\NotBlank()
     */
    private $product;

    /**
     * @var Variety
     *
     * @ORM\ManyToOne(targetEntity="Variety")
     * @ORM\JoinColumn(name="variety_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $variety;

    /**
     * @var ProductVarietyValue[]
     *
     * @ORM\ManyToMany(targetEntity="ProductVarietyValue", cascade={"persist", "remove"})
     * @ORM\JoinTable(name="product_variety_values",
     *      joinColumns={@ORM\JoinColumn(name="product_variety_id", referencedColumnName="id", onDelete="CASCADE")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="value_id", referencedColumnName="id", onDelete="CASCADE")}
     *      )
     */
    private $values;

    public function __clone()
    {
        $this->id = null;
    }

    /**
     * @return Product
     */
    public function getProduct(): Product
    {
        return $this->product;
    }

    /**
     * @param Product $product
     *
     * @return self
     */
    public function setProduct(Product $product): self
    {
        $this->product = $product;
        return $this;
    }

    /**
     * @return Variety
     */
    public function getVariety(): Variety
    {
        return $this->variety;
    }

    /**
     * @param Variety $variety
     *
     * @return self
     */
    public function setVariety(Variety $variety): self
    {
        $this->variety = $variety;
        return $this;
    }

    /**
     * @return ProductVarietyValue[]
     */
    public function getValues()
    {
        return $this->values;
    }

    /**
     * @param VarietyValue[] $values
     *
     * @return self
     */
    public function setValues(array $values): self
    {
        $this->values = $values;
        return $this;
    }

    /**
     * @param ProductVarietyValue $productVarietyValue
     *
     * @return $this
     */
    public function addValue(ProductVarietyValue $productVarietyValue): self
    {
        $this->values[] = $productVarietyValue;
        return $this;
    }

    public function clearValues(): self
    {
        $this->values = [];
        return $this;
    }
}
