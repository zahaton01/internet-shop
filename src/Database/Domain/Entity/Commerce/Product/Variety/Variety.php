<?php

namespace App\Database\Domain\Entity\Commerce\Product\Variety;

use App\Database\Domain\Entity\AbstractEntity;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\PersistentCollection;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 *
 * @ORM\Entity(repositoryClass="App\Database\Domain\Repository\Commerce\Product\ProductVarietyRepository")
 */
class Variety extends AbstractEntity
{
    /**
     * @var PersistentCollection|VarietyTranslation[]
     *
     * @ORM\OneToMany(targetEntity="VarietyTranslation", mappedBy="variety", cascade={"persist", "remove"})
     */
    private $translations;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $name;

    /**
     * @var PersistentCollection|VarietyValue[]
     *
     * @ORM\OneToMany(targetEntity="VarietyValue", mappedBy="variety", cascade={"persist", "remove"})
     */
    private $values;

    /**
     * @return VarietyTranslation[]|PersistentCollection
     */
    public function getTranslations()
    {
        return $this->translations;
    }

    /**
     * @param VarietyTranslation[]|PersistentCollection $translations
     *
     * @return self
     */
    public function setTranslations($translations): self
    {
        $this->translations = $translations;
        return $this;
    }

    /**
     * @return string
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return self
     */
    public function setName(string $name): self
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return VarietyValue[]|PersistentCollection
     */
    public function getValues()
    {
        return $this->values;
    }

    /**
     * @param VarietyValue[]|PersistentCollection $values
     *
     * @return self
     */
    public function setValues($values): self
    {
        $this->values = $values;
        return $this;
    }
}
