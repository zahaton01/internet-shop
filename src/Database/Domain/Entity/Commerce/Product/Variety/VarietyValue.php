<?php

namespace App\Database\Domain\Entity\Commerce\Product\Variety;

use App\Database\Domain\Entity\AbstractEntity;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\PersistentCollection;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 *
 * @ORM\Entity()
 */
class VarietyValue extends AbstractEntity
{
    /**
     * @var PersistentCollection|VarietyValueTranslation[]
     *
     * @ORM\OneToMany(targetEntity="VarietyValueTranslation", mappedBy="belongedValue", cascade={"persist", "remove"})
     */
    private $translations;

    /**
     * @var Variety
     *
     * @ORM\ManyToOne(targetEntity="Variety", inversedBy="values")
     * @ORM\JoinColumn(name="variety_id", referencedColumnName="id")
     *
     * @Assert\Valid()
     */
    private $variety;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     *
     * @Assert\NotBlank(message="Variety value cannot be empty")
     * @Assert\NotNull(message="Variety value cannot be empty")
     */
    private $value;

    /**
     * @return VarietyValueTranslation[]|PersistentCollection
     */
    public function getTranslations()
    {
        return $this->translations;
    }

    /**
     * @param VarietyValueTranslation[]|PersistentCollection $translations
     *
     * @return self
     */
    public function setTranslations($translations): self
    {
        $this->translations = $translations;
        return $this;
    }

    /**
     * @return Variety
     */
    public function getVariety(): Variety
    {
        return $this->variety;
    }

    /**
     * @param Variety $variety
     *
     * @return self
     */
    public function setVariety(Variety $variety): self
    {
        $this->variety = $variety;
        return $this;
    }

    /**
     * @return string
     */
    public function getValue(): ?string
    {
        return $this->value;
    }

    /**
     * @param string $value
     *
     * @return self
     */
    public function setValue(string $value): self
    {
        $this->value = $value;
        return $this;
    }
}
