<?php

namespace App\Database\Domain\Entity\Commerce\Product\Variety;

use App\Database\Domain\Entity\AbstractEntity;
use App\Database\Domain\Traits\LocaleTrait;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 *
 * @ORM\Entity()
 */
class VarietyValueTranslation extends AbstractEntity
{
    use LocaleTrait;

    /**
     * @var VarietyValue
     *
     * @ORM\ManyToOne(targetEntity="VarietyValue", inversedBy="translations")
     * @ORM\JoinColumn(name="value_id", referencedColumnName="id")
     *
     * @Assert\Valid()
     */
    private $belongedValue;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     *
     * @Assert\NotNull(message="Variety value cannot be empty")
     * @Assert\NotBlank(message="Variety value cannot be empty")
     */
    private $value;

    /**
     * @return VarietyValue
     */
    public function getBelongedValue(): VarietyValue
    {
        return $this->belongedValue;
    }

    /**
     * @param VarietyValue $belongedValue
     *
     * @return self
     */
    public function setBelongedValue(VarietyValue $belongedValue): self
    {
        $this->belongedValue = $belongedValue;
        return $this;
    }

    /**
     * @return string
     */
    public function getValue(): ?string
    {
        return $this->value;
    }

    /**
     * @param string $value
     *
     * @return self
     */
    public function setValue(string $value): self
    {
        $this->value = $value;
        return $this;
    }
}
