<?php

namespace App\Database\Domain\Entity\Commerce\Product\Variety;

use App\Database\Domain\Entity\AbstractEntity;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 *
 * @ORM\Entity()
 */
class ProductVarietyValue extends AbstractEntity
{
    /**
     * @var string
     *
     * @ORM\Column(name="availability", type="string", nullable=false)
     *
     * @Assert\NotBlank(message="Product variety availability cannot be empty")
     * @Assert\NotNull(message="Product variety availability cannot be empty")
     */
    private $availability;

    /**
     * @var VarietyValue
     *
     * @ORM\ManyToOne(targetEntity="VarietyValue")
     * @ORM\JoinColumn(name="value_id", referencedColumnName="id", onDelete="CASCADE")
     *
     * @Assert\Valid()
     */
    private $attachedValue;

    public function __clone()
    {
        $this->id = null;
    }

    /**
     * @return string
     */
    public function getAvailability(): string
    {
        return $this->availability;
    }

    /**
     * @param string $availability
     *
     * @return self
     */
    public function setAvailability(string $availability): self
    {
        $this->availability = $availability;
        return $this;
    }

    /**
     * @return VarietyValue
     */
    public function getAttachedValue(): VarietyValue
    {
        return $this->attachedValue;
    }

    /**
     * @param VarietyValue $value
     *
     * @return self
     */
    public function setAttachedValue(VarietyValue $value): self
    {
        $this->attachedValue = $value;
        return $this;
    }
}
