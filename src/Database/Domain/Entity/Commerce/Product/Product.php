<?php

namespace App\Database\Domain\Entity\Commerce\Product;

use App\Application\Util\Identifier;
use App\Database\Domain\Entity\AbstractEntity;
use App\Database\Domain\Entity\Commerce\Product\Category\ProductCategory;
use App\Database\Domain\Entity\Commerce\Product\Media\ProductMedia;
use App\Database\Domain\Entity\Commerce\Product\Meta\ProductMeta;
use App\Database\Domain\Entity\Commerce\Product\Specification\ProductSpecification;
use App\Database\Domain\Entity\Commerce\Product\SubCategory\ProductSubCategory;
use App\Database\Domain\Entity\Commerce\Product\Variety\ProductVariety;
use App\Database\Domain\Traits\CreationDateTrait;
use App\Database\Domain\Traits\SlugTrait;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\PersistentCollection;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 *
 * @ORM\Entity(repositoryClass="App\Database\Domain\Repository\Commerce\Product\ProductRepository")
 *
 * @UniqueEntity(fields={"vendorCode"}, message="Product with such vendor code already exists")
 */
class Product extends AbstractEntity
{
    use SlugTrait;
    use CreationDateTrait;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=false)
     *
     * @Assert\NotNull(message="Product name cannot be empty")
     * @Assert\NotBlank(message="Product name cannot be empty")
     */
    private $name;

    /**
     * @var ProductMedia
     *
     * @ORM\OneToOne(targetEntity=ProductMedia::class, cascade={"persist", "remove"})
     * @ORM\JoinColumn(name="media_id", referencedColumnName="id")
     *
     * @Assert\Valid()
     */
    private $media;

    /**
     * @var ProductMeta
     *
     * @ORM\OneToOne(targetEntity=ProductMeta::class, cascade={"persist", "remove"})
     * @ORM\JoinColumn(name="meta_id", referencedColumnName="id")
     *
     * @Assert\Valid()
     */
    private $meta;

    /**
     * @var PersistentCollection|ProductVariety[]
     *
     * @ORM\OneToMany(targetEntity=ProductVariety::class, mappedBy="product")
     */
    private $varieties;

    /**
     * @var PersistentCollection|ProductTranslation[]
     *
     * @ORM\OneToMany(targetEntity="ProductTranslation", mappedBy="product", cascade={"persist", "remove"})
     */
    private $translations;

    /**
     * @var ProductCategory
     *
     * @ORM\ManyToOne(targetEntity=ProductCategory::class, inversedBy="products", cascade={"persist"})
     * @ORM\JoinColumn(name="category_id", referencedColumnName="id", onDelete="SET NULL")
     *
     * @Assert\Valid()
     */
    private $productCategory;

    /**
     * @var ProductSubCategory
     *
     * @ORM\ManyToOne(targetEntity=ProductSubCategory::class, inversedBy="products")
     * @ORM\JoinColumn(name="sub_category_id", referencedColumnName="id")
     *
     * @Assert\Valid()
     */
    private $productSubCategory;

    /**
     * @var PersistentCollection|ProductSpecification[]
     *
     * @ORM\OneToMany(targetEntity=ProductSpecification::class, mappedBy="product", cascade={"persist", "remove"})
     */
    private $specifications;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean", nullable=false)
     *
     * @Assert\NotNull(message="Product visibility status cannot be empty")
     */
    private $isVisible;

    /**
     * @var string
     *
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    /**
     * @var float
     *
     * @ORM\Column(type="float", nullable=true)
     */
    private $price;

    /**
     * @var ProductPromo
     *
     * @ORM\OneToOne(targetEntity="ProductPromo", cascade={"persist", "remove"})
     * @ORM\JoinColumn(name="promo_id", referencedColumnName="id")
     *
     * @Assert\Valid()
     */
    private $promo;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true, unique=true)
     */
    private $vendorCode;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=false)
     *
     * @Assert\NotNull(message="Product availability status cannot be empty")
     * @Assert\NotBlank(message="Product availability status cannot be empty")
     */
    private $availability;

    public function __clone()
    {
        $this->id             = Identifier::uuid();
        $this->vendorCode     = uniqid();
        $this->meta           = clone $this->meta;
        $this->media          = clone $this->media;
        $this->promo          = clone $this->promo;
        $this->translations   = [];
        $this->varieties      = [];
        $this->specifications = [];
    }

    /**
     * @return string
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return self
     */
    public function setName(string $name): self
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return ProductMedia
     */
    public function getMedia(): ProductMedia
    {
        return $this->media;
    }

    /**
     * @param ProductMedia $media
     *
     * @return self
     */
    public function setMedia(ProductMedia $media): self
    {
        $this->media = $media;
        return $this;
    }

    /**
     * @return ProductMeta
     */
    public function getMeta(): ProductMeta
    {
        return $this->meta;
    }

    /**
     * @param ProductMeta $meta
     *
     * @return self
     */
    public function setMeta(ProductMeta $meta): self
    {
        $this->meta = $meta;
        return $this;
    }

    /**
     * @return ProductVariety[]|PersistentCollection
     */
    public function getVarieties()
    {
        return $this->varieties;
    }

    /**
     * @param ProductVariety[]|PersistentCollection $varieties
     *
     * @return self
     */
    public function setVarieties($varieties): self
    {
        $this->varieties = $varieties;
        return $this;
    }

    /**
     * @return ProductTranslation[]|PersistentCollection
     */
    public function getTranslations()
    {
        return $this->translations;
    }

    /**
     * @param ProductTranslation[]|PersistentCollection $translations
     *
     * @return self
     */
    public function setTranslations($translations): self
    {
        $this->translations = $translations;
        return $this;
    }

    /**
     * @return ProductCategory
     */
    public function getProductCategory(): ?ProductCategory
    {
        return $this->productCategory;
    }

    /**
     * @param ProductCategory $productCategory
     *
     * @return self
     */
    public function setProductCategory(?ProductCategory $productCategory): self
    {
        $this->productCategory = $productCategory;
        return $this;
    }

    /**
     * @return ProductSubCategory
     */
    public function getProductSubCategory(): ?ProductSubCategory
    {
        return $this->productSubCategory;
    }

    /**
     * @param ProductSubCategory $productSubCategory
     *
     * @return self
     */
    public function setProductSubCategory(?ProductSubCategory $productSubCategory): self
    {
        $this->productSubCategory = $productSubCategory;
        return $this;
    }

    /**
     * @return ProductSpecification[]|PersistentCollection
     */
    public function getSpecifications()
    {
        return $this->specifications;
    }

    /**
     * @param ProductSpecification[]|PersistentCollection $specifications
     *
     * @return self
     */
    public function setSpecifications($specifications): self
    {
        $this->specifications = $specifications;
        return $this;
    }

    /**
     * @return bool
     */
    public function isVisible(): bool
    {
        return $this->isVisible;
    }

    /**
     * @param bool $isVisible
     *
     * @return self
     */
    public function setIsVisible(bool $isVisible): self
    {
        $this->isVisible = $isVisible;
        return $this;
    }

    /**
     * @return string
     */
    public function getDescription(): ?string
    {
        return html_entity_decode($this->description);
    }

    /**
     * @param string $desc
     *
     * @return self
     */
    public function setDescription(?string $desc): self
    {
        $this->description = $desc;
        return $this;
    }

    /**
     * @return float
     */
    public function getPrice(): ?float
    {
        return $this->price;
    }

    /**
     * @param float $price
     *
     * @return self
     */
    public function setPrice(float $price): self
    {
        $this->price = $price;
        return $this;
    }

    /**
     * @return ProductPromo
     */
    public function getPromo(): ProductPromo
    {
        return $this->promo;
    }

    /**
     * @param ProductPromo $promo
     *
     * @return self
     */
    public function setPromo(ProductPromo $promo): self
    {
        $this->promo = $promo;
        return $this;
    }

    /**
     * @return string
     */
    public function getVendorCode(): ?string
    {
        return $this->vendorCode;
    }

    /**
     * @param string $vendorCode
     *
     * @return self
     */
    public function setVendorCode(string $vendorCode): self
    {
        $this->vendorCode = $vendorCode;
        return $this;
    }

    /**
     * @return string
     */
    public function getAvailability(): string
    {
        return $this->availability;
    }

    /**
     * @param string $availability
     *
     * @return self
     */
    public function setAvailability(string $availability): self
    {
        $this->availability = $availability;
        return $this;
    }

    /**
     * @return float|null
     */
    public function getRealPrice()
    {
        if ($this->getPromo()->isSaleStatus()) {
            return $this->getPromo()->getSalePrice();
        }

        return $this->price;
    }

    /**
     * @param ProductSpecification $specification
     *
     * @return $this
     */
    public function addSpecification(ProductSpecification $specification): self
    {
        $this->specifications[] = $specification;
        return $this;
    }

    /**
     * @return $this
     */
    public function makeUniqueSpecifications(): self
    {
        $this->specifications = array_unique($this->specifications);

        foreach ($this->specifications as $specification) {
            $specification->makeUniqueValues();
        }

        return $this;
    }
}
