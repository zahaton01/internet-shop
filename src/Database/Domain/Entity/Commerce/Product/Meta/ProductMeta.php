<?php

namespace App\Database\Domain\Entity\Commerce\Product\Meta;

use App\Application\Util\Identifier;
use App\Database\Domain\Entity\AbstractMeta;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\PersistentCollection;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 *
 * @ORM\Entity()
 */
class ProductMeta extends AbstractMeta
{
    /**
     * @var PersistentCollection|ProductMetaTranslation[]
     *
     * @ORM\OneToMany(targetEntity=ProductMetaTranslation::class, mappedBy="meta", cascade={"persist", "remove"})
     */
    private $translations;

    public function __clone()
    {
        $this->id           = Identifier::uuid();
        $this->translations = clone $this->translations;
    }

    /**
     * @return ProductMetaTranslation[]|PersistentCollection
     */
    public function getTranslations()
    {
        return $this->translations;
    }

    /**
     * @param ProductMetaTranslation[]|PersistentCollection $translations
     *
     * @return self
     */
    public function setTranslations($translations): self
    {
        $this->translations = $translations;
        return $this;
    }
}
