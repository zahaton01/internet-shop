<?php

namespace App\Database\Domain\Entity\Commerce\Product\Meta;

use App\Application\Util\Identifier;
use App\Database\Domain\Entity\AbstractMeta;
use App\Database\Domain\Traits\LocaleTrait;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 *
 * @ORM\Entity()
 */
class ProductMetaTranslation extends AbstractMeta
{
    use LocaleTrait;

    /**
     * @var ProductMeta
     *
     * @ORM\ManyToOne(targetEntity=ProductMeta::class, inversedBy="translations")
     * @ORM\JoinColumn(name="meta_id", referencedColumnName="id")
     *
     * @Assert\Valid()
     */
    private $meta;

    public function __clone()
    {
        $this->id = Identifier::uuid();
    }

    /**
     * @return ProductMeta|null
     */
    public function getMeta(): ?ProductMeta
    {
        return $this->meta;
    }

    /**
     * @param ProductMeta $meta
     *
     * @return $this
     */
    public function setMeta(ProductMeta $meta): self
    {
        $this->meta = $meta;
        return $this;
    }
}
