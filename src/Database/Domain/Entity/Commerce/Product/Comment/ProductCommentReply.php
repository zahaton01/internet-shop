<?php


namespace App\Database\Domain\Entity\Commerce\Product\Comment;

use App\Database\Domain\Entity\AbstractComment;
use App\Database\Domain\Entity\Commerce\Product\Comment\ProductComment;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 *
 * @ORM\Entity()
 */
class ProductCommentReply extends AbstractComment
{
    /**
     * @var ProductComment
     *
     * @ORM\ManyToOne(targetEntity=ProductComment::class, inversedBy="replies")
     * @ORM\JoinColumn(name="comment_id", referencedColumnName="id", onDelete="CASCADE")
     *
     * @Assert\Valid()
     */
    private $comment;

    /**
     * @return ProductComment
     */
    public function getComment(): ?ProductComment
    {
        return $this->comment;
    }

    /**
     * @param ProductComment $comment
     *
     * @return self
     */
    public function setComment(?ProductComment $comment): self
    {
        $this->comment = $comment;
        return $this;
    }
}
