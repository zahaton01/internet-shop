<?php

namespace App\Database\Domain\Entity\Commerce\Product\Comment;

use App\Database\Domain\Entity\AbstractComment;
use App\Database\Domain\Entity\Commerce\Product\Product;
use App\Database\Domain\Entity\Commerce\Product\Comment\ProductCommentReply;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\PersistentCollection;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 *
 * @ORM\Entity(repositoryClass="App\Database\Domain\Repository\Commerce\Product\ProductCommentRepository")
 */
class ProductComment extends AbstractComment
{
    /**
     * @var Product
     *
     * @ORM\ManyToOne(targetEntity=Product::class)
     * @ORM\JoinColumn(name="product_id", referencedColumnName="id", onDelete="CASCADE")
     *
     * @Assert\Valid()
     */
    private $product;

    /**
     * @var PersistentCollection|ProductCommentReply[]
     *
     * @ORM\OneToMany(targetEntity=ProductCommentReply::class, mappedBy="comment", cascade={"remove"})
     */
    private $replies;

    /**
     * @return Product
     */
    public function getProduct(): ?Product
    {
        return $this->product;
    }

    /**
     * @param Product $product
     *
     * @return self
     */
    public function setProduct(?Product $product): self
    {
        $this->product = $product;
        return $this;
    }

    /**
     * @return PersistentCollection
     */
    public function getReplies(): ?PersistentCollection
    {
        return $this->replies;
    }

    /**
     * @param ProductCommentReply $commentReply
     *
     * @return $this
     */
    public function addReply(ProductCommentReply $commentReply)
    {
        $this->replies[] = $commentReply;
        return $this;
    }
}
