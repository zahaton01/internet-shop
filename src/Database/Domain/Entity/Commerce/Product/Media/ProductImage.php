<?php

namespace App\Database\Domain\Entity\Commerce\Product\Media;

use App\Database\Domain\Entity\AbstractEntity;
use App\Database\Domain\Entity\Media;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 *
 * @ORM\Entity()
 */
class ProductImage extends Media
{
    /**
     * @var ProductMedia
     *
     * @ORM\ManyToOne(targetEntity=ProductMedia::class, inversedBy="images")
     * @ORM\JoinColumn(name="media_id", referencedColumnName="id")
     */
    private $media;

    /**
     * @return ProductMedia
     */
    public function getMedia(): ?ProductMedia
    {
        return $this->media;
    }

    /**
     * @param ProductMedia $media
     *
     * @return self
     */
    public function setMedia(?ProductMedia $media): self
    {
        $this->media = $media;
        return $this;
    }
}
