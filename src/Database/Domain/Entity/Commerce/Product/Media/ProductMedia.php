<?php

namespace App\Database\Domain\Entity\Commerce\Product\Media;

use App\Application\Util\Identifier;
use App\Database\Domain\Entity\AbstractEntity;
use App\Database\Domain\Entity\Media;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\PersistentCollection;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 *
 * @ORM\Entity()
 */
class ProductMedia extends AbstractEntity
{
    /**
     * @var PersistentCollection|ProductImage[]
     *
     * @ORM\OneToMany(targetEntity=ProductImage::class, mappedBy="media", cascade={"persist", "remove"})
     */
    private $images;

    public function __clone()
    {
        $this->id     = Identifier::uuid();
        $this->images = [];
    }

    /**
     * @return PersistentCollection
     */
    public function getImages(): ?PersistentCollection
    {
        return $this->images;
    }

    /**
     * @param ProductImage $image
     *
     * @return $this
     */
    public function addImage(ProductImage $image): self
    {
        $this->images[] = $image;
        return $this;
    }
}
