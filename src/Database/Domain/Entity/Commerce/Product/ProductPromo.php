<?php

namespace App\Database\Domain\Entity\Commerce\Product;

use App\Application\Util\Identifier;
use App\Database\Domain\Entity\AbstractEntity;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 *
 * @ORM\Entity()
 */
class ProductPromo extends AbstractEntity
{
    public const SPECIAL_STATUS_TOP_SALE = 'top_sale';
    public const SPECIAL_STATUS_OUT_SOON = 'out_soon';

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean", nullable=false)
     *
     * @Assert\NotNull(message="Product promo sale status cannot be empty")
     */
    private $saleStatus;

    /**
     * @var float
     *
     * @ORM\Column(type="float", nullable=true)
     */
    private $salePrice;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean", nullable=false)
     *
     * @Assert\NotNull(message="Product promo special status cannot be empty")
     */
    private $isSpecialStatus;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $specialStatus;

    public function __clone()
    {
        $this->id = Identifier::uuid();
    }

    /**
     * @return bool
     */
    public function isSaleStatus(): ?bool
    {
        return $this->saleStatus;
    }

    /**
     * @param bool $saleStatus
     *
     * @return self
     */
    public function setSaleStatus(?bool $saleStatus = false): self
    {
        $this->saleStatus = $saleStatus;
        return $this;
    }

    /**
     * @return float
     */
    public function getSalePrice(): ?float
    {
        return $this->salePrice;
    }

    /**
     * @param float $salePrice
     *
     * @return self
     */
    public function setSalePrice(?float $salePrice): self
    {
        $this->salePrice = $salePrice;
        return $this;
    }

    /**
     * @return bool
     */
    public function isSpecialStatus(): ?bool
    {
        return $this->isSpecialStatus;
    }

    /**
     * @param bool $isSpecialStatus
     *
     * @return self
     */
    public function setIsSpecialStatus(?bool $isSpecialStatus): self
    {
        $this->isSpecialStatus = $isSpecialStatus;
        return $this;
    }

    /**
     * @return string
     */
    public function getSpecialStatus(): ?string
    {
        return $this->specialStatus;
    }

    /**
     * @param string $specialStatus
     *
     * @return self
     */
    public function setSpecialStatus(?string $specialStatus): self
    {
        $this->specialStatus = $specialStatus;
        return $this;
    }
}
