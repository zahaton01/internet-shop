<?php

namespace App\Database\Domain\Entity\Commerce\Product\Specification;

use App\Database\Domain\Entity\AbstractEntity;
use App\Database\Domain\Traits\LocaleTrait;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 *
 * @ORM\Entity()
 */
class SpecificationValueTranslation extends AbstractEntity
{
    use LocaleTrait;

    /**
     * @var string
     *
     * @ORM\Column(name="value", type="string")
     *
     * @Assert\NotNull(message="Specification value cannot be empty")
     * @Assert\NotBlank(message="Specification value cannot be empty")
     */
    private $value;

    /**
     * @var SpecificationValue
     *
     * @ORM\ManyToOne(targetEntity="SpecificationValue", inversedBy="translations")
     * @ORM\JoinColumn(name="specification_id", referencedColumnName="id")
     *
     * @Assert\Valid()
     */
    private $belongedValue;

    /**
     * @return string
     */
    public function getValue(): ?string
    {
        return $this->value;
    }

    /**
     * @param string $value
     *
     * @return self
     */
    public function setValue(string $value): self
    {
        $this->value = $value;
        return $this;
    }

    /**
     * @return SpecificationValue
     */
    public function getBelongedValue(): SpecificationValue
    {
        return $this->belongedValue;
    }

    /**
     * @param SpecificationValue $belongedValue
     *
     * @return self
     */
    public function setBelongedValue(SpecificationValue $belongedValue): self
    {
        $this->belongedValue = $belongedValue;
        return $this;
    }
}
