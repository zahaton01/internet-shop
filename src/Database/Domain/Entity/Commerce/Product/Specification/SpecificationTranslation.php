<?php

namespace App\Database\Domain\Entity\Commerce\Product\Specification;

use App\Database\Domain\Entity\AbstractEntity;
use App\Database\Domain\Traits\LocaleTrait;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 *
 * @ORM\Entity()
 */
class SpecificationTranslation extends AbstractEntity
{
    use LocaleTrait;

    /**
     * @var Specification
     *
     * @ORM\ManyToOne(targetEntity="Specification", inversedBy="translations")
     * @ORM\JoinColumn(name="specification_id", referencedColumnName="id")
     *
     * @Assert\Valid()
     */
    private $specification;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $name;

    /**
     * @return Specification
     */
    public function getSpecification(): ?Specification
    {
        return $this->specification;
    }

    /**
     * @param Specification $specification
     *
     * @return self
     */
    public function setSpecification(?Specification $specification): self
    {
        $this->specification = $specification;
        return $this;
    }

    /**
     * @return string
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return self
     */
    public function setName(?string $name): self
    {
        $this->name = $name;
        return $this;
    }
}
