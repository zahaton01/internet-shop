<?php

namespace App\Database\Domain\Entity\Commerce\Product\Specification;

use App\Database\Domain\Entity\AbstractEntity;
use App\Database\Domain\Entity\Commerce\Product\Category\ProductCategory;
use App\Database\Domain\Entity\Commerce\Product\Product;
use App\Database\Domain\Entity\Commerce\Product\SubCategory\ProductSubCategory;
use App\Database\Domain\Validation\Constraints\ProductSpecification\UniqueProductSpecification;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\PersistentCollection;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 *
 * @ORM\Entity()
 *
 * @UniqueProductSpecification()
 */
class ProductSpecification extends AbstractEntity
{
    /**
     * @var Product
     *
     * @ORM\ManyToOne(targetEntity=Product::class, inversedBy="specifications")
     * @ORM\JoinColumn(name="product_id", referencedColumnName="id", onDelete="CASCADE")
     *
     * @Assert\Valid()
     */
    private $product;

    /**
     * @var Specification
     *
     * @ORM\ManyToOne(targetEntity=Specification::class)
     * @ORM\JoinColumn(name="specification_id", referencedColumnName="id", onDelete="CASCADE")
     *
     * @Assert\Valid()
     */
    private $specification;

    /**
     * @var PersistentCollection|SpecificationValue[]
     *
     * @ORM\ManyToMany(targetEntity=SpecificationValue::class, cascade={"persist"})
     * @ORM\JoinTable(
     *      joinColumns={@ORM\JoinColumn(name="product_specification_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="value_id", referencedColumnName="id")}
     *   )
     */
    private $values;

    /**
     * @var ProductCategory|null
     *
     * @ORM\ManyToOne(targetEntity=ProductCategory::class, inversedBy="specifications")
     */
    private $productCategory;

    /**
     * @var ProductSubCategory|null
     *
     * @ORM\ManyToOne(targetEntity=ProductSubCategory::class, inversedBy="specifications")
     */
    private $productSubCategory;

    public function __clone()
    {
        $this->id = null;
    }

    /**
     * @return Product
     */
    public function getProduct(): Product
    {
        return $this->product;
    }

    /**
     * @param Product $product
     *
     * @return self
     */
    public function setProduct(Product $product): self
    {
        $this->product = $product;
        return $this;
    }

    /**
     * @return Specification
     */
    public function getSpecification(): ?Specification
    {
        return $this->specification;
    }

    /**
     * @param Specification $specification
     *
     * @return self
     */
    public function setSpecification(Specification $specification): self
    {
        $this->specification = $specification;
        return $this;
    }

    /**
     * @return SpecificationValue[]|PersistentCollection
     */
    public function getValues()
    {
        return $this->values;
    }

    /**
     * @param SpecificationValue[]|PersistentCollection $values
     *
     * @return self
     */
    public function setValues($values): self
    {
        $this->values = $values;
        return $this;
    }

    /**
     * @param SpecificationValue $value
     *
     * @return $this
     */
    public function addValue(SpecificationValue $value): self
    {
        $this->values[] = $value;
        return $this;
    }

    /**
     * @return ProductCategory
     */
    public function getProductCategory(): ?ProductCategory
    {
        return $this->productCategory;
    }

    /**
     * @param ProductCategory $productCategory
     *
     * @return self
     */
    public function setProductCategory(?ProductCategory $productCategory): self
    {
        $this->productCategory = $productCategory;
        return $this;
    }

    /**
     * @return ProductSubCategory|null
     */
    public function getProductSubCategory(): ?ProductSubCategory
    {
        return $this->productSubCategory;
    }

    /**
     * @param ProductSubCategory|null $productSubCategory
     *
     * @return self
     */
    public function setProductSubCategory(?ProductSubCategory $productSubCategory): self
    {
        $this->productSubCategory = $productSubCategory;
        return $this;
    }

    public function makeUniqueValues()
    {
        $this->values = array_unique($this->values);
        return $this;
    }
}
