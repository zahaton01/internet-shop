<?php

namespace App\Database\Domain\Entity\Commerce\Product\Specification;

use App\Database\Domain\Entity\AbstractEntity;
use App\Database\Domain\Entity\Commerce\Product\Category\ProductCategory;
use App\Database\Domain\Entity\Commerce\Product\SubCategory\ProductSubCategory;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\PersistentCollection;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 *
 * @ORM\Entity(repositoryClass="App\Database\Domain\Repository\Commerce\Product\ProductSpecificationRepository")
 */
class Specification extends AbstractEntity
{
    /**
     * @var PersistentCollection|SpecificationTranslation[]
     *
     * @ORM\OneToMany(targetEntity="SpecificationTranslation", mappedBy="specification", cascade={"persist", "remove"})
     */
    private $translations;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $name;

    /**
     * @var PersistentCollection
     *
     * @ORM\OneToMany(targetEntity="SpecificationValue", mappedBy="specification", cascade={"persist", "remove"})
     */
    private $values;

    /**
     * @var ProductCategory
     *
     * @ORM\ManyToOne(targetEntity=ProductCategory::class, inversedBy="specifications", cascade={"persist"})
     * @ORM\JoinColumn(name="category_id", referencedColumnName="id", onDelete="SET NULL")
     *
     * @Assert\Valid()
     */
    private $productCategory;

    /**
     * @var ProductSubCategory
     *
     * @ORM\ManyToOne(targetEntity=ProductSubCategory::class, inversedBy="specifications")
     * @ORM\JoinColumn(name="sub_category_id", referencedColumnName="id", onDelete="SET NULL")
     *
     * @Assert\Valid()
     */
    private $productSubCategory;

    /**
     * @var bool
     *
     * @ORM\Column(name="is_enabled", type="boolean", nullable=false)
     *
     * @Assert\NotNull(message="Specification is enabled status cannot be null")
     */
    private $isEnabled;

    /**
     * @return SpecificationTranslation[]|PersistentCollection
     */
    public function getTranslations()
    {
        return $this->translations;
    }

    /**
     * @param SpecificationTranslation[]|PersistentCollection $translations
     *
     * @return self
     */
    public function setTranslations($translations): self
    {
        $this->translations = $translations;
        return $this;
    }

    /**
     * @return string
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return self
     */
    public function setName(string $name): self
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return PersistentCollection
     */
    public function getValues(): PersistentCollection
    {
        return $this->values;
    }

    /**
     * @param PersistentCollection $values
     *
     * @return self
     */
    public function setValues(PersistentCollection $values): self
    {
        $this->values = $values;
        return $this;
    }

    /**
     * @return ProductCategory
     */
    public function getProductCategory(): ?ProductCategory
    {
        return $this->productCategory;
    }

    /**
     * @param ProductCategory $productCategory
     *
     * @return self
     */
    public function setProductCategory(ProductCategory $productCategory): self
    {
        $this->productCategory = $productCategory;
        return $this;
    }

    /**
     * @return ProductSubCategory
     */
    public function getProductSubCategory(): ?ProductSubCategory
    {
        return $this->productSubCategory;
    }

    /**
     * @param ProductSubCategory $subCategory
     *
     * @return self
     */
    public function setProductSubCategory(ProductSubCategory $subCategory): self
    {
        $this->productSubCategory = $subCategory;
        return $this;
    }

    /**
     * @return bool
     */
    public function isEnabled(): bool
    {
        return $this->isEnabled;
    }

    /**
     * @param bool $isEnabled
     *
     * @return self
     */
    public function setIsEnabled(bool $isEnabled): self
    {
        $this->isEnabled = $isEnabled;
        return $this;
    }
}
