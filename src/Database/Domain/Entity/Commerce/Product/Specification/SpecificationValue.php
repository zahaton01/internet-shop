<?php

namespace App\Database\Domain\Entity\Commerce\Product\Specification;

use App\Database\Domain\Entity\AbstractEntity;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\PersistentCollection;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 *
 * @ORM\Entity()
 */
class SpecificationValue extends AbstractEntity
{
    /**
     * @var string
     *
     * @ORM\Column(name="value", type="string")
     */
    private $value;

    /**
     * @var PersistentCollection|SpecificationValueTranslation[]
     *
     * @ORM\OneToMany(targetEntity="SpecificationValueTranslation", mappedBy="belongedValue", cascade={"persist", "remove"})
     */
    private $translations;

    /**
     * @var Specification
     *
     * @ORM\ManyToOne(targetEntity="Specification", inversedBy="values")
     * @ORM\JoinColumn(name="specification_id", referencedColumnName="id")
     *
     * @Assert\Valid()
     */
    private $specification;

    /**
     * @return PersistentCollection
     */
    public function getTranslations(): ?PersistentCollection
    {
        return $this->translations;
    }

    /**
     * @param SpecificationValueTranslation $translation
     * @return $this
     */
    public function addTranslation(SpecificationValueTranslation $translation): self
    {
        $this->translations[] = $translation;
        return $this;
    }

    /**
     * @return Specification
     */
    public function getSpecification(): ?Specification
    {
        return $this->specification;
    }

    /**
     * @param Specification $specification
     *
     * @return self
     */
    public function setSpecification(?Specification $specification): self
    {
        $this->specification = $specification;
        return $this;
    }

    /**
     * @return string
     */
    public function getValue(): ?string
    {
        return $this->value;
    }

    /**
     * @param string $value
     *
     * @return self
     */
    public function setValue(string $value): self
    {
        $this->value = $value;
        return $this;
    }
}
