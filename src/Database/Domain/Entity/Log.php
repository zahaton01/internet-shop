<?php

namespace App\Database\Domain\Entity;

use App\Database\Domain\Entity\User\AbstractUser;
use App\Database\Domain\Traits\CreationDateTrait;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\JoinColumn;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 *
 * @ORM\Entity(repositoryClass="App\Database\Domain\Repository\LogRepository")
 */
class Log extends AbstractEntity
{
    use CreationDateTrait;

    const LEVEL_INFO         = 'info';
    const LEVEL_WARNING      = 'warning';
    const LEVEL_ERROR        = 'error';

    const SOURCE_GLOBAL      = 'global';
    const SOURCE_ADMIN       = 'admin';
    const SOURCE_NOVA_POSHTA = 'nova_poshta';

    /**
     * @var AbstractUser
     *
     * @ManyToOne(targetEntity=AbstractUser::class)
     * @JoinColumn(name="user_id", referencedColumnName="id", nullable=true)
     */
    private $user;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=false)
     */
    private $level;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=false)
     */
    private $source;

    /**
     * @var array|null
     *
     * @ORM\Column(type="json", nullable=true)
     */
    private $extras;

    /**
     * @var string
     *
     * @ORM\Column(type="text", nullable=false)
     */
    private $message;

    /**
     * @return AbstractUser|null
     */
    public function getUser(): ?AbstractUser
    {
        return $this->user;
    }

    /**
     * @param AbstractUser|null $user
     *
     * @return self
     */
    public function setUser(?AbstractUser $user = null): self
    {
        $this->user = $user;
        return $this;
    }

    /**
     * @return string
     */
    public function getLevel(): ?string
    {
        return $this->level;
    }

    /**
     * @param string $level
     *
     * @return self
     */
    public function setLevel(?string $level): self
    {
        $this->level = $level;
        return $this;
    }

    /**
     * @return string
     */
    public function getSource(): ?string
    {
        return $this->source;
    }

    /**
     * @param string $source
     *
     * @return self
     */
    public function setSource(?string $source): self
    {
        $this->source = $source;
        return $this;
    }

    /**
     * @return array|null
     */
    public function getExtras(): ?array
    {
        return $this->extras;
    }

    /**
     * @param array|null $extras
     *
     * @return self
     */
    public function setExtras(?array $extras = null): self
    {
        $this->extras = $extras;
        return $this;
    }

    /**
     * @return string
     */
    public function getMessage(): ?string
    {
        return $this->message;
    }

    /**
     * @param string $message
     *
     * @return self
     */
    public function setMessage(?string $message): self
    {
        $this->message = $message;
        return $this;
    }

    /**
     * @return array
     */
    public static function levels(): array
    {
        return [
            self::LEVEL_ERROR,
            self::LEVEL_WARNING,
            self::LEVEL_INFO
        ];
    }

    /**
     * @return array
     */
    public static function sources(): array
    {
        return [
            self::SOURCE_GLOBAL,
            self::SOURCE_ADMIN,
            self::SOURCE_NOVA_POSHTA
        ];
    }
}
