<?php

namespace App\Database\Domain\Entity\User;

use App\Database\Domain\Entity\User\AbstractUser;
use Doctrine\ORM\Mapping as ORM;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 *
 * @ORM\Entity()
 */
class Admin extends AbstractUser
{
    public const TYPE = 1;
}
