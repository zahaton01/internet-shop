<?php

namespace App\Database\Domain\Entity\User;

use App\Application\Model\AppEnv\Roles;
use App\Database\Domain\Entity\AbstractEntity;
use App\Database\Domain\Traits\CreationDateTrait;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 *
 * @ORM\Table("users")
 * @ORM\Entity(repositoryClass="App\Database\Domain\Repository\UserRepository")
 * @ORM\InheritanceType("SINGLE_TABLE")
 * @ORM\DiscriminatorColumn(name="type", type="integer")
 * @ORM\DiscriminatorMap(value={
 *     App\Database\Domain\Entity\User\User::TYPE  = "App\Database\Domain\Entity\User\User",
 *     App\Database\Domain\Entity\User\Admin::TYPE = "App\Database\Domain\Entity\User\Admin"
 * })
 */
abstract class AbstractUser extends AbstractEntity implements UserInterface
{
    use CreationDateTrait;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=180, unique=true)
     */
    protected $login;

    /**
     * @var string The hashed password
     *
     * @ORM\Column(type="string")
     */
    protected $password;

    /**
     * @var array
     *
     * @ORM\Column(type="json")
     */
    protected $roles = [];

    /**
     * @return string
     */
    public function getLogin(): string
    {
        return $this->login;
    }

    /**
     * @param $login
     *
     * @return $this
     */
    public function setLogin(string $login)
    {
        $this->login = $login;
        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getRoles()
    {
        $roles = $this->roles;
        return array_unique($roles);
    }

    /**
     * @param array $roles
     *
     * @return $this
     */
    public function setRoles(array $roles)
    {
        $this->roles = $roles;
        return $this;
    }

    /**
     * @param string $role
     *
     * @return $this
     */
    public function addRole(string $role)
    {
        $this->roles[] = $role;
        $this->roles   = array_unique($this->roles);

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUsername()
    {
        return (string) $this->login;
    }

    /**
     * @see UserInterface
     */
    public function getPassword()
    {
        return (string) $this->password;
    }

    /**
     * @param $password
     * @return $this
     */
    public function setPassword($password)
    {
        $this->password = $password;
        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getSalt()
    {

    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {

    }

    /**
     * @return bool
     */
    public function isAdmin()
    {
        return in_array(Roles::ADMIN, $this->roles);
    }
}
