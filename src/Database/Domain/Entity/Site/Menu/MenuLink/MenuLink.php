<?php

namespace App\Database\Domain\Entity\Site\Menu\MenuLink;

use App\Database\Domain\Entity\AbstractEntity;
use App\Database\Domain\Entity\Site\SitePage\SitePage;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 *
 * @ORM\Entity()
 */
class MenuLink extends AbstractEntity
{
    public const INTERNAL = 'internal';
    public const EXTERNAL = 'external';

    /**
     * @var string|null
     *
     * @ORM\Column(type="string")
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     *
     * @Assert\NotBlank()
     * @Assert\NotNull()
     */
    private $type;

    /**
     * @var SitePage|null
     *
     * @ORM\ManyToOne(targetEntity=SitePage::class)
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    private $sitePage;

    /**
     * @var string|null
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $url;

    /**
     * @var MenuLinkTranslation[]|Collection
     *
     * @ORM\OneToMany(targetEntity="MenuLinkTranslation", mappedBy="menuLink")
     */
    private $translations;

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string|null $name
     *
     * @return self
     */
    public function setName(?string $name): self
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     *
     * @return self
     */
    public function setType(string $type): self
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return SitePage|null
     */
    public function getSitePage(): ?SitePage
    {
        return $this->sitePage;
    }

    /**
     * @param SitePage|null $sitePage
     *
     * @return self
     */
    public function setSitePage(?SitePage $sitePage): self
    {
        $this->sitePage = $sitePage;
        return $this;
    }

    /**
     * @return string
     */
    public function getUrl(): ?string
    {
        return $this->url;
    }

    /**
     * @param string $url
     *
     * @return self
     */
    public function setUrl(?string $url): self
    {
        $this->url = $url;
        return $this;
    }

    /**
     * @return bool
     */
    public function isInternal(): bool
    {
        return $this->type === self::INTERNAL;
    }

    /**
     * @return bool
     */
    public function isExternal(): bool
    {
        return $this->type === self::EXTERNAL;
    }

    /**
     * @return MenuLinkTranslation[]|Collection
     */
    public function getTranslations()
    {
        return $this->translations;
    }
}