<?php

namespace App\Database\Domain\Entity\Site\Menu\MenuLink;

use App\Database\Domain\Entity\AbstractEntity;
use App\Database\Domain\Traits\LocaleTrait;
use Doctrine\ORM\Mapping as ORM;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 *
 * @ORM\Entity()
 */
class MenuLinkTranslation extends AbstractEntity
{
    use LocaleTrait;

    /**
     * @var string|null
     *
     * @ORM\Column(type="string")
     */
    private $name;

    /**
     * @var MenuLink
     *
     * @ORM\ManyToOne(targetEntity="MenuLink", inversedBy="translations")
     */
    private $menuLink;

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string|null $name
     *
     * @return self
     */
    public function setName(?string $name): self
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return MenuLink
     */
    public function getMenuLink(): MenuLink
    {
        return $this->menuLink;
    }

    /**
     * @param MenuLink $menuLink
     *
     * @return self
     */
    public function setMenuLink(MenuLink $menuLink): self
    {
        $this->menuLink = $menuLink;
        return $this;
    }
}