<?php

namespace App\Database\Domain\Entity\Site\Menu;

use App\Database\Domain\Entity\AbstractEntity;
use App\Database\Domain\Entity\Site\Menu\MenuElement\MenuElement;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 *
 * @ORM\Entity()
 */
class Menu extends AbstractEntity
{
    public const FOOTER = 'footer';
    public const HEADER = 'header';

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     *
     * @Assert\NotBlank()
     * @Assert\NotNull()
     */
    private $type;

    /**
     * @var MenuElement[]|Collection
     *
     * @ORM\ManyToMany(targetEntity=MenuElement::class, cascade={"persist"})
     */
    private $elements;

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     *
     * @return self
     */
    public function setType(string $type): self
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return MenuElement[]|Collection
     */
    public function getElements()
    {
        return $this->elements;
    }

    /**
     * @param MenuElement[] $elements
     *
     * @return self
     */
    public function setElements(array $elements): self
    {
        $this->elements = $elements;
        return $this;
    }

    /**
     * @param MenuElement $element
     *
     * @return $this
     */
    public function addElement(MenuElement $element)
    {
        $this->elements[] = $element;
        return $this;
    }

    /**
     * @return bool
     */
    public function isHeader()
    {
        return $this->type === self::HEADER;
    }

    /**
     * @return bool
     */
    public function isFooter()
    {
        return $this->type === self::FOOTER;
    }
}