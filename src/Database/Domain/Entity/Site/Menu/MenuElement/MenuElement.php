<?php

namespace App\Database\Domain\Entity\Site\Menu\MenuElement;

use App\Database\Domain\Entity\AbstractEntity;
use App\Database\Domain\Entity\Site\Menu\MenuLink\MenuLink;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 *
 * @ORM\Entity()
 */
class MenuElement extends AbstractEntity
{
    public const LINK            = 'link';
    public const LINK_COLLECTION = 'link_collection';

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     *
     * @Assert\NotNull()
     * @Assert\NotBlank()
     */
    private $type;

    /**
     * @var MenuLink|null
     *
     * @ORM\ManyToOne(targetEntity=MenuLink::class, cascade={"persist"})
     */
    private $link;

    /**
     * @var MenuLink[]|Collection
     *
     * @ORM\ManyToMany(targetEntity=MenuLink::class)
     */
    private $links;

    /**
     * @var string|null
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $linkCollectionName;

    /**
     * @var MenuElementTranslation[]|Collection
     *
     * @ORM\OneToMany(targetEntity=MenuElementTranslation::class, mappedBy="menuElement")
     */
    private $translations;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean")
     *
     * @Assert\NotNull()
     */
    private $isEnabled;

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     *
     * @return self
     */
    public function setType(string $type): self
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return MenuLink[]
     */
    public function getLinks()
    {
        return $this->links;
    }

    /**
     * @param MenuLink[] $links
     *
     * @return self
     */
    public function setLinks(?array $links): self
    {
        $this->links = $links;
        return $this;
    }

    /**
     * @param MenuLink $link
     *
     * @return $this
     */
    public function addLink(MenuLink $link): self
    {
        $this->links[] = $link;
        return $this;
    }

    /**
     * @param MenuLink $link
     *
     * @return $this
     */
    public function removeLink(MenuLink $link): self
    {
        $this->links->remove($link);
        return $this;
    }

    /**
     * @return bool
     */
    public function isLinkCollection(): bool
    {
        return $this->type === self::LINK_COLLECTION;
    }

    /**
     * @return bool
     */
    public function isLink(): bool
    {
        return $this->type === self::LINK;
    }

    /**
     * @return string|null
     */
    public function getLinkCollectionName(): ?string
    {
        return $this->linkCollectionName;
    }

    /**
     * @param string|null $linkCollectionName
     *
     * @return self
     */
    public function setLinkCollectionName(?string $linkCollectionName): self
    {
        $this->linkCollectionName = $linkCollectionName;
        return $this;
    }

    /**
     * @return MenuElementTranslation[]|Collection
     */
    public function getTranslations()
    {
        return $this->translations;
    }

    /**
     * @param MenuElementTranslation[]|Collection $translations
     *
     * @return self
     */
    public function setTranslations($translations): self
    {
        $this->translations = $translations;
        return $this;
    }

    /**
     * @return MenuLink|null
     */
    public function getLink(): ?MenuLink
    {
        return $this->link;
    }

    /**
     * @param MenuLink|null $link
     *
     * @return self
     */
    public function setLink(?MenuLink $link): self
    {
        $this->link = $link;
        return $this;
    }

    /**
     * @return bool
     */
    public function isEnabled(): bool
    {
        return $this->isEnabled;
    }

    /**
     * @param bool $isEnabled
     *
     * @return self
     */
    public function setIsEnabled(bool $isEnabled): self
    {
        $this->isEnabled = $isEnabled;
        return $this;
    }
}