<?php

namespace App\Database\Domain\Entity\Site\Menu\MenuElement;

use App\Database\Domain\Entity\AbstractEntity;
use App\Database\Domain\Traits\LocaleTrait;
use Doctrine\ORM\Mapping as ORM;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 *
 * @ORM\Entity()
 */
class MenuElementTranslation extends AbstractEntity
{
    use LocaleTrait;

    /**
     * @var string|null
     *
     * @ORM\Column(type="string")
     */
    private $linkCollectionName;

    /**
     * @var MenuElement
     *
     * @ORM\ManyToOne(targetEntity=MenuElement::class, inversedBy="translations")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $menuElement;

    /**
     * @return MenuElement
     */
    public function getMenuElement(): MenuElement
    {
        return $this->menuElement;
    }

    /**
     * @param MenuElement $menuElement
     *
     * @return self
     */
    public function setMenuElement(MenuElement $menuElement): self
    {
        $this->menuElement = $menuElement;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getLinkCollectionName(): ?string
    {
        return $this->linkCollectionName;
    }

    /**
     * @param string|null $linkCollectionName
     *
     * @return self
     */
    public function setLinkCollectionName(?string $linkCollectionName): self
    {
        $this->linkCollectionName = $linkCollectionName;
        return $this;
    }
}