<?php

namespace App\Database\Domain\Entity\Site;

use App\Database\Domain\Entity\AbstractComment;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 *
 * @ORM\Entity()
 */
class SiteCommentReply extends AbstractComment
{
    /**
     * @var SiteComment
     *
     * @ORM\ManyToOne(targetEntity="SiteComment", inversedBy="replies")
     * @ORM\JoinColumn(name="comment_id", referencedColumnName="id")
     *
     * @Assert\Valid()
     */
    private $comment;

    /**
     * @return SiteComment
     */
    public function getComment(): ?SiteComment
    {
        return $this->comment;
    }

    /**
     * @param SiteComment $comment
     *
     * @return self
     */
    public function setComment(?SiteComment $comment): self
    {
        $this->comment = $comment;
        return $this;
    }
}