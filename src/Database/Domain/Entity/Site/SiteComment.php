<?php

namespace App\Database\Domain\Entity\Site;

use App\Database\Domain\Entity\AbstractComment;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\PersistentCollection;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 *
 * @ORM\Entity(repositoryClass="App\Database\Domain\Repository\Site\SiteCommentRepository")
 */
class SiteComment extends AbstractComment
{
    /**
     * @var PersistentCollection|SiteCommentReply[]
     *
     * @ORM\OneToMany(targetEntity="SiteCommentReply", mappedBy="comment")
     */
    private $replies;

    /**
     * @return PersistentCollection
     */
    public function getReplies(): ?PersistentCollection
    {
        return $this->replies;
    }

    /**
     * @param SiteCommentReply $commentReply
     * @return $this
     */
    public function addReply(SiteCommentReply $commentReply)
    {
        $this->replies[] = $commentReply;
        return $this;
    }
}