<?php

namespace App\Database\Domain\Entity\Site\Post;

use App\Database\Domain\Entity\AbstractEntity;
use App\Database\Domain\Entity\Site\Post\Media\PostImage;
use App\Database\Domain\Entity\Site\Post\Media\PostMedia;
use App\Database\Domain\Entity\Site\Post\Meta\PostMeta;
use App\Database\Domain\Traits\CreationDateTrait;
use App\Database\Domain\Traits\SlugTrait;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 *
 * @ORM\Entity(repositoryClass="App\Database\Domain\Repository\Post\PostRepository")
 */
class Post extends AbstractEntity
{
    use SlugTrait;
    use CreationDateTrait;

    public const NEWS  = 'news';
    public const PROMO = 'promo';

    /**
     * @var string|null
     *
     * @ORM\Column(type="string", nullable=false)
     *
     * @Assert\NotBlank()
     * @Assert\NotNull()
     */
    private $title;

    /**
     * @var string|null
     *
     * @ORM\Column(type="text", nullable=true)
     */
    private $content;

    /**
     * @var string|null
     *
     * @ORM\Column(type="string", nullable=false)
     */
    private $type;

    /**
     * @var PostImage|null
     *
     * @ORM\ManyToOne(targetEntity=PostImage::class)
     */
    private $previewImage;

    /**
     * @var PostMeta|null
     *
     * @ORM\OneToOne(targetEntity=PostMeta::class, cascade={"all"})
     */
    private $meta;

    /**
     * @var PostTranslation[]|Collection
     *
     * @ORM\OneToMany(targetEntity=PostTranslation::class, mappedBy="post")
     */
    private $translations;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean", nullable=false)
     *
     * @Assert\NotNull()
     */
    private $isVisible;

    /**
     * @var PostMedia
     *
     * @ORM\OneToOne(targetEntity=PostMedia::class, cascade={"all"})
     */
    private $media;

    /**
     * @return string|null
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * @param string|null $title
     *
     * @return self
     */
    public function setTitle(?string $title): self
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getContent(): ?string
    {
        return $this->content;
    }

    /**
     * @param string|null $content
     *
     * @return self
     */
    public function setContent(?string $content): self
    {
        $this->content = $content;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getType(): ?string
    {
        return $this->type;
    }

    /**
     * @param string|null $type
     *
     * @return self
     */
    public function setType(?string $type): self
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return PostImage|null
     */
    public function getPreviewImage(): ?PostImage
    {
        return $this->previewImage;
    }

    /**
     * @param PostImage|null $previewImage
     *
     * @return self
     */
    public function setPreviewImage(?PostImage $previewImage): self
    {
        $this->previewImage = $previewImage;
        return $this;
    }

    /**
     * @return PostMeta|null
     */
    public function getMeta(): ?PostMeta
    {
        return $this->meta;
    }

    /**
     * @param PostMeta|null $meta
     *
     * @return self
     */
    public function setMeta(?PostMeta $meta): self
    {
        $this->meta = $meta;
        return $this;
    }

    /**
     * @return PostTranslation[]|Collection
     */
    public function getTranslations()
    {
        return $this->translations;
    }

    /**
     * @return bool
     */
    public function isNews(): bool
    {
        return $this->type === self::NEWS;
    }

    /**
     * @return bool
     */
    public function isPromo(): bool
    {
        return $this->type === self::PROMO;
    }

    /**
     * @return bool
     */
    public function isVisible(): bool
    {
        return $this->isVisible;
    }

    /**
     * @param bool $isVisible
     *
     * @return self
     */
    public function setIsVisible(bool $isVisible): self
    {
        $this->isVisible = $isVisible;
        return $this;
    }

    /**
     * @return PostMedia
     */
    public function getMedia(): PostMedia
    {
        return $this->media;
    }

    /**
     * @param PostMedia $media
     *
     * @return self
     */
    public function setMedia(PostMedia $media): self
    {
        $this->media = $media;
        return $this;
    }
}