<?php

namespace App\Database\Domain\Entity\Site\Post\Media;

use App\Database\Domain\Entity\Media;
use Doctrine\ORM\Mapping as ORM;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 *
 * @ORM\Entity()
 */
class PostImage extends Media
{
    /**
     * @var PostMedia
     *
     * @ORM\ManyToOne(targetEntity=PostMedia::class, inversedBy="images")
     */
    private $media;

    /**
     * @return PostMedia
     */
    public function getMedia(): PostMedia
    {
        return $this->media;
    }

    /**
     * @param PostMedia $media
     *
     * @return self
     */
    public function setMedia(PostMedia $media): self
    {
        $this->media = $media;
        return $this;
    }
}