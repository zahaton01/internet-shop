<?php

namespace App\Database\Domain\Entity\Site\Post\Media;

use App\Database\Domain\Entity\AbstractEntity;
use Doctrine\ORM\Mapping as ORM;
use phpDocumentor\Reflection\Types\Collection;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 *
 * @ORM\Entity()
 */
class PostMedia extends AbstractEntity
{
    /**
     * @var PostImage[]|Collection
     *
     * @ORM\OneToMany(targetEntity="PostImage", mappedBy="media")
     */
    private $images;

    /**
     * @return PostImage[]|Collection
     */
    public function getImages()
    {
        return $this->images;
    }
}