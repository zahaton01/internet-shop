<?php

namespace App\Database\Domain\Entity\Site\Post\Meta;

use App\Database\Domain\Entity\AbstractMeta;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 *
 * @ORM\Entity()
 */
class PostMeta extends AbstractMeta
{
    /**
     * @var PostMetaTranslation[]|Collection
     *
     * @ORM\OneToMany(targetEntity=PostMetaTranslation::class, mappedBy="meta")
     */
    private $translations;

    /**
     * @return PostMetaTranslation[]|Collection
     */
    public function getTranslations()
    {
        return $this->translations;
    }
}