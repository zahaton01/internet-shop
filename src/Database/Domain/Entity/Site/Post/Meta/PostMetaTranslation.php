<?php

namespace App\Database\Domain\Entity\Site\Post\Meta;

use App\Database\Domain\Entity\AbstractMeta;
use App\Database\Domain\Traits\LocaleTrait;
use Doctrine\ORM\Mapping as ORM;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 *
 * @ORM\Entity()
 */
class PostMetaTranslation extends AbstractMeta
{
    use LocaleTrait;

    /**
     * @var PostMeta
     *
     * @ORM\ManyToOne(targetEntity=PostMeta::class, inversedBy="translations")
     */
    private $meta;

    /**
     * @return PostMeta
     */
    public function getMeta(): PostMeta
    {
        return $this->meta;
    }

    /**
     * @param PostMeta $meta
     *
     * @return self
     */
    public function setMeta(PostMeta $meta): self
    {
        $this->meta = $meta;
        return $this;
    }
}