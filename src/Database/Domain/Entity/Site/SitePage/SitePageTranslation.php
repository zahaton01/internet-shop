<?php

namespace App\Database\Domain\Entity\Site\SitePage;

use App\Database\Domain\Entity\AbstractEntity;
use App\Database\Domain\Traits\LocaleTrait;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @author Anton Zakharuk <anton.zakharuk@cosmonova.net>
 *
 * @ORM\Table("site_page_translations")
 * @ORM\Entity()
 */
class SitePageTranslation extends AbstractEntity
{
    use LocaleTrait;

    /**
     * @var SitePage
     *
     * @ORM\ManyToOne(targetEntity="SitePage", inversedBy="translations")
     * @ORM\JoinColumn(name="site_page_id", referencedColumnName="id")
     *
     * @Assert\Valid()
     */
    private $sitePage;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", nullable=false)
     *
     * @Assert\NotBlank(message="Site page title cannot be empty (translation)")
     * @Assert\NotNull(message="Site page title cannot be empty (translation)")
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="content", type="text", nullable=true)
     */
    private $content;

    /**
     * @return SitePage
     */
    public function getSitePage(): ?SitePage
    {
        return $this->sitePage;
    }

    /**
     * @param SitePage $sitePage
     *
     * @return self
     */
    public function setSitePage(?SitePage $sitePage): self
    {
        $this->sitePage = $sitePage;
        return $this;
    }

    /**
     * @return string
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * @param string $title
     *
     * @return self
     */
    public function setTitle(?string $title): self
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return string
     */
    public function getContent(): ?string
    {
        return html_entity_decode($this->content);
    }

    /**
     * @param string $content
     *
     * @return self
     */
    public function setContent(?string $content): self
    {
        $this->content = $content;
        return $this;
    }
}