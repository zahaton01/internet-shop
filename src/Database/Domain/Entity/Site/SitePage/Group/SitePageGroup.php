<?php

namespace App\Database\Domain\Entity\Site\SitePage\Group;

use App\Database\Domain\Entity\AbstractEntity;
use App\Database\Domain\Entity\Site\SitePage\SitePage;
use App\Database\Domain\Traits\SlugTrait;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\PersistentCollection;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 *
 * @ORM\Entity()
 */
class SitePageGroup extends AbstractEntity
{
    use SlugTrait;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", nullable=false)
     *
     * @Assert\NotNull(message="Site page group name cannot be empty")
     * @Assert\NotBlank(message="Site page group name cannot be empty")
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="twig_template", type="string", nullable=true)
     */
    private $twigTemplate;

    /**
     * @var PersistentCollection|SitePage[]
     *
     * @ORM\OneToMany(targetEntity=SitePage::class, mappedBy="group")
     */
    private $pages;

    /**
     * @return string
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return self
     */
    public function setName(?string $name): self
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getTwigTemplate(): ?string
    {
        return $this->twigTemplate;
    }

    /**
     * @param string $twigTemplate
     *
     * @return self
     */
    public function setTwigTemplate(?string $twigTemplate): self
    {
        $this->twigTemplate = $twigTemplate;
        return $this;
    }

    /**
     * @return PersistentCollection|SitePage[]
     */
    public function getPages(): ?PersistentCollection
    {
        return $this->pages;
    }

    /**
     * @param SitePage $page
     *
     * @return $this
     */
    public function addPage(SitePage $page)
    {
        $this->pages[] = $page;
        return $this;
    }
}