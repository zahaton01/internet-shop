<?php

namespace App\Database\Domain\Entity\Site\SitePage;

use App\Database\Domain\Entity\AbstractEntity;
use App\Database\Domain\Entity\Site\SitePage\Group\SitePageGroup;
use App\Database\Domain\Entity\Site\SitePage\Meta\SitePageMeta;
use App\Database\Domain\Traits\SlugTrait;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\PersistentCollection;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 *
 * @ORM\Entity(repositoryClass="App\Database\Domain\Repository\Site\SitePageRepository")
 */
class SitePage extends AbstractEntity
{
    use SlugTrait;

    public const STATIC_MAIN     = 'main';
    public const STATIC_CATALOG  = 'catalog';
    public const STATIC_CART     = 'cart';
    public const STATIC_CONTACT  = 'contacts';
    public const STATIC_CHECKOUT = 'checkout';
    public const STATIC_BLOG     = 'blog';

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=false)
     *
     * @Assert\NotNull(message="Site page title cannot be empty")
     * @Assert\NotBlank(message="Site page title cannot be empty")
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(type="text", nullable=true)
     */
    private $content;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean", nullable=false)
     *
     * @Assert\NotNull(message="Site page is enabled status cannot be empty")
     */
    private $isEnabled;

    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean", nullable=false)
     *
     * @Assert\NotNull(message="Site page is static status cannot be empty")
     */
    private $isStatic;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $staticType;

    /**
     * @var PersistentCollection|SitePageTranslation[]
     *
     * @ORM\OneToMany(targetEntity="SitePageTranslation", mappedBy="sitePage", cascade={"persist", "remove"})
     */
    private $translations;

    /**
     * @var SitePageMeta
     *
     * @ORM\OneToOne(targetEntity=SitePageMeta::class, cascade={"persist", "remove"})
     * @ORM\JoinColumn(name="site_meta_id", referencedColumnName="id")
     */
    private $meta;

    /**
     * @var SitePageGroup
     *
     * @ORM\ManyToOne(targetEntity=SitePageGroup::class, inversedBy="pages")
     * @ORM\JoinColumn(name="group_id", referencedColumnName="id", onDelete="SET NULL")
     */
    private $group;

    /**
     * @return string
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * @param string $title
     *
     * @return self
     */
    public function setTitle(?string $title): self
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return string
     */
    public function getContent(): ?string
    {
        return $this->content;
    }

    /**
     * @param string $content
     *
     * @return self
     */
    public function setContent(?string $content): self
    {
        $this->content = $content;
        return $this;
    }

    /**
     * @return bool
     */
    public function isEnabled(): ?bool
    {
        return $this->isEnabled;
    }

    /**
     * @param bool $isEnabled
     *
     * @return self
     */
    public function setIsEnabled(?bool $isEnabled): self
    {
        $this->isEnabled = $isEnabled;
        return $this;
    }

    /**
     * @return bool
     */
    public function isStatic(): ?bool
    {
        return $this->isStatic;
    }

    /**
     * @param bool $isStatic
     *
     * @return self
     */
    public function setIsStatic(?bool $isStatic): self
    {
        $this->isStatic = $isStatic;
        return $this;
    }

    /**
     * @return string
     */
    public function getStaticType(): ?string
    {
        return $this->staticType;
    }

    /**
     * @param string $staticType
     *
     * @return self
     */
    public function setStaticType(?string $staticType): self
    {
        $this->staticType = $staticType;
        return $this;
    }

    /**
     * @return PersistentCollection
     */
    public function getTranslations(): ?PersistentCollection
    {
        return $this->translations;
    }

    /**
     * @param SitePageTranslation $translation
     *
     * @return self
     */
    public function addTranslation(?SitePageTranslation $translation): self
    {
        $this->translations[] = $translation;
        return $this;
    }

    /**
     * @return SitePageMeta
     */
    public function getMeta(): ?SitePageMeta
    {
        return $this->meta;
    }

    /**
     * @param SitePageMeta $meta
     *
     * @return self
     */
    public function setMeta(?SitePageMeta $meta): self
    {
        $this->meta = $meta;
        return $this;
    }

    /**
     * @return bool
     */
    public function isStaticNews(): bool
    {
        return $this->staticType === self::STATIC_NEWS;
    }

    /**
     * @return SitePageGroup
     */
    public function getGroup(): ?SitePageGroup
    {
        return $this->group;
    }

    /**
     * @param SitePageGroup $group
     *
     * @return self
     */
    public function setGroup(?SitePageGroup $group): self
    {
        $this->group = $group;
        return $this;
    }
}