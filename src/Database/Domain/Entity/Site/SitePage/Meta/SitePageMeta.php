<?php

namespace App\Database\Domain\Entity\Site\SitePage\Meta;

use App\Database\Domain\Entity\AbstractMeta;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\PersistentCollection;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 *
 * @ORM\Entity()
 */
class SitePageMeta extends AbstractMeta
{
    /**
     * @var PersistentCollection|SitePageMetaTranslation[]
     *
     * @ORM\OneToMany(targetEntity="SitePageMetaTranslation", mappedBy="meta", cascade={"persist", "remove"})
     */
    private $translations;

    /**
     * @return PersistentCollection
     */
    public function getTranslations(): ?PersistentCollection
    {
        return $this->translations;
    }

    /**
     * @param SitePageMetaTranslation $translation
     *
     * @return $this
     */
    public function addTranslation(SitePageMetaTranslation $translation)
    {
        $this->translations[] = $translation;
        return $this;
    }
}