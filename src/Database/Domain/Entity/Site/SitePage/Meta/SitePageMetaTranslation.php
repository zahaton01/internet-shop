<?php

namespace App\Database\Domain\Entity\Site\SitePage\Meta;

use App\Database\Domain\Entity\AbstractMeta;
use App\Database\Domain\Traits\LocaleTrait;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 *
 * @ORM\Entity()
 */
class SitePageMetaTranslation extends AbstractMeta
{
    use LocaleTrait;

    /**
     * @var SitePageMeta
     *
     * @ORM\ManyToOne(targetEntity="SitePageMeta", inversedBy="translations")
     * @ORM\JoinColumn(name="meta_id", referencedColumnName="id")
     *
     * @Assert\Valid()
     */
    private $meta;

    /**
     * @return SitePageMeta
     */
    public function getMeta(): ?SitePageMeta
    {
        return $this->meta;
    }

    /**
     * @param SitePageMeta $meta
     *
     * @return self
     */
    public function setMeta(?SitePageMeta $meta): self
    {
        $this->meta = $meta;
        return $this;
    }
}