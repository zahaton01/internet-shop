<?php

namespace App\Database\Domain\Entity;

use App\Database\Domain\Entity\User\AbstractUser;
use App\Database\Domain\Traits\CreationDateTrait;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 *
 * @ORM\MappedSuperclass()
 */
abstract class AbstractComment extends AbstractEntity
{
    use CreationDateTrait;

    /**
     * @var string
     *
     * @ORM\Column(type="text", nullable=false)
     *
     * @Assert\NotBlank(message="Comment text cannot be empty")
     * @Assert\NotNull(message="Comment text cannot be empty")
     */
    protected $text;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=false)
     *
     * @Assert\NotNull(message="Comment sender name cannot be empty")
     * @Assert\NotBlank(message="Comment sender name cannot be empty")
     */
    protected $name;

    /**
     * @var AbstractUser|null
     *
     * @ORM\ManyToOne(targetEntity=AbstractUser::class)
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=true)
     *
     * @Assert\Valid()
     */
    protected $user;

    /**
     * @return string
     */
    public function getText(): string
    {
        return $this->text;
    }

    /**
     * @param string $text
     *
     * @return self
     */
    public function setText(string $text): self
    {
        $this->text = $text;
        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return self
     */
    public function setName(string $name): self
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return AbstractUser|null
     */
    public function getUser(): ?AbstractUser
    {
        return $this->user;
    }

    /**
     * @param AbstractUser|null $user
     *
     * @return self
     */
    public function setUser(?AbstractUser $user): self
    {
        $this->user = $user;
        return $this;
    }
}