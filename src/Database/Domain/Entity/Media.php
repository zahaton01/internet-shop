<?php

namespace App\Database\Domain\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 *
 * @ORM\MappedSuperclass()
 */
class Media extends AbstractEntity
{
    public const LOCAL  = 'local';
    public const REMOTE = 'remote';

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $fullPath;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $assetPath;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $url;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     *
     * @Assert\NotNull()
     * @Assert\NotBlank()
     * @Assert\Choice(choices={
     *     App\Database\Domain\Entity\Media::LOCAL,
     *     App\Database\Domain\Entity\Media::REMOTE
     *  })
     */
    private $type;

    /**
     * @return string
     */
    public function getFullPath(): ?string
    {
        return $this->fullPath;
    }

    /**
     * @param string $fullPath
     *
     * @return self
     */
    public function setFullPath(?string $fullPath): self
    {
        $this->fullPath = $fullPath;
        return $this;
    }

    /**
     * @return string
     */
    public function getAssetPath(): ?string
    {
        return $this->assetPath;
    }

    /**
     * @param string $assetPath
     *
     * @return self
     */
    public function setAssetPath(?string $assetPath): self
    {
        $this->assetPath = $assetPath;
        return $this;
    }

    /**
     * @return string
     */
    public function getType(): ?string
    {
        return $this->type;
    }

    /**
     * @param string $type
     *
     * @return self
     */
    public function setType(?string $type): self
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return string
     */
    public function getUrl(): ?string
    {
        return $this->url;
    }

    /**
     * @param string $url
     *
     * @return self
     */
    public function setUrl(?string $url): self
    {
        $this->url = $url;
        return $this;
    }

    /**
     * @return bool
     */
    public function isRemote()
    {
        return $this->type === Media::REMOTE;
    }

    /**
     * @return bool
     */
    public function isLocal()
    {
        return $this->type === Media::LOCAL;
    }
}