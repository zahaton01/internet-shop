<?php

namespace App\Database\Domain\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * @ORM\MappedSuperclass()
 */
abstract class AbstractEntity
{
    /**
     * @var string
     *
     * @ORM\Id()
     * @ORM\Column(type="guid")
     *
     * @Assert\Uuid()
     * @Assert\NotBlank()
     * @Assert\NotNull()
     */
    protected $id;

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @param string $id
     *
     * @return self
     */
    public function setId(string $id): self
    {
        $this->id = $id;
        return $this;
    }

    public function __toString(): string
    {
        return $this->id;
    }
}
