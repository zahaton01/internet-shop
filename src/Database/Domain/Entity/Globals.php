<?php

namespace App\Database\Domain\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 *
 * @ORM\Entity()
 */
class Globals extends AbstractEntity
{
    public const DEFAULT_LOCALE                = 'default_locale';
    public const ENABLED_LOCALES               = 'enabled_locales';
    public const DATE_DISPLAYING_FORMAT        = 'date_displaying_format';
    public const HOST_ADDRESS                  = 'host_address';
    public const APP_NAME                      = 'app_name';
    public const APP_DESCRIPTION               = 'app_description';
    public const APP_CURRENCY                  = 'app_currency';

    public const CONTACT_PHONE                 = 'contact_phone';
    public const CONTACT_EMAIL                 = 'contact_email';
    public const CONTACT_VIBER                 = 'viber';
    public const CONTACT_TELEGRAM              = 'telegram';
    public const CONTACT_WHATS_UP              = 'whatsup';

    public const METRICS_YANDEX                = 'metrics_yandex';
    public const METRICS_GOOGLE                = 'metrics_google';
    public const METRICS_GOOGLE_SEARCH_CONSOLE = 'metrics_google_search_console';

    public const NOVA_POSHTA_IS_ENABLED        = 'nova_poshta_is_enabled';
    public const NOVA_POSHTA_HOST              = 'nova_poshta_host';
    public const NOVA_POSHTA_API_KEY           = 'nova_poshta_api_key';
    public const NOVA_POSHTA_PRICE             = 'nova_poshta_price';

    public const COURIER_PRICE                 = 'courier_price';

    public const LIQPAY_PUBLIC_KEY             = 'liqpay_public_key';
    public const LIQPAY_PRIVATE_KEY            = 'liqpay_private_key';
    public const LIQPAY_IS_ENABLED             = 'liqpay_is_enabled';
    public const LIQPAY_API_URL                = 'liqpay_api_url';
    public const LIQPAY_CHECKOUT_URL           = 'liqpay_checkout_url';

    /**
     * @var string
     *
     * @ORM\Column(type="string", name="globals_key", nullable=false)
     *
     * @Assert\NotBlank()
     */
    private $key;

    /**
     * @var string
     *
     * @ORM\Column(type="text")
     */
    private $value;

    public function __toString(): string
    {
        return $this->value;
    }

    /**
     * @return string
     */
    public function getKey(): ?string
    {
        return $this->key;
    }

    /**
     * @param string $key
     *
     * @return self
     */
    public function setKey(?string $key): self
    {
        $this->key = $key;
        return $this;
    }

    /**
     * @return string
     */
    public function getValue(): string
    {
        return $this->value;
    }

    /**
     * @param $value
     *
     * @return self
     */
    public function setValue(string $value): self
    {
        $this->value = $value;
        return $this;
    }

    /**
     * @param array $value
     *
     * @return $this
     */
    public function setArray(array $value): self
    {
        $this->value = json_encode($value);

        return $this;
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return json_decode($this->value, true);
    }

    /**
     * @return int
     */
    public function toInt(): int
    {
        return (int) $this->value;
    }

    /**
     * @return float
     */
    public function toFloat(): float
    {
        return (float) $this->value;
    }

    /**
     * @return bool
     */
    public function toBoolean(): bool
    {
        return $this->value === 'true' ? true : false;
    }
}
