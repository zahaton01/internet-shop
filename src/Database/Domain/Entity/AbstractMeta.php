<?php


namespace App\Database\Domain\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 *
 * @ORM\MappedSuperclass()
 */
abstract class AbstractMeta extends AbstractEntity
{
    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    protected $pageTitle;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    protected $metaTitle;

    /**
     * @var string
     *
     * @ORM\Column(type="text", nullable=true)
     */
    protected $metaDesc;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    protected $metaKeywords;

    /**
     * @return string
     */
    public function getPageTitle(): ?string
    {
        return $this->pageTitle;
    }

    /**
     * @param string $pageTitle
     *
     * @return self
     */
    public function setPageTitle(string $pageTitle): self
    {
        $this->pageTitle = $pageTitle;
        return $this;
    }

    /**
     * @return string
     */
    public function getMetaTitle(): ?string
    {
        return $this->metaTitle;
    }

    /**
     * @param string $metaTitle
     *
     * @return self
     */
    public function setMetaTitle(?string $metaTitle): self
    {
        $this->metaTitle = $metaTitle;
        return $this;
    }

    /**
     * @return string
     */
    public function getMetaDesc(): ?string
    {
        return $this->metaDesc;
    }

    /**
     * @param string $metaDesc
     *
     * @return self
     */
    public function setMetaDesc(?string $metaDesc): self
    {
        $this->metaDesc = $metaDesc;
        return $this;
    }

    /**
     * @return string
     */
    public function getMetaKeywords(): ?string
    {
        return $this->metaKeywords;
    }

    /**
     * @param string $metaKeywords
     *
     * @return self
     */
    public function setMetaKeywords(?string $metaKeywords): self
    {
        $this->metaKeywords = $metaKeywords;
        return $this;
    }
}
