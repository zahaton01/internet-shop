<?php

namespace App\Database\Domain\Factory\Product;

use App\Application\Util\Generator\SlugGenerator;
use App\Application\Util\Identifier;
use App\Database\Domain\Entity\Commerce\Product\Category\Meta\ProductCategoryMeta;
use App\Database\Domain\Entity\Commerce\Product\Category\ProductCategory;
use App\Database\Domain\Factory\EntityFactoryInterface;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 */
class ProductCategoryFactory implements EntityFactoryInterface
{
    /** @var SlugGenerator */
    private $slugGenerator;

    public function __construct(SlugGenerator $slugGenerator)
    {
        $this->slugGenerator = $slugGenerator;
    }

    public function basic(string $name): ProductCategory
    {
        $category = new ProductCategory();
        $category
            ->setId(Identifier::uuid())
            ->setMeta((new ProductCategoryMeta())->setId(Identifier::uuid()))
            ->setName($name)
            ->setSlug($this->slugGenerator->fromString($name, ProductCategory::class));

        return $category;
    }
}