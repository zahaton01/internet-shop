<?php

namespace App\Database\Domain\Factory\Product;

use App\Application\Core\ClassWrappers\DateTime;
use App\Application\Util\Generator\SlugGenerator;
use App\Application\Util\Identifier;
use App\Database\Domain\Entity\Commerce\Product\Media\ProductMedia;
use App\Database\Domain\Entity\Commerce\Product\Meta\ProductMeta;
use App\Database\Domain\Entity\Commerce\Product\Product;
use App\Database\Domain\Entity\Commerce\Product\ProductPromo;
use App\Database\Domain\Factory\EntityFactoryInterface;
use App\Database\Domain\Model\EntityState\Availability;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 */
class ProductFactory implements EntityFactoryInterface
{
    /** @var SlugGenerator  */
    private $slugGenerator;

    public function __construct(SlugGenerator $slugGenerator)
    {
        $this->slugGenerator = $slugGenerator;
    }

    public function basic(string $name): Product
    {
        $product = new Product();
        $promo   = new ProductPromo();
        $promo
            ->setId(Identifier::uuid())
            ->setIsSpecialStatus(false)
            ->setSaleStatus(false);

        $product
            ->setId(Identifier::uuid())
            ->setSlug($this->slugGenerator->withPrefix($name))
            ->setMedia((new ProductMedia())->setId(Identifier::uuid()))
            ->setIsVisible(true)
            ->setPromo($promo)
            ->setCreationDate(DateTime::now())
            ->setAvailability(Availability::AVAILABLE)
            ->setMeta((new ProductMeta())->setId(Identifier::uuid()));

        return $product;
    }
}