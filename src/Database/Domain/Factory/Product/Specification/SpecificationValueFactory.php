<?php

namespace App\Database\Domain\Factory\Product\Specification;

use App\Application\Util\Identifier;
use App\Database\Domain\Entity\Commerce\Product\Specification\Specification;
use App\Database\Domain\Entity\Commerce\Product\Specification\SpecificationValue;
use App\Database\Domain\Factory\EntityFactoryInterface;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 */
class SpecificationValueFactory implements EntityFactoryInterface
{
    public function basic(string $value, Specification $specification): SpecificationValue
    {
        $valueEntity = new SpecificationValue();
        $valueEntity
            ->setId(Identifier::uuid())
            ->setValue($value)
            ->setSpecification($specification);

        return $valueEntity;
    }
}