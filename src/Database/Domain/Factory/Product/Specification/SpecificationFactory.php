<?php

namespace App\Database\Domain\Factory\Product\Specification;

use App\Application\Util\Identifier;
use App\Database\Domain\Entity\Commerce\Product\Category\ProductCategory;
use App\Database\Domain\Entity\Commerce\Product\Specification\Specification;
use App\Database\Domain\Factory\EntityFactoryInterface;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 */
class SpecificationFactory implements EntityFactoryInterface
{
    public function basic(string $name, ProductCategory $category)
    {
        $specification = new Specification();
        $specification
            ->setId(Identifier::uuid())
            ->setIsEnabled(true)
            ->setProductCategory($category)
            ->setName($name);

        return $specification;
    }
}