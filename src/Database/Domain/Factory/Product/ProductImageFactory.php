<?php

namespace App\Database\Domain\Factory\Product;

use App\Application\Util\Identifier;
use App\Database\Domain\Entity\Commerce\Product\Media\ProductImage;
use App\Database\Domain\Entity\Commerce\Product\Media\ProductMedia;
use App\Database\Domain\Entity\Media;
use App\Database\Domain\Factory\EntityFactoryInterface;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 */
class ProductImageFactory implements EntityFactoryInterface
{
    public function remote(string $url, ProductMedia $media): ProductImage
    {
        $image = new ProductImage();
        $image
            ->setId(Identifier::uuid())
            ->setType(Media::REMOTE)
            ->setMedia($media)
            ->setUrl($url);

        return $image;
    }
}