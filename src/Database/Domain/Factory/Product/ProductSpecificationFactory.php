<?php

namespace App\Database\Domain\Factory\Product;

use App\Application\Util\Identifier;
use App\Database\Domain\Entity\Commerce\Product\Product;
use App\Database\Domain\Entity\Commerce\Product\Specification\ProductSpecification;
use App\Database\Domain\Entity\Commerce\Product\Specification\Specification;
use App\Database\Domain\Factory\EntityFactoryInterface;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 */
class ProductSpecificationFactory implements EntityFactoryInterface
{
    public function basic(Product $product, Specification $specification): ProductSpecification
    {
        $productSpecification = new ProductSpecification();
        $productSpecification
            ->setId(Identifier::uuid())
            ->setProduct($product)
            ->setSpecification($specification);

        return $productSpecification;
    }
}