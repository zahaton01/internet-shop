<?php

namespace App\Database\Domain\Factory\Order;

use App\Application\Core\ClassWrappers\DateTime;
use App\Application\Libs\Api\NovaPoshta\NovaPoshtaApi;
use App\Application\Model\Commerce\Cart\Cart;
use App\Application\Service\GlobalsService;
use App\Application\Util\Identifier;
use App\Database\Domain\Entity\Commerce\Order\Coupon;
use App\Database\Domain\Entity\Commerce\Order\Order;
use App\Database\Domain\Entity\Globals;
use App\Database\Domain\Factory\EntityFactoryInterface;
use App\Database\Domain\Operation\Manager;
use App\Database\Domain\ValueObject\Order\NovaPoshtaDeliveryData;
use Psr\Cache\InvalidArgumentException;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 */
class OrderFactory implements EntityFactoryInterface
{
    /** @var Manager  */
    private $manager;

    /** @var GlobalsService  */
    private $globals;

    /** @var SessionInterface */
    private $session;

    /** @var NovaPoshtaApi */
    private $novaPoshtaApi;

    public function __construct(
        Manager $manager,
        GlobalsService $globalsService,
        SessionInterface $session,
        NovaPoshtaApi $novaPoshtaApi
    ) {
        $this->manager       = $manager;
        $this->globals       = $globalsService;
        $this->session       = $session;
        $this->novaPoshtaApi = $novaPoshtaApi;
    }

    /**
     * @param Form $form
     *
     * @return Order
     *
     * @throws InvalidArgumentException
     */
    public function fromType(Form $form): Order
    {
        /** @var Order $order */
        $order = $form->getData();

        if ($order->isDeliveryNovaPoshta()) {
            $shipmentPrice = $this->globals->getByKey(Globals::NOVA_POSHTA_PRICE)->toInt();
            $novaPoshtaCityName      = $this->novaPoshtaApi->getCity($form['nova_city']->getData())[0]['Description'] ?? null;
            $novaPoshtaWarehouseName = $this->novaPoshtaApi->getWarehouse($form['nova_warehouse']->getData(), $form['nova_city']->getData())['Description'] ?? null;
            $novaPoshtaCityRef       = $form['nova_city']->getData();
            $novaPoshtaWarehouseRef  = $form['nova_warehouse']->getData();

            $novaPoshtaDeliveryData = (new NovaPoshtaDeliveryData())
                ->setCity($novaPoshtaCityName)
                ->setWarehouse($novaPoshtaWarehouseName)
                ->setCityRef($novaPoshtaCityRef)
                ->setWarehouseRef($novaPoshtaWarehouseRef);

            $order
                ->setShipmentPrice($shipmentPrice)
                ->setDeliveryData($novaPoshtaDeliveryData);
        }

        if ($order->isDeliveryCourier()) {
            $shipmentPrice = $this->globals->getByKey(Globals::COURIER_PRICE)->toInt();
            $order->setShipmentPrice($shipmentPrice);
        }

        if ($couponCode = $form['coupon']->getData()) {
            /** @var Coupon $coupon */
            $coupon = $this->manager->em()->getRepository(Coupon::class)->findOneBy(['code' => $couponCode]);

            if (null !== $coupon) {
                $order->setCoupon($coupon);
            }
        }

        return $order;
    }

    /**
     * @return Order
     */
    public function basic(): Order
    {
        return (new Order())
            ->setId(Identifier::uuid())
            ->setCreationDate(DateTime::now())
            ->setPaymentStatus(Order::PAYMENT_STATUS_AWAITING_PAYMENT)
            ->setStatus(Order::ORDER_STATUS_NOT_CONFIRMED)
            ->setShipmentPrice(0);
    }
}