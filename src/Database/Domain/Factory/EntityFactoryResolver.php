<?php

namespace App\Database\Domain\Factory;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 */
class EntityFactoryResolver
{
    /** @var EntityFactoryInterface[] */
    private $factories;

    public function __construct(iterable $factories)
    {
        $this->factories = $factories;
    }

    /**
     * @param string $className
     *
     * @return EntityFactoryInterface
     */
    public function get(string $className): EntityFactoryInterface
    {
        foreach ($this->factories as $factory) {
            if ($className === get_class($factory)) {
                return $factory;
            }
        }

        return null;
    }
}