<?php

namespace App\Database\Domain\Factory;

use App\Application\Core\ClassWrappers\DateTime;
use App\Application\Util\Identifier;
use App\Database\Domain\Entity\Log;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 */
class LogFactory
{
    /**
     * @param string $message
     * @param string $source
     * @param string $level
     * @param array $extras
     *
     * @return Log
     */
    public function createBasic(
        string $message,
        string $source,
        string $level,
        array $extras = []
    ): Log {
        $log = new Log();
        $log
            ->setId(Identifier::uuid())
            ->setMessage($message)
            ->setSource($source)
            ->setExtras($extras)
            ->setLevel($level)
            ->setCreationDate(DateTime::now());

        return $log;
    }
}