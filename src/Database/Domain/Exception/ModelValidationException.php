<?php

namespace App\Database\Domain\Exception;

use Symfony\Component\Validator\ConstraintViolationInterface;
use Symfony\Component\Validator\ConstraintViolationListInterface;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 */
class ModelValidationException extends \Exception
{
    /** @var ConstraintViolationInterface[] */
    private $errors = [];

    /**
     * @return ConstraintViolationInterface[]
     */
    public function getErrors(): array
    {
        return $this->errors;
    }

    /**
     * @param $errors
     *
     * @return self
     */
    public function setErrors($errors): self
    {
        if ($errors instanceof ConstraintViolationListInterface) {
            $this->addErrors($errors);
        }

        if (is_array($errors)) {
            foreach ($errors as $list) {
                $this->addErrors($list);
            }
        }

        if ($_ENV['APP_ENV'] === 'dev') {
            foreach ($this->errors as $error) {
                var_dump($error->getMessage(), $error->getPropertyPath());
            }
        }

        return $this;
    }

    /**
     * @param ConstraintViolationListInterface $errors
     *
     * @return $this
     */
    public function addErrors(ConstraintViolationListInterface $errors): self
    {
        foreach ($errors as $error) {
            $this->errors[] = $error;
        }

        return $this;
    }
}