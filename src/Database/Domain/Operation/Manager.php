<?php

namespace App\Database\Domain\Operation;

use App\Database\Domain\Exception\ModelValidationException;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 */
class Manager
{
    /** @var EntityManagerInterface  */
    private $em;

    /** @var ValidatorInterface */
    private $validator;

    public function __construct(EntityManagerInterface $em, ValidatorInterface $validator)
    {
        $this->em        = $em;
        $this->validator = $validator;
    }

    /**
     * @param      $content
     * @param bool $validate
     *
     * @return mixed
     *
     * @throws ModelValidationException
     */
    public function save($content, $validate = true)
    {
        if ($validate) {
            $this->validate($content);
        }

        if (is_iterable($content)) {
            foreach ($content as $item) {
                $this->em->persist($item);
            }

            $this->em->flush();

            return true;
        }

        $this->em->persist($content);
        $this->em->flush();

        return $content;
    }

    /**
     * @param $content
     *
     * @return null
     */
    public function remove($content)
    {
        if (is_iterable($content)) {
            foreach ($content as $item) {
                $this->em->remove($item);
            }

            $this->em->flush();

            return true;
        }

        $this->em->remove($content);
        $this->em->flush();

        return null;
    }

    /**
     * @return EntityManagerInterface
     */
    public function em(): EntityManagerInterface
    {
        return $this->em;
    }

    /**
     * @param $content
     *
     * @throws ModelValidationException
     */
    public function validate($content): void
    {
        $errors = [];

        if (is_iterable($content)) {
            foreach ($content as $entity) {
                $entityErrors = $this->validator->validate($entity);

                if (count($entityErrors) > 0) {
                    $errors[] = $entityErrors;
                }
            }
        } else {
            $errors = $this->validator->validate($content);
        }

        if (count($errors) > 0) {
            throw (new ModelValidationException('Model validation failed'))->setErrors($errors);
        }
    }
}