<?php

namespace App\Database\Domain\Model\EntityState;

use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 */
class Availability
{
    public const AVAILABLE       = 'available';
    public const NOT_AVAILABLE   = 'not_available';
    public const UNDER_THE_ORDER = 'under_the_order';
    public const AWAITING        = 'awaiting';

    /** @var TranslatorInterface  */
    private $translator;

    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    /**
     * @return array
     */
    public static function allAsChoices()
    {
        return [
            static::AVAILABLE       => static::AVAILABLE,
            static::NOT_AVAILABLE   => static::NOT_AVAILABLE,
            static::UNDER_THE_ORDER => static::UNDER_THE_ORDER,
            static::AWAITING        => static::AWAITING
        ];
    }

    /**
     * @return array
     */
    public function allAsTranslatedChoices()
    {
        return [
            $this->translator->trans(self::AVAILABLE)       => self::AVAILABLE,
            $this->translator->trans(self::NOT_AVAILABLE)   => self::NOT_AVAILABLE,
            $this->translator->trans(self::UNDER_THE_ORDER) => self::UNDER_THE_ORDER,
            $this->translator->trans(self::AWAITING)        => self::AWAITING
        ];
    }
}
