<?php

namespace App\Database\Domain\Model\EntityState\Order;

use App\Database\Domain\Entity\Commerce\Order\Order;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 */
class OrderPaymentStatus
{
    /** @var TranslatorInterface  */
    private $translator;

    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    /**
     * @return array
     */
    public function allAsTranslatedChoices()
    {
        return [
            $this->translator->trans(Order::PAYMENT_STATUS_AWAITING_PAYMENT) => Order::PAYMENT_STATUS_AWAITING_PAYMENT,
            $this->translator->trans(Order::PAYMENT_STATUS_PAID)             => Order::PAYMENT_STATUS_PAID
        ];
    }
}