<?php

namespace App\Database\Domain\Model\EntityState\Order;

use App\Database\Domain\Entity\Commerce\Order\Order;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 */
class OrderStatus
{
    /** @var TranslatorInterface  */
    private $translator;

    /**
     * Availability constructor.
     * @param TranslatorInterface $translator
     */
    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    /**
     * @return array
     */
    public function allAsTranslatedChoices()
    {
        return [
            $this->translator->trans(Order::ORDER_STATUS_CANCELLED)     => Order::ORDER_STATUS_CANCELLED,
            $this->translator->trans(Order::ORDER_STATUS_COMPLETED)     => Order::ORDER_STATUS_COMPLETED,
            $this->translator->trans(Order::ORDER_STATUS_CONFIRMED)     => Order::ORDER_STATUS_CONFIRMED,
            $this->translator->trans(Order::ORDER_STATUS_NOT_CONFIRMED) => Order::ORDER_STATUS_NOT_CONFIRMED,
            $this->translator->trans(Order::ORDER_STATUS_ON_DELIVER)    => Order::ORDER_STATUS_ON_DELIVER,
        ];
    }
}