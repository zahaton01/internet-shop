<?php

namespace App\Database\Domain\Model\Query\Params;

use App\Database\Domain\Model\Query\AbstractQueryFilter;

/**
 * @author Anton Zakharuk <anton.zakharuk@cosmonova.net>
 */
class QueryFilter extends AbstractQueryFilter
{
    /**
     * @return string|null
     */
    public function formatted(): ?string
    {
        return mb_strtolower(trim($this->value));
    }
}