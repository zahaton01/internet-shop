<?php

namespace App\Database\Domain\Model\Query\Product;

use App\Database\Domain\Entity\Commerce\Product\Specification\Specification;
use App\Database\Domain\Entity\Commerce\Product\Specification\SpecificationValue;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 */
class ProductSpecificationFilter
{
    /** @var Specification|null */
    private $specification = null;

    /** @var SpecificationValue|null */
    private $value         = null;

    /**
     * @return Specification|null
     */
    public function getSpecification(): ?Specification
    {
        return $this->specification;
    }

    /**
     * @param Specification|null $specification
     *
     * @return self
     */
    public function setSpecification(?Specification $specification): self
    {
        $this->specification = $specification;
        return $this;
    }

    /**
     * @return SpecificationValue|null
     */
    public function getValue(): ?SpecificationValue
    {
        return $this->value;
    }

    /**
     * @param SpecificationValue|null $value
     *
     * @return self
     */
    public function setValue(?SpecificationValue $value): self
    {
        $this->value = $value;
        return $this;
    }

    /**
     * @return bool
     */
    public function isValid(): bool
    {
        return $this->specification !== null && $this->value !== null;
    }
}
