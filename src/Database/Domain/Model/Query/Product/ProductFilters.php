<?php

namespace App\Database\Domain\Model\Query\Product;

use App\Database\Domain\Entity\Commerce\Product\Category\ProductCategory;
use App\Database\Domain\Entity\Commerce\Product\SubCategory\ProductSubCategory;
use App\Database\Domain\Model\Query\AbstractQuery;
use App\Database\Domain\Model\Query\AbstractQueryFilter;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 */
class ProductFilters extends AbstractQuery
{
    /** @var ProductCategory|null */
    private $category       = null;

    /** @var ProductSubCategory|null */
    private $subCategory    = null;

    /** @var ProductSpecificationFilter[] */
    private $specifications = [];

    /**
     * @return ProductCategory|null
     */
    public function getCategory(): ?ProductCategory
    {
        return $this->category;
    }

    /**
     * @param ProductCategory|null $category
     *
     * @return $this
     */
    public function setCategory(?ProductCategory $category): self
    {
        $this->category = $category;
        return $this;
    }

    /**
     * @return ProductSubCategory|null
     */
    public function getSubCategory(): ?ProductSubCategory
    {
        return $this->subCategory;
    }

    /**
     * @param ProductSubCategory|null $subCategory
     *
     * @return $this
     */
    public function setSubCategory(?ProductSubCategory $subCategory): self
    {
        $this->subCategory = $subCategory;
        return $this;
    }

    /**
     * @return ProductSpecificationFilter[]
     */
    public function getSpecifications(): ?array
    {
        return $this->specifications;
    }

    /**
     * @param ProductSpecificationFilter[] $specifications
     *
     * @return self
     */
    public function setSpecifications(?array $specifications): self
    {
        $this->specifications = $specifications;
        return $this;
    }

    /**
     * @param ProductSpecificationFilter $specification
     *
     * @return $this
     */
    public function addSpecification(ProductSpecificationFilter $specification): self
    {
        $this->specifications[] = $specification;
        return $this;
    }

    /**
     * @return bool
     */
    public function hasCategory(): bool
    {
        return $this->category !== null;
    }

    /**
     * @return bool
     */
    public function hasSubCategory():bool
    {
        return $this->subCategory !== null;
    }

    /**
     * @return array
     */
    public function getSpecificationIds()
    {
        $res = [];
        foreach ($this->getSpecifications() as $specification) {
            if (!$specification->isValid()) {
                continue;
            }

            $res[] = $specification->getSpecification()->getId();
        }

        return $res;
    }

    /**
     * @return array
     */
    public function getSpecificationValueIds()
    {
        $res = [];
        foreach ($this->getSpecifications() as $specification) {
            if (!$specification->isValid()) {
                continue;
            }

            $res[] = $specification->getValue()->getId();
        }

        return $res;
    }

    /**
     * @return bool
     */
    public function hasSpecifications()
    {
        return count($this->getSpecificationValueIds()) > 0 && count($this->getSpecificationIds()) > 0;
    }

    /**
     * @param $specification
     *
     * @return string|null
     */
    public function getSpecificationValue($specification)
    {
        foreach ($this->getSpecifications() as $productSpecification) {
            if ($productSpecification->getSpecification()->getId() === $specification) {
                return $productSpecification->getValue()->getId();
            }
        }

        return null;
    }

    /**
     * @param string $filterName
     *
     * @return AbstractQueryFilter|null
     */
    public function getFilter(string $filterName): ?AbstractQueryFilter
    {
        if (!$this->hasFilter($filterName)) {
            return null;
        }

        return new ProductFilter($this->filters[$filterName]);
    }
}
