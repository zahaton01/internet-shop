<?php

namespace App\Database\Domain\Model\Query\Product;

use App\Database\Domain\Model\Query\AbstractQueryFilter;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 */
class ProductFilter extends AbstractQueryFilter
{

}
