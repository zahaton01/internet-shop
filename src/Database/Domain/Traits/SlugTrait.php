<?php

namespace App\Database\Domain\Traits;

use App\Database\Domain\Validation\Constraints\Slug\UniqueSlug;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 */
trait SlugTrait
{
    /**
     * @UniqueSlug()
     *
     * @var string
     *
     * @ORM\Column(type="string", nullable=false, unique=true)
     */
    private $slug;

    /**
     * @return string
     */
    public function getSlug(): ?string
    {
        return $this->slug;
    }

    /**
     * @param string $slug
     *
     * @return self
     */
    public function setSlug(string $slug): self
    {
        $this->slug = $slug;
        return $this;
    }
}