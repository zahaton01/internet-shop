<?php

namespace App\Database\Domain\Traits;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 */
trait LocaleTrait
{
    /**
     * @var string
     *
     * @ORM\Column(name="locale", type="string")
     *
     * @Assert\NotBlank()
     * @Assert\NotNull()
     * @Assert\Choice(choices={
     *    App\Application\Model\AppEnv\Locale::EN,
     *    App\Application\Model\AppEnv\Locale::RU,
     *    App\Application\Model\AppEnv\Locale::UA,
     *  }, message="Invalid locale")
     */
    private $locale;

    /**
     * @return string
     */
    public function getLocale(): ?string
    {
        return $this->locale;
    }

    /**
     * @param string $locale
     *
     * @return self
     */
    public function setLocale(?string $locale): self
    {
        $this->locale = $locale;
        return $this;
    }
}
