<?php

namespace App\Database\Domain\Validation\Constraints\Slug;

use Symfony\Component\Validator\Constraint;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 *
 * @Annotation
 *
 * @Target({"PROPERTY", "METHOD", "ANNOTATION"})
 */
class UniqueSlug extends Constraint
{
    public $message = 'Specified link must be unique';
}