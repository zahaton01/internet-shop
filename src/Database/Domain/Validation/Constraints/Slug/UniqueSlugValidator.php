<?php

namespace App\Database\Domain\Validation\Constraints\Slug;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Translation\Translator;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 */
class UniqueSlugValidator extends ConstraintValidator
{
    /** @var EntityManagerInterface  */
    private $em;

    /** @var Translator */
    private $translator;

    public function __construct(EntityManagerInterface $em, TranslatorInterface $translator)
    {
        $this->em = $em;
        $this->translator = $translator;
    }

    /**
     * @param mixed $value
     * @param Constraint $constraint
     *
     * @throws \Exception
     */
    public function validate($value, Constraint $constraint)
    {
        if (!$constraint instanceof UniqueSlug) {
            throw new \Exception('Unexpected UniqueSlug constraint');
        }

        $currentObject = $this->context->getObject();
        $entity = $this->em->getRepository(get_class($currentObject))->findOneBy(['slug' => $value]);

        if (null !== $entity && $entity->getId() !== $currentObject->getId()) {
            $this->context->buildViolation($this->translator->trans($constraint->message))
                ->addViolation();
        }
    }
}