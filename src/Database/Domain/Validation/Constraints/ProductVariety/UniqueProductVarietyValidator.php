<?php

namespace App\Database\Domain\Validation\Constraints\ProductVariety;

use App\Database\Domain\Entity\Commerce\Product\Variety\ProductVariety;
use App\Database\Domain\Validation\Constraints\Slug\UniqueSlug;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Translation\Translator;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 */
class UniqueProductVarietyValidator extends ConstraintValidator
{
    /** @var EntityManagerInterface  */
    private $em;

    /** @var Translator */
    private $translator;

    /** @var Request */
    private $request;

    public function __construct(EntityManagerInterface $em, TranslatorInterface $translator, RequestStack $requestStack)
    {
        $this->em         = $em;
        $this->translator = $translator;
        $this->request    = $requestStack->getCurrentRequest();
    }

    /**
     * @param mixed $value
     * @param Constraint $constraint
     *
     * @throws \Exception
     */
    public function validate($value, Constraint $constraint)
    {
        if (!$constraint instanceof UniqueProductVariety) {
            throw new \Exception('Unexpected UniqueProductVariety constraint');
        }

        /** @var ProductVariety $currentObject */
        $currentObject  = $this->context->getObject();
        $productVariety = $this->em->getRepository(ProductVariety::class)
            ->findOneBy(['variety' => $currentObject->getVariety()->getId(), 'product' => $currentObject->getProduct()->getId()]);

        if (null !== $productVariety &&
            !$this->request->isMethod('PUT') &&
            !$this->request->isMethod('PATCH')
        ) {
            $this->context->buildViolation($this->translator->trans($constraint->message))
                ->addViolation();
        }
    }
}