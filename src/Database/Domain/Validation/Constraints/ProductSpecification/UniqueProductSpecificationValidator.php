<?php

namespace App\Database\Domain\Validation\Constraints\ProductSpecification;

use App\Database\Domain\Entity\Commerce\Product\Specification\ProductSpecification;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Translation\Translator;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 */
class UniqueProductSpecificationValidator extends ConstraintValidator
{
    /** @var EntityManagerInterface  */
    private $em;

    /** @var Translator */
    private $translator;

    /** @var Request */
    private $request;

    public function __construct(EntityManagerInterface $em, TranslatorInterface $translator, RequestStack $requestStack)
    {
        $this->em         = $em;
        $this->translator = $translator;
        $this->request    = $requestStack->getCurrentRequest();
    }

    /**
     * @param mixed $value
     * @param Constraint $constraint
     *
     * @throws \Exception
     */
    public function validate($value, Constraint $constraint)
    {
        if (!$constraint instanceof UniqueProductSpecification) {
            throw new \Exception('Unexpected UniqueProductSpecification constraint');
        }

        /** @var ProductSpecification $currentObject */
        $currentObject  = $this->context->getObject();
        $productSpecification = $this->em->getRepository(ProductSpecification::class)
            ->findOneBy(['specification' => $currentObject->getSpecification()->getId(), 'product' => $currentObject->getProduct()->getId()]);

        if (null !== $productSpecification &&
            !$this->request->isMethod('PUT') &&
            !$this->request->isMethod('PATCH')
        ) {
            $this->context->buildViolation($this->translator->trans($constraint->message))
                ->addViolation();
        }
    }
}