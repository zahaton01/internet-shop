<?php

namespace App\Database\Domain\Validation\Constraints\ProductSpecification;

use Symfony\Component\Validator\Constraint;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 *
 * @Annotation
 *
 * @Target({"CLASS"})
 */
class UniqueProductSpecification extends Constraint
{
    public $message = 'You have already added such specification to product';

    public function getTargets()
    {
        return [self::PROPERTY_CONSTRAINT, self::CLASS_CONSTRAINT];
    }
}