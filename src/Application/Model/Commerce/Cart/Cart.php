<?php

namespace App\Application\Model\Commerce\Cart;

use App\Database\Domain\Entity\Commerce\Order\Coupon;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 */
class Cart
{
    /** @var CartProduct[] */
    private $cartProducts  = [];
    /** @var float */
    private $totalPrice    = 0;
    /** @var float */
    private $shipmentPrice = null;
    /** @var Coupon */
    private $coupon        = null;

    /**
     * @return CartProduct[]
     */
    public function getCartProducts(): ?array
    {
        return $this->cartProducts;
    }

    /**
     * @param CartProduct[] $products
     *
     * @return self
     */
    public function setCartProducts(?array $products): self
    {
        $this->cartProducts = $products;
        return $this;
    }

    /**
     * @param CartProduct $cartProduct
     *
     * @return $this
     */
    public function addCartProduct(CartProduct $cartProduct)
    {
        $this->cartProducts[] = $cartProduct;
        return $this;
    }

    /**
     * @return float
     */
    public function getTotalPrice(): ?float
    {
        return $this->totalPrice;
    }

    /**
     * @param float $totalPrice
     *
     * @return self
     */
    public function setTotalPrice(?float $totalPrice): self
    {
        $this->totalPrice = $totalPrice;
        return $this;
    }

    /**
     * @return float
     */
    public function getShipmentPrice(): ?float
    {
        return $this->shipmentPrice;
    }

    /**
     * @param float $shipmentPrice
     *
     * @return self
     */
    public function setShipmentPrice(?float $shipmentPrice): self
    {
        $this->shipmentPrice = $shipmentPrice;
        return $this;
    }

    /**
     * @return Coupon
     */
    public function getCoupon(): ?Coupon
    {
        return $this->coupon;
    }

    /**
     * @param Coupon $coupon
     *
     * @return self
     */
    public function setCoupon(?Coupon $coupon): self
    {
        $this->coupon = $coupon;
        return $this;
    }

    /**
     * @return float
     */
    public function getTotalPriceWithCoupon(): ?float
    {
        if (null !== $this->coupon) {
            if ($this->coupon->isPercent()) {
                return $this->totalPrice * $this->coupon->getDiscountPercent();
            }

            if ($this->coupon->isFixed()) {
                $calculated = $this->totalPrice - $this->coupon->getDiscountFixed();
                return $calculated < 0 ? 0.01 : $calculated;
            }
        }

        return $this->totalPrice;
    }

    /**
     * @return float
     */
    public function getTotalPriceWithShipment(): ?float
    {
        return $this->totalPrice + $this->shipmentPrice;
    }

    /**
     * @return float
     */
    public function getTotalPriceWithShipmentAndCoupon(): ?float
    {
        return $this->getTotalPriceWithCoupon() + $this->shipmentPrice;
    }

    /**
     * @return bool
     */
    public function isEmpty()
    {
        return count($this->products) === 0;
    }
}