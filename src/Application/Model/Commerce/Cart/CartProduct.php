<?php

namespace App\Application\Model\Commerce\Cart;

use App\Database\Domain\Entity\Commerce\Product\Product;
use App\Database\Domain\Entity\Commerce\Product\Variety\Variety;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 */
class CartProduct
{
    /** @var integer */
    private $quantity;

    /** @var float */
    private $totalPrice;

    /** @var Product */
    private $relatedProduct;

    /** @var CartProductVariety[] */
    private $varieties = [];

    /**
     * @return int
     */
    public function getQuantity(): ?int
    {
        return $this->quantity;
    }

    /**
     * @param int $quantity
     *
     * @return self
     */
    public function setQuantity(?int $quantity): self
    {
        $this->quantity = $quantity;
        return $this;
    }

    /**
     * @return float
     */
    public function getTotalPrice(): ?float
    {
        return $this->totalPrice;
    }

    /**
     * @param float $totalPrice
     *
     * @return self
     */
    public function setTotalPrice(?float $totalPrice): self
    {
        $this->totalPrice = $totalPrice;
        return $this;
    }

    /**
     * @return Product
     */
    public function getRelatedProduct(): ?Product
    {
        return $this->relatedProduct;
    }

    /**
     * @param Product $relatedProduct
     *
     * @return self
     */
    public function setRelatedProduct(?Product $relatedProduct): self
    {
        $this->relatedProduct = $relatedProduct;
        return $this;
    }

    /**
     * @return CartProductVariety[]
     */
    public function getVarieties(): ?array
    {
        return $this->varieties;
    }

    /**
     * @param array $varieties
     *
     * @return self
     */
    public function setVarieties(?array $varieties): self
    {
        $this->varieties = $varieties;
        return $this;
    }

    /**
     * @param CartProductVariety $variety
     * @return $this
     */
    public function addVariety(CartProductVariety $variety)
    {
        $this->varieties[] = $variety;
        return $this;
    }
}