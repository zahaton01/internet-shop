<?php

namespace App\Application\Model\Commerce\Cart;

use App\Database\Domain\Entity\Commerce\Product\Variety\VarietyValue;
use App\Database\Domain\Entity\Commerce\Product\Variety\Variety;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 */
class CartProductVariety
{
    /** @var Variety */
    private $relatedVariety;

    /** @var VarietyValue */
    private $relatedValue;

    /**
     * @return Variety
     */
    public function getRelatedVariety(): ?Variety
    {
        return $this->relatedVariety;
    }

    /**
     * @param Variety $relatedVariety
     *
     * @return self
     */
    public function setRelatedVariety(?Variety $relatedVariety): self
    {
        $this->relatedVariety = $relatedVariety;
        return $this;
    }

    /**
     * @return VarietyValue
     */
    public function getRelatedValue(): ?VarietyValue
    {
        return $this->relatedValue;
    }

    /**
     * @param VarietyValue $relatedValue
     *
     * @return self
     */
    public function setRelatedValue(?VarietyValue $relatedValue): self
    {
        $this->relatedValue = $relatedValue;
        return $this;
    }
}