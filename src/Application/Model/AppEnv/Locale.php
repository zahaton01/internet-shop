<?php

namespace App\Application\Model\AppEnv;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 */
class Locale
{
    public const EN = 'en';
    public const RU = 'ru';
    public const UA = 'ua';

    /**
     * @return array
     */
    public static function allAsChoices(): array
    {
        return [
            strtoupper(static::EN) => static::EN,
            strtoupper(static::RU) => static::RU,
            strtoupper(static::UA) => static::UA
        ];
    }

    /**
     * @return array
     */
    public static function all(): array
    {
        return [
            static::EN,
            static::RU,
            static::UA
        ];
    }
}
