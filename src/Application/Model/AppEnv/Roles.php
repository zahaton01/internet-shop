<?php

namespace App\Application\Model\AppEnv;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 */
class Roles
{
    public const ADMIN = 'ROLE_ADMIN';
    public const USER  = 'ROLE_USER';

    /**
     * @return array
     */
    public static function allAsChoices(): array
    {
        return [
            static::ADMIN => static::ADMIN,
            static::USER  => static::USER
        ];
    }

    /**
     * @return array
     */
    public static function all(): array
    {
        return [
            static::ADMIN,
            static::USER
        ];
    }
}
