<?php

namespace App\Application\Model\AppEnv;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 */
class Currency
{
    public const USD = 'USD';
    public const UAH = 'UAH';
    public const EUR = 'EUR';
    public const RUB = 'RUB';

    /**
     * @return array
     */
    public static function all()
    {
        return [
            self::EUR => self::EUR,
            self::RUB => self::RUB,
            self::USD => self::USD,
            self::UAH => self::UAH
        ];
    }
}
