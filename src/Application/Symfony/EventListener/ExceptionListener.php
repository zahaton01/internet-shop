<?php

namespace App\Application\Symfony\EventListener;

use App\Application\Logs\InternalDatabase\Logger\AdminInternalDatabaseLogger;
use App\Application\Logs\InternalDatabase\Logger\GlobalInternalDatabaseLogger;
use App\Application\Util\Router\RouterUtil;
use App\Database\Domain\Exception\ModelValidationException;
use Doctrine\ORM\ORMException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Router;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Translation\Translator;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 */
class ExceptionListener
{
    /** @var Router  */
    private $router;

    /** @var SessionInterface */
    private $session;

    /** @var Translator */
    private $translator;

    /** @var AdminInternalDatabaseLogger */
    private $adminLogger;

    /** @var GlobalInternalDatabaseLogger */
    private $globalLogger;

    /** @var string */
    private $locale;

    public function __construct(
        RouterInterface $router,
        SessionInterface $session,
        TranslatorInterface $translator,
        AdminInternalDatabaseLogger $adminLogger,
        GlobalInternalDatabaseLogger $globalLogger
    ) {
        $this->router       = $router;
        $this->session      = $session;
        $this->translator   = $translator;
        $this->adminLogger  = $adminLogger;
        $this->globalLogger = $globalLogger;
        $this->locale       = $session->get('_locale', 'en');
    }

    /**
     * @param ExceptionEvent $event
     */
    public function onKernelException(ExceptionEvent $event): void
    {
        // Disabling listener in dev env (but not in ajax requests) to get detailed error messages
        if ($_ENV['APP_ENV'] === 'dev' && !$this->isAjaxException($event)) return;

        if ($event->getThrowable() instanceof HttpExceptionInterface) {
            if (RouterUtil::doesRouteExistWithoutLocale(
                $this->router->getRouteCollection(), $event->getRequest()->getPathInfo())) {
                return; // if user requests /admin/dashboard another listener will automatically redirect him to /{_locale}/admin/dashboard
                // but /admin/dashboard causes RouteListener 404 exception which is handled by this listener and returns 404
                // so wee need to disable it in such specific case (redirect listener priority is 15 but RouteListener priority is 32)
            }
        }

        if ($this->isAdminException($event)) {
            $this->handleAdminException($event);
        }

        if ($this->isAjaxException($event)) {
            $this->handleAjaxException($event);
        }

        if (!$this->isAjaxException($event) && !$this->isAdminException($event)) {
            $this->handleException($event);
        }
    }

    /**
     * @param ExceptionEvent $event
     */
    private function handleAjaxException(ExceptionEvent $event): void
    {
        $exception = $event->getThrowable();
        $response  = new JsonResponse([$exception->getMessage()], Response::HTTP_INTERNAL_SERVER_ERROR);

        if ($exception instanceof NotFoundHttpException) {
            $response = new JsonResponse([$exception->getMessage()], Response::HTTP_NOT_FOUND);
        }

        if ($exception instanceof BadRequestHttpException) {
            $response = new JsonResponse([$exception->getMessage()], Response::HTTP_BAD_REQUEST);
        }

        if ($exception instanceof ModelValidationException) {
            $responseData = ['errors' => []];

            foreach ($exception->getErrors() as $error) {
                $responseData['errors'][] = [
                    'message' => $error->getMessage(),
                    'pointer' => $error->getPropertyPath()
                ];
            }

            $response = new JsonResponse($responseData, Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        $event->setResponse($response);
    }

    /**
     * @param ExceptionEvent $event
     */
    private function handleAdminException(ExceptionEvent $event): void
    {
        $exception = $event->getThrowable();

        if ($exception instanceof HttpExceptionInterface) {
            $code    = Response::HTTP_NOT_FOUND;
            $message = $this->translator->trans("Resource was not found!") . " {$exception->getMessage()}";
        } else {
            $code    = Response::HTTP_INTERNAL_SERVER_ERROR;
            $message = $this->translator->trans("Oops... Something went wrong...") . " {$exception->getMessage()}";

            try {
                $this->adminLogger->error($exception->getMessage());
            } catch (ORMException | ModelValidationException $e) { // EntityManager is closed or log is incorrect
                //
            }
        }

        $this->session->set('error.code', $code);
        $this->session->set('error.message', $message);

        $response = new RedirectResponse($this->router->generate('admin_error', ['_locale' => $this->locale]));

        $event->setResponse($response);
    }

    /**
     * @param ExceptionEvent $event
     */
    private function handleException(ExceptionEvent $event): void
    {
        $exception = $event->getThrowable();

        if ($exception instanceof HttpExceptionInterface) {
            $code    = Response::HTTP_NOT_FOUND;
            $message = $this->translator->trans("Resource was not found!") . " {$exception->getMessage()}";
        } else {
            $code    = Response::HTTP_INTERNAL_SERVER_ERROR;
            $message = $this->translator->trans("Oops... Something went wrong...") . " {$exception->getMessage()}";

            try {
                $this->globalLogger->error($exception->getMessage());
            } catch (ORMException | ModelValidationException $e) { // EntityManager is closed or log is incorrect
                //
            }
        }

        $this->session->set('error.code', $code);
        $this->session->set('error.message', $message);

        $response = new RedirectResponse($this->router->generate('site_error', ['_locale' => $this->locale]));

        $event->setResponse($response);
    }

    /**
     * @param ExceptionEvent $event
     *
     * @return bool
     */
    private function isAdminException(ExceptionEvent $event)
    {
        return strpos($event->getRequest()->getRequestUri(), 'admin') !== false;
    }

    /**
     * @param ExceptionEvent $event
     *
     * @return bool
     */
    private function isAjaxException(ExceptionEvent $event)
    {
        return strpos($event->getRequest()->getRequestUri(), 'ajax') !== false;
    }
}