<?php

namespace App\Application\Symfony\EventSubscriber;

use App\Application\Service\GlobalsService;
use App\Application\Util\Router\RouterUtil;
use App\Database\Domain\Entity\Globals;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Routing\RouteCollection;
use Symfony\Component\Routing\RouterInterface;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 */
class LocaleRewriteSubscriber implements EventSubscriberInterface
{
    /** @var RouterInterface  */
    private $router;

    public function __construct(RouterInterface $router)
    {
        $this->router = $router;
    }

    /**
     * @param RequestEvent $event
     */
    public function onKernelRequest(RequestEvent $event)
    {
        $request     = $event->getRequest();
        $path        = $request->getPathInfo();
        $routeExists = RouterUtil::doesRouteExistWithoutLocale($this->router->getRouteCollection(), $path);

        if($routeExists) {
            $event->setResponse(new RedirectResponse("/{$request->getLocale()}$path"));
        }
    }

    /**
     * @return array
     */
    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::REQUEST => [
                ['onKernelRequest', 15] // before default locale listener
            ]
        ];
    }
}