<?php

namespace App\Application\Symfony\EventSubscriber;

use App\Application\Service\GlobalsService;
use App\Database\Domain\Entity\Globals;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 */
class AppLocaleSubscriber implements EventSubscriberInterface
{
    /** @var string|null  */
    private $defaultLocale;

    /** @var array|null  */
    private $supportedLocales;

    public function __construct(GlobalsService $globalsService)
    {
        $this->defaultLocale    = (string) $globalsService->getByKey(Globals::DEFAULT_LOCALE);
        $this->supportedLocales = $globalsService->getByKey(Globals::ENABLED_LOCALES)->toArray();
    }

    /**
     * @param RequestEvent $event
     *
     * @return RedirectResponse|void
     */
    public function onKernelRequest(RequestEvent $event)
    {
        $request = $event->getRequest();

        if ($locale = $request->attributes->get('_locale')) {
            if (!$this->isLocaleSupported($locale)) {
                $locale = $this->getPreferred($request);
            }

            $request->getSession()->set('_locale', $locale);
        } else {
            $request->setLocale($request->getSession()->get('_locale', $this->defaultLocale));
        }
    }

    /**
     * @return array
     */
    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::REQUEST => [['onKernelRequest', 17]],
        ];
    }

    /**
     * @param $locale
     *
     * @return bool
     */
    private function isLocaleSupported($locale)
    {
        return in_array($locale, $this->supportedLocales);
    }

    /**
     * @param Request $request
     *
     * @return string
     */
    private function getPreferred(Request $request): string
    {
        $preferred       = $request->getPreferredLanguage();
        $preferredLocale = explode('_', $preferred)[0] ?? '';

        if (!$this->isLocaleSupported($preferredLocale)) {
            return $this->defaultLocale;
        }

        return $preferredLocale;
    }
}