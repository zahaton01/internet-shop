<?php

namespace App\Application\Service;

use App\Application\Libs\Component\CacheAdapter\CacheAdapter;
use App\Database\Domain\Entity\Globals;
use App\Database\Domain\Exception\ModelValidationException;
use App\Database\Domain\Operation\Manager;
use Psr\Cache\InvalidArgumentException;
use Symfony\Contracts\Cache\ItemInterface;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 */
class GlobalsService
{
    /** @var Manager  */
    private $manager;

    /** @var CacheAdapter */
    private $cacheAdapter;

    public function __construct(Manager $manager, CacheAdapter $cacheAdapter)
    {
        $this->manager      = $manager;
        $this->cacheAdapter = $cacheAdapter;
    }

    /**
     * @param string $key
     *
     * @return Globals|null
     *
     * @throws InvalidArgumentException
     */
    public function getByKey(string $key): ?Globals
    {
        return $this->cacheAdapter->cache()->get($key, function (ItemInterface $item) use ($key) {
            $item->expiresAfter(CacheAdapter::EXPIRES_DAY);
            $item->tag(CacheAdapter::TAG_GLOBALS);

            return $this->manager->em()->getRepository(Globals::class)->findOneBy(['key' => $key]);
        }, 1.0);
    }

    /**
     * @param $value
     * @param string $key
     *
     * @return Globals
     *
     * @throws InvalidArgumentException
     * @throws ModelValidationException
     */
    public function setByKey($value, string $key): Globals
    {
        /** @var Globals $globals */
        $globals = $this->manager->em()->getRepository(Globals::class)->findOneBy(['key' => $key]);

        if (is_array($value)) {
            $globals->setArray($value);
        } else {
            $globals->setValue($value);
        }

        $this->manager->save($globals);
        $this->cacheAdapter->cache()->invalidateTags([CacheAdapter::TAG_GLOBALS]);

        return $globals;
    }
}
