<?php

namespace App\Application\Service;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 */
class TranslationService
{
    /** @var Request|null  */
    private $request;

    /**
     * TranslationService constructor.
     * @param RequestStack $requestStack
     */
    public function __construct(RequestStack $requestStack)
    {
        $this->request = $requestStack->getCurrentRequest();
    }

    /**
     * @param $entity
     * @param string $property
     *
     * @return mixed
     */
    public function trans($entity, string $property)
    {
        if (!$this->hasTranslations($entity)) {
            return $entity->{$this->retrieveGetMethod($property)}(); // returning original value
        }

        foreach ($entity->getTranslations() as $translation) {
            if ($this->request->getLocale() === $translation->getLocale()) {
                return $translation->{$this->retrieveGetMethod($property)}() ?: $entity->{$this->retrieveGetMethod($property)}() ; // returning translation if not empty
            }
        }

        return $entity->{$this->retrieveGetMethod($property)}(); // returning original value
    }

    /**
     * @param $entity
     *
     * @return bool
     */
    private function hasTranslations($entity)
    {
        return method_exists($entity, 'getTranslations');
    }

    /**
     * @param string $property
     *
     * @return string
     */
    private function retrieveGetMethod(string $property)
    {
        return 'get' . ucfirst($property);
    }
}