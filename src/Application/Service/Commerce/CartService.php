<?php

namespace App\Application\Service\Commerce;

use App\Database\Domain\Entity\Commerce\Order\Order;
use App\Database\Domain\Entity\Commerce\Product\Product;
use App\Database\Domain\Entity\Commerce\Product\Variety\VarietyValue;
use App\Database\Domain\Entity\Commerce\Product\Variety\Variety;
use App\Application\Model\Commerce\Cart\Cart;
use App\Application\Model\Commerce\Cart\CartProduct;
use App\Application\Model\Commerce\Cart\CartProductVariety;
use App\Application\Service\GlobalsService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 */
class CartService
{
    /** @var EntityManagerInterface  */
    private $em;

    /** @var GlobalsService  */
    private $globalsService;

    /** @var SessionInterface  */
    private $session;

    public function __construct(EntityManagerInterface $em, GlobalsService $globalsService, SessionInterface $session)
    {
        $this->em             = $em;
        $this->globalsService = $globalsService;
        $this->session        = $session;
    }

    /**
     * @param Request $request
     *
     * @return bool
     */
    public function add(Request $request): bool
    {
        $cart = $this->session->get('cart', []);

        try {
            $findProduct = array_filter($cart, function ($item) use ($request) {
                return $item['id'] === $request->request->get('id');
            });

            if (count($findProduct) !== 0) {
                throw new \Exception('Product is already in cart');
            }

            $cart[] = [
                'id'        => $request->request->get('id'),
                'quantity'  => $request->request->get('quantity'),
                'varieties' => $request->request->get('varieties', [])
            ];

            $this->session->set('cart', $cart);

            return true;
        } catch (\Exception $e) {
            return false; // nothing happens. We don't add product to cart.
        }
    }

    /**
     * @param Request $request
     */
    public function refill(Request $request): void
    {
        $cart = [];
        foreach ($request->request->get('cart') as $product) {
            $cart[] = [
                'id'        => $product['id'],
                'quantity'  => $product['quantity'],
                'varieties' => isset($product['varieties']) ? $product['varieties'] : []
            ];
        }

        $this->session->set('cart', $cart);
    }

    /**
     * @param Request $request
     */
    public function remove(Request $request): void
    {
        $cart = $this->session->get('cart', []);

        foreach ($cart as $index => $product) {
            if ($product['id'] === $request->query->get('id')) {
                unset($cart[$index]);
            }
        }

        $this->session->set('cart', $cart);
    }

    /**
     * @return Cart
     */
    public function fromSession()
    {
        return $this->fillCartFromArray($this->session->get('cart', []));
    }

    /**
     * @param Order $order
     *
     * @return Cart
     */
    public function fromOrder(Order $order)
    {
        $cart = $this->fillCartFromArray($order->getCart());
        $cart
            ->setCoupon($order->getCoupon())
            ->setShipmentPrice($order->getShipmentPrice());

        return $cart;
    }

    /**
     * $products = [
     *  [
     *    'id' => id
     *    'quantity' => quantity,
     *    'varieties' => [
     *     [
     *       'variety' => id,
     *       'value' => id
     *    ]
     *   ],
     * ]
     *
     * @param array $products
     *
     * @return Cart
     */
    private function fillCartFromArray(array $products = [])
    {
        $cart = new Cart();

        $totalCartPrice = 0;
        foreach ($products as $product) {
            /** @var Product $productEntity */
            $productEntity = $this->em->getRepository(Product::class)->find($product['id']);

            if (null === $productEntity) {
                continue;
            }

            $totalProductPrice = $productEntity->getRealPrice() * $product['quantity'];
            $cartProduct       = (new CartProduct())
                ->setQuantity((int) $product['quantity'])
                ->setRelatedProduct($productEntity)
                ->setTotalPrice($totalProductPrice);

            $totalCartPrice += $totalProductPrice;

            foreach ($product['varieties'] as $variety) {
                /** @var Variety $varietyEntity */
                $varietyEntity      = $this->em->getRepository(Variety::class)->find($variety['variety']);
                /** @var VarietyValue $varietyValueEntity */
                $varietyValueEntity = $this->em->getRepository(VarietyValue::class)->find($variety['value']);

                if (null === $varietyEntity || null === $varietyValueEntity) {
                    continue;
                }

                $cartProductVariety = (new CartProductVariety())
                    ->setRelatedVariety($varietyEntity)
                    ->setRelatedValue($varietyValueEntity);

                $cartProduct->addVariety($cartProductVariety);
            }

            $cart->addCartProduct($cartProduct);
        }

        $cart->setTotalPrice($totalCartPrice);

        return $cart;
    }

    public function clear(): void
    {
        $this->session->set('cart', []);
    }
}