<?php

namespace App\Application\Service;

use App\Application\Service\GlobalsService;
use App\Database\Domain\Entity\Globals;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 */
class ParamsService
{
    /** @var string  */
    private $defaultHost;

    /** @var Request */
    private $request;

    /** @var ContainerInterface */
    private $container;

    public function __construct(GlobalsService $globalsService, RequestStack $request, ContainerInterface $container)
    {
        $this->defaultHost = $globalsService->getByKey(Globals::HOST_ADDRESS) ?
            (string) $globalsService->getByKey(Globals::HOST_ADDRESS) : null;

        $this->request     = $request->getCurrentRequest();
        $this->container   = $container;
    }

    /**
     * @return string
     */
    public function getHost()
    {
        $host   = $this->request->getHost();
        $scheme = $this->request->getScheme();

        if ($host === 'localhost') {
            $host .= ':8080';
        }

        if (empty($host) || empty($scheme)) {
            return $this->defaultHost;
        }

        return $scheme . '://' . $host;
    }

    /**
     * @return ContainerInterface
     */
    public function container(): ContainerInterface
    {
        return $this->container;
    }
}
