<?php

namespace App\Application\Util\Generator;

use App\Application\Util\Text\TextUtil;
use App\Database\Domain\Operation\Manager;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 */
class SlugGenerator
{
    /** @var Manager */
    private $manager;

    public function __construct(Manager $manager)
    {
        $this->manager = $manager;
    }

    /**
     * @param string $string
     * @param $entity
     *
     * @return string
     */
    public function fromString(string $string, $entity = null)
    {
        $slug = TextUtil::pregUrl(TextUtil::translit(TextUtil::replaceSpaces(mb_strtolower($string))));

        if (null !== $entity) {
            while ($check = $this->manager->em()->getRepository($entity)->findOneBy(['slug' => $slug])) {
                $slug .= '_' . uniqid();
            }
        }

        return $slug;
    }

    /**
     * @param string $string
     *
     * @return string
     */
    public function withPrefix(string $string): string
    {
        return $this->fromString($string) . '_' . uniqid();
    }
}