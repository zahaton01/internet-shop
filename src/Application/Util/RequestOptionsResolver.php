<?php

namespace App\Application\Util;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 */
class RequestOptionsResolver
{
    public static function bool($name, $context): bool
    {
        if (count($context) > 0) {
            return isset($context[$name]) && ($context[$name] === 'on' || $context[$name] === true);
        }

        return false;
    }
}