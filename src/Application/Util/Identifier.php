<?php

namespace App\Application\Util;

use Ramsey\Uuid\Uuid;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 */
class Identifier
{
    public static function uuid(): string
    {
        return Uuid::uuid4()->toString();
    }
}