<?php

namespace App\Application\Util\Router;

use Symfony\Component\Routing\RouteCollection;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 */
class RouterUtil
{
    /**
     * @param RouteCollection $routeCollection
     * @param string          $path
     *
     * @return bool
     */
    public static function doesRouteExistWithoutLocale(RouteCollection $routeCollection, string $path): bool
    {
        $routeExists = false;
        foreach($routeCollection as $routeObject) {
            $routePath = $routeObject->getPath();

            if($routePath === "/{_locale}$path") {
                $routeExists = true;
                break;
            }
        }

        return $routeExists;
    }
}