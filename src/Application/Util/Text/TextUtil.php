<?php

namespace App\Application\Util\Text;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 */
class TextUtil
{
    /**
     * @param string $string
     *
     * @return string|string[]|null
     */
    public static function translit(string $string)
    {
        $converter = [
            'а' => 'a',   'б' => 'b',   'в' => 'v',
            'г' => 'g',   'д' => 'd',   'е' => 'e',
            'ё' => 'e',   'ж' => 'zh',  'з' => 'z',
            'и' => 'i',   'й' => 'y',   'к' => 'k',
            'л' => 'l',   'м' => 'm',   'н' => 'n',
            'о' => 'o',   'п' => 'p',   'р' => 'r',
            'с' => 's',   'т' => 't',   'у' => 'u',
            'ф' => 'f',   'х' => 'h',   'ц' => 'c',
            'ч' => 'ch',  'ш' => 'sh',  'щ' => 'sch',
            'ь' => '',    'ы' => 'y',   'ъ' => '',
            'э' => 'e',   'ю' => 'yu',  'я' => 'ya',

            'А' => 'A',   'Б' => 'B',   'В' => 'V',
            'Г' => 'G',   'Д' => 'D',   'Е' => 'E',
            'Ё' => 'E',   'Ж' => 'Zh',  'З' => 'Z',
            'И' => 'I',   'Й' => 'Y',   'К' => 'K',
            'Л' => 'L',   'М' => 'M',   'Н' => 'N',
            'О' => 'O',   'П' => 'P',   'Р' => 'R',
            'С' => 'S',   'Т' => 'T',   'У' => 'U',
            'Ф' => 'F',   'Х' => 'H',   'Ц' => 'C',
            'Ч' => 'Ch',  'Ш' => 'Sh',  'Щ' => 'Sch',
            'Ь' => '',    'Ы' => 'Y',   'Ъ' => '',
            'Э' => 'E',   'Ю' => 'Yu',  'Я' => 'Ya',
        ];

        return strtr($string, $converter);
    }

    /**
     * @param string $text
     * @param string $replacer
     *
     * @return mixed
     */
    public static function replaceSpaces(string $text, $replacer = '-')
    {
        return str_replace(' ', $replacer, $text);
    }

    /**
     * @param string $string
     *
     * @return string|null
     */
    public static function incrementString(string $string)
    {
        if (1 !== preg_match('~[0-9]~', $string)) {
            $string .= '0';
        }

        return preg_replace_callback('|(\d+)|', function ($matches) {
            if(isset($matches[1])) {
                $length = strlen($matches[1]);

                return sprintf("%0".$length."d", ++$matches[1]);
            } else {
                return '1';
            }
        }, $string);
    }

    /**
     * @param string $url
     *
     * @return string|string[]|null
     */
    public static function pregUrl(string $url)
    {
        return preg_replace("/[^A-Za-z0-9 ]/", '-', $url);
    }

    /**
     * @param string $string
     * @param int $length
     *
     * @return string
     */
    public static function short(string $string, $length = 70) {
        $short = mb_substr($string, 0, $length);

        if (mb_strlen($short) !== mb_strlen($string)) {
            $short .= '...';
        }

        return $short;
    }

    /**
     * @param string $text
     *
     * @return string|string[]|null
     */
    public static function stripTags(?string $text)
    {
        return preg_replace('/<[^>]+>/', ' ', $text);
    }
}