<?php

namespace App\Application\Libs\Api\NovaPoshta;

use App\Application\Logs\InternalDatabase\Logger\Api\NovaPoshtaInternalDatabaseLogger;
use App\Application\Service\GlobalsService;
use App\Database\Domain\Entity\Globals;
use GuzzleHttp\Client;
use GuzzleHttp\RequestOptions;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 */
class NovaPoshtaApi
{
    /** @var string  */
    private $apiKey;

    /** @var Client  */
    private $client;

    /** @var NovaPoshtaInternalDatabaseLogger  */
    private $internalLogger;

    public function __construct(GlobalsService $globalsService, NovaPoshtaInternalDatabaseLogger $internalLogger)
    {
        $this->internalLogger = $internalLogger;
        $this->client         = new Client(['base_uri' => (string) $globalsService->getByKey(Globals::NOVA_POSHTA_HOST)]);
        $this->apiKey         = (string) $globalsService->getByKey(Globals::NOVA_POSHTA_API_KEY);
    }

    /**
     * @return bool|array
     */
    public function getCities()
    {
        $modelName        = 'Address';
        $calledMethod     = 'getCities';
        $methodProperties = ['' => '']; // Nova's api needs this

        return $this->request($modelName, $calledMethod, $methodProperties)['data'];
    }

    /**
     * @return array
     */
    public function getCitiesAsChoices()
    {
        $data = $this->getCities();

        $res = [];
        foreach ($data as $item) {
            $res[$item['Description']] = $item['Ref'];
        }

        return $res;
    }

    /**
     * @param $ref
     *
     * @return bool|mixed
     */
    public function getWarehouses(string $ref)
    {
        $modelName        = 'AddressGeneral';
        $calledMethod     = 'getWarehouses';
        $methodProperties = ['CityRef' => $ref];

        return $this->request($modelName, $calledMethod, $methodProperties)['data'];
    }


    /**
     * @param string $cityRef
     *
     * @return array
     */
    public function getWarehousesAsChoices(?string $cityRef)
    {
        if (null === $cityRef) {
            return [];
        }

        $data = $this->getWarehouses($cityRef);

        $res = [];
        foreach ($data as $item) {
            $res[$item['Description']] = $item['Ref'];
        }

        return $res;
    }

    /**
     * @param string $ref
     * @param string $cityRef
     *
     * @return mixed|null
     */
    public function getWarehouse(string $ref, string $cityRef)
    {
        $warehouses = $this->getWarehouses($cityRef);
        foreach ($warehouses as $warehouse) {
            if ($warehouse['Ref'] === $ref) {
                return $warehouse;
            }
        }

        return null;
    }


    /**
     * @param string $ref
     *
     * @return mixed
     */
    public function getCity(string $ref)
    {
        $modelName        = 'Address';
        $calledMethod     = 'getCities';
        $methodProperties = ['Ref' => $ref];

        return $this->request($modelName, $calledMethod, $methodProperties)['data'];
    }

    /**
     * @param $modelName
     * @param $calledMethod
     * @param $methodProperties
     *
     * @return bool|mixed
     */
    private function request($modelName, $calledMethod, $methodProperties)
    {
        $postFields = [
            'apiKey'           => $this->apiKey,
            'modelName'        => $modelName,
            'calledMethod'     => $calledMethod,
            'methodProperties' => $methodProperties
        ];

        try {
            $response = $this->client->request('POST', '', [RequestOptions::JSON => $postFields]);
            return json_decode($response->getBody()->getContents(), true);
        } catch (\Exception $e) {
            $this->internalLogger->error($e->getMessage());
        }

        return false;
    }
}