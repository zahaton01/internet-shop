<?php

namespace App\Application\Libs\Tools\Statistic;

use App\Database\Domain\Entity\Commerce\Order\Order;
use Doctrine\DBAL\DBALException;
use Doctrine\ORM\EntityManagerInterface;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 */
class OrderStatistic
{
    /** @var EntityManagerInterface  */
    private $em;

    /**
     * OrderStatistic constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @return array
     */
    public function getOrderCount()
    {
        return [
            'total' => count($this->em->getRepository(Order::class)->findAll()),
            'totalCompleted' => count($this->em->getRepository(Order::class)->findBy(['status' => Order::ORDER_STATUS_COMPLETED]))
        ];
    }
}
