<?php

namespace App\Application\Libs\Tools\EntityImporter\Util;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 */
class ProductImportUtil
{
    /**
     * @param string $haystack
     *
     * @return bool
     */
    public static function isSaleStatus(?string $haystack): bool
    {
        if (!$haystack) {
            return false;
        }

        if ($haystack === 'Да' || $haystack === 'Yes') {
            return true;
        }

        return false;
    }

    /**
     * @param string $data
     *
     * @return array
     */
    public static function getPhotos(?string $data): array
    {
        if (!$data) return [];

        $arr = explode(',', $data);
        $res = [];

        foreach ($arr as $item) {
            if (!$item) continue;

            $res[] = trim($item);
        }

        return $res;
    }

    /**
     * @param string $data
     *
     * @return array
     */
    public static function getSpecifications(?string $data): array
    {
        if (!$data) return [];

        $res = [];

        $specs = explode(';', $data);
        foreach ($specs as $spec) {
            if (!$spec) continue;

            $items = explode(':', trim($spec));
            $key   = trim($items[0]) ?? null;

            if (null === $key) continue;

            if (!array_key_exists($key, $res)) {
                $res[$key] = [];
            }

            $val = $items[1] ?? null;
            if (null === $val) continue;

            $res[$key][] = trim($val);
        }

        return $res;
    }
}
