<?php

namespace App\Application\Libs\Tools\EntityImporter\Instance;

use App\Application\Libs\Tools\EntityImporter\Settings\ProductImportSettings;
use App\Application\Util\Generator\SlugGenerator;
use App\Application\Util\Identifier;
use App\Application\Util\RequestOptionsResolver;
use App\Database\Domain\Entity\Commerce\Product\Category\Meta\ProductCategoryMeta;
use App\Database\Domain\Entity\Commerce\Product\Category\ProductCategory;
use App\Database\Domain\Entity\Commerce\Product\Media\ProductImage;
use App\Database\Domain\Entity\Commerce\Product\Media\ProductMedia;
use App\Database\Domain\Entity\Commerce\Product\Meta\ProductMeta;
use App\Database\Domain\Entity\Commerce\Product\Product;
use App\Database\Domain\Entity\Commerce\Product\ProductPromo;
use App\Database\Domain\Entity\Commerce\Product\Specification\ProductSpecification;
use App\Database\Domain\Entity\Commerce\Product\Specification\Specification;
use App\Database\Domain\Entity\Commerce\Product\Specification\SpecificationValue;
use App\Database\Domain\Factory\EntityFactoryResolver;
use App\Database\Domain\Factory\Product\ProductCategoryFactory;
use App\Database\Domain\Factory\Product\ProductFactory;
use App\Database\Domain\Factory\Product\ProductImageFactory;
use App\Database\Domain\Factory\Product\ProductSpecificationFactory;
use App\Database\Domain\Factory\Product\Specification\SpecificationFactory;
use App\Database\Domain\Factory\Product\Specification\SpecificationValueFactory;
use App\Database\Domain\Model\EntityState\Availability;
use App\Database\Domain\Operation\Manager;
use App\Application\Libs\Tools\EntityImporter\Exception\ImportException;
use App\Application\Libs\Tools\EntityImporter\EntityImporterInstanceInterface;
use App\Application\Libs\Tools\EntityImporter\Util\ProductImportUtil;
use PhpOffice\PhpSpreadsheet\IOFactory;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 */
class ProductEntityImporterInstance implements EntityImporterInstanceInterface
{
    /** @var ProductImportSettings */
    private $settings;

    /** @var Manager */
    private $manager;

    /** @var SlugGenerator  */
    private $slugGenerator;

    /** @var EntityFactoryResolver */
    private $entityFactories;

    public function __construct(Manager $manager, SlugGenerator $slugGenerator, EntityFactoryResolver $factoryResolver)
    {
        $this->manager         = $manager;
        $this->slugGenerator   = $slugGenerator;
        $this->entityFactories = $factoryResolver;
    }

    /**
     * @param ProductImportSettings $settings
     *
     * @return ProductEntityImporterInstance
     */
    public function configure(ProductImportSettings $settings): ProductEntityImporterInstance
    {
        $this->settings = $settings;

        return $this;
    }

    /**
     * @throws ImportException
     */
    public function import()
    {
        switch ($this->settings->getImportExtension()) {
            case 'xlsx':
                return $this->importFromXlsx();
                break;
            default:
                throw new ImportException('Unknown extension');
                break;
        }
    }

    /**
     * @return bool
     *
     * @throws ImportException
     */
    private function importFromXlsx(): bool
    {
        try {
            foreach ($this->settings->getFiles() as $file) {
                $spreadsheet = IOFactory::load($file->getRealPath());
                $sheet       = $spreadsheet->getActiveSheet();

                for ($i = 2; $i <= $sheet->getHighestRow(); $i++) {
                    $vendorCode = $sheet->getCell('A' . $i)->getValue() ?: 0;

                    /** @var Product $product */
                    $product    = $this->manager->em()->getRepository(Product::class)->findOneBy(['vendorCode' => $vendorCode]);
                    if (null !== $product && RequestOptionsResolver::bool('not_create_duplicate', $this->settings->getExtras())) {
                        continue;
                    }

                    $vendorCode   = $product ? $vendorCode . '_' . uniqid() : $vendorCode; // prefix vendor code cuz it's unique
                    $categoryName = $sheet->getCell('D' . $i)->getValue();
                    $category     = $this->manager->em()->getRepository(ProductCategory::class)->findOneBy(['name' => $categoryName]);

                    if (null === $category && !empty($categoryName)) {
                        $category = $this->entityFactories->get(ProductCategoryFactory::class)->basic($categoryName);
                    }

                    $name           = $sheet->getCell('B' . $i)->getValue();
                    $description    = $sheet->getCell('C' . $i)->getValue();
                    $price          = $sheet->getCell('E' . $i)->getValue();
                    $saleStatus     = ProductImportUtil::isSaleStatus($sheet->getCell('F' . $i)->getValue());
                    $salePrice      = $sheet->getCell('G' . $i)->getValue();
                    $photos         = ProductImportUtil::getPhotos($sheet->getCell('I' . $i)->getValue());
                    $specifications = ProductImportUtil::getSpecifications($sheet->getCell('K' . $i)->getValue());
                    $metaTitle      = $sheet->getCell('L' . $i)->getValue();
                    $metaDesc       = $sheet->getCell('M' . $i)->getValue();
                    $metaKeywords   = $sheet->getCell('N' . $i)->getValue();

                    $product        = $this->entityFactories->get(ProductFactory::class)->basic($name);

                    $product
                        ->setName($name)
                        ->setPrice($price)
                        ->setProductCategory($category)
                        ->setVendorCode($vendorCode)
                        ->setDescription($description);

                    $product->getMeta()
                        ->setMetaTitle($metaTitle)
                        ->setMetaDesc($metaDesc)
                        ->setMetaKeywords($metaKeywords);

                    $product->getPromo()
                        ->setSaleStatus($saleStatus)
                        ->setSalePrice($salePrice);

                    foreach ($photos as $photo) {
                        $image = $this->entityFactories->get(ProductImageFactory::class)->remote($photo, $product->getMedia());
                        $product->getMedia()->addImage($image);
                    }

                    foreach ($specifications as $specificationName => $specificationValues) {
                        $specification = $this->manager->em()->getRepository(Specification::class)->findOneBy(['name' => $specificationName]);

                        if (null === $specification) {
                            $specification = $this->entityFactories->get(SpecificationFactory::class)->basic($specificationName, $category);
                            $this->manager->save($specification);
                        }

                        $productSpecification = $this->entityFactories->get(ProductSpecificationFactory::class)->basic($product, $specification);

                        foreach ($specificationValues as $value) {
                            $specificationValue = $this->manager->em()->getRepository(SpecificationValue::class)->findOneBy([
                                'value'         => $value,
                                'specification' => $specification->getId()
                            ]);

                            if (null === $specificationValue) {
                                $specificationValue = $this->entityFactories->get(SpecificationValueFactory::class)->basic($value, $specification);
                                $this->manager->save($specificationValue);
                            }

                            $productSpecification->addValue($specificationValue);
                        }

                        $product->addSpecification($productSpecification);
                    }

                    $product->makeUniqueSpecifications();

                    $this->manager->em()->persist($product);
                }
            }

            $this->manager->em()->flush();
        } catch (\Exception $e) {
            throw new ImportException($e->getMessage());
        }

        return true;
    }
}
