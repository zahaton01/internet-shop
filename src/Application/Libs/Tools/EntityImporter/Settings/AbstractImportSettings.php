<?php

namespace App\Application\Libs\Tools\EntityImporter\Settings;

use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 */
abstract class AbstractImportSettings
{
    /** @var string */
    private $importExtension;

    /** @var UploadedFile[] */
    private $files;

    /** @var array */
    private $extras;

    public function __construct(string $importExtension, array $files, array $extras = [])
    {
        $this->importExtension = $importExtension;
        $this->files           = $files;
        $this->extras          = $extras;
    }

    /**
     * @return string
     */
    public function getImportExtension(): string
    {
        return $this->importExtension;
    }

    /**
     * @param string $importExtension
     *
     * @return self
     */
    public function setImportExtension(string $importExtension): self
    {
        $this->importExtension = $importExtension;
        return $this;
    }

    /**
     * @return UploadedFile[]
     */
    public function getFiles(): ?array
    {
        return $this->files;
    }

    /**
     * @param UploadedFile $file
     *
     * @return $this
     */
    public function addFile(UploadedFile $file)
    {
        $this->files[] = $file;
        return $this;
    }

    /**
     * @return array
     */
    public function getExtras(): ?array
    {
        return $this->extras;
    }
}