<?php

namespace App\Application\Libs\Tools\EntityImporter\Settings;

use App\Application\Libs\Tools\EntityImporter\Exception\ImportException;
use Symfony\Component\HttpFoundation\Request;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 */
class ProductImportSettings extends AbstractImportSettings
{
    /**
     * @param Request $request
     *
     * @return ProductImportSettings
     *
     * @throws ImportException
     */
    public static function fromRequest(Request $request)
    {
        if ($request->isMethod('post')) {
            $data = $request->request->all();
        }

        if (!isset($data)) {
            throw new ImportException('Unknown request method');
        }

        if (!isset($data['extension'])) {
            throw new ImportException('Import extension is not specified');
        }

        if (!$request->files->all()) {
            throw new ImportException('None files');
        }

        return new self($data['extension'], $request->files->all(), $data);
    }
}