<?php

namespace App\Application\Libs\Tools\EntityImporter\Exception;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 */
class ImportException extends \Exception
{

}
