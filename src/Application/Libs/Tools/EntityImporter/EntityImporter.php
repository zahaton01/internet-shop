<?php

namespace App\Application\Libs\Tools\EntityImporter;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 */
class EntityImporter
{
    /** @var iterable  */
    private $instances;

    public function __construct(iterable $instances)
    {
        $this->instances = $instances;
    }

    /**
     * @param string $class
     *
     * @return EntityImporterInstanceInterface|null
     */
    public function get(string $class): ?EntityImporterInstanceInterface
    {
        foreach ($this->instances as $instance) {
            if (get_class($instance) === $class) return $instance;
        }

        return null;
    }
}
