<?php

namespace App\Application\Libs\Tools\EntityExporter\Exception;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 */
class ExportException extends \Exception
{

}