<?php

namespace App\Application\Libs\Tools\EntityExporter\Instance;

use App\Application\Libs\Tools\EntityExporter\EntityExporterInstanceInterface;
use App\Application\Libs\Tools\EntityExporter\Exception\ExportException;
use App\Application\Libs\Tools\EntityExporter\Settings\ProductExportSettings;
use App\Application\Libs\Tools\EntityExporter\Util\ProductExporterUtil;
use App\Application\Core\ClassWrappers\DateTime;
use App\Application\Service\GlobalsService;
use App\Application\Service\ParamsService;
use App\Application\Util\RequestOptionsResolver;
use App\Database\Domain\Entity\Commerce\Product\Product;
use App\Database\Domain\Entity\Globals;
use App\Database\Domain\Operation\Manager;
use ParseCsv\Csv;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 */
class ProductEntityExporterInstance implements EntityExporterInstanceInterface
{
    /** @var ProductExportSettings */
    private $settings;

    /** @var Manager */
    private $manager;

    /** @var ParamsService */
    private $params;

    /** @var GlobalsService */
    private $globals;

    /** @var TranslatorInterface */
    private $translator;

    /** @var RouterInterface */
    private $router;

    /** @var ContainerInterface */
    private $container;

    public function __construct(
        Manager $manager,
        ParamsService $params,
        GlobalsService $globalsService,
        TranslatorInterface $translator,
        RouterInterface $router,
        ContainerInterface $container
    ) {
        $this->manager    = $manager;
        $this->params     = $params;
        $this->globals    = $globalsService;
        $this->translator = $translator;
        $this->router     = $router;
        $this->container  = $container;
    }

    /**
     * @param ProductExportSettings $settings
     *
     * @return ProductEntityExporterInstance
     */
    public function configure(ProductExportSettings $settings): ProductEntityExporterInstance
    {
        $this->settings = $settings;

        return $this;
    }

    /**
     * @return Response
     *
     * @throws ExportException
     */
    public function export()
    {
        switch ($this->settings->getExportExtension()) {
            case 'xml':
                return $this->exportToXml();
                break;
            case 'csv' :
                return new Response($this->exportToCsv());
                break;
            case 'xlsx' :
                return $this->exportToXlsx();
                break;
            default:
                throw new ExportException('Unknown extension');
                break;
        }
    }

    private function exportToXml()
    {
        $products = $this->manager->em()->getRepository(Product::class)->findAll();

        if (RequestOptionsResolver::bool('xml_return_life_link', $this->settings->getExtras())) {
            return new RedirectResponse($this->router->generate('product_xml'));
        }

        $rendered = $this->container->get('twig')->render('product_xml.html.twig', ['products' => $products]);
        if (RequestOptionsResolver::bool('xml_return_content', $this->settings->getExtras())) {
            return $rendered;
        }

        $response = new Response($rendered);
        $response->headers->set('Content-Type', 'application/xml; charset=utf-8');

        return $response;

    }

    private function exportToXlsx(): Response
    {
        $response = new Response();
        $products = $this->manager->em()->getRepository(Product::class)->findAll();

        if ($products) {
            $spreadsheet = new Spreadsheet();

            $sheet = $spreadsheet->getActiveSheet();

            // setting titles
            $sheet->setCellValue('A1', $this->translator->trans('Vendor code'));
            $sheet->setCellValue('B1', $this->translator->trans('Product name'));
            $sheet->setCellValue('C1', $this->translator->trans('Product description'));
            $sheet->setCellValue('D1', $this->translator->trans('Category'));
            $sheet->setCellValue('E1', $this->translator->trans('Price'));
            $sheet->setCellValue('F1', $this->translator->trans('Product is on sale'));
            $sheet->setCellValue('G1', $this->translator->trans('Product sale price'));
            $sheet->setCellValue('H1', $this->translator->trans('Currency of the app'));
            $sheet->setCellValue('I1', $this->translator->trans('Photos'));
            $sheet->setCellValue('J1', $this->translator->trans('Link on site'));
            $sheet->setCellValue('K1', $this->translator->trans('Specifications'));
            $sheet->setCellValue('L1', $this->translator->trans('Meta title'));
            $sheet->setCellValue('M1', $this->translator->trans('Meta desc'));
            $sheet->setCellValue('N1', $this->translator->trans('Meta keywords'));

            /**
             * @var int $index
             * @var Product $product
             */
            foreach ($products as $index => $product) {
                $category    = $product->getProductCategory() ? $product->getProductCategory()->getName() : $this->translator->trans('Not related to any');
                $saleStatus  = $product->getPromo()->isSaleStatus() ? $this->translator->trans('Yes') : $this->translator->trans('No');
                $productLink = $this->params->getHost() . $this->router->generate('site_product_index', ['product' => $product->getSlug()]);

                $sheet->setCellValue('A' . ($index + 2), $product->getVendorCode());
                $sheet->setCellValue('B' . ($index + 2), $product->getName());
                $sheet->setCellValue('C' . ($index + 2), $product->getDescription());
                $sheet->setCellValue('D' . ($index + 2), $category);
                $sheet->setCellValue('E' . ($index + 2), $product->getPrice());
                $sheet->setCellValue('F' . ($index + 2), $saleStatus);
                $sheet->setCellValue('G' . ($index + 2), $product->getPromo()->getSalePrice());
                $sheet->setCellValue('H' . ($index + 2), (string) $this->globals->getByKey(Globals::APP_CURRENCY));
                $sheet->setCellValue('I' . ($index + 2), ProductExporterUtil::getPhotos($product, $this->params->getHost()));
                $sheet->setCellValue('J' . ($index + 2), $productLink);
                $sheet->setCellValue('K' . ($index + 2), ProductExporterUtil::getSpecifications($product));
                $sheet->setCellValue('L' . ($index + 2), $product->getMeta()->getMetaTitle());
                $sheet->setCellValue('M' . ($index + 2), $product->getMeta()->getMetaDesc());
                $sheet->setCellValue('N' . ($index + 2), $product->getMeta()->getMetaKeywords());
            }

            $writer = new Xlsx($spreadsheet);

            $fileName = 'Product_export_' . DateTime::now()->format('d-m-Y_H:i:s') . '.xlsx';
            $temp_file = tempnam(sys_get_temp_dir(), $fileName);

            $writer->save($temp_file);

            $response = new BinaryFileResponse($temp_file);
            $response->setContentDisposition(ResponseHeaderBag::DISPOSITION_INLINE, $fileName);
        }

        return $response;
    }

    private function exportToCsv()
    {
        $products = $this->manager->em()->getRepository(Product::class)->findAll();

        if ($products) {
            $output = new Csv();
            $output->encoding('UTF-8', 'UTF-8');

            $titles = [
                $this->translator->trans('Vendor code'),
                $this->translator->trans('Product name'),
                $this->translator->trans('Product description'),
                $this->translator->trans('Category'),
                $this->translator->trans('Price'),
                $this->translator->trans('Is product on sale?'),
                $this->translator->trans('Product sale price'),
                $this->translator->trans('Currency of the app'),
                $this->translator->trans('Photos'),
                $this->translator->trans('Link on site'),
                $this->translator->trans('Specifications'),
                $this->translator->trans('Meta title'),
                $this->translator->trans('Meta desc'),
                $this->translator->trans('Meta keywords')
            ];

            $data = [];
            /** @var Product $product */
            foreach ($products as $product) {
                $data[] = [
                    $product->getVendorCode(),
                    $product->getName(),
                    $product->getDescription(),
                    $product->getProductCategory() ? $product->getProductCategory()->getName() : '',
                    $product->getPrice(),
                    $product->getPromo()->isSaleStatus() ? $this->translator->trans('Yes') : $this->translator->trans('No'),
                    $product->getPromo()->getSalePrice(),
                    $this->globals->getByKey(Globals::APP_CURRENCY)->getValue(),
                    ProductExporterUtil::getPhotos($product, $this->params->getHost()),
                    $this->params->getHost() . $this->router->generate('site_product_index', ['product' => $product->getSlug()]),
                    ProductExporterUtil::getSpecifications($product),
                    $product->getMeta()->getMetaTitle(),
                    $product->getMeta()->getMetaDesc(),
                    $product->getMeta()->getMetaKeywords()
                ];
            }

            $output->output('Product_export_' . DateTime::now()->format('d-m-Y_H:i:s') . '.csv',
                            $data, $titles, ',');
        }
    }
}