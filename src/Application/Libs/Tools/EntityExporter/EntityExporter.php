<?php

namespace App\Application\Libs\Tools\EntityExporter;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 */
class EntityExporter
{
    /** @var iterable  */
    private $instances;

    public function __construct(iterable $instances)
    {
        $this->instances = $instances;
    }

    /**
     * @param string $class
     *
     * @return EntityExporterInstanceInterface|null
     */
    public function get(string $class): ?EntityExporterInstanceInterface
    {
        foreach ($this->instances as $instance) {
            if (get_class($instance) === $class) {
                return $instance;
            }
        }

        return null;
    }
}