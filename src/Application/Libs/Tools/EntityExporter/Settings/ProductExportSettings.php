<?php

namespace App\Application\Libs\Tools\EntityExporter\Settings;

use App\Application\Libs\Tools\EntityExporter\Exception\ExportException;
use Symfony\Component\HttpFoundation\Request;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 */
class ProductExportSettings extends AbstractExportSettings
{
    /**
     * @param Request $request
     *
     * @return ProductExportSettings
     *
     * @throws ExportException
     */
    public static function fromRequest(Request $request)
    {
        if ($request->isMethod('post')) {
            $data = $request->request->all();
        }

        if (!isset($data)) {
            throw new ExportException('Unknown request method');
        }

        if (!isset($data['extension'])) {
            throw new ExportException('Export extension is not specified');
        }

        return new self($data['extension'], $data);
    }
}