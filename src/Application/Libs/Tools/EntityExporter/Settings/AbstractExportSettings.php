<?php

namespace App\Application\Libs\Tools\EntityExporter\Settings;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 */
abstract class AbstractExportSettings
{
    /** @var string */
    private $exportExtension;

    /** @var array */
    private $extras;

    public function __construct(string $exportExtension, array $extras = [])
    {
        $this->exportExtension = $exportExtension;
        $this->extras          = $extras;
    }

    /**
     * @return string
     */
    public function getExportExtension(): string
    {
        return $this->exportExtension;
    }

    /**
     * @param string $exportExtension
     *
     * @return self
     */
    public function setExportExtension(string $exportExtension): self
    {
        $this->exportExtension = $exportExtension;
        return $this;
    }

    /**
     * @return array
     */
    public function getExtras(): array
    {
        return $this->extras;
    }

    /**
     * @param array $extras
     *
     * @return self
     */
    public function setExtras(array $extras): self
    {
        $this->extras = $extras;
        return $this;
    }
}