<?php

namespace App\Application\Libs\Tools\EntityExporter\Util;

use App\Database\Domain\Entity\Commerce\Product\Media\ProductImage;
use App\Database\Domain\Entity\Commerce\Product\Product;
use App\Database\Domain\Entity\Commerce\Product\Specification\ProductSpecification;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 */
class ProductExporterUtil
{
    /**
     * @param Product $product
     *
     * @return string
     */
    public static function getSpecifications(Product $product)
    {
        $specifications = '';

        /** @var ProductSpecification $productSpecification */
        foreach ($product->getSpecifications() as $index => $productSpecification) {
            foreach ($productSpecification->getValues() as $specificationValue) {
                $specifications .= "{$productSpecification->getSpecification()->getName()}:{$specificationValue->getValue()};";
            }
        }

        return $specifications;
    }


    /**
     * @param Product $product
     * @param string  $host
     *
     * @return string
     */
    public static function getPhotos(Product $product, string $host)
    {
        $links = '';
        $max   = count($product->getMedia()->getImages());

        /** @var ProductImage $image */
        foreach ($product->getMedia()->getImages() as $index => $image) {
            if ($image->isRemote()) {
                $links .= "{$image->getUrl()}";
            }

            if ($image->isLocal()) {
                $links .= "{$host}/{$image->getAssetPath()}";
            }

            if (($index + 1) !== $max) {
                $links .= ',';
            }
        }

        return $links;
    }
}