<?php

namespace App\Application\Libs\Component\Notifier;

use App\Application\Libs\Component\Notifier\Transport\EmailTransport;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Mime\Email;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 */
class AdminNotifier extends Notifier
{
    /** @var string */
    private $adminEmail;

    public function __construct(EmailTransport $emailTransport, TranslatorInterface $translator, ParameterBagInterface $parameterBag)
    {
        parent::__construct($emailTransport, $translator);

        $this->adminEmail = $parameterBag->get('admin_email');
    }

    /**
     * @param string $text
     */
    public function contactFormRequest(string $text)
    {
        $email = (new Email())
            ->to($this->adminEmail)
            ->subject('Email from site (contact page)')
            ->text($text);

        $this->emailTransport->send($email);
    }
}