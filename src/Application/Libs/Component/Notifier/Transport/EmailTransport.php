<?php

namespace App\Application\Libs\Component\Notifier\Transport;

use App\Application\Libs\Component\Notifier\Exception\NotifierException;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 */
class EmailTransport
{
    /** @var MailerInterface  */
    private $mailer;

    /** @var string */
    private $senderEmail;

    public function __construct(MailerInterface $mailer, ParameterBagInterface $parameterBag)
    {
        $this->mailer      = $mailer;
        $this->senderEmail = $parameterBag->get('email_sender');
    }

    /**
     * @param Email $email
     */
    public function send(Email $email): void
    {
        $email->from($this->senderEmail);

        try {
            $this->mailer->send($email);
        } catch (\Exception | TransportExceptionInterface $e) {
            throw new NotifierException($e->getMessage());
        }
    }
}