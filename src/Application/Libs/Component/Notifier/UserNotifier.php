<?php

namespace App\Application\Libs\Component\Notifier;

use App\Application\Libs\Component\Notifier\Exception\NotifierException;
use App\Application\Libs\Component\Notifier\Transport\EmailTransport;
use Symfony\Component\Mime\Email;
use Symfony\Contracts\Translation\TranslatorInterface;
use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 */
class UserNotifier extends Notifier
{
    private $twig;

    public function __construct(EmailTransport $emailTransport, TranslatorInterface $translator, Environment $twig)
    {
        parent::__construct($emailTransport, $translator);

        $this->twig = $twig;
    }

    /**
     * @param string $email
     *
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function orderCreated(string $email): void
    {
        $rendered = $this->twig->render('email/user/order_created.html.twig');

        $email = (new Email())
            ->to($email)
            ->subject($this->translator->trans('Your order was created!'))
            ->html($rendered);

        $this->emailTransport->send($email);
    }
}