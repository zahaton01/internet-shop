<?php

namespace App\Application\Libs\Component\Notifier\Exception;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 */
class NotifierException extends \RuntimeException
{

}