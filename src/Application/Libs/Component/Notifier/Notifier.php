<?php

namespace App\Application\Libs\Component\Notifier;

use App\Application\Libs\Component\Notifier\Transport\EmailTransport;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 */
abstract class Notifier
{
    /** @var EmailTransport */
    protected $emailTransport;

    /** @var TranslatorInterface */
    protected $translator;

    public function __construct(EmailTransport $emailTransport, TranslatorInterface $translator)
    {
        $this->emailTransport = $emailTransport;
        $this->translator     = $translator;
    }
}