<?php

namespace App\Application\Libs\Component\CacheAdapter;

use Symfony\Component\Cache\Adapter\FilesystemAdapter;
use Symfony\Component\Cache\Adapter\TagAwareAdapter;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 */
class CacheAdapter
{
    public const TAG_GLOBALS = 'globals';
    public const EXPIRES_DAY = 86400;

    /** @var TagAwareAdapter  */
    private $cache;

    public function __construct()
    {
        $this->cache = new TagAwareAdapter(new FilesystemAdapter());
    }

    /**
     * @return TagAwareAdapter
     */
    public function cache(): TagAwareAdapter
    {
        return $this->cache;
    }
}