<?php

namespace App\Application\Libs\Component\FileManager;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 */
abstract class AbstractFileManager
{
    /** @var string  */
    protected $projectDir;

    public function __construct(ContainerInterface $container)
    {
        $this->projectDir = $container->getParameter('kernel.project_dir');
    }

    /**
     * @param null $name
     *
     * @return string|null
     */
    protected function getFilename($name = null)
    {
        return empty($name) ? uniqid() : $name;
    }

    /**
     * @param string $path
     *
     * @return string
     */
    protected function getAssetPath(string $path)
    {
        $exploded = explode('/', $path);

        $start = $exploded[0] === '' ? '' : '/';
        $end = $exploded[count($exploded) - 1] === '' ? '' : '/';

        return $start . implode('/', $exploded) . $end;
    }

    /**
     * @param UploadedFile $file
     *
     * @return string
     */
    protected function getImageExtension(UploadedFile $file)
    {
        $extension = $file->guessExtension() ?: 'png';

        return '.' . $extension;
    }

    /**
     * @param string $path
     * @param string $extension
     * @param null $name
     *
     * @return string
     */
    protected function getFullPath(string $path, string $extension, $name = null)
    {
        return $this->projectDir . $this->getAssetPath($path) . $this->getFilename($name) . $extension;
    }
}
