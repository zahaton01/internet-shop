<?php

namespace App\Application\Libs\Component\FileManager\Model;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 */
class File
{
    /** @var string */
    private $extension = null;

    /** @var string */
    private $assetPath = null;

    /** @var string */
    private $filename  = null;

    /** @var string */
    private $fullPath  = null;

    /** @var string */
    private $type      = null;

    /**
     * @return string
     */
    public function getExtension(): ?string
    {
        return $this->extension;
    }

    /**
     * @param string $extension
     *
     * @return self
     */
    public function setExtension(?string $extension): self
    {
        $this->extension = $extension;
        return $this;
    }

    /**
     * @return string
     */
    public function getAssetPath(): ?string
    {
        return $this->assetPath;
    }

    /**
     * @param string $assetPath
     *
     * @return self
     */
    public function setAssetPath(?string $assetPath): self
    {
        $this->assetPath = $assetPath;
        return $this;
    }

    /**
     * @return string
     */
    public function getFilename(): ?string
    {
        return $this->filename;
    }

    /**
     * @param string $filename
     *
     * @return self
     */
    public function setFilename(?string $filename): self
    {
        $this->filename = $filename;
        return $this;
    }

    /**
     * @return string
     */
    public function getFullPath(): ?string
    {
        return $this->fullPath;
    }

    /**
     * @param string $fullPath
     *
     * @return self
     */
    public function setFullPath(?string $fullPath): self
    {
        $this->fullPath = $fullPath;
        return $this;
    }

    /**
     * @return string
     */
    public function getType(): ?string
    {
        return $this->type;
    }

    /**
     * @param string $type
     *
     * @return self
     */
    public function setType(?string $type): self
    {
        $this->type = $type;
        return $this;
    }
}
