<?php

namespace App\Application\Libs\Component\FileManager;

use App\Application\Libs\Component\FileManager\Model\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 */
class UploadManager extends AbstractFileManager
{
    /**
     * @param string $path
     * @param UploadedFile $file
     * @param null $name
     *
     * @return File
     */
    private function saveImageFile(string $path, UploadedFile $file, $name = null)
    {
        $uploadedFile = new File();

        $name = $this->getFilename($name);
        $uploadedFile
            ->setFilename($name)
            ->setExtension($this->getImageExtension($file))
            ->setFullPath($this->getFullPath($path, $this->getImageExtension($file), $name));

        $oldMask = umask(0);
        if (!file_exists($this->projectDir . $this->getAssetPath($path))) {
            mkdir($this->projectDir . $this->getAssetPath($path), 0777, true);
        }
        umask($oldMask);

        move_uploaded_file($file->getPathName(), $uploadedFile->getFullPath());

        return $uploadedFile;
    }

    /**
     * @param UploadedFile $file
     * @param null $name
     *
     * @return File
     */
    public function uploadProductImage(UploadedFile $file, $name = null)
    {
        $uploadedFile =  $this->saveImageFile('public/entity/products', $file, $name);
        $uploadedFile->setAssetPath('entity/products/' . $uploadedFile->getFilename() . $uploadedFile->getExtension());

        return $uploadedFile;
    }

    /**
     * @param UploadedFile $file
     * @param null $name
     *
     * @return File
     */
    public function uploadPostImage(UploadedFile $file, $name = null)
    {
        $uploadedFile =  $this->saveImageFile('public/entity/post', $file, $name);
        $uploadedFile->setAssetPath('entity/post/' . $uploadedFile->getFilename() . $uploadedFile->getExtension());

        return $uploadedFile;
    }
}
