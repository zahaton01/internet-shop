<?php

namespace App\Application\Logs\InternalDatabase;

use App\Database\Domain\Entity\Log;
use App\Database\Domain\Factory\LogFactory;
use App\Database\Domain\Operation\Manager;
use App\Database\Domain\Exception\ModelValidationException;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 */
abstract class AbstractInternalDatabaseLogger
{
    /** @var LogFactory */
    private $logFactory;

    /** @var Manager */
    private $manager;

    public function __construct(LogFactory $logFactory, Manager $manager)
    {
        $this->logFactory = $logFactory;
        $this->manager    = $manager;
    }

    /**
     * @param string $message
     * @param string $source
     * @param string $level
     * @param array $extras
     *
     * @return Log|mixed
     *
     * @throws ModelValidationException
     */
    private function createLog(
        string $message,
        string $source,
        string $level,
        array $extras = []
    ): Log {
        $log = $this->logFactory->createBasic(
            $message,
            $source,
            $level,
            $extras
        );

        return $this->manager->save($log);
    }

    /**
     * @param string $message
     * @param string $source
     * @param array $extras
     *
     * @return Log|mixed
     *
     * @throws ModelValidationException
     */
    protected function infoLog(
        string $message,
        string $source,
        array $extras = []
    ): Log {
        return $this->createLog($message, $source, Log::LEVEL_INFO, $extras);
    }

    /**
     * @param string $message
     * @param string $source
     * @param array $extras
     *
     * @return Log|mixed
     *
     * @throws ModelValidationException
     */
    protected function warningLog(
        string $message,
        string $source,
        array $extras = []
    ): Log {
        return $this->createLog($message, $source, Log::LEVEL_WARNING, $extras);
    }

    /**
     * @param string $message
     * @param string $source
     * @param array $extras
     *
     * @return Log|mixed
     *
     * @throws ModelValidationException
     */
    protected function errorLog(
        string $message,
        string $source,
        array $extras = []
    ): Log {
        return $this->createLog($message, $source, Log::LEVEL_ERROR, $extras);
    }
}
