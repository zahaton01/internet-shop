<?php

namespace App\Application\Logs\InternalDatabase\Logger\Api;

use App\Application\Logs\InternalDatabase\AbstractInternalDatabaseLogger;
use App\Database\Domain\Entity\Log;
use App\Database\Domain\Exception\ModelValidationException;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 */
class NovaPoshtaInternalDatabaseLogger extends AbstractInternalDatabaseLogger
{
    /**
     * @param string $message
     * @param array $extras
     *
     * @return Log|mixed
     *
     * @throws ModelValidationException
     */
    public function info(string $message, array $extras = [])
    {
        return $this->infoLog($message, Log::SOURCE_NOVA_POSHTA, $extras);
    }

    /**
     * @param string $message
     * @param array $extras
     *
     * @return Log|mixed
     *
     * @throws ModelValidationException
     */
    public function warning(string $message, array $extras = [])
    {
        return $this->warningLog($message, Log::SOURCE_NOVA_POSHTA, $extras);
    }

    /**
     * @param string $message
     * @param array $extras
     *
     * @return Log|mixed
     *
     * @throws ModelValidationException
     */
    public function error(string $message, array $extras = [])
    {
        return $this->errorLog($message, Log::SOURCE_NOVA_POSHTA, $extras);
    }
}