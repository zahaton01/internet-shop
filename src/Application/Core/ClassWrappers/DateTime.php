<?php

namespace App\Application\Core\ClassWrappers;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 */
class DateTime
{
    /**
     * @return \DateTime|null
     */
    public static function now(): ?\DateTime
    {
        try {
            return new \DateTime('now');
        } catch (\Exception $e) {
            return null;
        }
    }
}