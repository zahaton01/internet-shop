<?php

namespace App\Http\EventBus\EventHandler;

use App\Application\Libs\Component\Notifier\UserNotifier;
use App\Http\EventBus\Event\OrderCreatedEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 */
class OrderCreatedEventHandler implements EventSubscriberInterface
{
    /** @var UserNotifier */
    private $userNotifier;

    public function __construct(UserNotifier $notifier)
    {
        $this->userNotifier = $notifier;
    }

    /**
     * @param OrderCreatedEvent $event
     *
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function onOrderCreated(OrderCreatedEvent $event): void
    {
        $order = $event->getOrder();

        if ($order->getEmail() || ($order->getUser() && $order->getUser()->getEmail())) {
            $email = $order->getEmail() ?: $order->getUser()->getEmail();

            $this->userNotifier->orderCreated($email);
        }
    }

    public static function getSubscribedEvents(): array
    {
        return [OrderCreatedEvent::NAME => 'onOrderCreated'];
    }
}