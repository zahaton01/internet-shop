<?php

namespace App\Http\EventBus\Event;

use App\Database\Domain\Entity\Commerce\Order\Order;
use Symfony\Contracts\EventDispatcher\Event;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 */
class OrderCreatedEvent extends Event
{
    public const NAME = 'order_created';

    /** @var Order  */
    protected $order;

    public function __construct(Order $order)
    {
        $this->order = $order;
    }

    /**
     * @return Order
     */
    public function getOrder(): Order
    {
        return $this->order;
    }
}