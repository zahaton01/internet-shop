<?php

namespace App\Http\Rest\Model;

use App\Database\Domain\Entity\Commerce\Product\Product;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 */
class WatchedProducts
{
    /** @var array */
    private $products = [];

    /**
     * @return array
     */
    public function getProducts()
    {
        return $this->products;
    }

    /**
     * @param Product $product
     *
     * @return $this
     */
    public function addProduct(Product $product)
    {
        $this->products[] = $product;

        return $this;
    }
}
