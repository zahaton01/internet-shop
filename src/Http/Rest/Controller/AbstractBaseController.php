<?php

namespace App\Http\Rest\Controller;

use App\Application\Service\GlobalsService;
use App\Application\Service\ParamsService;
use App\Database\Domain\Operation\Manager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 */
abstract class AbstractBaseController extends AbstractController
{
    /** @var Manager */
    protected $manager;

    /** @var GlobalsService */
    protected $globals;

    /** @var ParamsService */
    protected $params;

    /** @var TranslatorInterface */
    protected $translator;

    public function __construct(
        Manager $manager,
        GlobalsService $globalsService,
        ParamsService $paramService,
        TranslatorInterface $translator
    ) {
        $this->manager    = $manager;
        $this->globals    = $globalsService;
        $this->params     = $paramService;
        $this->translator = $translator;
    }
}