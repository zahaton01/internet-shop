<?php

namespace App\Http\Rest\Controller\Site;

use App\Database\Domain\Entity\Commerce\Product\Category\ProductCategory;
use App\Database\Domain\Entity\Commerce\Product\Product;
use App\Database\Domain\Entity\Site\SitePage\SitePage;
use App\Http\Rest\Controller\AbstractSiteController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 */
class HomeController extends AbstractSiteController
{
    /**
     * @Route("/", name="index")
     *
     * @return Response
     */
    public function index()
    {
        $page            = $this->manager->em()->getRepository(SitePage::class)->findOneBy(['staticType' => SitePage::STATIC_MAIN]);
        $popularProducts = $this->manager->em()->getRepository(Product::class)->findBy([], [], 12);
        $newProducts     = $this->manager->em()->getRepository(Product::class)->findBy([], ['creationDate' => 'DESC'], 8);
        $categories      = $this->manager->em()->getRepository(ProductCategory::class)->findBy([], [], 6);

        return $this->renderTemplate('pages/main/index.html.twig', [
            'popularProducts'   => $popularProducts,
            'newProducts'       => $newProducts,
            'catalogCategories' => $categories,
            'page'              => $page
        ]);
    }
}
