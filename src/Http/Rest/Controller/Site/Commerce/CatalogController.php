<?php

namespace App\Http\Rest\Controller\Site\Commerce;

use App\Database\Domain\Entity\Commerce\Product\Category\ProductCategory;
use App\Database\Domain\Entity\Commerce\Product\SubCategory\ProductSubCategory;
use App\Database\Domain\Entity\Site\SitePage\SitePage;
use App\Http\Rest\Controller\AbstractSiteController;
use App\Database\Domain\Entity\Commerce\Product\Product;
use App\Database\Domain\Entity\Commerce\Product\Specification\Specification;
use App\Database\Domain\Model\Query\Product\ProductFilters;
use App\Database\Domain\Service\PaginatorService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 *
 * @Route("/catalog", name="catalog_")
 */
class CatalogController extends AbstractSiteController
{
    /**
     * @Route("", name="index")
     *
     * @param PaginatorService $paginator
     * @param ProductFilters   $productFilters
     *
     * @return Response
     */
    public function index(PaginatorService $paginator, ProductFilters $productFilters): Response
    {
        $categories = $this->manager->em()->getRepository(ProductCategory::class)->findAll();
        $products   = $this->manager->em()->getRepository(Product::class)->findByProductFilters($productFilters);
        $sitePage   = $this->manager->em()->getRepository(SitePage::class)->findOneBy(['staticType' => SitePage::STATIC_CATALOG]);

        return $this->renderTemplate('pages/commerce/catalog/index.html.twig', [
            'categories'     => $categories,
            'products'       => $paginator->paginate($products),
            'productFilters' => $productFilters,
            'specifications' => [],
            'page'           => $sitePage
        ]);
    }

    /**
     * @Route("/{categorySlug}", name="category")
     * @ParamConverter("category", options={"mapping": {"categorySlug": "slug"}})
     * 
     * @param ProductCategory  $category
     * @param ProductFilters   $productFilters
     * @param PaginatorService $paginator
     *
     * @return RedirectResponse|Response
     */
    public function category(
        ProductCategory $category,
        PaginatorService $paginator,
        ProductFilters $productFilters
    ): Response {
        $productFilters->setCategory($category);

        $products       = $this->manager->em()->getRepository(Product::class)->findByProductFilters($productFilters);
        $categories     = $this->manager->em()->getRepository(ProductCategory::class)->findAll();
        $specifications = $this->manager->em()->getRepository(Specification::class)->findByCategories($category);

        return $this->renderTemplate('pages/commerce/catalog/index.html.twig', [
            'categories'     => $categories,
            'category'       => $category,
            'products'       => $paginator->paginate($products),
            'productFilters' => $productFilters,
            'specifications' => $specifications,
            'page'           => $category //faking page variable to display category meta
        ]);
    }

    /**
     * @Route("/{categorySlug}/{subCategorySlug}", name="sub_category")
     * @ParamConverter("category", options={"mapping": {"categorySlug": "slug"}})
     * @ParamConverter("subCategory", options={"mapping": {"subCategorySlug": "slug"}})
     *
     * @param ProductCategory    $category
     * @param ProductSubCategory $subCategory
     * @param PaginatorService   $paginator
     * @param ProductFilters     $productFilters
     *
     * @return RedirectResponse|Response
     */
    public function subCategory(
        ProductCategory $category,
        ProductSubCategory $subCategory,
        PaginatorService $paginator,
        ProductFilters $productFilters
    ): Response {
        $productFilters->setCategory($category);
        $productFilters->setSubCategory($subCategory);

        $products       = $this->manager->em()->getRepository(Product::class)->findByProductFilters($productFilters);
        $categories     = $this->manager->em()->getRepository(ProductCategory::class)->findAll();
        $specifications = $this->manager->em()->getRepository(Specification::class)->findByCategories($category, $subCategory);

        return $this->renderTemplate('pages/commerce/catalog/index.html.twig', [
            'categories'     => $categories,
            'category'       => $category,
            'subCategory'    => $subCategory,
            'products'       => $paginator->paginate($products),
            'specifications' => $specifications,
            'productFilters' => $productFilters,
            'page'           => $subCategory //faking page variable to display category meta
        ]);
    }
}
