<?php

namespace App\Http\Rest\Controller\Site\Commerce;

use App\Database\Domain\Entity\Site\SitePage\SitePage;
use App\Http\Rest\Controller\AbstractSiteController;
use App\Application\Service\Commerce\CartService;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 *
 * @Route("/cart", name="cart_")
 */
class CartController extends AbstractSiteController
{
    /**
     * @Route("", name="index")
     *
     * @return Response
     */
    public function index()
    {
        $page = $this->manager->em()->getRepository(SitePage::class)->findOneBy(['staticType' => SitePage::STATIC_CART]);

        return $this->renderTemplate('pages/commerce/cart/index.html.twig', ['page' => $page]);
    }

    /**
     * @Route("/remove-product", name="remove_product")
     *
     * @param Request     $request
     * @param CartService $cartService
     *
     * @return RedirectResponse
     */
    public function removeProduct(Request $request, CartService $cartService)
    {
        $cartService->remove($request);

        return $this->redirectToRoute('site_cart_index');
    }
}
