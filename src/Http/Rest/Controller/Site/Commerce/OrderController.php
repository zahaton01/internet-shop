<?php

namespace App\Http\Rest\Controller\Site\Commerce;

use App\Database\Domain\Entity\Site\SitePage\SitePage;
use App\Database\Domain\Exception\ModelValidationException;
use App\Database\Domain\Factory\EntityFactoryResolver;
use App\Database\Domain\Factory\Order\OrderFactory;
use App\Http\EventBus\Event\OrderCreatedEvent;
use App\Http\Rest\Controller\AbstractSiteController;
use App\Database\Domain\Entity\Commerce\Order\Order;
use App\Http\Rest\Response\Form\Types\Commerce\Order\OrderType;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 *
 * @Route("/order", name="order_")
 */
class OrderController extends AbstractSiteController
{
    /**
     * @Route("", name="index")
     *
     * @param Request                  $request
     * @param EventDispatcherInterface $eventBus
     * @param EntityFactoryResolver    $entityFactories
     *
     * @return RedirectResponse|Response
     *
     * @throws ModelValidationException
     */
    public function index(
        Request $request,
        EventDispatcherInterface $eventBus,
        EntityFactoryResolver $entityFactories
    ): Response {
        $sitePage = $this->manager->em()->getRepository(SitePage::class)->findOneBy(['staticType' => SitePage::STATIC_CHECKOUT]);
        $order    = $entityFactories->get(OrderFactory::class)->basic();

        $form = $this->createForm(OrderType::class, $order);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var Order $order */
            $order = $entityFactories->get(OrderFactory::class)->fromType($form);
            $order->setCart($this->cartService->fromSession());

            $this->manager->save($order);
            $this->cartService->clear();
            $eventBus->dispatch(new OrderCreatedEvent($order), OrderCreatedEvent::NAME);

            return $this->redirectToRoute('site_order_created', ['id' => $order->getId()]);
        }

        return $this->renderTemplate('pages/commerce/order/index.html.twig', [
            'form' => $form->createView(),
            'page' => $sitePage
        ]);
    }

    /**
     * @Route("/{id}", name="created")
     *
     * @param Order $order
     *
     * @return Response
     */
    public function thankYou(Order $order): Response
    {
        $sitePage = $this->manager->em()->getRepository(SitePage::class)->findOneBy(['staticType' => SitePage::STATIC_CHECKOUT]);

        $city     = $order->getCity();
        $address  = $order->getAddress();
        if ($order->isDeliveryNovaPoshta()) {
            $novaPoshta = $order->getDeliveryData();
            $city       = $novaPoshta->getCity() ?: $city;
            $address    = $novaPoshta->getWarehouse() ?: $address;
        }

        return $this->renderTemplate('pages/commerce/order/thank_you.html.twig', [
            'order'     => $order,
            'city'      => $city,
            'address'   => $address,
            'page'      => $sitePage,
            'orderCart' => $this->cartService->fromOrder($order)
        ]);
    }
}
