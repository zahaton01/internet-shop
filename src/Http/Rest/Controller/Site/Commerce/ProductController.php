<?php

namespace App\Http\Rest\Controller\Site\Commerce;

use App\Http\Rest\Controller\AbstractSiteController;
use App\Database\Domain\Entity\Commerce\Product\Product;
use App\Http\Rest\Service\WatchedProductsService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 *
 * @Route("/product", name="product_")
 */
class ProductController extends AbstractSiteController
{
    /**
     * @Route("/{product}", name="index")
     * @ParamConverter("product", options={"mapping": {"product": "slug"}})
     *
     * @param Product                $product
     * @param WatchedProductsService $watchedProductsService
     *
     * @return Response
     */
    public function index(Product $product, WatchedProductsService $watchedProductsService)
    {
        if (!$product->isVisible()) {
            throw new NotFoundHttpException('Product was not found');
        }

        $watchedProductsService->add($product);

        $popularProducts = $this->manager->em()->getRepository(Product::class)->findBy([], [], 12);
        $newProducts     = $this->manager->em()->getRepository(Product::class)->findBy([], ['creationDate' => 'DESC'], 8);

        return $this->renderTemplate('pages/commerce/product/index.html.twig', [
            'product'         => $product,
            'popularProducts' => $popularProducts,
            'newProducts'     => $newProducts,
            'watchedProducts' => $watchedProductsService->wrapper()->getProducts()
        ]);
    }
}
