<?php

namespace App\Http\Rest\Controller\Site\Pages;

use App\Database\Domain\Entity\Site\SitePage\SitePage;
use App\Http\Rest\Controller\AbstractSiteController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * @Route("/contact", name="contact_")
 */
class ContactController extends AbstractSiteController
{
    /**
     * @Route("/contacts", name="index")
     *
     * @return Response
     */
    public function index()
    {
        $page = $this->manager->em()->getRepository(SitePage::class)->findOneBy(['staticType' => SitePage::STATIC_CONTACT]);

        return $this->renderTemplate('pages/contact/index.html.twig', ['page' => $page]);
    }
}
