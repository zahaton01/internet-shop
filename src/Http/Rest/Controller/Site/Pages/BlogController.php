<?php

namespace App\Http\Rest\Controller\Site\Pages;

use App\Database\Domain\Entity\Site\Post\Post;
use App\Database\Domain\Entity\Site\SitePage\SitePage;
use App\Database\Domain\Model\Query\Params\QueryParams;
use App\Http\Rest\Controller\AbstractSiteController;
use App\Database\Domain\Service\PaginatorService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 *
 * @Route("/blog", name="blog_")
 */
class BlogController extends AbstractSiteController
{
    /**
     * @Route("", name="index")
     *
     * @param PaginatorService $paginator
     *
     * @return Response
     */
    public function index(PaginatorService $paginator)
    {
        $posts = $this->manager->em()->getRepository(Post::class)->search();
        $page  = $this->manager->em()->getRepository(SitePage::class)->findOneBy(['staticType' => SitePage::STATIC_BLOG]);

        return $this->renderTemplate('pages/blog/index.html.twig', [
            'posts' => $paginator->paginate($posts),
            'page'  => $page
        ]);
    }

    /**
     * @Route("/{slug}", name="read")
     * @ParamConverter("post", options={"mapping": {"slug": "slug"}})
     *
     * @param Post $post
     *
     * @return RedirectResponse|Response
     */
    public function read(Post $post)
    {
        return $this->renderTemplate('pages/blog/read.html.twig', ['post' => $post]);
    }
}
