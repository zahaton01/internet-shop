<?php

namespace App\Http\Rest\Controller\Site\Pages;

use App\Database\Domain\Entity\Site\SitePage\SitePage;
use App\Http\Rest\Controller\AbstractSiteController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 *
 * @Route("/pages", name="pages_")
 */
class PagesController extends AbstractSiteController
{
    /**
     * @Route("/{pageSlug}", name="index")
     * @ParamConverter("page", options={"mapping": {"pageSlug": "slug"}})
     *
     * @param SitePage $page
     *
     * @return RedirectResponse|Response
     */
    public function index(SitePage $page)
    {
        if (!$page->isEnabled()) {
            return $this->notFound();
        }

        if ($page->getStaticType() === SitePage::STATIC_BLOG) {
            return $this->redirectToRoute('site_blog_index');
        }

        return $this->renderTemplate('pages/site_page/index.html.twig', ['page' => $page]);
    }
}
