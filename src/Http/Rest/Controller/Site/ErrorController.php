<?php

namespace App\Http\Rest\Controller\Site;

use App\Http\Rest\Controller\AbstractSiteController;
use App\Http\Rest\Model\Error\Error;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 */
class ErrorController extends AbstractSiteController
{
    /**
     * @Route("/error", name="error")
     *
     * @param Error $error
     *
     * @return Response
     */
    public function renderError(Error $error)
    {
        return $this->renderTemplate('error.html.twig', ['error' => $error]);
    }
}
