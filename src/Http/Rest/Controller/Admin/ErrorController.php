<?php

namespace App\Http\Rest\Controller\Admin;

use App\Http\Rest\Controller\Admin\AbstractAdminController;
use App\Http\Rest\Model\Error\Error;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 */
class ErrorController extends AbstractAdminController
{
    /**
     * @Route("/error", name="error")
     *
     * @param Error $error
     *
     * @return Response
     */
    public function renderError(Error $error): Response
    {
        return $this->renderTemplate('error.html.twig', ['error' => $error]);
    }
}
