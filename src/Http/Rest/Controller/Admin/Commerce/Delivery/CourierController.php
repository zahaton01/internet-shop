<?php

namespace App\Http\Rest\Controller\Admin\Commerce\Delivery;

use App\Application\Util\Identifier;
use App\Database\Domain\Model\Query\Params\QueryParams;
use App\Http\Rest\Controller\Admin\AbstractAdminController;
use App\Database\Domain\Entity\Commerce\Order\Courier;
use App\Database\Domain\Entity\Globals;
use App\Database\Domain\Exception\ModelValidationException;
use App\Http\Rest\Response\Form\Types\Commerce\Courier\CourierType;
use App\Http\Rest\Response\Form\Types\Commerce\Courier\PriceType;
use App\Database\Domain\Service\PaginatorService;
use Psr\Cache\InvalidArgumentException;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 *
 * @Route("/courier", name="courier_")
 */
class CourierController extends AbstractAdminController
{
    /**
     * @Route("", name="index")
     *
     * @param Request          $request
     * @param QueryParams      $params
     * @param PaginatorService $paginator
     *
     * @return Response
     *
     * @throws ModelValidationException
     * @throws InvalidArgumentException
     */
    public function index(
        Request $request,
        QueryParams $params,
        PaginatorService $paginator
    ): Response {
        $form = $this->createForm(PriceType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->globals->setByKey(number_format($form['price']->getData(),0,'.',''),Globals::COURIER_PRICE);

            return $this->redirectToRoute('admin_courier_index');
        }

        $couriers = $this->manager->em()->getRepository(Courier::class)->findByQuery($params);

        return $this->renderTemplate('pages/commerce/delivery/courier/index.html.twig', [
            'couriers' => $paginator->paginate($couriers),
            'form'     => $form->createView()
        ]);
    }

    /**
     * @Route("/create", name="create")
     *
     * @param Request $request
     *
     * @return RedirectResponse|Response
     *
     * @throws ModelValidationException
     */
    public function create(Request $request): Response
    {
        $courier = (new Courier())->setId(Identifier::uuid());

        $form    = $this->createForm(CourierType::class, $courier);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->manager->save($form->getData());

            return $this->redirectToRoute('admin_courier_index');
        }

        return $this->renderTemplate('pages/commerce/delivery/courier/create.html.twig', ['form' => $form->createView()]);
    }

    /**
     * @Route("/edit/{id}", name="edit")
     *
     * @param Request $request
     * @param Courier $courier
     *
     * @return RedirectResponse|Response
     *
     * @throws ModelValidationException
     */
    public function edit(Request $request, Courier $courier): Response
    {
        $form = $this->createForm(CourierType::class, $courier);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->manager->save($form->getData());

            return $this->redirectToRoute('admin_courier_index');
        }

        return $this->renderTemplate('pages/commerce/delivery/courier/edit.html.twig', ['form' => $form->createView()]);
    }

    /**
     * @Route("/remove/{id}", name="remove")
     *
     * @param Courier $courier
     *
     * @return RedirectResponse
     */
    public function remove(Courier $courier): Response
    {
        $this->manager->remove($courier);

        return $this->redirectToRoute('admin_courier_index');
    }
}