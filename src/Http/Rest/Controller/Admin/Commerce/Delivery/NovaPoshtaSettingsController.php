<?php

namespace App\Http\Rest\Controller\Admin\Commerce\Delivery;

use App\Database\Domain\Exception\ModelValidationException;
use App\Http\Rest\Controller\Admin\AbstractAdminController;
use App\Database\Domain\Entity\Globals;
use App\Http\Rest\Response\Form\Types\Commerce\DeliveryCompany\NovaPoshtaType;
use Psr\Cache\InvalidArgumentException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 *
 * @Route("/delivery-comapny", name="delivery_company_")
 */
class NovaPoshtaSettingsController extends AbstractAdminController
{
    /**
     * @Route("/nova-poshta/settings", name="nova_poshta_settings")
     *
     * @param Request $request
     *
     * @return Response
     *
     * @throws ModelValidationException
     * @throws InvalidArgumentException
     */
    public function index(Request $request): Response
    {
        $form = $this->createForm(NovaPoshtaType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->globals->setByKey($form['api_host']->getData(), Globals::NOVA_POSHTA_HOST);
            $this->globals->setByKey($form['is_enabled']->getData() ? 'true' : 'false', Globals::NOVA_POSHTA_IS_ENABLED);
            $this->globals->setByKey(number_format($form['price']->getData(),0,'.',''), Globals::NOVA_POSHTA_PRICE);
            //$this->globals->setByKey($form['api_key']->getData(), Globals::NOVA_POSHTA_API_KEY);

            return $this->redirectToRoute('admin_delivery_company_nova_poshta_settings');
        }

        return $this->renderTemplate('pages/commerce/delivery/index.html.twig', ['form' => $form->createView()]);
    }
}