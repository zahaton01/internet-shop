<?php

namespace App\Http\Rest\Controller\Admin\Commerce\Payment;

use App\Database\Domain\Exception\ModelValidationException;
use App\Http\Rest\Controller\Admin\AbstractAdminController;
use App\Database\Domain\Entity\Globals;
use App\Http\Rest\Response\Form\Types\Commerce\OnlinePayment\Liqpay\LiqpaySettingsType;
use Psr\Cache\InvalidArgumentException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 *
 * @Route("/payment-methods", name="payment_")
 */
class PaymentController extends AbstractAdminController
{
    /**
     * @Route("/liqpay", name="liqpay_settings")
     *
     * @param Request $request
     *
     * @return Response
     *
     * @throws ModelValidationException
     * @throws InvalidArgumentException
     */
    public function settings(Request $request): Response
    {
        $form = $this->createForm(LiqpaySettingsType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->globals->setByKey($form['api_url']->getData(), Globals::LIQPAY_API_URL);
            $this->globals->setByKey($form['checkout_url']->getData(), Globals::LIQPAY_CHECKOUT_URL);
            $this->globals->setByKey($form['is_enabled']->getData() ? 'true' : 'false', Globals::LIQPAY_IS_ENABLED);
            //$this->globals->setByKey($form['public_key']->getData(), Globals::LIQPAY_PUBLIC_KEY);
            //$this->globals->setByKey($form['private_key']->getData(), Globals::LIQPAY_PRIVATE_KEY);

            return $this->redirectToRoute('admin_payment_liqpay_settings');
        }

        return $this->renderTemplate('pages/commerce/payment/liqpay/settings.html.twig', ['form' => $form->createView()]);
    }
}