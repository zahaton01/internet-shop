<?php

namespace App\Http\Rest\Controller\Admin\Commerce\Order;

use App\Application\Libs\Api\NovaPoshta\NovaPoshtaApi;
use App\Database\Domain\Model\Query\Params\QueryParams;
use App\Http\Rest\Controller\Admin\AbstractAdminController;
use App\Database\Domain\Entity\Commerce\Order\Order;
use App\Database\Domain\Exception\ModelValidationException;
use App\Http\Rest\Response\Form\Types\Commerce\Order\AdminOrderEditType;
use App\Application\Service\Commerce\CartService;
use App\Database\Domain\Service\PaginatorService;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 *
 * @Route("/order", name="order_")
 */
class OrderController extends AbstractAdminController
{
    /**
     * @Route("", name="index")
     *
     * @param QueryParams      $params
     * @param PaginatorService $paginator
     *
     * @return Response
     */
    public function index(QueryParams $params, PaginatorService $paginator): Response
    {
        $orders = $this->manager->em()->getRepository(Order::class)->findByQuery($params);

        return $this->renderTemplate('pages/commerce/order/index.html.twig', [
            'orders'                  => $paginator->paginate($orders),
            'queryParams'             => $params,
            'possibleStatuses'        => Order::statuses(),
            'possiblePaymentStatuses' => Order::paymentStatuses()
        ]);
    }

    /**
     * @Route("/edit/{id}", name="edit")
     *
     * @param Request       $request
     * @param Order         $order
     * @param CartService   $cartService
     * @param NovaPoshtaApi $novaPoshtaApi
     *
     * @return RedirectResponse|Response
     *
     * @throws ModelValidationException
     */
    public function edit(
        Request $request,
        Order $order,
        CartService $cartService,
        NovaPoshtaApi $novaPoshtaApi
    ): Response {
        $form = $this->createForm(AdminOrderEditType::class, $order);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var Order $order */
            $order = $form->getData();

            if ($order->isDeliveryNovaPoshta()) {
                $cityRef        = $form['novaCity']->getData();
                $warehouseRef   = $form['novaWarehouse']->getData();
                $cityName       = $novaPoshtaApi->getCity($cityRef)[0]['Description'] ?? null;
                $warehouseName  = $novaPoshtaApi->getWarehouse($warehouseRef, $cityRef)['Description'] ?? null;

                $novaPoshtaData = $order->getDeliveryData();
                $novaPoshtaData
                    ->setCityRef($cityRef)
                    ->setWarehouseRef($warehouseRef)
                    ->setCity($cityName)
                    ->setWarehouse($warehouseName);

                $order->setDeliveryData($novaPoshtaData);
            }

            $order = $this->manager->save($order);

            return $this->redirectToRoute('admin_order_edit', ['id' => $order->getId()]);
        }

        return $this->renderTemplate('pages/commerce/order/edit.html.twig', [
            'form'  => $form->createView(),
            'order' => $order,
            'cart'  => $cartService->fromOrder($order)
        ]);
    }

    /**
     * @Route("/remove/{id}", name="remove")
     *
     * @param Order $order
     *
     * @return RedirectResponse
     */
    public function remove(Order $order)
    {
        $this->manager->remove($order);

        return $this->redirectToRoute('admin_order_index');
    }
}