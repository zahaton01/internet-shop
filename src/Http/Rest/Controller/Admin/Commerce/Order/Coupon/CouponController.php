<?php

namespace App\Http\Rest\Controller\Admin\Commerce\Order\Coupon;

use App\Application\Util\Identifier;
use App\Database\Domain\Model\Query\Params\QueryParams;
use App\Http\Rest\Controller\Admin\AbstractAdminController;
use App\Database\Domain\Entity\Commerce\Order\Coupon;
use App\Database\Domain\Exception\ModelValidationException;
use App\Http\Rest\Response\Form\Types\Commerce\Coupon\FixedPriceCouponType;
use App\Database\Domain\Service\PaginatorService;
use App\Http\Rest\Response\Form\Types\Commerce\Coupon\PercentageCouponType;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 *
 * @Route("/coupon", name="coupon_")
 */
class CouponController extends AbstractAdminController
{
    /**
     * @Route("", name="index")
     *
     * @param QueryParams      $params
     * @param PaginatorService $paginator
     *
     * @return Response
     */
    public function index(QueryParams $params, PaginatorService $paginator): Response
    {
        $coupons = $this->manager->em()->getRepository(Coupon::class)->findByQuery($params);

        return $this->renderTemplate('pages/commerce/order/coupon/index.html.twig', ['coupons' => $paginator->paginate($coupons)]);
    }

    /**
     * @Route("/create-fixed", name="create_fixed")
     *
     * @param Request $request
     *
     * @return RedirectResponse|Response
     *
     * @throws ModelValidationException
     */
    public function createFixed(Request $request): Response
    {
        $coupon = (new Coupon())
            ->setId(Identifier::uuid())
            ->setDiscountType(Coupon::TYPE_FIXED);

        $form   = $this->createForm(FixedPriceCouponType::class, $coupon);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->manager->save($form->getData());

            return $this->redirectToRoute('admin_coupon_index');
        }

        return $this->renderTemplate('pages/commerce/order/coupon/create.html.twig', ['form' => $form->createView()]);
    }

    /**
     * @Route("/edit-fixed/{id}", name="edit_fixed")
     *
     * @param Request $request
     * @param Coupon  $coupon
     *
     * @return RedirectResponse|Response
     *
     * @throws ModelValidationException
     */
    public function editFixed(Request $request, Coupon $coupon): Response
    {
        $form = $this->createForm(FixedPriceCouponType::class, $coupon);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->manager->save($form->getData());

            return $this->redirectToRoute('admin_coupon_index');
        }

        return $this->renderTemplate('pages/commerce/order/coupon/edit.html.twig', ['form' => $form->createView()]);
    }

    /**
     * @Route("/create-percentage", name="create_percentage")
     *
     * @param Request $request
     *
     * @return RedirectResponse|Response
     *
     * @throws ModelValidationException
     */
    public function createPercentage(Request $request): Response
    {
        $coupon = (new Coupon())
            ->setId(Identifier::uuid())
            ->setDiscountType(Coupon::TYPE_PERCENT);

        $form   = $this->createForm(PercentageCouponType::class, $coupon);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->manager->save($form->getData());

            return $this->redirectToRoute('admin_coupon_index');
        }

        return $this->renderTemplate('pages/commerce/order/coupon/create.html.twig', ['form' => $form->createView()]);
    }

    /**
     * @Route("/edit-percentage/{id}", name="edit_percentage")
     *
     * @param Request $request
     * @param Coupon  $coupon
     *
     * @return RedirectResponse|Response
     *
     * @throws ModelValidationException
     */
    public function editPercentage(Request $request, Coupon $coupon): Response
    {
        $form = $this->createForm(PercentageCouponType::class, $coupon);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->manager->save($form->getData());

            return $this->redirectToRoute('admin_coupon_index');
        }

        return $this->renderTemplate('pages/commerce/order/coupon/edit.html.twig', ['form' => $form->createView()]);
    }

    /**
     * @Route("/remove/{id}", name="remove")
     *
     * @param Coupon $coupon
     *
     * @return RedirectResponse
     */
    public function remove(Coupon $coupon): Response
    {
        $this->manager->remove($coupon);

        return $this->redirectToRoute('admin_coupon_index');
    }
}
