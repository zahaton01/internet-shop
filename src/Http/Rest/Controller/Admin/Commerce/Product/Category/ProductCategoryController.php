<?php

namespace App\Http\Rest\Controller\Admin\Commerce\Product\Category;

use App\Application\Util\Generator\SlugGenerator;
use App\Application\Util\Identifier;
use App\Database\Domain\Entity\Commerce\Product\Category\Meta\ProductCategoryMeta;
use App\Database\Domain\Entity\Commerce\Product\Category\Meta\ProductCategoryMetaTranslation;
use App\Database\Domain\Entity\Commerce\Product\Category\ProductCategory;
use App\Database\Domain\Entity\Commerce\Product\Category\ProductCategoryTranslation;
use App\Database\Domain\Model\Query\Params\QueryParams;
use App\Http\Rest\Controller\Admin\AbstractAdminController;
use App\Database\Domain\Exception\ModelValidationException;
use App\Http\Rest\Response\Form\Types\Commerce\Product\Category\ProductCategoryMetaTranslationType;
use App\Http\Rest\Response\Form\Types\Commerce\Product\Category\ProductCategoryTranslationType;
use App\Http\Rest\Response\Form\Types\Commerce\Product\Category\ProductCategoryType;
use App\Http\Rest\Response\Form\Types\AbstractCategoryTranslationType;
use App\Database\Domain\Service\PaginatorService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 *
 * @Route("/product/category", name="product_category_")
 */
class ProductCategoryController extends AbstractAdminController
{
    /**
     * @Route("", name="index")
     *
     * @param PaginatorService $paginator
     * @param QueryParams      $params
     *
     * @return Response
     */
    public function index(PaginatorService $paginator, QueryParams $params): Response
    {
        $categories = $this->manager->em()->getRepository(ProductCategory::class)->findByQuery($params);

        return $this->renderTemplate('pages/commerce/product/category/index.html.twig', ['categories' => $paginator->paginate($categories)]);
    }

    /**
     * @Route("/create", name="create")
     *
     * @param Request       $request
     * @param SlugGenerator $slugGenerator
     *
     * @return RedirectResponse|Response
     *
     * @throws \Exception
     */
    public function createCategory(Request $request, SlugGenerator $slugGenerator): Response
    {
        $category = (new ProductCategory())
            ->setMeta((new ProductCategoryMeta())->setId(Identifier::uuid()))
            ->setId(Identifier::uuid());

        $form = $this->createForm(ProductCategoryType::class, $category);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var ProductCategory $category */
            $category = $form->getData();
            $category->setSlug($slugGenerator->fromString($category->getName(), ProductCategory::class));

            $this->manager->save($category);

            return $this->redirectToRoute('admin_product_category_edit', ['id' => $category->getId()]);
        }

        return $this->renderTemplate('pages/commerce/product/category/create_category.html.twig', ['form' => $form->createView()]);
    }

    /**
     * @Route("/edit/{id}", name="edit")
     *
     * @param Request         $request
     * @param ProductCategory $category
     *
     * @return RedirectResponse|Response
     *
     * @throws ModelValidationException
     */
    public function editCategory(Request $request, ProductCategory $category): Response
    {
        $form = $this->createForm(ProductCategoryType::class, $category);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->manager->save($form->getData());

            return $this->redirectToRoute('admin_product_category_index');
        }

        return $this->renderTemplate('pages/commerce/product/category/edit_category.html.twig', [
            'form' => $form->createView(),
            'category' => $category
        ]);
    }

    /**
     * @Route("/remove/{id}", name="remove")
     *
     * @param ProductCategory $category
     *
     * @return RedirectResponse
     */
    public function removeCategory(ProductCategory $category): Response
    {
        $this->manager->remove($category);

        return $this->redirectToRoute('admin_product_category_index');
    }

    /**
     * @Route("/edit/{id}/meta-translation", name="create_meta_translation")
     *
     * @param Request         $request
     * @param ProductCategory $category
     *
     * @return RedirectResponse|Response
     *
     * @throws ModelValidationException
     */
    public function createMetaTranslation(Request $request, ProductCategory $category): Response
    {
        $translation = (new ProductCategoryMetaTranslation())
            ->setId(Identifier::uuid())
            ->setMeta($category->getMeta());

        $form = $this->createForm(ProductCategoryMetaTranslationType::class, $translation);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->manager->save($form->getData());

            return $this->redirectToRoute('admin_product_category_edit', ['id' => $category->getId()]);
        }

        return $this->renderTemplate('pages/commerce/product/category/create_meta_translation.html.twig', ['form' => $form->createView()]);
    }

    /**
     * @Route("/edit/{id}/meta-translation/edit/{transId}", name="edit_meta_translation")
     * @ParamConverter("translation", options={"id" = "transId"})
     *
     * @param Request                        $request
     * @param ProductCategory                $category
     * @param ProductCategoryMetaTranslation $translation
     *
     * @return RedirectResponse|Response
     *
     * @throws ModelValidationException
     */
    public function editMetaTranslation(
        Request $request,
        ProductCategory $category,
        ProductCategoryMetaTranslation $translation
    ): Response {
        $form = $this->createForm(ProductCategoryMetaTranslationType::class, $translation);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->manager->save($translation);

            return $this->redirectToRoute('admin_product_category_edit', ['id' => $category->getId()]);
        }

        return $this->renderTemplate('pages/commerce/product/category/edit_meta_translation.html.twig', ['form' => $form->createView()]);
    }

    /**
     * @Route("/edit/{id}/meta-translation/remove/{transId}", name="remove_meta_translation")
     * @ParamConverter("translation", options={"id" = "transId"})
     *
     * @param ProductCategory                $category
     * @param ProductCategoryMetaTranslation $translation
     *
     * @return RedirectResponse
     */
    public function removeMetaTranslation(ProductCategory $category, ProductCategoryMetaTranslation $translation): Response
    {
        $this->manager->remove($translation);

        return $this->redirectToRoute('admin_product_category_edit', ['id' => $category->getId()]);
    }

    /**
     * @Route("/edit/{id}/translation", name="create_translation")
     *
     * @param Request         $request
     * @param ProductCategory $category
     *
     * @return RedirectResponse|Response
     *
     * @throws ModelValidationException
     */
    public function createTranslation(Request $request, ProductCategory $category): Response
    {
        $translation = (new ProductCategoryTranslation())
            ->setId(Identifier::uuid())
            ->setProductCategory($category);

        $form = $this->createForm(ProductCategoryTranslationType::class, $translation);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->manager->save($form->getData());

            return $this->redirectToRoute('admin_product_category_edit', ['id' => $category->getId()]);
        }

        return $this->renderTemplate('pages/commerce/product/category/create_translation.html.twig', ['form' => $form->createView()]);
    }

    /**
     * @Route("/edit/{id}/translation/edit/{transId}", name="edit_translation")
     * @ParamConverter("translation", options={"id" = "transId"})
     *
     * @param Request                    $request
     * @param ProductCategory            $category
     * @param ProductCategoryTranslation $translation
     *
     * @return RedirectResponse|Response
     *
     * @throws ModelValidationException
     */
    public function editTranslation(
        Request $request,
        ProductCategory $category,
        ProductCategoryTranslation $translation
    ): Response {
        $form = $this->createForm(ProductCategoryTranslationType::class, $translation);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->manager->save($form->getData());

            return $this->redirectToRoute('admin_product_category_edit', ['id' => $category->getId()]);
        }

        return $this->renderTemplate('pages/commerce/product/category/edit_translation.html.twig', ['form' => $form->createView()]);
    }

    /**
     * @Route("/edit/{id}/translation/remove/{transId}", name="remove_translation")
     * @ParamConverter("translation", options={"id" = "transId"})
     *
     * @param ProductCategory            $category
     * @param ProductCategoryTranslation $translation
     *
     * @return RedirectResponse
     */
    public function removeTranslation(ProductCategory $category, ProductCategoryTranslation $translation): Response
    {
        $this->manager->remove($translation);

        return $this->redirectToRoute('admin_product_category_edit', ['id' => $category->getId()]);
    }
}