<?php

namespace App\Http\Rest\Controller\Admin\Commerce\Product;

use App\Application\Libs\Tools\EntityImporter\Settings\ProductImportSettings;
use App\Http\Rest\Controller\AbstractSiteController;
use App\Application\Libs\Tools\EntityImporter\Exception\ImportException;
use App\Application\Libs\Tools\EntityImporter\EntityImporter;
use App\Application\Libs\Tools\EntityImporter\Instance\ProductEntityImporterInstance;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 *
 * @Route("/product-import", name="product_import_")
 */
class ProductImportController extends AbstractSiteController
{
    /**
     * @Route("", name="index")
     *
     * @param Request        $request
     * @param EntityImporter $importer
     *
     * @return RedirectResponse
     *
     * @throws ImportException
     */
    public function import(Request $request, EntityImporter $importer): Response
    {
        $importer->get(ProductEntityImporterInstance::class)
            ->configure(ProductImportSettings::fromRequest($request))
            ->import();

        return $this->redirectToRoute('admin_product_index');
    }
}
