<?php

namespace App\Http\Rest\Controller\Admin\Commerce\Product;

use App\Database\Domain\Entity\Commerce\Product\Product;
use App\Database\Domain\Entity\Commerce\Product\Variety\ProductVariety;
use App\Http\Rest\Controller\Admin\AbstractAdminController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 *
 * @Route("/product/edit/{id}/varieties", name="product_varieties_")
 */
class ProductVarietyController extends AbstractAdminController
{
    /**
     * @Route("/add", name="add")
     *
     * @param Product $product
     *
     * @return Response
     */
    public function add(Product $product): Response
    {
        return $this->renderTemplate('pages/commerce/product/add_product_variety.html.twig', ['product' => $product]);
    }

    /**
     * @Route("/edit/{varietyId}", name="edit")
     * @ParamConverter("variety", options={"id" = "varietyId"})
     *
     * @param Product        $product
     * @param ProductVariety $variety
     *
     * @return Response
     */
    public function edit(Product $product, ProductVariety $variety): Response
    {
        return $this->renderTemplate('pages/commerce/product/edit_product_variety.html.twig', [
            'variety' => $variety,
            'product' => $product
        ]);
    }

    /**
     * @Route("/remove/{varietyId}", name="remove")
     * @ParamConverter("variety", options={"id" = "varietyId"})
     *
     * @param Product        $product
     * @param ProductVariety $variety
     *
     * @return RedirectResponse
     */
    public function remove(Product $product, ProductVariety $variety): Response
    {
        $this->manager->remove($variety);

        return $this->redirectToRoute('admin_product_edit', ['id' => $product->getId()]);
    }
}