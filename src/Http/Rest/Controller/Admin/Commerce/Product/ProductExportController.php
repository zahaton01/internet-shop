<?php

namespace App\Http\Rest\Controller\Admin\Commerce\Product;

use App\Application\Libs\Tools\EntityExporter\EntityExporter;
use App\Application\Libs\Tools\EntityExporter\Exception\ExportException;
use App\Application\Libs\Tools\EntityExporter\Instance\ProductEntityExporterInstance;
use App\Application\Libs\Tools\EntityExporter\Settings\ProductExportSettings;
use App\Http\Rest\Controller\Admin\AbstractAdminController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 *
 * @Route("/product-export", name="product_export_")
 */
class ProductExportController extends AbstractAdminController
{
    /**
     * @Route("", name="index")
     *
     * @param Request        $request
     * @param EntityExporter $exporter
     *
     * @return Response
     *
     * @throws ExportException
     */
    public function export(Request $request, EntityExporter $exporter): Response
    {
        return $exporter->get(ProductEntityExporterInstance::class)
            ->configure(ProductExportSettings::fromRequest($request))
            ->export();
    }
}
