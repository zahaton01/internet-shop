<?php

namespace App\Http\Rest\Controller\Admin\Commerce\Product\SubCategory;

use App\Application\Util\Generator\SlugGenerator;
use App\Application\Util\Identifier;
use App\Database\Domain\Entity\Commerce\Product\Category\ProductCategory;
use App\Database\Domain\Entity\Commerce\Product\SubCategory\ProductSubCategory;
use App\Database\Domain\Entity\Commerce\Product\SubCategory\ProductSubCategoryTranslation;
use App\Database\Domain\Model\Query\Params\QueryParams;
use App\Database\Domain\Service\PaginatorService;
use App\Http\Rest\Controller\Admin\AbstractAdminController;
use App\Database\Domain\Entity\Commerce\Product\SubCategory\Meta\ProductSubCategoryMeta;
use App\Database\Domain\Entity\Commerce\Product\SubCategory\Meta\ProductSubCategoryMetaTranslation;
use App\Database\Domain\Exception\ModelValidationException;
use App\Http\Rest\Response\Form\Types\Commerce\Product\SubCategory\ProductSubCategoryMetaTranslationType;
use App\Http\Rest\Response\Form\Types\Commerce\Product\SubCategory\ProductSubCategoryTranslationType;
use App\Http\Rest\Response\Form\Types\Commerce\Product\SubCategory\ProductSubCategoryType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 *
 * @Route("/product/sub-categories", name="product_sub_category_")
 */
class ProductSubCategoryController extends AbstractAdminController
{
    /**
     * @Route("", name="index")
     *
     * @param PaginatorService $paginator
     * @param QueryParams      $params
     *
     * @return Response
     */
    public function index(PaginatorService $paginator, QueryParams $params): Response
    {
        $categories = $this->manager->em()->getRepository(ProductSubCategory::class)->findByQuery($params);

        return $this->renderTemplate('pages/commerce/product/sub_category/index.html.twig', ['categories' => $paginator->paginate($categories)]);
    }

    /**
     * @Route("/create-with-category/{categoryId}", name="create_with_category")
     * @ParamConverter("category", options={"id" = "categoryId"})
     *
     * @param Request         $request
     * @param ProductCategory $category
     *
     * @return RedirectResponse|Response
     *
     * @throws ModelValidationException
     */
    public function createWithCategory(Request $request, ProductCategory $category): Response
    {
        $subCategory = (new ProductSubCategory())
            ->setId(Identifier::uuid())
            ->setProductCategory($category)
            ->setMeta((new ProductSubCategoryMeta())->setId(Identifier::uuid()));

        $form = $this->createForm(ProductSubCategoryType::class, $subCategory);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->manager->save($form->getData());

            return $this->redirectToRoute('admin_product_sub_category_edit', ['id' => $subCategory->getId()]);
        }

        return $this->renderTemplate('pages/commerce/product/sub_category/create_sub_category.html.twig', ['form' => $form->createView()]);
    }

    /**
     * @Route("/create", name="create")
     *
     * @param Request $request
     *
     * @return RedirectResponse|Response
     *
     * @throws ModelValidationException
     */
    public function create(Request $request): Response
    {
        $subCategory = (new ProductSubCategory())
            ->setId(Identifier::uuid())
            ->setMeta((new ProductSubCategoryMeta())->setId(Identifier::uuid()));

        $form = $this->createForm(ProductSubCategoryType::class, $subCategory);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->manager->save($form->getData());

            return $this->redirectToRoute('admin_product_sub_category_edit', ['id' => $subCategory->getId()]);
        }

        return $this->renderTemplate('pages/commerce/product/sub_category/create_sub_category.html.twig', ['form' => $form->createView()]);
    }

    /**
     * @Route("/edit/{id}", name="edit")
     *
     * @param Request            $request
     * @param ProductSubCategory $subCategory
     *
     * @return RedirectResponse|Response
     *
     * @throws ModelValidationException
     */
    public function edit(Request $request, ProductSubCategory $subCategory): Response
    {
        $form = $this->createForm(ProductSubCategoryType::class, $subCategory);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->manager->save($form->getData());

            return $this->redirectToRoute('admin_product_category_index');
        }

        return $this->renderTemplate('pages/commerce/product/sub_category/edit_sub_category.html.twig', [
            'form' => $form->createView(),
            'category' => $subCategory
        ]);
    }


    /**
     * @Route("/remove/{id}", name="remove")
     *
     * @param ProductSubCategory $subCategory
     *
     * @return RedirectResponse
     */
    public function remove(ProductSubCategory $subCategory): Response
    {
        $this->manager->remove($subCategory);

        return $this->redirectToRoute('admin_product_sub_category_index');
    }

    /**
     * @Route("/edit/{id}/meta-translation", name="create_meta_translation")
     *
     * @param ProductSubCategory $subCategory
     * @param Request            $request
     *
     * @return RedirectResponse|Response
     *
     * @throws ModelValidationException
     */
    public function createMetaTranslation(Request $request, ProductSubCategory $subCategory): Response
    {
        $translation = (new ProductSubCategoryMetaTranslation())
            ->setId(Identifier::uuid())
            ->setMeta($subCategory->getMeta());

        $form = $this->createForm(ProductSubCategoryMetaTranslationType::class, $translation);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->manager->save($form->getData());

            return $this->redirectToRoute('admin_product_sub_category_edit', ['id' => $subCategory->getId()]);
        }

        return $this->renderTemplate('pages/commerce/product/sub_category/create_meta_translation.html.twig', ['form' => $form->createView()]);
    }

    /**
     * @Route("/edit/{id}/meta-translation/edit/{transId}", name="edit_meta_translation")
     * @ParamConverter("translation", options={"id" = "transId"})
     *
     * @param Request                           $request
     * @param ProductSubCategory                $subCategory
     * @param ProductSubCategoryMetaTranslation $translation
     *
     * @return RedirectResponse|Response
     *
     * @throws ModelValidationException
     */
    public function editMetaTranslation(
        Request $request,
        ProductSubCategory $subCategory,
        ProductSubCategoryMetaTranslation $translation
    ): Response {
        $form = $this->createForm(ProductSubCategoryMetaTranslationType::class, $translation);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->manager->save($form->getData());

            return $this->redirectToRoute('admin_product_sub_category_edit', ['id' => $subCategory->getId()]);
        }

        return $this->renderTemplate('pages/commerce/product/sub_category/edit_meta_translation.html.twig', ['form' => $form->createView()]);
    }

    /**
     * @Route("/edit/{id}/meta-translation/remove/{transId}", name="remove_meta_translation")
     * @ParamConverter("translation", options={"id" = "transId"})
     *
     * @param ProductSubCategory                $subCategory
     * @param ProductSubCategoryMetaTranslation $translation
     *
     * @return RedirectResponse
     */
    public function removeMetaTranslation(ProductSubCategory $subCategory, ProductSubCategoryMetaTranslation $translation): Response
    {
        $this->manager->remove($translation);

        return $this->redirectToRoute('admin_sub_category_edit', ['id' => $subCategory->getId()]);
    }

    /**
     * @Route("/edit/{id}/translation", name="create_translation")
     *
     * @param Request            $request
     * @param ProductSubCategory $subCategory
     *
     * @return RedirectResponse|Response
     *
     * @throws ModelValidationException
     */
    public function createTranslation(Request $request, ProductSubCategory $subCategory): Response
    {
        $translation = (new ProductSubCategoryTranslation())
            ->setId(Identifier::uuid())
            ->setProductSubCategory($subCategory);

        $form = $this->createForm(ProductSubCategoryTranslationType::class, $translation);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->manager->save($form->getData());

            return $this->redirectToRoute('admin_product_sub_category_edit', ['id' => $subCategory->getId()]);
        }

        return $this->renderTemplate('pages/commerce/product/sub_category/create_translation.html.twig', ['form' => $form->createView()]);
    }

    /**
     * @Route("/edit/{id}/translation/edit/{transId}", name="edit_translation")
     * @ParamConverter("translation", options={"id" = "transId"})
     *
     * @param Request                       $request
     * @param ProductSubCategory            $subCategory
     * @param ProductSubCategoryTranslation $translation
     *
     * @return RedirectResponse|Response
     *
     * @throws ModelValidationException
     */
    public function editTranslation(
        Request $request,
        ProductSubCategory $subCategory,
        ProductSubCategoryTranslation $translation
    ): Response {
        $form = $this->createForm(ProductSubCategoryTranslationType::class, $translation);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->manager->save($form->getData());

            return $this->redirectToRoute('admin_product_sub_category_edit', ['id' => $subCategory->getId()]);
        }

        return $this->renderTemplate('pages/commerce/product/sub_category/edit_translation.html.twig', ['form' => $form->createView()]);
    }

    /**
     * @Route("/edit/{id}/translation/remove/{transId}", name="remove_translation")
     * @ParamConverter("translation", options={"id" = "transId"})
     *
     * @param ProductSubCategory            $subCategory
     * @param ProductSubCategoryTranslation $translation
     *
     * @return RedirectResponse
     */
    public function removeTranslation(ProductSubCategory $subCategory, ProductSubCategoryTranslation $translation): Response
    {
        $this->manager->remove($translation);

        return $this->redirectToRoute('admin_product_sub_category_edit', ['id' => $subCategory->getId()]);
    }
}