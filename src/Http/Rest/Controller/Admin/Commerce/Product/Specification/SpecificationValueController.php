<?php

namespace App\Http\Rest\Controller\Admin\Commerce\Product\Specification;

use App\Application\Util\Identifier;
use App\Database\Domain\Entity\Commerce\Product\Specification\Specification;
use App\Database\Domain\Entity\Commerce\Product\Specification\SpecificationValue;
use App\Database\Domain\Entity\Commerce\Product\Specification\SpecificationValueTranslation;
use App\Database\Domain\Exception\ModelValidationException;
use App\Http\Rest\Controller\Admin\AbstractAdminController;
use App\Http\Rest\Response\Form\Types\Commerce\Product\Specification\SpecificationValueTranslationType;
use App\Http\Rest\Response\Form\Types\Commerce\Product\Specification\SpecificationValueType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 *
 * @Route("/specification/{id}/values", name="specification_values_")
 */
class SpecificationValueController extends AbstractAdminController
{
    /**
     * @Route("/create", name="create")
     *
     * @param Request       $request
     * @param Specification $specification
     *
     * @return RedirectResponse|Response
     *
     * @throws ModelValidationException
     */
    public function create(Request $request, Specification $specification): Response
    {
        $value = (new SpecificationValue())
            ->setId(Identifier::uuid())
            ->setSpecification($specification);

        $form = $this->createForm(SpecificationValueType::class, $value);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->manager->save($form->getData());

            return $this->redirectToRoute('admin_specification_edit', ['id' => $specification->getId()]);
        }

        return $this->renderTemplate('pages/commerce/product/specification/create_value.html.twig', ['form' => $form->createView()]);
    }

    /**
     * @Route("/edit/{valueId}", name="edit")
     * @ParamConverter("value", options={"id" = "valueId"})
     *
     * @param Request            $request
     * @param SpecificationValue $value
     *
     * @return RedirectResponse|Response
     *
     * @throws ModelValidationException
     */
    public function edit(Request $request, SpecificationValue $value): Response
    {
        $form = $this->createForm(SpecificationValueType::class, $value);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->manager->save($form->getData());

            return $this->redirectToRoute('admin_specification_edit', ['id' => $value->getSpecification()->getId()]);
        }

        return $this->renderTemplate('pages/commerce/product/specification/edit_value.html.twig', [
            'form'  => $form->createView(),
            'value' => $value
        ]);
    }

    /**
     * @Route("/remove/{valueId}", name="remove")
     * @ParamConverter("value", options={"id" = "valueId"})
     *
     * @param SpecificationValue $value
     *
     * @return RedirectResponse
     */
    public function remove(SpecificationValue $value): Response
    {
        $this->manager->remove($value);

        return $this->redirectToRoute('admin_specification_edit', ['id' => $value->getSpecification()->getId()]);
    }

    /**
     * @Route("/edit/{valueId}/create-translation", name="create_translation")
     * @ParamConverter("value", options={"id" = "valueId"})
     *
     * @param Request            $request
     * @param SpecificationValue $value
     *
     * @return RedirectResponse|Response
     *
     * @throws ModelValidationException
     */
    public function createTranslation(Request $request, SpecificationValue $value): Response
    {
        $translation = (new SpecificationValueTranslation())
            ->setId(Identifier::uuid())
            ->setBelongedValue($value);

        $form = $this->createForm(SpecificationValueTranslationType::class, $translation);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->manager->save($form->getData());

            return $this->redirectToRoute('admin_specification_values_edit', [
                'id'      => $value->getSpecification()->getId(),
                'valueId' => $value->getId()
            ]);
        }

        return $this->renderTemplate('pages/commerce/product/specification/create_value_translation.html.twig', ['form' => $form->createView()]);
    }

    /**
     * @Route("/edit/{valueId}/edit-translation/{transId}", name="edit_translation")
     * @ParamConverter("value", options={"id" = "valueId"})
     * @ParamConverter("translation", options={"id" = "transId"})
     *
     * @param Request                       $request
     * @param SpecificationValue            $value
     * @param SpecificationValueTranslation $translation
     *
     * @return RedirectResponse|Response
     *
     * @throws ModelValidationException
     */
    public function editTranslation(
        Request $request,
        SpecificationValue $value,
        SpecificationValueTranslation $translation
    ): Response {
        $form = $this->createForm(SpecificationValueTranslationType::class, $translation);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->manager->save($form->getData());

            return $this->redirectToRoute('admin_specification_values_edit', [
                'id'      => $value->getSpecification()->getId(),
                'valueId' => $value->getId()
            ]);
        }

        return $this->renderTemplate('pages/commerce/product/specification/edit_value_translation.html.twig', ['form' => $form->createView()]);
    }

    /**
     * @Route("/edit/{valueId}/remove-value-translation/{transId}", name="remove_translation")
     * @ParamConverter("value", options={"id" = "valueId"})
     * @ParamConverter("translation", options={"id" = "transId"})
     *
     * @param SpecificationValue            $value
     * @param SpecificationValueTranslation $translation
     *
     * @return RedirectResponse
     */
    public function removeTranslation(SpecificationValue $value, SpecificationValueTranslation $translation): Response
    {
        $this->manager->remove($translation);

        return $this->redirectToRoute('admin_specification_values_edit', [
            'id'      => $value->getSpecification()->getId(),
            'valueId' => $value->getId()
        ]);
    }
}