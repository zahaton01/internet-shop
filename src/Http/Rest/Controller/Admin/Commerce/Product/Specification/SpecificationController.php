<?php

namespace App\Http\Rest\Controller\Admin\Commerce\Product\Specification;

use App\Application\Util\Identifier;
use App\Database\Domain\Entity\Commerce\Product\Category\ProductCategory;
use App\Database\Domain\Entity\Commerce\Product\SubCategory\ProductSubCategory;
use App\Database\Domain\Model\Query\Params\QueryParams;
use App\Http\Rest\Controller\Admin\AbstractAdminController;
use App\Database\Domain\Entity\Commerce\Product\Specification\Specification;
use App\Database\Domain\Entity\Commerce\Product\Specification\SpecificationTranslation;
use App\Database\Domain\Entity\Commerce\Product\Specification\SpecificationValue;
use App\Database\Domain\Entity\Commerce\Product\Specification\SpecificationValueTranslation;
use App\Database\Domain\Exception\ModelValidationException;
use App\Http\Rest\Response\Form\Types\Commerce\Product\Specification\SpecificationTranslationType;
use App\Http\Rest\Response\Form\Types\Commerce\Product\Specification\SpecificationType;
use App\Http\Rest\Response\Form\Types\Commerce\Product\Specification\SpecificationValueTranslationType;
use App\Http\Rest\Response\Form\Types\Commerce\Product\Specification\SpecificationValueType;
use App\Database\Domain\Service\PaginatorService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 *
 * @Route("/specification", name="specification_")
 */
class SpecificationController extends AbstractAdminController
{
    /**
     * @Route("", name="index")
     *
     * @param QueryParams      $params
     * @param PaginatorService $paginator
     *
     * @return Response
     */
    public function index(QueryParams $params, PaginatorService $paginator): Response
    {
        $specifications = $this->manager->em()->getRepository(Specification::class)->findByQuery($params);
        $categories     = $this->manager->em()->getRepository(ProductCategory::class)->findAll();
        $subCategories  = $this->manager->em()->getRepository(ProductSubCategory::class)->findAll();

        return $this->renderTemplate('pages/commerce/product/specification/index.html.twig', [
            'specifications'        => $paginator->paginate($specifications),
            'possibleCategories'    => $categories,
            'possibleSubCategories' => $subCategories,
            'queryParams'           => $params
        ]);
    }

    /**
     * @Route("/create", name="create")
     *
     * @param Request $request
     *
     * @return RedirectResponse|Response
     *
     * @throws ModelValidationException
     */
    public function create(Request $request): Response
    {
        $specification = (new Specification())
            ->setIsEnabled(true)
            ->setId(Identifier::uuid());

        $form = $this->createForm(SpecificationType::class, $specification);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->manager->save($form->getData());

            return $this->redirectToRoute('admin_specification_index');
        }

        return $this->renderTemplate('pages/commerce/product/specification/create.html.twig', ['form' => $form->createView()]);
    }

    /**
     * @Route("/edit/{id}", name="edit")
     *
     * @param Request       $request
     * @param Specification $specification
     *
     * @return RedirectResponse|Response
     *
     * @throws ModelValidationException
     */
    public function edit(Request $request, Specification $specification): Response
    {
        $form = $this->createForm(SpecificationType::class, $specification);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->manager->save($form->getData());

            return $this->redirectToRoute('admin_specification_index');
        }

        return $this->renderTemplate('pages/commerce/product/specification/edit.html.twig', [
            'form'          => $form->createView(),
            'specification' => $specification
        ]);
    }

    /**
     * @Route("/remove/{id}", name="remove")
     *
     * @param Specification $specification
     *
     * @return RedirectResponse
     */
    public function remove(Specification $specification): Response
    {
        $this->manager->remove($specification);

        return $this->redirectToRoute('admin_specification_index');
    }

    /**
     * @Route("/edit/{id}/create-translation", name="create_translation")
     *
     * @param Request       $request
     * @param Specification $specification
     *
     * @return RedirectResponse|Response
     *
     * @throws ModelValidationException
     */
    public function createTranslation(Request $request, Specification $specification): Response
    {
        $translation = (new SpecificationTranslation())
            ->setId(Identifier::uuid())
            ->setSpecification($specification);

        $form = $this->createForm(SpecificationTranslationType::class, $translation);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->manager->save($form->getData());

            return $this->redirectToRoute('admin_specification_edit', ['id' => $specification->getId()]);
        }

        return $this->renderTemplate('pages/commerce/product/specification/create_translation.html.twig', ['form' => $form->createView()]);
    }

    /**
     * @Route("/edit/{id}/edit-translation/{transId}", name="edit_translation")
     * @ParamConverter("translation", options={"id" = "transId"})
     *
     * @param Request                  $request
     * @param Specification            $specification
     * @param SpecificationTranslation $translation
     *
     * @return RedirectResponse|Response
     *
     * @throws ModelValidationException
     */
    public function editTranslation(
        Request $request,
        Specification $specification,
        SpecificationTranslation $translation
    ): Response {
        $form = $this->createForm(SpecificationTranslationType::class, $translation);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->manager->save($form->getData());

            return $this->redirectToRoute('admin_specification_edit', ['id' => $specification->getId()]);
        }

        return $this->renderTemplate('pages/commerce/product/specification/edit_translation.html.twig', ['form' => $form->createView()]);
    }

    /**
     * @Route("/edit/{id}/remove-translation/{transId}", name="remove_translation")
     * @ParamConverter("translation", options={"id" = "transId"})
     *
     * @param Specification            $specification
     * @param SpecificationTranslation $translation
     *
     * @return RedirectResponse
     */
    public function removeTranslation(Specification $specification, SpecificationTranslation $translation): Response
    {
        $this->manager->remove($translation);

        return $this->redirectToRoute('admin_specification_edit', ['id' => $specification->getId()]);
    }
}
