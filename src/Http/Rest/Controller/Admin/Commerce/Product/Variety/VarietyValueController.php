<?php

namespace App\Http\Rest\Controller\Admin\Commerce\Product\Variety;

use App\Application\Util\Identifier;
use App\Database\Domain\Entity\Commerce\Product\Variety\Variety;
use App\Database\Domain\Entity\Commerce\Product\Variety\VarietyValue;
use App\Database\Domain\Entity\Commerce\Product\Variety\VarietyValueTranslation;
use App\Database\Domain\Exception\ModelValidationException;
use App\Http\Rest\Controller\Admin\AbstractAdminController;
use App\Http\Rest\Response\Form\Types\Commerce\Product\Variety\VarietyValueTranslationType;
use App\Http\Rest\Response\Form\Types\Commerce\Product\Variety\VarietyValueType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 *
 * @Route("/variety/{id}/values", name="variety_values_")
 */
class VarietyValueController extends AbstractAdminController
{
    /**
     * @Route("/create", name="create")
     *
     * @param Request $request
     * @param Variety $variety
     *
     * @return RedirectResponse|Response
     *
     * @throws ModelValidationException
     */
    public function create(Request $request, Variety $variety): Response
    {
        $value = (new VarietyValue())
            ->setId(Identifier::uuid())
            ->setVariety($variety);

        $form = $this->createForm(VarietyValueType::class, $value);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->manager->save($form->getData());

            return $this->redirectToRoute('admin_variety_index');
        }

        return $this->renderTemplate('pages/commerce/product/variety/create_value.html.twig', ['form' => $form->createView()]);
    }

    /**
     * @Route("/edit/{valueId}", name="edit")
     * @ParamConverter("value", options={"id" = "valueId"})
     *
     * @param Request $request
     * @param VarietyValue   $value
     *
     * @return RedirectResponse|Response
     *
     * @throws ModelValidationException
     */
    public function edit(Request $request, VarietyValue $value): Response
    {
        $form = $this->createForm(VarietyValueType::class, $value);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->manager->save($form->getData());

            return $this->redirectToRoute('admin_variety_index');
        }

        return $this->renderTemplate('pages/commerce/product/variety/edit_value.html.twig', [
            'form'  => $form->createView(),
            'value' => $value
        ]);
    }

    /**
     * @Route("/remove/{valueId}", name="remove")
     * @ParamConverter("value", options={"id" = "valueId"})
     *
     * @param VarietyValue $value
     *
     * @return RedirectResponse
     */
    public function remove(VarietyValue $value): Response
    {
        $this->manager->remove($value);

        return $this->redirectToRoute('admin_variety_index');
    }

    /**
     * @Route("/edit/{valueId}/create-translation", name="create_translation")
     * @ParamConverter("value", options={"id" = "valueId"})
     *
     * @param Request $request
     * @param VarietyValue   $value
     *
     * @return RedirectResponse|Response
     *
     * @throws ModelValidationException
     */
    public function createTranslation(Request $request, VarietyValue $value): Response
    {
        $translation = (new VarietyValueTranslation())
            ->setId(Identifier::uuid())
            ->setBelongedValue($value);

        $form = $this->createForm(VarietyValueTranslationType::class, $translation);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->manager->save( $form->getData());

            return $this->redirectToRoute('admin_variety_values_edit', [
                'id'      => $value->getVariety()->getId(),
                'valueId' => $value->getId()
            ]);
        }

        return $this->renderTemplate('pages/commerce/product/variety/create_value_translation.html.twig', ['form' => $form->createView()]);
    }

    /**
     * @Route("/edit/{valueId}/edit-translation/{transId}", name="edit_translation")
     * @ParamConverter("value", options={"id" = "valueId"})
     * @ParamConverter("translation", options={"id" = "transId"})
     *
     * @param Request          $request
     * @param VarietyValue            $value
     * @param VarietyValueTranslation $translation
     *
     * @return RedirectResponse|Response
     *
     * @throws ModelValidationException
     */
    public function editTranslation(
        Request $request,
        VarietyValue $value,
        VarietyValueTranslation $translation
    ): Response {
        $form = $this->createForm(VarietyValueTranslationType::class, $translation);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->manager->save($form->getData());

            return $this->redirectToRoute('admin_variety_values_edit', [
                'id'      => $value->getVariety()->getId(),
                'valueId' => $value->getId()
            ]);
        }

        return $this->renderTemplate('pages/commerce/product/variety/edit_value_translation.html.twig', ['form' => $form->createView()]);
    }

    /**
     * @Route("/edit/{valueId}/remove-value-translation/{transId}", name="remove_translation")
     * @ParamConverter("value", options={"id" = "valueId"})
     * @ParamConverter("translation", options={"id" = "transId"})
     *
     * @param VarietyValue            $value
     * @param VarietyValueTranslation $translation
     *
     * @return RedirectResponse
     */
    public function removeTranslation(VarietyValue $value, VarietyValueTranslation $translation): Response
    {
        $this->manager->remove($translation);

        return $this->redirectToRoute('admin_variety_values_edit', [
            'id'      => $value->getVariety()->getId(),
            'valueId' => $value->getId()
        ]);
    }
}