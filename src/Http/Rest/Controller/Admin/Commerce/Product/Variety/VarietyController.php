<?php

namespace App\Http\Rest\Controller\Admin\Commerce\Product\Variety;

use App\Application\Util\Identifier;
use App\Database\Domain\Model\Query\Params\QueryParams;
use App\Http\Rest\Controller\Admin\AbstractAdminController;
use App\Database\Domain\Entity\Commerce\Product\Variety\VarietyValue;
use App\Database\Domain\Entity\Commerce\Product\Variety\VarietyValueTranslation;
use App\Database\Domain\Entity\Commerce\Product\Variety\Variety;
use App\Database\Domain\Entity\Commerce\Product\Variety\VarietyTranslation;
use App\Database\Domain\Exception\ModelValidationException;
use App\Http\Rest\Response\Form\Types\Commerce\Product\Variety\VarietyTranslationType;
use App\Http\Rest\Response\Form\Types\Commerce\Product\Variety\VarietyType;
use App\Http\Rest\Response\Form\Types\Commerce\Product\Variety\VarietyValueTranslationType;
use App\Http\Rest\Response\Form\Types\Commerce\Product\Variety\VarietyValueType;
use App\Database\Domain\Service\PaginatorService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 *
 * @Route("/variety", name="variety_")
 */
class VarietyController extends AbstractAdminController
{
    /**
     * @Route("", name="index")
     *
     * @param QueryParams      $params
     * @param PaginatorService $paginator
     *
     * @return Response
     */
    public function index(QueryParams $params, PaginatorService $paginator): Response
    {
        $varieties = $this->manager->em()->getRepository(Variety::class)->findByQuery($params);

        return $this->renderTemplate('pages/commerce/product/variety/index.html.twig', ['varieties' => $paginator->paginate($varieties)]);
    }

    /**
     * @Route("/create", name="create")
     *
     * @param Request $request
     *
     * @return RedirectResponse|Response
     *
     * @throws ModelValidationException
     */
    public function create(Request $request): Response
    {
        $variety = (new Variety())->setId(Identifier::uuid());

        $form    = $this->createForm(VarietyType::class, $variety);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->manager->save($form->getData());

            return $this->redirectToRoute('admin_variety_index');
        }

        return $this->renderTemplate('pages/commerce/product/variety/create.html.twig', ['form' => $form->createView()]);
    }

    /**
     * @Route("/edit/{id}", name="edit")
     *
     * @param Request $request
     * @param Variety $variety
     *
     * @return RedirectResponse|Response
     *
     * @throws ModelValidationException
     */
    public function edit(Request $request, Variety $variety): Response
    {
        $form = $this->createForm(VarietyType::class, $variety);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->manager->save($form->getData());

            return $this->redirectToRoute('admin_variety_index');
        }

        return $this->renderTemplate('pages/commerce/product/variety/edit.html.twig', [
            'form'    => $form->createView(),
            'variety' => $variety
        ]);
    }

    /**
     * @Route("/remove/{id}", name="remove")
     *
     * @param Variety $variety
     *
     * @return RedirectResponse
     */
    public function remove(Variety $variety): Response
    {
        $this->manager->remove($variety);

        return $this->redirectToRoute('admin_variety_index');
    }

    /**
     * @Route("/edit/{id}/create-translation", name="create_translation")
     *
     * @param Request $request
     * @param Variety $variety
     *
     * @return RedirectResponse|Response
     *
     * @throws ModelValidationException
     */
    public function createTranslation(Request $request, Variety $variety): Response
    {
        $translation = (new VarietyTranslation())
            ->setId(Identifier::uuid())
            ->setVariety($variety);

        $form = $this->createForm(VarietyTranslationType::class, $translation);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->manager->save($form->getData());

            return $this->redirectToRoute('admin_variety_edit', ['id' => $variety->getId()]);
        }

        return $this->renderTemplate('pages/commerce/product/variety/create_translation.html.twig', ['form' => $form->createView()]);
    }

    /**
     * @Route("/edit/{id}/edit-translation/{transId}", name="edit_translation")
     * @ParamConverter("translation", options={"id" = "transId"})
     *
     * @param Request            $request
     * @param Variety            $variety
     * @param VarietyTranslation $translation
     *
     * @return RedirectResponse|Response
     *
     * @throws ModelValidationException
     */
    public function editTranslation(
        Request $request,
        Variety $variety,
        VarietyTranslation $translation
    ): Response {
        $form = $this->createForm(VarietyTranslationType::class, $translation);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->manager->save($form->getData());

            return $this->redirectToRoute('admin_variety_edit', ['id' => $variety->getId()]);
        }

        return $this->renderTemplate('pages/commerce/product/variety/edit_translation.html.twig', ['form' => $form->createView()]);
    }

    /**
     * @Route("/edit/{id}/remove-translation/{transId}", name="remove_translation")
     * @ParamConverter("translation", options={"id" = "transId"})
     *
     * @param Variety            $variety
     * @param VarietyTranslation $translation
     *
     * @return RedirectResponse
     */
    public function removeTranslation(Variety $variety, VarietyTranslation $translation): Response
    {
        $this->manager->remove($translation);

        return $this->redirectToRoute('admin_variety_edit', ['id' => $variety->getId()]);
    }
}