<?php

namespace App\Http\Rest\Controller\Admin\Commerce\Product;

use App\Application\Core\ClassWrappers\DateTime;
use App\Application\Util\Generator\SlugGenerator;
use App\Application\Util\Identifier;
use App\Database\Domain\Entity\Commerce\Product\Category\ProductCategory;
use App\Database\Domain\Entity\Commerce\Product\SubCategory\ProductSubCategory;
use App\Database\Domain\Model\Query\Params\QueryParams;
use App\Http\Rest\Controller\Admin\AbstractAdminController;
use App\Database\Domain\Entity\Commerce\Product\Media\ProductImage;
use App\Database\Domain\Entity\Commerce\Product\Media\ProductMedia;
use App\Database\Domain\Entity\Commerce\Product\Meta\ProductMeta;
use App\Database\Domain\Entity\Commerce\Product\Meta\ProductMetaTranslation;
use App\Database\Domain\Entity\Commerce\Product\Product;
use App\Database\Domain\Entity\Commerce\Product\ProductPromo;
use App\Database\Domain\Entity\Commerce\Product\ProductTranslation;
use App\Database\Domain\Entity\Commerce\Product\Specification\ProductSpecification;
use App\Database\Domain\Entity\Commerce\Product\Variety\ProductVariety;
use App\Database\Domain\Exception\ModelValidationException;
use App\Http\Rest\Response\Form\Types\Commerce\Product\ProductMetaTranslationType;
use App\Http\Rest\Response\Form\Types\Commerce\Product\MultipleProductEditType;
use App\Http\Rest\Response\Form\Types\Commerce\Product\ProductSpecificationEditType;
use App\Http\Rest\Response\Form\Types\Commerce\Product\ProductSpecificationType;
use App\Http\Rest\Response\Form\Types\Commerce\Product\ProductTranslationType;
use App\Http\Rest\Response\Form\Types\Commerce\Product\ProductType;
use App\Database\Domain\Model\EntityState\Availability;
use App\Database\Domain\Service\PaginatorService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 *
 * @Route("/product", name="product_")
 */
class ProductController extends AbstractAdminController
{
    /**
     * @Route("", name="index")
     *
     * @param PaginatorService $paginator
     * @param QueryParams $params
     *
     * @return Response
     */
    public function index(PaginatorService $paginator, QueryParams $params): Response
    {
        $products      = $this->manager->em()->getRepository(Product::class)->findByQuery($params);
        $categories    = $this->manager->em()->getRepository(ProductCategory::class)->findAll();
        $subCategories = $this->manager->em()->getRepository(ProductSubCategory::class)->findAll();

        return $this->renderTemplate('pages/commerce/product/index.html.twig', [
            'products'              => $paginator->paginate($products),
            'possibleCategories'    => $categories,
            'possibleSubCategories' => $subCategories,
            'queryParams'           => $params
        ]);
    }

    /**
     * @Route("/create", name="create")
     *
     * @param Request $request
     *
     * @return RedirectResponse|Response
     *
     * @throws ModelValidationException
     */
    public function create(Request $request): Response
    {
        $product = (new Product())
            ->setId(Identifier::uuid())
            ->setMedia((new ProductMedia())->setId(Identifier::uuid()))
            ->setPromo((new ProductPromo())
                           ->setId(Identifier::uuid())
                           ->setSaleStatus(false)
                           ->setIsSpecialStatus(false))
            ->setAvailability(Availability::AVAILABLE)
            ->setIsVisible(true)
            ->setCreationDate(DateTime::now())
            ->setMeta((new ProductMeta())->setId(Identifier::uuid()));

        $form = $this->createForm(ProductType::class, $product);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->manager->save($form->getData());

            return $this->redirectToRoute('admin_product_edit', ['id' => $product->getId()]);
        }

        return $this->renderTemplate('pages/commerce/product/create.html.twig', ['form' => $form->createView()]);
    }

    /**
     * @Route("/edit/{id}", name="edit")
     *
     * @param Product $product
     * @param Request $request
     *
     * @return RedirectResponse|Response
     *
     * @throws ModelValidationException
     */
    public function edit(Request $request, Product $product): Response
    {
        $form = $this->createForm(ProductType::class, $product);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->manager->save($form->getData());

            return $this->redirectToRoute('admin_product_index');
        }

        return $this->renderTemplate('pages/commerce/product/edit.html.twig', [
            'form'    => $form->createView(),
            'product' => $product
        ]);
    }

    /**
     * @Route("/remove/{id}", name="remove")
     *
     * @param Product $product
     *
     * @return RedirectResponse
     */
    public function removeProduct(Product $product): Response
    {
        /** @var ProductImage $image */
        foreach ($product->getMedia()->getImages() as $image) {
            if (file_exists($image->getFullPath())) {
                unlink($image->getFullPath());
            }
        }

        $this->manager->remove($product);

        return $this->redirectToRoute('admin_product_index');
    }

    /**
     * @Route("/edit/{id}/meta-translation", name="create_meta_translation")
     *
     * @param Product $product
     * @param Request $request
     *
     * @return RedirectResponse|Response
     *
     * @throws ModelValidationException
     */
    public function createMetaTranslation(Request $request, Product $product): Response
    {
        $translation = (new ProductMetaTranslation())
            ->setId(Identifier::uuid())
            ->setMeta($product->getMeta());

        $form = $this->createForm(ProductMetaTranslationType::class, $translation);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->manager->save($form->getData());

            return $this->redirectToRoute('admin_product_edit', ['id' => $product->getId()]);
        }

        return $this->renderTemplate('pages/commerce/product/create_meta_translation.html.twig', ['form' => $form->createView()]);
    }

    /**
     * @Route("/edit/{id}/meta-translation/edit/{transId}", name="edit_meta_translation")
     * @ParamConverter("translation", options={"id" = "transId"})
     *
     * @param Request                $request
     * @param Product                $product
     * @param ProductMetaTranslation $translation
     *
     * @return RedirectResponse|Response
     *
     * @throws ModelValidationException
     */
    public function editMetaTranslation(
        Request $request,
        Product $product,
        ProductMetaTranslation $translation
    ): Response {
        $form = $this->createForm(ProductMetaTranslationType::class, $translation);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->manager->save($form->getData());

            return $this->redirectToRoute('admin_product_edit', ['id' => $product->getId()]);
        }

        return $this->renderTemplate('pages/commerce/product/edit_meta_translation.html.twig', ['form' => $form->createView()]);
    }

    /**
     * @Route("/edit/{id}/meta-translation/remove/{transId}", name="remove_meta_translation")
     * @ParamConverter("translation", options={"id" = "transId"})
     *
     * @param Product                $product
     * @param ProductMetaTranslation $translation
     *
     * @return RedirectResponse
     */
    public function removeMetaTranslation(Product $product, ProductMetaTranslation $translation): Response
    {
        $this->manager->remove($translation);

        return $this->redirectToRoute('admin_product_edit', ['id' => $product->getId()]);
    }

    /**
     * @Route("/edit/{id}/translation", name="create_translation")
     *
     * @param Product $product
     * @param Request $request
     *
     * @return RedirectResponse|Response
     *
     * @throws ModelValidationException
     */
    public function createTranslation(Request $request, Product $product): Response
    {
        $translation = (new ProductTranslation())
            ->setId(Identifier::uuid())
            ->setProduct($product);

        $form = $this->createForm(ProductTranslationType::class, $translation);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->manager->save($form->getData());

            return $this->redirectToRoute('admin_product_edit', ['id' => $product->getId()]);
        }

        return $this->renderTemplate('pages/commerce/product/create_translation.html.twig', ['form' => $form->createView()]);
    }

    /**
     * @Route("/edit/{id}/translation/edit/{transId}", name="edit_translation")
     * @ParamConverter("translation", options={"id" = "transId"})
     *
     * @param Request            $request
     * @param Product            $product
     * @param ProductTranslation $translation
     *
     * @return RedirectResponse|Response
     *
     * @throws ModelValidationException
     */
    public function editTranslation(Request $request, Product $product, ProductTranslation $translation): Response
    {
        $form = $this->createForm(ProductTranslationType::class, $translation);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->manager->save($form->getData());

            return $this->redirectToRoute('admin_product_edit', ['id' => $product->getId()]);
        }

        return $this->renderTemplate('pages/commerce/product/edit_translation.html.twig', ['form' => $form->createView()]);
    }

    /**
     * @Route("/edit/{id}/translation/remove/{transId}", name="remove_translation")
     * @ParamConverter("translation", options={"id" = "transId"})
     *
     * @param Product            $product
     * @param ProductTranslation $translation
     *
     * @return RedirectResponse
     */
    public function removeTranslation(Product $product, ProductTranslation $translation): Response
    {
        $this->manager->remove($translation);

        return $this->redirectToRoute('admin_product_edit', ['id' => $product->getId()]);
    }

    /**
     * @Route("/clone/{id}", name="clone")
     *
     * @param Product       $product
     * @param SlugGenerator $slugGenerator
     *
     * @return RedirectResponse
     *
     * @throws ModelValidationException
     */
    public function cloneProduct(Product $product, SlugGenerator $slugGenerator): Response
    {
        $cloned = clone $product;
        $cloned->setSlug($slugGenerator->fromString($cloned->getName(), Product::class));

        $this->manager->save($cloned);

        return $this->redirectToRoute('admin_product_index');
    }

    /**
     * @Route("/edit-multiple", name="edit_multiple")
     *
     * @param Request $request
     *
     * @return RedirectResponse|Response
     *
     * @throws ModelValidationException
     */
    public function editMultiple(Request $request): Response
    {
        $products = $this->manager->em()->getRepository(Product::class)->findByIds(explode(',', $request->query->get('products')));
        $form     = $this->createForm(MultipleProductEditType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var Product $product */
            foreach ($products as $product) {
                $product
                    ->setAvailability($form['availability']->getData())
                    ->setProductSubCategory($form['subCategory']->getData())
                    ->setProductCategory($form['category']->getData())
                    ->setIsVisible($form['isVisible']->getData());
            }

            $this->manager->save($products);

            return $this->redirectToRoute('admin_product_index');
        }

        return $this->renderTemplate('pages/commerce/product/mass_edit.html.twig', [
            'form'     => $form->createView(),
            'products' => $products
        ]);
    }

    /**
     * @Route("/remove-multiple", name="remove_multiple")
     *
     * @param Request $request
     *
     * @return Response
     */
    public function removeMultiple(Request $request): Response
    {
        $products = $this->manager->em()->getRepository(Product::class)->findByIds($request->get('products'));

        $this->manager->remove($products);

        return $this->redirectToRoute('admin_product_index');
    }
}
