<?php

namespace App\Http\Rest\Controller\Admin\Commerce\Product;

use App\Application\Util\Identifier;
use App\Database\Domain\Entity\Commerce\Product\Product;
use App\Database\Domain\Entity\Commerce\Product\Specification\ProductSpecification;
use App\Database\Domain\Exception\ModelValidationException;
use App\Http\Rest\Controller\Admin\AbstractAdminController;
use App\Http\Rest\Response\Form\Types\Commerce\Product\ProductSpecificationEditType;
use App\Http\Rest\Response\Form\Types\Commerce\Product\ProductSpecificationType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 *
 * @Route("/product/edit/{id}/specifications", name="product_specifications_")
 */
class ProductSpecificationController extends AbstractAdminController
{
    /**
     * @Route("/add", name="add")
     *
     * @param Product $product
     * @param Request $request
     *
     * @return RedirectResponse|Response
     *
     * @throws ModelValidationException
     */
    public function add(Request $request, Product $product): Response
    {
        $productSpecification = (new ProductSpecification())
            ->setId(Identifier::uuid())
            ->setProduct($product);

        $form = $this->createForm(ProductSpecificationType::class, $productSpecification);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->manager->save($productSpecification);

            return $this->redirectToRoute('admin_product_specifications_edit', [
                'id'              => $product->getId(),
                'specificationId' => $productSpecification->getId()
            ]);
        }

        return $this->renderTemplate('pages/commerce/product/add_product_specification.html.twig', ['form' => $form->createView()]);
    }

    /**
     * @Route("/edit/{specificationId}", name="edit")
     * @ParamConverter("productSpecification", options={"id" = "specificationId"})
     *
     * @param Request              $request
     * @param Product              $product
     * @param ProductSpecification $productSpecification
     *
     * @return RedirectResponse|Response
     *
     * @throws ModelValidationException
     */
    public function edit(
        Request $request,
        Product $product,
        ProductSpecification $productSpecification
    ): Response {
        $form = $this->createForm(ProductSpecificationEditType::class,
                                  $productSpecification, ['specification_id' => $productSpecification->getSpecification()->getId()]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->manager->save($productSpecification);

            return $this->redirectToRoute('admin_product_edit', ['id' => $product->getId()]);
        }

        return $this->renderTemplate('pages/commerce/product/edit_product_specification.html.twig', ['form' => $form->createView()]);
    }

    /**
     * @Route("/remove/{specificationId}", name="remove")
     * @ParamConverter("productSpecification", options={"id" = "specificationId"})
     *
     * @param Product              $product
     * @param ProductSpecification $productSpecification
     *
     * @return RedirectResponse
     */
    public function remove(Product $product, ProductSpecification $productSpecification): Response
    {
        $this->manager->remove($productSpecification);

        return $this->redirectToRoute('admin_product_edit', ['id' => $product->getId()]);
    }
}