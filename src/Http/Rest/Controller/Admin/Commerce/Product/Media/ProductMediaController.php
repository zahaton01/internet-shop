<?php

namespace App\Http\Rest\Controller\Admin\Commerce\Product\Media;

use App\Application\Util\Identifier;
use App\Database\Domain\Entity\Media;
use App\Http\Rest\Controller\Admin\AbstractAdminController;
use App\Database\Domain\Entity\Commerce\Product\Media\ProductImage;
use App\Database\Domain\Entity\Commerce\Product\Product;
use App\Database\Domain\Exception\ModelValidationException;
use App\Http\Rest\Response\Form\Types\Commerce\Product\Media\LocalImageType;
use App\Http\Rest\Response\Form\Types\Commerce\Product\Media\RemoteImageType;
use App\Application\Libs\Component\FileManager\UploadManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 *
 * @Route("/product/{id}/media", name="product_media_")
 */
class ProductMediaController extends AbstractAdminController
{
    /**
     * @Route("/add-local-image", name="add_local_image")
     *
     * @param Product       $product
     * @param Request       $request
     * @param UploadManager $uploadManager
     *
     * @return RedirectResponse|Response
     *
     * @throws ModelValidationException
     */
    public function addLocalImage(
        Request $request,
        Product $product,
        UploadManager $uploadManager
    ): Response {
        $image = (new ProductImage())
            ->setId(Identifier::uuid())
            ->setType(Media::LOCAL)
            ->setMedia($product->getMedia());

        $form = $this->createForm(LocalImageType::class, $image);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var ProductImage $image */
            $image = $form->getData();
            /** @var UploadedFile $file */
            $file  = $form['attachment']->getData();

            $uploadedFile = $uploadManager->uploadProductImage($file);

            $image
                ->setAssetPath($uploadedFile->getAssetPath())
                ->setFullPath($uploadedFile->getFullPath());

            $this->manager->save($image);

            return $this->redirectToRoute('admin_product_edit', ['id' => $product->getId()]);
        }

        return $this->renderTemplate('pages/commerce/product/media/add_local_image.html.twig', [
            'form' => $form->createView(),
            'product' => $product
        ]);
    }

    /**
     * @Route("/add-remote-image", name="add_remote_image")
     *
     * @param Product $product
     * @param Request $request
     *
     * @return RedirectResponse|Response
     *
     * @throws ModelValidationException
     */
    public function addRemoteImage(Request $request, Product $product): Response
    {
        $image = (new ProductImage())
            ->setId(Identifier::uuid())
            ->setType(Media::REMOTE)
            ->setMedia($product->getMedia());

        $form = $this->createForm(RemoteImageType::class, $image);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->manager->save($form->getData());

            return $this->redirectToRoute('admin_product_edit', ['id' => $product->getId(),]);
        }

        return $this->renderTemplate('pages/commerce/product/media/add_remote_image.html.twig', ['form' => $form->createView()]);
    }

    /**
     * @Route("/remove-image/{imageId}", name="remove_image")
     * @ParamConverter("image", options={"id" = "imageId"})
     *
     * @param Product      $product
     * @param ProductImage $image
     *
     * @return RedirectResponse
     */
    public function removeImage(Product $product, ProductImage $image): Response
    {
        if (file_exists($image->getFullPath())) {
            unlink($image->getFullPath());
        }

        $this->manager->remove($image);

        return $this->redirectToRoute('admin_product_edit', ['id' => $product->getId()]);
    }

    /**
     * @Route("/upload-multiple", name="upload_multiple_images")
     *
     * @param Request       $request
     * @param Product       $product
     * @param UploadManager $uploadService
     *
     * @return RedirectResponse|Response
     *
     * @throws ModelValidationException
     */
    public function uploadMultiple(
        Request $request,
        Product $product,
        UploadManager $uploadService
    ): Response {
        if ($request->isMethod('post')) {
            foreach ($request->files->all() as $file) {
                if (empty($file)) continue;

                $uploaded = $uploadService->uploadProductImage($file);

                $image = (new ProductImage())->setId(Identifier::uuid());
                $image
                    ->setType(Media::LOCAL)
                    ->setMedia($product->getMedia())
                    ->setAssetPath($uploaded->getAssetPath())
                    ->setFullPath($uploaded->getFullPath());

                $this->manager->save($image);
            }

            return $this->redirectToRoute('admin_product_edit', ['id' => $product->getId()]);
        }

        return $this->renderTemplate('pages/commerce/product/media/upload_multiple.html.twig', ['product' => $product]);
    }
}
