<?php

namespace App\Http\Rest\Controller\Admin;

use App\Http\Rest\Controller\Admin\AbstractAdminController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 */
class AuthController extends AbstractAdminController
{
    /**
     * @Route("/", name="login")
     *
     * @param AuthenticationUtils $authenticationUtils
     *
     * @return Response
     */
    public function login(AuthenticationUtils $authenticationUtils): Response
    {
        return $this->renderTemplate('login.html.twig', ['error' => $authenticationUtils->getLastAuthenticationError()]);
    }

    /**
     * @return boolean
     *
     * @Route("/logout", name="logout")
     */
    public function logout()
    {
        return true;
    }
}
