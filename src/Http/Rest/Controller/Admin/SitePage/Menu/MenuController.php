<?php

namespace App\Http\Rest\Controller\Admin\SitePage\Menu;

use App\Database\Domain\Entity\Site\Menu\Menu;
use App\Http\Rest\Controller\Admin\AbstractAdminController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 *
 * @Route("/site/menu", name="site_menu_")
 */
class MenuController extends AbstractAdminController
{
    /**
     * @Route("", name="index")
     *
     * @return Response
     */
    public function index(): Response
    {
        $menus = $this->manager->em()->getRepository(Menu::class)->findAll();

        return $this->renderTemplate('pages/menu/index.html.twig', ['menus' => $menus]);
    }
}