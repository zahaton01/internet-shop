<?php

namespace App\Http\Rest\Controller\Admin\SitePage\Menu;

use App\Application\Util\Identifier;
use App\Database\Domain\Entity\Site\Menu\Menu;
use App\Database\Domain\Entity\Site\Menu\MenuElement\MenuElement;
use App\Database\Domain\Entity\Site\Menu\MenuElement\MenuElementTranslation;
use App\Database\Domain\Exception\ModelValidationException;
use App\Http\Rest\Controller\Admin\AbstractAdminController;
use App\Http\Rest\Response\Form\Types\Site\Menu\MenuElement\MenuElementLinkCollectionTranslationType;
use App\Http\Rest\Response\Form\Types\Site\Menu\MenuElement\MenuElementLinkCollectionType;
use App\Http\Rest\Response\Form\Types\Site\Menu\MenuElement\MenuElementLinkType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 *
 * @Route("/site/menu", name="site_menu_elements_")
 */
class MenuElementController extends AbstractAdminController
{
    /**
     * @Route("/{id}/create-link", name="create_link")
     *
     * @param Request $request
     * @param Menu    $menu
     *
     * @return Response
     *
     * @throws ModelValidationException
     */
    public function createLink(Request $request, Menu $menu): Response
    {
        $element = (new MenuElement())
            ->setId(Identifier::uuid())
            ->setIsEnabled(true)
            ->setType(MenuElement::LINK);

        $form = $this->createForm(MenuElementLinkType::class, $element);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $menu->addElement($form->getData());

            $this->manager->save($menu);

            return $this->redirectToRoute('admin_site_menu_index');
        }

        return $this->renderTemplate('pages/menu/menu_element/create.html.twig', ['form' => $form->createView()]);
    }

    /**
     * @Route("/edit-link/{id}", name="edit_link")
     *
     * @param Request     $request
     * @param MenuElement $element
     *
     * @return Response
     *
     * @throws ModelValidationException
     */
    public function editLink(Request $request, MenuElement $element): Response
    {
        $form = $this->createForm(MenuElementLinkType::class, $element);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->manager->save($form->getData());

            return $this->redirectToRoute('admin_site_menu_index');
        }

        return $this->renderTemplate('pages/menu/menu_element/edit.html.twig', [
            'form'        => $form->createView(),
            'menuElement' => $element
        ]);
    }

    /**
     * @Route("/{id}/create-link-collection", name="create_link_collection")
     *
     * @param Request $request
     * @param Menu    $menu
     *
     * @return Response
     *
     * @throws ModelValidationException
     */
    public function createLinkCollection(Request $request, Menu $menu): Response
    {
        $element = (new MenuElement())
            ->setId(Identifier::uuid())
            ->setIsEnabled(true)
            ->setType(MenuElement::LINK_COLLECTION);

        $form = $this->createForm(MenuElementLinkCollectionType::class, $element);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $menu->addElement($form->getData());

            $this->manager->save($menu);

            return $this->redirectToRoute('admin_site_menu_index');
        }

        return $this->renderTemplate('pages/menu/menu_element/create.html.twig', ['form' => $form->createView()]);
    }

    /**
     * @Route("/edit-link-collection/{id}", name="edit_link_collection")
     *
     * @param Request     $request
     * @param MenuElement $element
     *
     * @return Response
     *
     * @throws ModelValidationException
     */
    public function editLinkCollection(Request $request, MenuElement $element): Response
    {
        $form = $this->createForm(MenuElementLinkCollectionType::class, $element);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->manager->save($form->getData());

            return $this->redirectToRoute('admin_site_menu_index');
        }

        return $this->renderTemplate('pages/menu/menu_element/edit.html.twig', [
            'form'        => $form->createView(),
            'menuElement' => $element
        ]);
    }

    /**
     * @Route("/remove/{id}", name="remove")
     *
     * @param MenuElement $element
     *
     * @return Response
     */
    public function removeMenuElement(MenuElement $element): Response
    {
        $this->manager->remove($element);

        return $this->redirectToRoute('admin_site_menu_index');
    }

    /**
     * @Route("/edit-link-collection/{id}/create-translation", name="create_link_collection_translation")
     *
     * @param Request     $request
     * @param MenuElement $element
     *
     * @return Response
     *
     * @throws ModelValidationException
     */
    public function createLinkCollectionTranslation(Request $request, MenuElement $element): Response
    {
        $translation = (new MenuElementTranslation())
            ->setId(Identifier::uuid())
            ->setMenuElement($element);

        $form = $this->createForm(MenuElementLinkCollectionTranslationType::class, $translation);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->manager->save($form->getData());

            return $this->redirectToRoute('admin_site_menu_elements_edit_link_collection', ['id' => $element->getId()]);
        }

        return $this->renderTemplate('pages/menu/menu_element/create_translation.html.twig', ['form' => $form->createView()]);
    }

    /**
     * @Route("/edit-link-collection/{id}/edit-translation/{transId}", name="edit_link_collection_translation")
     * @ParamConverter("translation", options={"id" = "transId"})
     *
     * @param Request                $request
     * @param MenuElement            $element
     * @param MenuElementTranslation $translation
     *
     * @return Response
     *
     * @throws ModelValidationException
     */
    public function editLinkCollectionTranslation(
        Request $request,
        MenuElement $element,
        MenuElementTranslation $translation
    ): Response {
        $form = $this->createForm(MenuElementLinkCollectionTranslationType::class, $translation);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->manager->save($form->getData());

            return $this->redirectToRoute('admin_site_menu_elements_edit_link_collection', ['id' => $element->getId()]);
        }

        return $this->renderTemplate('pages/menu/menu_element/edit_translation.html.twig', ['form' => $form->createView()]);
    }

    /**
     * @Route("/edit/{id}/remove-translation/{transId}", name="remove_translation")
     * @ParamConverter("translation", options={"id" = "transId"})
     *
     * @param MenuElement            $element
     * @param MenuElementTranslation $translation
     *
     * @return Response
     */
    public function removeTranslation(MenuElement $element, MenuElementTranslation $translation): Response
    {
        $this->manager->remove($translation);

        return $this->redirectToRoute('admin_site_menu_elements_edit_link_collection', ['id' => $element->getId()]);
    }
}