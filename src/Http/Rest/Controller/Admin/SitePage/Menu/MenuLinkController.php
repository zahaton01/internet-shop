<?php

namespace App\Http\Rest\Controller\Admin\SitePage\Menu;

use App\Application\Util\Identifier;
use App\Database\Domain\Entity\Site\Menu\MenuLink\MenuLink;
use App\Database\Domain\Entity\Site\Menu\MenuLink\MenuLinkTranslation;
use App\Database\Domain\Exception\ModelValidationException;
use App\Http\Rest\Controller\Admin\AbstractAdminController;
use App\Http\Rest\Response\Form\Types\Site\Menu\MenuLink\MenuLinkExternalType;
use App\Http\Rest\Response\Form\Types\Site\Menu\MenuLink\MenuLinkInternalType;
use App\Http\Rest\Response\Form\Types\Site\Menu\MenuLink\MenuLinkTranslationType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 *
 * @Route("/site/menu/menu-links", name="site_menu_links_")
 */
class MenuLinkController extends AbstractAdminController
{
    /**
     * @Route("", name="index")
     *
     * @return Response
     */
    public function index(): Response
    {
        $links = $this->manager->em()->getRepository(MenuLink::class)->findAll();

        return $this->renderTemplate('pages/menu/menu_link/index.html.twig', ['links' => $links]);
    }

    /**
     * @Route("/create-internal", name="create_internal")
     *
     * @param Request $request
     *
     * @return Response
     *
     * @throws ModelValidationException
     */
    public function createInternalLink(Request $request): Response
    {
        $link = (new MenuLink())
            ->setId(Identifier::uuid())
            ->setType(MenuLink::INTERNAL);

        $form = $this->createForm(MenuLinkInternalType::class, $link);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->manager->save($form->getData());

            return $this->redirectToRoute('admin_site_menu_links_index');
        }

        return $this->renderTemplate('pages/menu/menu_link/create.html.twig', ['form' => $form->createView()]);
    }

    /**
     * @Route("/edit-internal/{id}", name="edit_internal")
     *
     * @param Request  $request
     * @param MenuLink $link
     *
     * @return Response
     *
     * @throws ModelValidationException
     */
    public function editInternal(Request $request, MenuLink $link): Response
    {
        $form = $this->createForm(MenuLinkInternalType::class, $link);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->manager->save($form->getData());

            return $this->redirectToRoute('admin_site_menu_links_index');
        }

        return $this->renderTemplate('pages/menu/menu_link/edit.html.twig', [
            'form' => $form->createView(),
            'link' => $link
        ]);
    }

    /**
     * @Route("/create-external", name="create_external")
     *
     * @param Request $request
     *
     * @return Response
     *
     * @throws ModelValidationException
     */
    public function createExternalLink(Request $request): Response
    {
        $link = (new MenuLink())
            ->setId(Identifier::uuid())
            ->setType(MenuLink::EXTERNAL);

        $form = $this->createForm(MenuLinkExternalType::class, $link);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->manager->save($form->getData());

            return $this->redirectToRoute('admin_site_menu_links_index');
        }

        return $this->renderTemplate('pages/menu/menu_link/create.html.twig', ['form' => $form->createView()]);
    }

    /**
     * @Route("/edit-external/{id}", name="edit_external")
     *
     * @param Request  $request
     * @param MenuLink $link
     *
     * @return Response
     *
     * @throws ModelValidationException
     */
    public function editExternal(Request $request, MenuLink $link): Response
    {
        $form = $this->createForm(MenuLinkExternalType::class, $link);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->manager->save($form->getData());

            return $this->redirectToRoute('admin_site_menu_links_index');
        }

        return $this->renderTemplate('pages/menu/menu_link/edit.html.twig', [
            'form' => $form->createView(),
            'link' => $link
        ]);
    }

    /**
     * @Route("/remove/{id}", name="remove")
     *
     * @param MenuLink $link
     *
     * @return Response
     */
    public function removeLink(MenuLink $link): Response
    {
        $this->manager->remove($link);

        return $this->redirectToRoute('admin_site_menu_links_index');
    }

    /**
     * @Route("/edit/{id}/create-translation", name="create_translation")
     *
     * @param Request  $request
     * @param MenuLink $link
     *
     * @return Response
     *
     * @throws ModelValidationException
     */
    public function createLinkTranslation(Request $request, MenuLink $link): Response
    {
        $translation = (new MenuLinkTranslation())
            ->setId(Identifier::uuid())
            ->setMenuLink($link);

        $form = $this->createForm(MenuLinkTranslationType::class, $translation);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->manager->save($form->getData());

            if ($link->isInternal()) {
                return $this->redirectToRoute('admin_site_menu_links_edit_internal', ['id' => $link->getId()]);
            }

            return $this->redirectToRoute('admin_site_menu_links_edit_external', ['id' => $link->getId()]);
        }

        return $this->renderTemplate('pages/menu/menu_link/create_translation.html.twig', ['form' => $form->createView()]);
    }

    /**
     * @Route("/edit/{id}/edit-translation/{transId}", name="edit_translation")
     * @ParamConverter("translation", options={"id" = "transId"})
     *
     * @param Request             $request
     * @param MenuLink            $link
     * @param MenuLinkTranslation $translation
     *
     * @return Response
     *
     * @throws ModelValidationException
     */
    public function editLinkTranslation(
        Request $request,
        MenuLink $link,
        MenuLinkTranslation $translation
    ): Response {
        $form = $this->createForm(MenuLinkTranslationType::class, $translation);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->manager->save($form->getData());

            if ($link->isInternal()) {
                return $this->redirectToRoute('admin_site_menu_links_edit_internal', ['id' => $link->getId()]);
            }

            return $this->redirectToRoute('admin_site_menu_links_edit_external', ['id' => $link->getId()]);
        }

        return $this->renderTemplate('pages/menu/menu_link/edit_translation.html.twig', ['form' => $form->createView()]);
    }


    /**
     * @Route("/edit/{id}/remove-translation/{transId}", name="remove_translation")
     * @ParamConverter("translation", options={"id" = "transId"})
     *
     * @param MenuLink            $link
     * @param MenuLinkTranslation $translation
     *
     * @return Response
     */
    public function removeTranslation(MenuLink $link, MenuLinkTranslation $translation): Response
    {
        $this->manager->remove($translation);

        if ($link->isInternal()) {
            return $this->redirectToRoute('admin_site_menu_links_edit_internal', ['id' => $link->getId()]);
        }

        return $this->redirectToRoute('admin_site_menu_links_edit_external', ['id' => $link->getId()]);
    }
}