<?php

namespace App\Http\Rest\Controller\Admin\SitePage\Pages;

use App\Application\Util\Identifier;
use App\Database\Domain\Entity\Site\SitePage\Meta\SitePageMeta;
use App\Database\Domain\Entity\Site\SitePage\Meta\SitePageMetaTranslation;
use App\Database\Domain\Entity\Site\SitePage\SitePage;
use App\Database\Domain\Entity\Site\SitePage\SitePageTranslation;
use App\Database\Domain\Exception\ModelValidationException;
use App\Database\Domain\Model\Query\Params\QueryParams;
use App\Database\Domain\Service\PaginatorService;
use App\Http\Rest\Controller\Admin\AbstractAdminController;
use App\Http\Rest\Response\Form\Types\Site\SitePage\Meta\SitePageMetaTranslationType;
use App\Http\Rest\Response\Form\Types\Site\SitePage\SitePageTranslationType;
use App\Http\Rest\Response\Form\Types\Site\SitePage\SitePageType;
use App\Http\Rest\Response\Form\Types\Site\SitePage\StaticSitePageType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @author Anton Zakharuk <anton.zakharuk@cosmonova.net>
 *
 * @Route("/pages", name="site_pages_")
 */
class SitePageController extends AbstractAdminController
{
    /**
     * @Route("", name="index")
     *
     * @param QueryParams      $params
     * @param PaginatorService $paginator
     *
     * @return Response
     */
    public function index(QueryParams $params, PaginatorService $paginator): Response
    {
        $pages = $this->manager->em()->getRepository(SitePage::class)->findByQuery($params);

        return $this->renderTemplate('pages/site_page/index.html.twig', ['pages' => $paginator->paginate($pages)]);
    }

    /**
     * @Route("/create", name="create")
     *
     * @param Request $request
     *
     * @return RedirectResponse|Response
     *
     * @throws ModelValidationException
     */
    public function create(Request $request): Response
    {
        $page = (new SitePage())
            ->setId(Identifier::uuid())
            ->setMeta((new SitePageMeta())->setId(Identifier::uuid()))
            ->setIsStatic(false);

        $form = $this->createForm(SitePageType::class, $page);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->manager->save($form->getData());

            return $this->redirectToRoute('admin_site_pages_edit', ['id' => $page->getId()]);
        }

        return $this->renderTemplate('pages/site_page/create.html.twig', ['form' => $form->createView()]);
    }

    /**
     * @Route("/{id}/edit", name="edit")
     *
     * @param Request  $request
     * @param SitePage $page
     *
     * @return RedirectResponse|Response
     *
     * @throws ModelValidationException
     */
    public function edit(Request $request, SitePage $page): Response
    {
        $form = $this->createForm(SitePageType::class, $page);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->manager->save($form->getData());

            return $this->redirectToRoute('admin_site_pages_index');
        }

        return $this->renderTemplate('pages/site_page/edit.html.twig', [
            'form' => $form->createView(),
            'page' => $page
        ]);
    }

    /**
     * @Route("/{id}/edit-static", name="edit_static")
     *
     * @param Request $request
     * @param SitePage $page
     *
     * @return RedirectResponse|Response
     *
     * @throws ModelValidationException
     */
    public function editStatic(Request $request, SitePage $page): Response
    {
        $form = $this->createForm(StaticSitePageType::class, $page);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->manager->save($form->getData());

            return $this->redirectToRoute('admin_site_pages_index');
        }

        return $this->renderTemplate('pages/site_page/edit.html.twig', [
            'form' => $form->createView(),
            'page' => $page
        ]);
    }

    /**
     * @Route("/{id}/remove", name="remove")
     *
     * @param SitePage $page
     *
     * @return RedirectResponse
     */
    public function remove(SitePage $page): Response
    {
        if ($page->isStatic())
            return $this->notFound();

        $this->manager->remove($page);

        return $this->redirectToRoute('admin_site_pages_index');
    }

    /**
     * @Route("/{id}/create-translation", name="create_translation")
     *
     * @param Request $request
     * @param SitePage $page
     *
     * @return RedirectResponse|Response
     *
     * @throws ModelValidationException
     */
    public function createTranslation(Request $request, SitePage $page): Response
    {
        $translation = (new SitePageTranslation())
            ->setId(Identifier::uuid())
            ->setSitePage($page);

        $form = $this->createForm(SitePageTranslationType::class, $translation);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->manager->save($form->getData());

            if ($page->isStatic()) {
                return $this->redirectToRoute('admin_site_pages_edit_static', ['id' => $page->getId()]);
            }

            return $this->redirectToRoute('admin_site_pages_edit', ['id' => $page->getId()]);
        }

        return $this->renderTemplate('pages/site_page/create_translation.html.twig', ['form' => $form->createView()]);
    }

    /**
     * @Route("/{id}/edit-translation/{transId}", name="edit_translation")
     * @ParamConverter("translation", options={"id" = "transId"})
     *
     * @param Request $request
     * @param SitePage $page
     * @param SitePageTranslation $translation
     *
     * @return RedirectResponse|Response
     *
     * @throws ModelValidationException
     */
    public function editTranslation(
        Request $request,
        SitePage $page,
        SitePageTranslation $translation
    ): Response {
        $form = $this->createForm(SitePageTranslationType::class, $translation);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->manager->save($translation);

            if ($page->isStatic()) {
                return $this->redirectToRoute('admin_site_pages_edit_static', ['id' => $page->getId()]);
            }

            return $this->redirectToRoute('admin_site_pages_edit', ['id' => $page->getId()]);
        }

        return $this->renderTemplate('pages/site_page/edit_translation.html.twig', ['form' => $form->createView()]);
    }

    /**
     * @Route("/{id}/remove-translation/{transId}", name="remove_translation")
     * @ParamConverter("translation", options={"id" = "transId"})
     *
     * @param SitePage $page
     * @param SitePageTranslation $translation
     *
     * @return RedirectResponse
     */
    public function removeTranslation(SitePage $page, SitePageTranslation $translation): Response
    {
        $this->manager->remove($translation);

        if ($page->isStatic()) {
            return $this->redirectToRoute('admin_site_pages_edit_static', ['id' => $page->getId()]);
        }

        return $this->redirectToRoute('admin_site_pages_edit', ['id' => $page->getId()]);
    }

    /**
     * @Route("/{id}/create-meta-translation", name="create_meta_translation")
     *
     * @param Request $request
     * @param SitePage $page
     *
     * @return RedirectResponse|Response
     *
     * @throws ModelValidationException
     */
    public function createMetaTranslation(Request $request, SitePage $page): Response
    {
        $translation = (new SitePageMetaTranslation())
            ->setId(Identifier::uuid())
            ->setMeta($page->getMeta());

        $form = $this->createForm(SitePageMetaTranslationType::class, $translation);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->manager->save($form->getData());

            if ($page->isStatic()) {
                return $this->redirectToRoute('admin_site_pages_edit_static', ['id' => $page->getId()]);
            }

            return $this->redirectToRoute('admin_site_pages_edit', ['id' => $page->getId()]);
        }

        return $this->renderTemplate('pages/site_page/create_meta_translation.html.twig', ['form' => $form->createView()]);
    }

    /**
     * @Route("/{id}/edit-meta-translation/{transId}", name="edit_meta_translation")
     * @ParamConverter("translation", options={"id" = "transId"})
     *
     * @param Request $request
     * @param SitePage $page
     * @param SitePageMetaTranslation $translation
     *
     * @return RedirectResponse|Response
     *
     * @throws ModelValidationException
     */
    public function editMetaTranslation(
        Request $request,
        SitePage $page,
        SitePageMetaTranslation $translation
    ): Response {
        $form = $this->createForm(SitePageMetaTranslationType::class, $translation);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
             $this->manager->save($form->getData());

            if ($page->isStatic()) {
                return $this->redirectToRoute('admin_site_pages_edit_static', ['id' => $page->getId()]);
            }

            return $this->redirectToRoute('admin_site_pages_edit', ['id' => $page->getId()]);
        }

        return $this->renderTemplate('pages/site_page/edit_meta_translation.html.twig', ['form' => $form->createView()]);
    }

    /**
     * @Route("/{id}/remove-meta-translation/{transId}", name="remove_meta_translation")
     * @ParamConverter("translation", options={"id" = "transId"})
     *
     * @param SitePage $page
     * @param SitePageMetaTranslation $translation
     *
     * @return RedirectResponse
     */
    public function removeMetaTranslation(SitePage $page, SitePageMetaTranslation $translation): Response
    {
        $this->manager->remove($translation);

        if ($page->isStatic()) {
            return $this->redirectToRoute('admin_site_pages_edit_static', ['id' => $page->getId()]);
        }

        return $this->redirectToRoute('admin_site_pages_edit', ['id' => $page->getId()]);
    }
}