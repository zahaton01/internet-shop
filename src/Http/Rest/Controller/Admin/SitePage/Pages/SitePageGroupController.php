<?php

namespace App\Http\Rest\Controller\Admin\SitePage\Pages;

use App\Application\Util\Identifier;
use App\Database\Domain\Entity\Site\SitePage\Group\SitePageGroup;
use App\Database\Domain\Exception\ModelValidationException;
use App\Http\Rest\Controller\Admin\AbstractAdminController;
use App\Http\Rest\Response\Form\Types\Site\SitePage\Group\SitePageGroupType;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @author Anton Zakharuk <anton.zakharuk@cosmonova.net>
 *
 * @Route("/site-pages/groups", name="site_pages_groups_")
 */
class SitePageGroupController extends AbstractAdminController
{
    /**
     * @Route("", name="index")
     *
     * @return Response
     */
    public function index(): Response
    {
        $groups = $this->manager->em()->getRepository(SitePageGroup::class)->findAll();

        return $this->renderTemplate('pages/site_page/group/index.html.twig', ['groups' => $groups]);
    }

    /**
     * @Route("/create", name="create")
     *
     * @param Request $request
     *
     * @return RedirectResponse|Response
     *
     * @throws ModelValidationException
     */
    public function create(Request $request): Response
    {
        $group = (new SitePageGroup())->setId(Identifier::uuid());

        $form = $this->createForm(SitePageGroupType::class, $group);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $group = $this->manager->save($form->getData());

            return $this->redirectToRoute('admin_site_pages_groups_edit', ['id' => $group->getId()]);
        }

        return $this->renderTemplate('pages/site_page/group/create.html.twig', ['form' => $form->createView()]);
    }

    /**
     * @Route("/{id}/edit", name="edit")
     *
     * @param Request $request
     * @param SitePageGroup $group
     *
     * @return RedirectResponse|Response
     *
     * @throws ModelValidationException
     */
    public function edit(Request $request, SitePageGroup $group): Response
    {
        $form = $this->createForm(SitePageGroupType::class, $group);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->manager->save($form->getData());

            return $this->redirectToRoute('admin_site_pages_groups_index');
        }

        return $this->renderTemplate('pages/site_page/group/edit.html.twig', ['form' => $form->createView()]);
    }

    /**
     * @Route("/{id}/remove", name="remove")
     *
     * @param SitePageGroup $group
     *
     * @return RedirectResponse
     */
    public function remove(SitePageGroup $group): Response
    {
        $this->manager->remove($group);

        return $this->redirectToRoute('admin_site_pages_groups_index');
    }
}