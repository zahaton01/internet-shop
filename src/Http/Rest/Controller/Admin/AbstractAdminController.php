<?php

namespace App\Http\Rest\Controller\Admin;

use App\Application\Service\GlobalsService;
use App\Application\Service\ParamsService;
use App\Database\Domain\Operation\Manager;
use App\Http\Rest\Controller\AbstractBaseController;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 */
abstract class AbstractAdminController extends AbstractBaseController
{
    /** @var Request  */
    protected $session;

    public function __construct(
        SessionInterface $session,
        TranslatorInterface $translator,
        Manager $manager,
        GlobalsService $globalsService,
        ParamsService $paramService
    ) {
        parent::__construct($manager, $globalsService, $paramService, $translator);

        $this->session = $session;
    }

    /**
     * @param string $path
     * @param array $data
     *
     * @return Response
     */
    public function renderTemplate(string $path, array $data = [])
    {
        return $this->render("admin/$path", $data);
    }

    /**
     * @param string $message
     * @param int $code
     *
     * @return RedirectResponse
     */
    public function error(string $message, int $code = Response::HTTP_INTERNAL_SERVER_ERROR)
    {
        $this->session->set('error.code', $code);
        $this->session->set('error.message', $message);

        return $this->redirectToRoute('admin_error');
    }

    /**
     * @param string $message
     *
     * @return RedirectResponse
     */
    public function notFound(string $message = '')
    {
        return $this->error("{$this->translator->trans('Resource was not found!')} {$this->translator->trans($message)}", Response::HTTP_NOT_FOUND);
    }

    /**
     * @param string $message
     *
     * @return RedirectResponse
     */
    public function internalError(string $message = '')
    {
        return $this->error("{$this->translator->trans('Oops... Something went wrong...')} {$this->translator->trans($message)}");
    }
}
