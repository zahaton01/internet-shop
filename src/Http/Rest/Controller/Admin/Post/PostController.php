<?php

namespace App\Http\Rest\Controller\Admin\Post;

use App\Application\Core\ClassWrappers\DateTime;
use App\Application\Util\Identifier;
use App\Database\Domain\Entity\Site\Post\Media\PostImage;
use App\Database\Domain\Entity\Site\Post\Media\PostMedia;
use App\Database\Domain\Entity\Site\Post\Meta\PostMeta;
use App\Database\Domain\Entity\Site\Post\Meta\PostMetaTranslation;
use App\Database\Domain\Entity\Site\Post\Post;
use App\Database\Domain\Entity\Site\Post\PostTranslation;
use App\Database\Domain\Exception\ModelValidationException;
use App\Database\Domain\Model\Query\Params\QueryParams;
use App\Database\Domain\Service\PaginatorService;
use App\Http\Rest\Controller\Admin\AbstractAdminController;
use App\Http\Rest\Response\Form\Types\Site\Post\PostMetaTranslationType;
use App\Http\Rest\Response\Form\Types\Site\Post\PostTranslationType;
use App\Http\Rest\Response\Form\Types\Site\Post\PostType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 *
 * @Route("/post", name="post_")
 */
class PostController extends AbstractAdminController
{
    /**
     * @Route("", name="index")
     *
     * @param PaginatorService $paginator
     * @param QueryParams $params
     *
     * @return Response
     */
    public function index(PaginatorService $paginator, QueryParams $params): Response
    {
        $posts = $this->manager->em()->getRepository(Post::class)->findByQuery($params);

        return $this->renderTemplate('pages/post/index.html.twig', [
            'posts'       => $paginator->paginate($posts),
            'queryParams' => $params
        ]);
    }

    /**
     * @Route("/create", name="create")
     *
     * @param Request $request
     *
     * @return RedirectResponse|Response
     *
     * @throws ModelValidationException
     */
    public function create(Request $request): Response
    {
        $post = (new Post())
            ->setId(Identifier::uuid())
            ->setCreationDate(DateTime::now())
            ->setMeta((new PostMeta())->setId(Identifier::uuid()))
            ->setIsVisible(true)
            ->setMedia((new PostMedia())->setId(Identifier::uuid()));

        $form = $this->createForm(PostType::class, $post);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->manager->save($form->getData());

            return $this->redirectToRoute('admin_post_edit', ['id' => $post->getId()]);
        }

        return $this->renderTemplate('pages/post/create.html.twig', ['form' => $form->createView()]);
    }

    /**
     * @Route("/edit/{id}", name="edit")
     *
     * @param Post $post
     * @param Request $request
     *
     * @return RedirectResponse|Response
     *
     * @throws ModelValidationException
     */
    public function edit(Request $request, Post $post): Response
    {
        $form = $this->createForm(PostType::class, $post);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->manager->save($form->getData());

            return $this->redirectToRoute('admin_post_index');
        }

        return $this->renderTemplate('pages/post/edit.html.twig', [
            'form' => $form->createView(),
            'post' => $post
        ]);
    }

    /**
     * @Route("/remove/{id}", name="remove")
     *
     * @param Post $post
     *
     * @return RedirectResponse
     */
    public function remove(Post $post): Response
    {
        /** @var PostImage $image */
        foreach ($post->getMedia()->getImages() as $image) {
            if (file_exists($image->getFullPath())) {
                unlink($image->getFullPath());
            }
        }

        $this->manager->remove($post);

        return $this->redirectToRoute('admin_post_index');
    }

    /**
     * @Route("/edit/{id}/meta-translation", name="create_meta_translation")
     *
     * @param Post    $post
     * @param Request $request
     *
     * @return RedirectResponse|Response
     *
     * @throws ModelValidationException
     */
    public function createMetaTranslation(Request $request, Post $post): Response
    {
        $translation = (new PostMetaTranslation())
            ->setId(Identifier::uuid())
            ->setMeta($post->getMeta());

        $form = $this->createForm(PostMetaTranslationType::class, $translation);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->manager->save($form->getData());

            return $this->redirectToRoute('admin_post_edit', ['id' => $post->getId()]);
        }

        return $this->renderTemplate('pages/post/create_meta_translation.html.twig', ['form' => $form->createView()]);
    }

    /**
     * @Route("/edit/{id}/meta-translation/edit/{transId}", name="edit_meta_translation")
     * @ParamConverter("translation", options={"id" = "transId"})
     *
     * @param Request                $request
     * @param Post                $post
     * @param PostMetaTranslation $translation
     *
     * @return RedirectResponse|Response
     *
     * @throws ModelValidationException
     */
    public function editMetaTranslation(
        Request $request,
        Post $post,
        PostMetaTranslation $translation
    ): Response {
        $form = $this->createForm(PostMetaTranslationType::class, $translation);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->manager->save($form->getData());

            return $this->redirectToRoute('admin_post_edit', ['id' => $post->getId()]);
        }

        return $this->renderTemplate('pages/post/edit_meta_translation.html.twig', ['form' => $form->createView()]);
    }

    /**
     * @Route("/edit/{id}/meta-translation/remove/{transId}", name="remove_meta_translation")
     * @ParamConverter("translation", options={"id" = "transId"})
     *
     * @param Post                $post
     * @param PostMetaTranslation $translation
     *
     * @return RedirectResponse
     */
    public function removeMetaTranslation(Post $post, PostMetaTranslation $translation): Response
    {
        $this->manager->remove($translation);

        return $this->redirectToRoute('admin_post_edit', ['id' => $post->getId()]);
    }

    /**
     * @Route("/edit/{id}/translation", name="create_translation")
     *
     * @param Post $post
     * @param Request $request
     *
     * @return RedirectResponse|Response
     *
     * @throws ModelValidationException
     */
    public function createTranslation(Request $request, Post $post): Response
    {
        $translation = (new PostTranslation())
            ->setId(Identifier::uuid())
            ->setPost($post);

        $form = $this->createForm(PostTranslationType::class, $translation);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->manager->save($form->getData());

            return $this->redirectToRoute('admin_post_edit', ['id' => $post->getId()]);
        }

        return $this->renderTemplate('pages/post/create_translation.html.twig', ['form' => $form->createView()]);
    }

    /**
     * @Route("/edit/{id}/translation/edit/{transId}", name="edit_translation")
     * @ParamConverter("translation", options={"id" = "transId"})
     *
     * @param Request         $request
     * @param Post            $post
     * @param PostTranslation $translation
     *
     * @return RedirectResponse|Response
     *
     * @throws ModelValidationException
     */
    public function editTranslation(
        Request $request,
        Post $post,
        PostTranslation $translation
    ): Response {
        $form = $this->createForm(PostTranslationType::class, $translation);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->manager->save($form->getData());

            return $this->redirectToRoute('admin_post_edit', ['id' => $post->getId()]);
        }

        return $this->renderTemplate('pages/post/edit_translation.html.twig', ['form' => $form->createView()]);
    }

    /**
     * @Route("/edit/{id}/translation/remove/{transId}", name="remove_translation")
     * @ParamConverter("translation", options={"id" = "transId"})
     *
     * @param Post            $post
     * @param PostTranslation $translation
     *
     * @return RedirectResponse
     */
    public function removeTranslation(Post $post, PostTranslation $translation): Response
    {
        $this->manager->remove($translation);

        return $this->redirectToRoute('admin_post_edit', ['id' => $post->getId()]);
    }

}