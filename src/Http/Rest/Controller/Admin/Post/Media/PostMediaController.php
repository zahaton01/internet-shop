<?php

namespace App\Http\Rest\Controller\Admin\Post\Media;

use App\Application\Libs\Component\FileManager\UploadManager;
use App\Application\Util\Identifier;
use App\Database\Domain\Entity\Media;
use App\Database\Domain\Entity\Site\Post\Media\PostImage;
use App\Database\Domain\Entity\Site\Post\Post;
use App\Database\Domain\Exception\ModelValidationException;
use App\Http\Rest\Controller\Admin\AbstractAdminController;
use App\Http\Rest\Response\Form\Types\Site\Post\Media\PostImageExternalType;
use App\Http\Rest\Response\Form\Types\Site\Post\Media\PostImageInternalType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 *
 * @Route("/post/{id}/media", name="post_media_")
 */
class PostMediaController extends AbstractAdminController
{
    /**
     * @Route("/add-local-image", name="add_local_image")
     *
     * @param Post          $post
     * @param Request       $request
     * @param UploadManager $uploadManager
     *
     * @return RedirectResponse|Response
     *
     * @throws ModelValidationException
     */
    public function addLocalImage(
        Request $request,
        Post $post,
        UploadManager $uploadManager
    ): Response {
        $image = (new PostImage())
            ->setId(Identifier::uuid())
            ->setType(Media::LOCAL)
            ->setMedia($post->getMedia());

        $form = $this->createForm(PostImageInternalType::class, $image);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var PostImage $image */
            $image = $form->getData();
            /** @var UploadedFile $file */
            $file  = $form['attachment']->getData();

            $uploadedFile = $uploadManager->uploadPostImage($file);

            $image
                ->setAssetPath($uploadedFile->getAssetPath())
                ->setFullPath($uploadedFile->getFullPath());

            $this->manager->save($image);

            return $this->redirectToRoute('admin_post_edit', ['id' => $post->getId()]);
        }

        return $this->renderTemplate('pages/post/media/create_internal.html.twig', [
            'form' => $form->createView(),
            'post' => $post
        ]);
    }

    /**
     * @Route("/add-remote-image", name="add_remote_image")
     *
     * @param Post $post
     * @param Request $request
     *
     * @return RedirectResponse|Response
     *
     * @throws ModelValidationException
     */
    public function addRemoteImage(Request $request, Post $post): Response
    {
        $image = (new PostImage())
            ->setId(Identifier::uuid())
            ->setType(Media::REMOTE)
            ->setMedia($post->getMedia());

        $form = $this->createForm(PostImageExternalType::class, $image);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->manager->save($form->getData());

            return $this->redirectToRoute('admin_post_edit', ['id' => $post->getId(),]);
        }

        return $this->renderTemplate('pages/post/media/create_external.html.twig', ['form' => $form->createView()]);
    }

    /**
     * @Route("/remove/{imageId}", name="remove")
     * @ParamConverter("image", options={"id" = "imageId"})
     *
     * @param Post      $post
     * @param PostImage $image
     *
     * @return RedirectResponse
     */
    public function remove(Post $post, PostImage $image): Response
    {
        $this->manager->remove($image);

        return $this->redirectToRoute('admin_post_edit', ['id' => $post->getId()]);
    }

    /**
     * @Route("/set-as-preview/{imageId}", name="set_as_preview")
     * @ParamConverter("image", options={"id" = "imageId"})
     *
     * @param Post      $post
     * @param PostImage $image
     *
     * @return RedirectResponse
     *
     * @throws ModelValidationException
     */
    public function setAsPreview(Post $post, PostImage $image): Response
    {
        $post->setPreviewImage($image);

        $this->manager->save($post);

        return $this->redirectToRoute('admin_post_edit', ['id' => $post->getId()]);
    }
}