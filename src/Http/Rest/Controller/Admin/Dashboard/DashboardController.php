<?php

namespace App\Http\Rest\Controller\Admin\Dashboard;

use App\Application\Core\ClassWrappers\DateTime;
use App\Database\Domain\Model\Query\Params\QueryParams;
use App\Http\Rest\Controller\Admin\AbstractAdminController;
use App\Database\Domain\Entity\Commerce\Order\Order;
use App\Database\Domain\Service\PaginatorService;
use App\Application\Libs\Tools\Statistic\OrderStatistic;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 *
 * @Route("/dashboard", name="dashboard_")
 */
class DashboardController extends AbstractAdminController
{
    /**
     * @Route("", name="index", methods={"GET"})
     *
     * @param OrderStatistic   $orderStatistic
     * @param QueryParams      $params
     * @param PaginatorService $paginator
     *
     * @return Response
     */
    public function index(
        OrderStatistic $orderStatistic,
        QueryParams $params,
        PaginatorService $paginator
    ): Response {
        $orderCount = $orderStatistic->getOrderCount();

        $params->setStartDate(DateTime::now()->setTime(0, 0, 0));
        $params->setEndDate(DateTime::now()->setTime(23, 59, 59));
        $todayOrders = $this->manager->em()->getRepository(Order::class)->findByQuery($params);

        return $this->renderTemplate('pages/dashboard/index.html.twig', [
            'orderCount'  => $orderCount,
            'todayOrders' => $paginator->paginate($todayOrders)
        ]);
    }
}
