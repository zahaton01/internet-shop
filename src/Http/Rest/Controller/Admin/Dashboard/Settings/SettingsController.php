<?php

namespace App\Http\Rest\Controller\Admin\Dashboard\Settings;

use App\Database\Domain\Exception\ModelValidationException;
use App\Http\Rest\Controller\Admin\AbstractAdminController;
use App\Database\Domain\Entity\Globals;
use App\Http\Rest\Response\Form\Types\Dashboard\Settings\MetricsType;
use App\Http\Rest\Response\Form\Types\Dashboard\Settings\SettingsType;
use Psr\Cache\InvalidArgumentException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 *
 * @Route("/settings", name="settings_")
 */
class SettingsController extends AbstractAdminController
{
    /**
     * @Route("", name="index")
     *
     * @param Request $request
     *
     * @return Response
     *
     * @throws ModelValidationException
     * @throws InvalidArgumentException
     */
    public function index(Request $request): Response
    {
        $form = $this->createForm(SettingsType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->globals->setByKey($form['contact_phone']->getData(), Globals::CONTACT_PHONE);
            $this->globals->setByKey($form['contact_email']->getData(), Globals::CONTACT_EMAIL);
            $this->globals->setByKey($form['contact_viber']->getData(), Globals::CONTACT_VIBER);
            $this->globals->setByKey($form['contact_telegram']->getData(), Globals::CONTACT_TELEGRAM);
            $this->globals->setByKey($form['contact_whats_up']->getData(), Globals::CONTACT_WHATS_UP);
            $this->globals->setByKey($form['host_address']->getData(), Globals::HOST_ADDRESS);
            $this->globals->setByKey($form['app_name']->getData(), Globals::APP_NAME);
            $this->globals->setByKey($form['app_description']->getData(), Globals::APP_DESCRIPTION);
            $this->globals->setByKey($form['app_currency']->getData(), Globals::APP_CURRENCY);
            $this->globals->setByKey($form['app_date_format']->getData(), Globals::DATE_DISPLAYING_FORMAT);
            $this->globals->setByKey($form['default_locale']->getData(), Globals::DEFAULT_LOCALE);
            $this->globals->setByKey(array_values($form['enabled_locales']->getData()), Globals::ENABLED_LOCALES);

            return $this->redirectToRoute('admin_settings_index');
        }

        return $this->renderTemplate('pages/dashboard/settings/index.html.twig', ['form' => $form->createView()]);
    }

    /**
     * @Route("/metrics", name="metrics")
     *
     * @param Request $request
     *
     * @return Response
     *
     * @throws ModelValidationException
     * @throws InvalidArgumentException
     */
    public function metrics(Request $request): Response
    {
        $form = $this->createForm(MetricsType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->globals->setByKey($form['metrics_google']->getData(), Globals::METRICS_GOOGLE);
            $this->globals->setByKey($form['metrics_google_search_console']->getData(), Globals::METRICS_GOOGLE_SEARCH_CONSOLE);
            $this->globals->setByKey($form['metrics_yandex']->getData(), Globals::METRICS_YANDEX);
        }

        return $this->renderTemplate('pages/dashboard/metrics/index.html.twig', ['form' => $form->createView(),]);
    }
}
