<?php

namespace App\Http\Rest\Controller\Admin\Dashboard\Log;

use App\Database\Domain\Model\Query\Params\QueryParams;
use App\Http\Rest\Controller\Admin\AbstractAdminController;
use App\Database\Domain\Entity\Log;
use App\Database\Domain\Model\Pagination\Pagination;
use App\Database\Domain\Service\PaginatorService;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 *
 * @Route("/log", name="log_")
 */
class LogController extends AbstractAdminController
{
    /**
     * @Route("", name="index")
     *
     * @param QueryParams      $params
     * @param PaginatorService $paginator
     *
     * @return Response
     */
    public function index(QueryParams $params, PaginatorService $paginator): Response
    {
        $logs = $this->manager->em()->getRepository(Log::class)->findByQuery($params);

        return $this->renderTemplate('pages/dashboard/log/index.html.twig', [
            'logs'            => $paginator->paginate($logs),
            'queryParams'     => $params,
            'possibleSources' => Log::sources(),
            'possibleLevels'  => Log::levels()
        ]);
    }

    /**
     * @Route("/clean-info-and-warning", name="clean_info_and_warning")
     *
     * @return RedirectResponse
     */
    public function cleanInfoAndWarnings(): Response
    {
        $pagination = new Pagination(100, 1);
        $logs = $this->manager->em()->getRepository(Log::class)->getAllInfoAndWarnings($pagination);
        $pagination->setTotalItems(count($logs));

        for ($i = 1; $i <= $pagination->getTotalPages(); $i++) {
            $pagination->setPage($i);
            $logs = $this->manager->em()->getRepository(Log::class)->getAllInfoAndWarnings($pagination);

            $this->manager->remove($logs);
            $this->manager->em()->clear();
        }

        return $this->redirectToRoute( 'admin_log_index');
    }

    /**
     * @Route("/clean-all-logs", name="clean_all_logs")
     *
     * @return RedirectResponse
     */
    public function cleanAllLogs(): Response
    {
        $pagination = new Pagination(100, 1);
        $logs = $this->manager->em()->getRepository(Log::class)->getAllLogs($pagination);
        $pagination->setTotalItems(count($logs));

        for ($i = 1; $i <= $pagination->getTotalPages(); $i++) {
            $pagination->setPage($i);
            $logs = $this->manager->em()->getRepository(Log::class)->getAllLogs($pagination);

            $this->manager->remove($logs);
            $this->manager->em()->clear();
        }

        return $this->redirectToRoute( 'admin_log_index');
    }
}