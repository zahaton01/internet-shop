<?php

namespace App\Http\Rest\Controller;

use App\Application\Libs\Tools\EntityExporter\EntityExporter;
use App\Application\Libs\Tools\EntityExporter\Instance\ProductEntityExporterInstance;
use App\Application\Libs\Tools\EntityExporter\Settings\ProductExportSettings;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 */
class ProductXmlExport extends AbstractBaseController
{
    /**
     * @Route("/product-xml", name="product_xml")
     *
     * @param EntityExporter $exporter
     *
     * @return Response
     */
    public function index(EntityExporter $exporter): Response
    {
        $exportSettings = new ProductExportSettings('xml', ['xml_return_content' => true]);
        $xmlContent     = $exporter->get(ProductEntityExporterInstance::class)->configure($exportSettings)->export();

        $response       = new Response($xmlContent);
        $response->headers->set('Content-Type', 'application/xml; charset=utf-8');

        return $response;
    }
}