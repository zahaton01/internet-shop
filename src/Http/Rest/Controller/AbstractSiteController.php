<?php

namespace App\Http\Rest\Controller;

use App\Application\Model\Commerce\Cart\Cart;
use App\Application\Service\Commerce\CartService;
use App\Application\Service\GlobalsService;
use App\Application\Service\Commerce\OrderService;
use App\Application\Service\ParamsService;
use App\Database\Domain\Entity\Site\Menu\Menu;
use App\Database\Domain\Operation\Manager;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 */
abstract class AbstractSiteController extends AbstractBaseController
{
    /** @var Request  */
    protected $session;

    /** @var CartService */
    protected $cartService;

    public function __construct(
        Manager $manager,
        GlobalsService $globalsService,
        ParamsService $paramService,
        TranslatorInterface $translator,
        SessionInterface $session,
        CartService $cartService
    ) {
        parent::__construct($manager, $globalsService, $paramService, $translator);

        $this->session      = $session;
        $this->cartService  = $cartService;
    }

    /**
     * @param string $path
     * @param array  $data
     *
     * @return Response
     */
    protected function renderTemplate(string $path, array $data = [])
    {
        $headerMenu = $this->manager->em()->getRepository(Menu::class)->findOneBy(['type' => Menu::HEADER]);
        $footerMenu = $this->manager->em()->getRepository(Menu::class)->findOneBy(['type' => Menu::FOOTER]);

        return $this->render("site/themes/default/$path", array_merge($data, [
            'cart'       => $this->getCart(),
            'headerMenu' => $headerMenu,
            'footerMenu' => $footerMenu
        ]));
    }

    /**
     * @param string $message
     * @param int    $code
     *
     * @return RedirectResponse
     */
    protected function error(string $message, int $code = Response::HTTP_INTERNAL_SERVER_ERROR)
    {
        $this->session->set('error.code', $code);
        $this->session->set('error.message', $message);

        return $this->redirectToRoute('site_error');
    }

    /**
     * @param string $message
     *
     * @return RedirectResponse
     */
    protected function notFound(string $message = '')
    {
        return $this->error("{$this->translator->trans('Resource was not found!')} {$this->translator->trans($message)}", Response::HTTP_NOT_FOUND);
    }

    /**
     * @param string $message
     *
     * @return RedirectResponse
     */
    protected function internalError(string $message = '')
    {
        return $this->error("{$this->translator->trans('Oops... Something went wrong...')} {$this->translator->trans($message)}");
    }

    /**
     * @return Cart
     */
    protected function getCart(): Cart
    {
        return $this->cartService->fromSession();
    }
}
