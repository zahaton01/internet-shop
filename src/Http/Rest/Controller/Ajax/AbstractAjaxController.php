<?php

namespace App\Http\Rest\Controller\Ajax;

use App\Application\Service\GlobalsService;
use App\Application\Service\ParamsService;
use App\Database\Domain\Operation\Manager;
use App\Http\Rest\Controller\AbstractBaseController;
use App\Http\Rest\Request\Ajax\DataResolver\DataResolver;
use App\Database\Domain\Exception\ModelValidationException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 */
abstract class AbstractAjaxController extends AbstractBaseController
{
    /** @var Manager */
    protected $manager;

    /** @var DataResolver */
    protected $dataResolver;

    public function __construct(
        Manager $manager,
        GlobalsService $globalsService,
        ParamsService $paramService,
        DataResolver $dataResolver,
        TranslatorInterface $translator
    ) {
        parent::__construct($manager, $globalsService, $paramService, $translator);

        $this->dataResolver = $dataResolver;
    }

    /**
     * @param Request $request
     *
     * @return mixed
     */
    protected function decode(Request $request)
    {
        return json_decode($request->getContent(), true);
    }
}
