<?php

namespace App\Http\Rest\Controller\Ajax\Api\NovaPoshta;

use App\Application\Libs\Api\NovaPoshta\NovaPoshtaApi;
use App\Http\Rest\Controller\Ajax\AbstractAjaxController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 *
 * @Route("/api/nova-poshta", name="api_nova_poshta_")
 */
class NovaPoshtaController extends AbstractAjaxController
{
    /**
     * @Route("/warehouses", name="warehouses", methods={"GET"})
     *
     * @param Request $request
     * @param NovaPoshtaApi $api
     *
     * @return JsonResponse
     */
    public function getWarehouses(Request $request, NovaPoshtaApi $api): JsonResponse
    {
        return $this->json($api->getWarehouses($request->query->get('ref')));
    }
}