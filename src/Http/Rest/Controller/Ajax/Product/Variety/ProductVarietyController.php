<?php

namespace App\Http\Rest\Controller\Ajax\Product\Variety;

use App\Http\Rest\Controller\Ajax\AbstractAjaxController;
use App\Database\Domain\Entity\Commerce\Product\Variety\ProductVariety;
use App\Database\Domain\Entity\Commerce\Product\Variety\Variety;
use App\Database\Domain\Exception\ModelValidationException;
use App\Http\Rest\Response\Ajax\Normalizer\Product\Variety\ProductVarietyNormalizer;
use App\Http\Rest\Response\Ajax\Normalizer\Product\Variety\VarietyNormalizer;
use App\Http\Rest\Response\Ajax\Normalizer\Product\Variety\VarietyValueNormalizer;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 *
 * @Route("/product-variety", name="product_variety_")
 */
class ProductVarietyController extends AbstractAjaxController
{
    /**
     * @Route("", name="get", methods={"GET"})
     *
     * @param Request                  $request
     * @param ProductVarietyNormalizer $normalizer
     *
     * @return JsonResponse
     */
    public function getProductVariety(Request $request, ProductVarietyNormalizer $normalizer): JsonResponse
    {
        /** @var ProductVariety $variety */
        $variety = $this->manager->em()->getRepository(ProductVariety::class)->find($request->query->get('id'));

        if (null === $variety) {
            throw new NotFoundHttpException('Product variety was not found');
        }

        return $this->json($normalizer->normalize($variety));
    }

    /**
     * @Route("", name="add", methods={"POST"})
     *
     * @return JsonResponse
     *
     * @throws ModelValidationException
     */
    public function addProductVariety(): JsonResponse
    {
        $productVariety = $this->dataResolver->resolve(['route' => 'ajax_product_variety_add']);

        $this->manager->save($productVariety);

        return $this->json([]);
    }

    /**
     * @Route("", name="edit", methods={"PUT"})
     *
     * @return JsonResponse
     *
     * @throws ModelValidationException
     */
    public function editProductVariety(): JsonResponse
    {
        $productVariety = $this->dataResolver->resolve(['route' => 'ajax_product_variety_edit']);

        $this->manager->save($productVariety);

        return $this->json([]);
    }

    /**
     * @Route("/varieties", name="get_varieties", methods={"GET"})
     *
     * @param VarietyNormalizer $normalizer
     *
     * @return JsonResponse
     */
    public function getPossibleVarieties(VarietyNormalizer $normalizer): JsonResponse
    {
        $varieties = $this->manager->em()->getRepository(Variety::class)->findByQuery()->getResult();

        return $this->json($normalizer->normalizeMultiple($varieties));
    }

    /**
     * @Route("/varieties/values", name="get_varieties_values", methods={"GET"})
     *
     * @param Request                $request
     * @param VarietyValueNormalizer $normalizer
     *
     * @return JsonResponse
     */
    public function getPossibleValues(Request $request, VarietyValueNormalizer $normalizer): JsonResponse
    {
        /** @var Variety $variety */
        $variety = $this->manager->em()->getRepository(Variety::class)->find($request->query->get('variety', 1));

        return $this->json($normalizer->normalizeMultiple($variety->getValues()));
    }
}
