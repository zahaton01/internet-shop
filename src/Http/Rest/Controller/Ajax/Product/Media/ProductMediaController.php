<?php

namespace App\Http\Rest\Controller\Ajax\Product\Media;

use App\Application\Libs\Component\FileManager\UploadManager;
use App\Application\Util\Identifier;
use App\Database\Domain\Entity\Commerce\Product\Media\ProductImage;
use App\Database\Domain\Entity\Commerce\Product\Media\ProductMedia;
use App\Database\Domain\Entity\Commerce\Product\Product;
use App\Database\Domain\Entity\Media;
use App\Http\Rest\Controller\Ajax\AbstractAjaxController;
use App\Database\Domain\Exception\ModelValidationException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 *
 * @Route("/product/{id}/product-media", name="product_media_")
 */
class ProductMediaController extends AbstractAjaxController
{
    /**
     * @Route("/tiny-mce-upload-image", name="tiny_mce_upload_image")
     *
     * @param Request       $request
     * @param Product       $product
     * @param UploadManager $uploadManager
     *
     * @return JsonResponse
     *
     * @throws ModelValidationException
     */
    public function tinyMceUploadImage(
        Request $request,
        Product $product,
        UploadManager $uploadManager
    ): JsonResponse {
        $file = $request->files->get('file', null);

        if (null === $file) {
            throw new BadRequestHttpException('File is missing');
        }

        $uploaded = $uploadManager->uploadProductImage($file);
        $image = (new ProductImage())->setId(Identifier::uuid());
        $image
            ->setType(Media::LOCAL)
            ->setMedia($product->getMedia())
            ->setAssetPath($uploaded->getAssetPath())
            ->setFullPath($uploaded->getFullPath());

        $this->manager->save($image);

        return $this->json(['location' => "{$this->params->getHost()}/{$image->getAssetPath()}"]);
    }
}