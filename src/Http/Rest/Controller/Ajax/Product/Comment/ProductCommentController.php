<?php

namespace App\Http\Rest\Controller\Ajax\Product\Comment;

use App\Database\Domain\Entity\User\Admin;
use App\Database\Domain\Model\Query\Params\QueryParams;
use App\Http\Rest\Controller\Ajax\AbstractAjaxController;
use App\Database\Domain\Entity\Commerce\Product\Comment\ProductComment;
use App\Database\Domain\Entity\Commerce\Product\Comment\ProductCommentReply;
use App\Database\Domain\Model\Pagination\Pagination;
use App\Database\Domain\Exception\ModelValidationException;
use App\Http\Rest\Response\Ajax\Normalizer\Product\Comment\ProductCommentNormalizer;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 *
 * @Route("/product-comment", name="product_comment_")
 */
class ProductCommentController extends AbstractAjaxController
{
    /**
     * @Route("", name="get", methods={"GET"})
     *
     * @param Request                  $request
     * @param ProductCommentNormalizer $commentNormalizer
     * @param QueryParams              $params
     *
     * @return JsonResponse
     */
    public function getComments(
        Request $request,
        ProductCommentNormalizer $commentNormalizer,
        QueryParams $params
    ): JsonResponse {
        $pagination = new Pagination(7, $request->query->get('page'));
        $comments   = $this->manager->em()->getRepository(ProductComment::class)->getComments($pagination, $params);
        $pagination->setTotalItems(count($comments));

        if ($pagination->getTotalPages() < $request->query->getInt('page')) {
            throw new BadRequestHttpException('Comments were not found');
        }

        return $this->json([
            'comments' => $commentNormalizer->normalizeMultiple($comments),
            'total'    => $pagination->getTotalItems()
        ]);
    }

    /**
     * @Route("", name="add", methods={"POST"})
     *
     * @return JsonResponse
     *
     * @throws ModelValidationException
     */
    public function addComment(): JsonResponse
    {
        /** @var ProductComment $comment */
        $comment = $this->dataResolver->resolve(['route' => 'ajax_product_comment_add']);

        $this->manager->save($comment);

        return $this->json([
            'id'      => $comment->getId(),
            'isAdmin' => $comment->getUser() instanceof Admin ? true : false
        ]);
    }

    /**
     * @Route("/replies", name="add_reply", methods={"POST"})
     *
     * @return JsonResponse
     *
     * @throws ModelValidationException
     */
    public function addReply(): JsonResponse
    {
        /** @var ProductCommentReply $reply */
        $reply = $this->dataResolver->resolve(['route' => 'ajax_product_comment_add_reply']);

        $this->manager->save($reply);

        return $this->json([
            'id'      => $reply->getId(),
            'isAdmin' => $reply->getUser() instanceof Admin ? true : false
        ]);
    }
}
