<?php

namespace App\Http\Rest\Controller\Ajax\Cart;

use App\Application\Service\Commerce\CartService;
use App\Http\Rest\Controller\Ajax\AbstractAjaxController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 *
 * @Route("/cart", name="cart_")
 */
class CartController extends AbstractAjaxController
{
    /**
     * @Route("/add", name="add_product")
     *
     * @param Request     $request
     * @param CartService $cartService
     *
     * @return JsonResponse
     */
    public function addProduct(Request $request, CartService $cartService): JsonResponse
    {
        if (!$cartService->add($request)) {
            throw new BadRequestHttpException('Already in cart');
        }

        return $this->json([]);
    }

    /**
     * @Route("/refill", name="refill")
     *
     * @param Request $request
     * @param CartService $cartService
     *
     * @return JsonResponse
     */
    public function refillCart(Request $request, CartService $cartService): JsonResponse
    {
        $cartService->refill($request);

        return $this->json([]);
    }
}