<?php

namespace App\Http\Rest\Controller\Ajax\Site\Post\Media;

use App\Application\Libs\Component\FileManager\UploadManager;
use App\Application\Util\Identifier;
use App\Database\Domain\Entity\Media;
use App\Database\Domain\Entity\Site\Post\Media\PostImage;
use App\Database\Domain\Entity\Site\Post\Post;
use App\Database\Domain\Exception\ModelValidationException;
use App\Http\Rest\Controller\Ajax\AbstractAjaxController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 *
 * @Route("/post/{id}/post-media", name="post_media_")
 */
class PostMediaController extends AbstractAjaxController
{
    /**
     * @Route("/tiny-mce-upload-image", name="tiny_mce_upload_image")
     *
     * @param Request       $request
     * @param Post          $post
     * @param UploadManager $uploadManager
     *
     * @return JsonResponse
     *
     * @throws ModelValidationException
     */
    public function tinyMceUploadImage(
        Request $request,
        Post $post,
        UploadManager $uploadManager
    ): JsonResponse {
        $file = $request->files->get('file', null);

        if (null === $file) {
            throw new BadRequestHttpException('File is missing');
        }

        $uploaded = $uploadManager->uploadProductImage($file);
        $image    = (new PostImage())->setId(Identifier::uuid());
        $image
            ->setType(Media::LOCAL)
            ->setMedia($post->getMedia())
            ->setAssetPath($uploaded->getAssetPath())
            ->setFullPath($uploaded->getFullPath());

        $this->manager->save($image);

        return $this->json(['location' => "{$this->params->getHost()}/{$image->getAssetPath()}"]);
    }
}