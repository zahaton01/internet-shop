<?php

namespace App\Http\Rest\Controller\Ajax\Site;

use App\Database\Domain\Entity\User\Admin;
use App\Http\Rest\Controller\Ajax\AbstractAjaxController;
use App\Database\Domain\Entity\Site\SiteComment;
use App\Database\Domain\Model\Pagination\Pagination;
use App\Database\Domain\Exception\ModelValidationException;
use App\Http\Rest\Response\Ajax\Normalizer\Site\Comment\SiteCommentNormalizer;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 *
 * @Route("/site-comment", name="site_comment_")
 */
class SiteCommentController extends AbstractAjaxController
{
    /**
     * @Route("", name="get", methods={"GET"})
     *
     * @param Request               $request
     * @param SiteCommentNormalizer $commentSerializer
     *
     * @return JsonResponse
     */
    public function getComments(Request $request, SiteCommentNormalizer $commentSerializer): JsonResponse
    {
        $pagination = new Pagination(3, $request->query->get('page'));
        $comments   = $this->manager->em()->getRepository(SiteComment::class)->getComments($pagination);
        $pagination->setTotalItems(count($comments));

        if ($pagination->getTotalPages() < $request->query->getInt('page')) {
            throw new NotFoundHttpException('Comments were not found');
        }

        return $this->json($commentSerializer->normalizeMultiple($comments));
    }

    /**
     * @Route("", name="add", methods={"POST"})
     *
     * @return JsonResponse
     *
     * @throws ModelValidationException
     */
    public function addComment(): JsonResponse
    {
        $comment = $this->dataResolver->resolve(['route' => 'ajax_site_comment_add']);

        $this->manager->save($comment);

        return $this->json(['id' => $comment->getId()]);
    }

    /**
     * @Route("/reply", name="add_reply", methods={"POST"})
     *
     * @return JsonResponse
     *
     * @throws ModelValidationException
     */
    public function addReply(): JsonResponse
    {
        $reply = $this->dataResolver->resolve(['route' => 'ajax_site_comment_add_reply']);

        $this->manager->save($reply);

        return $this->json([
            'id'      => $reply->getId(),
            'isAdmin' => $reply->getComment()->getUser() instanceof Admin ? true : false
        ]);
    }
}
