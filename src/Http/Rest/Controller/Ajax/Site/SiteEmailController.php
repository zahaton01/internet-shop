<?php

namespace App\Http\Rest\Controller\Ajax\Site;

use App\Application\Libs\Component\Notifier\AdminNotifier;
use App\Http\Rest\Controller\Ajax\AbstractAjaxController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 *
 * @Route("/send-email", name="send_email_")
 */
class SiteEmailController extends AbstractAjaxController
{
    /**
     * @Route("/contact-form", name="contact_form")
     *
     * @param Request       $request
     * @param AdminNotifier $notifier
     *
     * @return JsonResponse
     */
    public function fromContactPage(Request $request, AdminNotifier $notifier): JsonResponse
    {
        $content = $request->request->all();
        $session = $request->getSession();

        if (mb_strlen($content['text']) < 10 || (int) $session->get('email', 0) > 3)
            return $this->json(['Fuck off'], 500);

        $notifier->contactFormRequest($content['text']);
        $session->set('email', ((int) $session->get('email', 0) + 1));

        return $this->json([]);
    }
}