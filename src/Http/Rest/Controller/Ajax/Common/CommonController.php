<?php

namespace App\Http\Rest\Controller\Ajax\Common;

use App\Http\Rest\Controller\Ajax\AbstractAjaxController;
use App\Database\Domain\Model\EntityState\Availability;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 *
 * @Route("/common", name="common_")
 */
class CommonController extends AbstractAjaxController
{
    /**
     * @Route("/availabilities", name="availabilities", methods={"GET"})
     *
     * @param Availability $availability
     *
     * @return JsonResponse
     */
    public function getAvailabilities(Availability $availability): JsonResponse
    {
        return $this->json($availability->allAsTranslatedChoices());
    }
}
