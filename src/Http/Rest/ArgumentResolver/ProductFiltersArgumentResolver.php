<?php

namespace App\Http\Rest\ArgumentResolver;

use App\Database\Domain\Operation\Manager;
use App\Database\Domain\Entity\Commerce\Product\Specification\Specification;
use App\Database\Domain\Entity\Commerce\Product\Specification\SpecificationValue;
use App\Database\Domain\Model\Query\Product\ProductFilters;
use App\Database\Domain\Model\Query\Product\ProductSpecificationFilter;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Controller\ArgumentValueResolverInterface;
use Symfony\Component\HttpKernel\ControllerMetadata\ArgumentMetadata;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 */
class ProductFiltersArgumentResolver implements ArgumentValueResolverInterface
{
    /** @var Manager  */
    private $manager;

    public function __construct(Manager $manager)
    {
        $this->manager = $manager;
    }

    /**
     * @param Request $request
     * @param ArgumentMetadata $argument
     *
     * @return bool
     *
     * @throws \Exception
     */
    public function supports(Request $request, ArgumentMetadata $argument)
    {
        return $argument->getType() === ProductFilters::class;
    }

    /**
     * @param Request $request
     * @param ArgumentMetadata $argument
     *
     * @return \Generator
     *
     * @throws \Exception
     */
    public function resolve(Request $request, ArgumentMetadata $argument)
    {
        $productFilters       = new ProductFilters();
        $specificationFilters = $request->get('specifications', []);

        foreach ($specificationFilters as $specificationId => $specificationValueId) {
            /** @var Specification $specification */
            $specification = $this->manager->em()->getRepository(Specification::class)->find($specificationId);
            /** @var SpecificationValue $value */
            $value         = $this->manager->em()->getRepository(SpecificationValue::class)->find($specificationValueId);

            $productSpecification = (new ProductSpecificationFilter())
                ->setSpecification($specification)
                ->setValue($value);

            $productFilters->addSpecification($productSpecification);
        }

        $productFilters->setFilters($request->get('filters'));

        yield $productFilters;
    }
}
