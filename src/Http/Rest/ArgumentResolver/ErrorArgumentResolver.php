<?php

namespace App\Http\Rest\ArgumentResolver;

use App\Http\Rest\Model\Error\Error;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Controller\ArgumentValueResolverInterface;
use Symfony\Component\HttpKernel\ControllerMetadata\ArgumentMetadata;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 */
class ErrorArgumentResolver implements ArgumentValueResolverInterface
{
    /**
     * @param Request $request
     * @param ArgumentMetadata $argument
     *
     * @return bool
     *
     * @throws \Exception
     */
    public function supports(Request $request, ArgumentMetadata $argument)
    {
        return $argument->getType() === Error::class;
    }

    /**
     * @param Request $request
     * @param ArgumentMetadata $argument
     *
     * @return \Generator
     *
     * @throws \Exception
     */
    public function resolve(Request $request, ArgumentMetadata $argument)
    {
        $session = $request->getSession();
        $message = $session->get('error.message', '');
        $code    = (int) $session->get('error.code', 0);

        $error = new Error();
        $error
            ->setMessage($message)
            ->setCode($code);

        yield $error;
    }
}
