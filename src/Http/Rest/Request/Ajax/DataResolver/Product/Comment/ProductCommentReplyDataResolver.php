<?php

namespace App\Http\Rest\Request\Ajax\DataResolver\Product\Comment;

use App\Application\Core\ClassWrappers\DateTime;
use App\Application\Util\Identifier;
use App\Database\Domain\Entity\Commerce\Product\Comment\ProductComment;
use App\Database\Domain\Entity\Commerce\Product\Comment\ProductCommentReply;
use App\Database\Domain\Entity\User\AbstractUser;
use App\Http\Rest\Request\Ajax\DataResolver\AbstractDataResolver;
use App\Http\Rest\Request\Ajax\DataResolver\DataResolverInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 */
class ProductCommentReplyDataResolver extends AbstractDataResolver implements DataResolverInterface
{
    /**
     * @param Request $request
     * @param array $context
     *
     * @return ProductCommentReply
     *
     * @throws \Exception
     */
    public function resolve(Request $request, array $context = [])
    {
        $content = $this->decode($request);

        /** @var ProductComment $comment */
        $comment = $this->manager->em()->getRepository(ProductComment::class)->find($content['comment']);

        if (null === $comment) {
            throw new NotFoundHttpException('Comment was not found');
        }

        if ($content['user'] !== null) {
            /** @var AbstractUser $user */
            $user = $this->manager->em()->getRepository(AbstractUser::class)->find($content['user']);
        }

        $reply = (new ProductCommentReply())->setId(Identifier::uuid());
        $reply
            ->setText($content['text'])
            ->setName($content['name'])
            ->setUser($user ?? null)
            ->setCreationDate(DateTime::now())
            ->setComment($comment);

        return $reply;
    }

    public function supports(Request $request, array $context = []): bool
    {
        return isset($context['route']) && $context['route'] === 'ajax_product_comment_add_reply';
    }
}