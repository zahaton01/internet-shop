<?php

namespace App\Http\Rest\Request\Ajax\DataResolver\Product\Comment;

use App\Application\Core\ClassWrappers\DateTime;
use App\Application\Util\Identifier;
use App\Database\Domain\Entity\Commerce\Product\Comment\ProductComment;
use App\Database\Domain\Entity\Commerce\Product\Product;
use App\Database\Domain\Entity\User\AbstractUser;
use App\Http\Rest\Request\Ajax\DataResolver\AbstractDataResolver;
use App\Http\Rest\Request\Ajax\DataResolver\DataResolverInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 */
class ProductCommentDataResolver extends AbstractDataResolver implements DataResolverInterface
{
    /**
     * @param Request $request
     * @param array $context
     *
     * @return ProductComment
     *
     * @throws \Exception
     */
    public function resolve(Request $request, array $context = [])
    {
        $content = $this->decode($request);

        if ($content['user'] !== null) {
            /** @var AbstractUser $user */
            $user = $this->manager->em()->getRepository(AbstractUser::class)->find($content['user']);
        }

        /** @var Product $product */
        $product = $this->manager->em()->getRepository(Product::class)->find($content['product']);

        if (null === $product) {
            throw new NotFoundHttpException('Product was not found');
        }

        $comment = (new ProductComment())->setId(Identifier::uuid());
        $comment
            ->setName($content['name'])
            ->setText($content['text'])
            ->setUser($user ?? null)
            ->setProduct($product)
            ->setCreationDate(DateTime::now());

        return $comment;
    }

    public function supports(Request $request, array $context = []): bool
    {
        return isset($context['route']) && $context['route'] === 'ajax_product_comment_add';
    }
}