<?php

namespace App\Http\Rest\Request\Ajax\DataResolver\Product\Variety;

use App\Application\Util\Identifier;
use App\Database\Domain\Entity\Commerce\Product\Product;
use App\Database\Domain\Entity\Commerce\Product\Variety\ProductVariety;
use App\Database\Domain\Entity\Commerce\Product\Variety\ProductVarietyValue;
use App\Database\Domain\Entity\Commerce\Product\Variety\VarietyValue;
use App\Database\Domain\Entity\Commerce\Product\Variety\Variety;
use App\Http\Rest\Request\Ajax\DataResolver\AbstractDataResolver;
use App\Http\Rest\Request\Ajax\DataResolver\DataResolverInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 */
class ProductVarietyDataResolver extends AbstractDataResolver implements DataResolverInterface
{
    /**
     * @param Request $request
     * @param array $context
     *
     * @return ProductVariety
     */
    public function resolve(Request $request, array $context = [])
    {
        $content = $this->decode($request);

        /** @var Variety $variety */
        $variety = $this->manager->em()->getRepository(Variety::class)->find($content['variety']['id']);
        /** @var Product $product */
        $product = $this->manager->em()->getRepository(Product::class)->find($content['product']['id']);

        $productVariety = (new ProductVariety())->setId(Identifier::uuid());
        $productVariety
            ->setVariety($variety)
            ->setProduct($product);

        foreach ($content['values'] as $val) {
            /** @var VarietyValue $value */
            $value               = $this->manager->em()->getRepository(VarietyValue::class)->find($val['id']);
            $productVarietyValue = (new ProductVarietyValue())->setId(Identifier::uuid());
            $productVarietyValue
                ->setAttachedValue($value)
                ->setAvailability($val['availability']);

            $productVariety->addValue($productVarietyValue);
        }

        return $productVariety;
    }

    public function supports(Request $request, array $context = []): bool
    {
        return isset($context['route']) && $context['route'] === 'ajax_product_variety_add';
    }
}