<?php

namespace App\Http\Rest\Request\Ajax\DataResolver\Product\Variety;

use App\Application\Util\Identifier;
use App\Database\Domain\Entity\Commerce\Product\Product;
use App\Database\Domain\Entity\Commerce\Product\Variety\ProductVariety;
use App\Database\Domain\Entity\Commerce\Product\Variety\ProductVarietyValue;
use App\Database\Domain\Entity\Commerce\Product\Variety\VarietyValue;
use App\Database\Domain\Entity\Commerce\Product\Variety\Variety;
use App\Http\Rest\Request\Ajax\DataResolver\AbstractDataResolver;
use App\Http\Rest\Request\Ajax\DataResolver\DataResolverInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 */
class ProductVarietyEditDataResolver extends AbstractDataResolver implements DataResolverInterface
{
    /**
     * @param Request $request
     * @param array $context
     *
     * @return ProductVariety
     */
    public function resolve(Request $request, array $context = [])
    {
        $content        = $this->decode($request);
        /** @var ProductVariety $productVariety */
        $productVariety = $this->manager->em()->getRepository(ProductVariety::class)->find($content['id']);

        if (null === $productVariety) {
            throw new NotFoundHttpException('Product variety was not found');
        }

        $productVariety->clearValues();

        foreach ($content['values'] as $val) {
            /** @var VarietyValue $value */
            $value               = $this->manager->em()->getRepository(VarietyValue::class)->find($val['id']);
            $productVarietyValue = new ProductVarietyValue();
            $productVarietyValue
                ->setId(Identifier::uuid())
                ->setAttachedValue($value)
                ->setAvailability($val['availability']);

            $productVariety->addValue($productVarietyValue);
        }

        return $productVariety;
    }

    public function supports(Request $request, array $context = []): bool
    {
        return isset($context['route']) && $context['route'] === 'ajax_product_variety_edit';
    }
}