<?php

namespace App\Http\Rest\Request\Ajax\DataResolver;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 */
class DataResolver
{
    /** @var DataResolverInterface[] */
    private $resolvers;

    /** @var Request */
    private $request;

    public function __construct(iterable $resolvers, RequestStack $requestStack)
    {
        $this->resolvers = $resolvers;
        $this->request   = $requestStack->getCurrentRequest();
    }

    /**
     * @param array $context
     *
     * @return mixed
     *
     * @throws \LogicException
     */
    public function resolve(array $context = [])
    {
        foreach ($this->resolvers as $resolver) {
            if ($resolver->supports($this->request, $context)) {
                return $resolver->resolve($this->request, $context);
            }
        }

        throw new \LogicException('Resolver was not found');
    }
}