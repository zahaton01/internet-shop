<?php

namespace App\Http\Rest\Request\Ajax\DataResolver;

use App\Database\Domain\Operation\Manager;
use Symfony\Component\HttpFoundation\Request;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 */
abstract class AbstractDataResolver
{
    /** @var Manager */
    protected $manager;

    public function __construct(Manager $manager)
    {
        $this->manager = $manager;
    }

    protected function decode(Request $request): array
    {
        return json_decode($request->getContent(), true);
    }
}