<?php

namespace App\Http\Rest\Request\Ajax\DataResolver;

use Symfony\Component\HttpFoundation\Request;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 */
interface DataResolverInterface
{
    public function supports(Request $request, array $context = []): bool;

    public function resolve(Request $request, array $context = []);
}