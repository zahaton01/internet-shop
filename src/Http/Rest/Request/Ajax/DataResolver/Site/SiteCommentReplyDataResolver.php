<?php

namespace App\Http\Rest\Request\Ajax\DataResolver\Site;

use App\Application\Core\ClassWrappers\DateTime;
use App\Application\Util\Identifier;
use App\Database\Domain\Entity\Site\SiteComment;
use App\Database\Domain\Entity\Site\SiteCommentReply;
use App\Database\Domain\Entity\User\AbstractUser;
use App\Http\Rest\Request\Ajax\DataResolver\AbstractDataResolver;
use App\Http\Rest\Request\Ajax\DataResolver\DataResolverInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 */
class SiteCommentReplyDataResolver extends AbstractDataResolver implements DataResolverInterface
{
    /**
     * @param Request $request
     * @param array $context
     *
     * @return SiteCommentReply
     */
    public function resolve(Request $request, array $context = [])
    {
        $content = $this->decode($request);

        if (empty($content['name']) || empty($content['text']) || empty($content['comment'])) {
            throw new BadRequestHttpException('Name, text and comment can\'t be empty');
        }

        /** @var SiteComment $comment */
        $comment = $this->manager->em()->getRepository(SiteComment::class)->find($content['comment']);

        if (null === $comment) {
            throw new NotFoundHttpException('Comment wasn\'t found');
        }

        if ($content['user'] !== null) {
            /** @var AbstractUser $user */
            $user = $this->manager->em()->getRepository(AbstractUser::class)->find($content['user']);
        }

        $reply = (new SiteCommentReply())->setId(Identifier::uuid());
        $reply
            ->setText($content['text'])
            ->setName($content['name'])
            ->setUser($user ?? null)
            ->setCreationDate(DateTime::now())
            ->setComment($comment);

        return $reply;
    }

    public function supports(Request $request, array $context = []): bool
    {
        return isset($context['route']) && $context['route'] === 'ajax_site_comment_add_reply';
    }
}