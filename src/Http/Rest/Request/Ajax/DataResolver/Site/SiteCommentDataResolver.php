<?php

namespace App\Http\Rest\Request\Ajax\DataResolver\Site;

use App\Application\Core\ClassWrappers\DateTime;
use App\Application\Util\Identifier;
use App\Database\Domain\Entity\Site\SiteComment;
use App\Database\Domain\Entity\User\AbstractUser;
use App\Http\Rest\Request\Ajax\DataResolver\AbstractDataResolver;
use App\Http\Rest\Request\Ajax\DataResolver\DataResolverInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 */
class SiteCommentDataResolver extends AbstractDataResolver implements DataResolverInterface
{
    /**
     * @param Request $request
     * @param array $context
     *
     * @return SiteComment
     */
    public function resolve(Request $request, array $context = [])
    {
        $content = $this->decode($request);

        if (empty($content['name']) || empty($content['text'])) {
            throw new BadRequestHttpException('Name and comment can\'t be empty');
        }

        if ($content['user'] !== null) {
            /** @var AbstractUser $user */
            $user = $this->manager->em()->getRepository(AbstractUser::class)->find($content['user']);
        }

        $comment = (new SiteComment())->setId(Identifier::uuid());
        $comment
            ->setName($content['name'])
            ->setText($content['text'])
            ->setUser($user ?? null)
            ->setCreationDate(DateTime::now());

        return $comment;
    }

    public function supports(Request $request, array $context = []): bool
    {
        return isset($context['route']) && $context['route'] === 'ajax_site_comment_add';
    }
}