<?php

namespace App\Http\Rest\Service;

use App\Database\Domain\Entity\Commerce\Product\Product;
use App\Http\Rest\Model\WatchedProducts;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class WatchedProductsService
{
    /** @var EntityManagerInterface  */
    private $em;

    /** @var SessionInterface  */
    private $session;

    public function __construct(EntityManagerInterface $em, SessionInterface $session)
    {
        $this->em      = $em;
        $this->session = $session;
    }
    /**
     * @param Product $product
     */
    public function add(Product $product)
    {
        $watchedProducts = $this->session->get('watchedProducts', []);

        $filtered = array_filter($watchedProducts, function ($watchedProduct) use ($product) {
           return $watchedProduct === $product->getId();
        });

        if (!$filtered) {
            $watchedProducts[] = $product->getId();
        }

        $this->session->set('watchedProducts', $watchedProducts);
    }

    /**
     * @return WatchedProducts
     */
    public function wrapper()
    {
        $products        = $this->session->get('watchedProducts', []);
        $watchedProducts = new WatchedProducts();

        foreach ($products as $id) {
            /** @var Product $productEntity */
            $productEntity = $this->em->getRepository(Product::class)->find($id);

            if (null !== $productEntity) {
                $watchedProducts->addProduct($productEntity);
            }
        }

        return $watchedProducts;
    }
}
