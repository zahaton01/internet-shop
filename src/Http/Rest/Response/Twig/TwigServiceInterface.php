<?php

namespace App\Http\Rest\Response\Twig;

/**
 * @author Anton Zakharuk <anton.zakharuk@cosmonova.net>
 */
interface TwigServiceInterface
{
    /**
     * All services will be imported to twig as global variable named "services"
     *
     * To retrieve needed one you must specify alias for the service
     *
     * so you will be able to get it like this: services.alias
     *
     * @return string
     */
    public function getAlias(): string;
}