<?php

namespace App\Http\Rest\Response\Twig\Media;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 */
interface MediaResourceInterface
{
    public function getAlias(): string;
}