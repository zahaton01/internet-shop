<?php

namespace App\Http\Rest\Response\Twig\Media\Product;

use App\Database\Domain\Entity\Commerce\Product\Category\ProductCategory;
use App\Database\Domain\Entity\Commerce\Product\Media\ProductImage;
use App\Database\Domain\Entity\Commerce\Product\Product;
use App\Http\Rest\Response\Twig\Media\MediaResourceInterface;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 */
class ProductMediaResource implements MediaResourceInterface
{
    public function getAlias(): string
    {
        return 'product';
    }

    /**
     * @param $context
     *
     * @return string
     */
    public function pic($context)
    {
        $image = null;

        if ($context instanceof ProductImage) {
            $image = $context;
        }

        if ($context instanceof Product) {
            $image = $context->getMedia()->getImages()[0] ?? null;
        }

        if ($image !== null) {
            if ($image->isLocal()) {
                return $image->getAssetPath() ?: '';
            }

            return $image->getUrl() ?: '';
        }

        return '';
    }

    /**
     * @param ProductCategory $category
     *
     * @return string|null
     */
    public function picFromCategory(ProductCategory $category)
    {
        /** @var Product $product */
        foreach ($category->getProducts() as $product) {
            if ($product->getMedia()->getImages()) {
                /** @var ProductImage $image */
                $image = $product->getMedia()->getImages()[0];

                if ($image !== null) {
                    return $image->isLocal() ? $image->getAssetPath() : $image->getUrl();
                }
            }
        }

        return '';
    }
}