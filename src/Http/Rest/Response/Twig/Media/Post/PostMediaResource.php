<?php

namespace App\Http\Rest\Response\Twig\Media\Post;

use App\Database\Domain\Entity\Site\Post\Media\PostImage;
use App\Database\Domain\Entity\Site\Post\Post;
use App\Http\Rest\Response\Twig\Media\MediaResourceInterface;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 */
class PostMediaResource implements MediaResourceInterface
{
    /**
     * @param $context
     *
     * @return string
     */
    public function pic($context)
    {
        $image = null;

        if ($context instanceof PostImage) {
            $image = $context;
        }

        if ($context instanceof Post) {
            $image = $context->getMedia()->getImages()[0] ?? null;
        }

        if ($image !== null) {
            if ($image->isLocal()) {
                return $image->getAssetPath() ?: '';
            }

            return $image->getUrl() ?: '';
        }

        return '';
    }

    public function getAlias(): string
    {
        return 'post';
    }
}