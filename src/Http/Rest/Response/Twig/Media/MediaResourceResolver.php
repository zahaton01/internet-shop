<?php

namespace App\Http\Rest\Response\Twig\Media;

use App\Http\Rest\Response\Twig\TwigServiceInterface;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 */
class MediaResourceResolver implements TwigServiceInterface
{
    /** @var MediaResourceInterface[] */
    private $resources;

    public function __construct(iterable $resources)
    {
        $this->resources = $resources;
    }

    /**
     * @param $alias
     * @param $arguments
     *
     * @return MediaResourceInterface|null
     */
    public function __call($alias, $arguments)
    {
        foreach ($this->resources as $resource) {
            if ($resource->getAlias() === $alias) {
                return $resource;
            }
        }

        return null;
    }

    public function getAlias(): string
    {
        return 'media';
    }
}