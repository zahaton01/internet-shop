<?php

namespace App\Http\Rest\Response\Twig\Styles\Product;

use App\Database\Domain\Entity\Commerce\Order\Order;
use App\Database\Domain\Entity\Commerce\Product\Product;
use App\Database\Domain\Entity\Commerce\Product\ProductPromo;
use App\Database\Domain\Model\EntityState\Availability;
use App\Http\Rest\Response\Twig\Styles\StyleGeneratorInterface;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 */
class ProductColorGenerator implements StyleGeneratorInterface
{
    public function getAlias(): string
    {
        return 'product';
    }

    /**
     * @param Product $product
     *
     * @return string
     */
    public function getSpecialStatus(Product $product)
    {
        $status = $product->getPromo()->getSpecialStatus();

        switch ($status) {
            case ProductPromo::SPECIAL_STATUS_TOP_SALE:
                return 'badge-warning';
                break;
            case ProductPromo::SPECIAL_STATUS_OUT_SOON:
                return 'badge-danger';
                break;
            default:
                return 'badge-info';
                break;
        }
    }

    /**
     * @param Product $product
     *
     * @return string
     */
    public function getAvailability(Product $product)
    {
        $status = $product->getAvailability();

        switch ($status) {
            case Availability::UNDER_THE_ORDER:
                return 'text-warning';
                break;
            case Availability::NOT_AVAILABLE:
                return 'text-danger';
                break;
            case Availability::AWAITING:
                return 'text-info';
                break;
            case Availability::AVAILABLE:
                return 'text-success';
                break;
            default:
                return 'text-dark';
                break;
        }
    }
}