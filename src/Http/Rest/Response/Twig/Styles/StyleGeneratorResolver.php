<?php

namespace App\Http\Rest\Response\Twig\Styles;

use App\Http\Rest\Response\Twig\TwigServiceInterface;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 */
class StyleGeneratorResolver implements TwigServiceInterface
{
    /** @var StyleGeneratorInterface[] */
    private $styleGenerators;

    public function __construct(iterable $styleGenerators)
    {
        $this->styleGenerators = $styleGenerators;
    }

    /**
     * @param $alias
     * @param $arguments
     *
     * @return StyleGeneratorInterface|null
     */
    public function __call($alias, $arguments)
    {
        foreach ($this->styleGenerators as $generator) {
            if ($generator->getAlias() === $alias) {
                return $generator;
            }
        }

        return null;
    }

    public function getAlias(): string
    {
        return 'styles';
    }
}