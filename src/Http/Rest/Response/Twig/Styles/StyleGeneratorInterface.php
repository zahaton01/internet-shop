<?php

namespace App\Http\Rest\Response\Twig\Styles;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 */
interface StyleGeneratorInterface
{
    public function getAlias(): string;
}