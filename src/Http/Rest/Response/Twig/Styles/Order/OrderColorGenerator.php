<?php

namespace App\Http\Rest\Response\Twig\Styles\Order;

use App\Database\Domain\Entity\Commerce\Order\Order;
use App\Http\Rest\Response\Twig\Styles\StyleGeneratorInterface;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 */
class OrderColorGenerator implements StyleGeneratorInterface
{
    public function getAlias(): string
    {
        return 'order';
    }

    /**
     * @param Order $order
     *
     * @return string
     */
    public function getOrderStatus(Order $order)
    {
        $status = $order->getStatus();

        switch ($status) {
            case Order::ORDER_STATUS_NOT_CONFIRMED: {
                return 'text-danger';
                break;
            }
            case Order::ORDER_STATUS_COMPLETED: {
                return 'text-success';
                break;
            }
            case Order::ORDER_STATUS_CONFIRMED: {
                return 'text-info';
                break;
            }
            case Order::ORDER_STATUS_ON_DELIVER: {
                return 'text-warning';
                break;
            }
            default:
                return 'text-dark';
                break;
        }
    }
}