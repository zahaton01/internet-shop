<?php

namespace App\Http\Rest\Response\Twig\Translator;

use App\Application\Service\TranslationService;
use App\Http\Rest\Response\Twig\TwigServiceInterface;

/**
 * @author Anton Zakharuk <anton.zakharuk@cosmonova.net>
 */
class TranslatorService implements TwigServiceInterface
{
    /** @var TranslationService  */
    private $translator;

    /**
     * TranslatorService constructor.
     * @param TranslationService $translationService
     */
    public function __construct(TranslationService $translationService)
    {
        $this->translator = $translationService;
    }

    /**
     * @return string
     */
    public function getAlias(): string
    {
        return 'translator';
    }

    /**
     * @param $entity
     * @param string $property
     *
     * @return string
     */
    public function trans($entity, string $property): ?string
    {
        return $this->translator->trans($entity, $property);
    }
}