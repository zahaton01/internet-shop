<?php

namespace App\Http\Rest\Response\Twig;

/**
 * @author Anton Zakharuk <anton.zakharuk@cosmonova.net>
 */
class TwigServicesResolver
{
    /** @var TwigServiceInterface[] */
    private $services;

    /**
     * TwigServicesResolver constructor.
     * @param iterable $services
     */
    public function __construct(iterable $services)
    {
        $this->services = $services;
    }

    /**
     * @param $alias
     * @param $arguments
     *
     * @return TwigServiceInterface|null
     */
    public function __call($alias, $arguments)
    {
        foreach ($this->services as $service) {
            if ($service->getAlias() === $alias) {
                return $service;
            }
        }

        return null;
    }
}