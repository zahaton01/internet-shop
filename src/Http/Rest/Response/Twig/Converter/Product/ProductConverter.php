<?php

namespace App\Http\Rest\Response\Twig\Converter\Product;

use App\Database\Domain\Entity\Commerce\Product\Product;
use App\Database\Domain\Model\EntityState\Availability;
use App\Http\Rest\Response\Twig\Converter\ConverterInterface;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 */
class ProductConverter implements ConverterInterface
{
    private static $googleXmlProductAvailabilityMapping = [
        Availability::AVAILABLE       => 'in stock',
        Availability::UNDER_THE_ORDER => 'preorder',
        Availability::AWAITING        => 'out of stock',
        Availability::NOT_AVAILABLE   => 'out of stock'
    ];

    public function getAlias(): string
    {
        return 'product';
    }

    public function availabilityToGoogleXml(Product $product): string
    {
        return static::$googleXmlProductAvailabilityMapping[$product->getAvailability()];
    }
}