<?php

namespace App\Http\Rest\Response\Twig\Converter;

use App\Http\Rest\Response\Twig\Media\MediaResourceInterface;
use App\Http\Rest\Response\Twig\TwigServiceInterface;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 */
class ConverterResolver implements TwigServiceInterface
{
    /** @var ConverterInterface[] */
    private $converters;

    public function __construct(iterable $converters)
    {
        $this->converters = $converters;
    }

    /**
     * @param $alias
     * @param $arguments
     *
     * @return ConverterInterface|null
     */
    public function __call($alias, $arguments)
    {
        foreach ($this->converters as $converter) {
            if ($converter->getAlias() === $alias) {
                return $converter;
            }
        }

        return null;
    }

    public function getAlias(): string
    {
        return 'converter';
    }
}