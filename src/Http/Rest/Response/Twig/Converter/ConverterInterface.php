<?php

namespace App\Http\Rest\Response\Twig\Converter;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 */
interface ConverterInterface
{
    public function getAlias(): string;
}