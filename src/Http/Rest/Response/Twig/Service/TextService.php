<?php

namespace App\Http\Rest\Response\Twig\Service;

use App\Application\Util\Text\TextUtil;
use App\Http\Rest\Response\Twig\TwigServiceInterface;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 */
class TextService implements TwigServiceInterface
{
    /**
     * @return string
     */
    public function getAlias(): string
    {
        return 'text';
    }

    /**
     * @param string $text
     * @param int    $length
     *
     * @return string
     */
    public function short(string $text, int $length = 70): string
    {
        return TextUtil::short($text, $length);
    }

    /**
     * @param string $html
     * @param int    $length
     *
     * @return string
     */
    public function formatHtmlInput(string $html, int $length = 120)
    {
        return TextUtil::short(TextUtil::stripTags($html), $length);
    }
}