<?php

namespace App\Http\Rest\Response\Twig\Service;

use App\Application\Service\ParamsService;
use App\Http\Rest\Response\Twig\TwigServiceInterface;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 */
class ParamService implements TwigServiceInterface
{
    /** @var ParamsService  */
    private $params;

    public function __construct(ParamsService $params)
    {
        $this->params = $params;
    }

    public function getHost()
    {
        return $this->params->getHost();
    }

    public function getAlias(): string
    {
       return 'params';
    }
}