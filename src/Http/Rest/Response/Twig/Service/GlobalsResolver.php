<?php

namespace App\Http\Rest\Response\Twig\Service;

use App\Application\Service\GlobalsService;
use App\Database\Domain\Entity\Globals;
use App\Http\Rest\Response\Twig\TwigServiceInterface;
use Psr\Cache\InvalidArgumentException;

/**
 * @author Anton Zakharuk <anton.zakharuk@cosmonova.net>
 */
class GlobalsResolver implements TwigServiceInterface
{
    private $aliases = [
        'default_locale'  => Globals::DEFAULT_LOCALE,
        'enabled_locales' => Globals::ENABLED_LOCALES,
        'date_format'     => Globals::DATE_DISPLAYING_FORMAT,
        'currency'        => Globals::APP_CURRENCY,
        'contact_phone'   => Globals::CONTACT_PHONE,
        'contact_email'   => Globals::CONTACT_EMAIL
    ];

    /** @var GlobalsService  */
    private $globalsService;

    /**
     * GlobalsResolver constructor.
     * @param GlobalsService $globalsService
     */
    public function __construct(GlobalsService $globalsService)
    {
        $this->globalsService = $globalsService;
    }

    /**
     * @return string
     */
    public function getAlias(): string
    {
        return 'globals';
    }

    /**
     * @param string $requestedAlias
     *
     * @return Globals|null
     *
     * @throws InvalidArgumentException
     */
    public function get(string $requestedAlias)
    {
        foreach ($this->aliases as $alias => $value) {
            if ($alias === $requestedAlias) {
                return $this->getByKey($value);
            }
        }

        return null;
    }

    /**
     * @param string $key
     *
     * @return Globals|null
     *
     * @throws InvalidArgumentException
     */
    public function getByKey(string $key)
    {
        return $this->globalsService->getByKey($key);
    }
}