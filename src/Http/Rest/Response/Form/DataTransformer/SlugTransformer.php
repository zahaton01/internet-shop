<?php

namespace App\Http\Rest\Response\Form\DataTransformer;

use App\Application\Util\Generator\SlugGenerator;
use Symfony\Component\Form\DataTransformerInterface;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 */
class SlugTransformer implements DataTransformerInterface
{
    /** @var SlugGenerator */
    private $generator;

    public function __construct(SlugGenerator $generator)
    {
        $this->generator = $generator;
    }

    /**
     * @param mixed $slug
     *
     * @return mixed|string|null
     */
    public function transform($slug)
    {
        return $slug;
    }

    /**
     * @param string $slug
     *
     * @return \DateTime|mixed
     *
     * @throws \Exception
     */
    public function reverseTransform($slug)
    {
        return $this->generator->fromString(strtolower($slug));
    }
}