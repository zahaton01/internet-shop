<?php

namespace App\Http\Rest\Response\Form\ChoiceBuilder\Delivery;

use App\Database\Domain\Entity\Commerce\Order\Order;
use App\Database\Domain\Entity\Globals;
use App\Http\Rest\Response\Form\ChoiceBuilder\AbstractChoiceBuilder;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 */
class DeliveryChoiceBuilder extends AbstractChoiceBuilder
{
    public function types(): array
    {
        $choices = [];
        $choices[$this->translator->trans(Order::DELIVERY_COURIER)] = Order::DELIVERY_COURIER;

        $isNovaEnabled = $this->globalsService->getByKey(Globals::NOVA_POSHTA_IS_ENABLED)->toBoolean();
        if ($isNovaEnabled) {
            $choices[$this->translator->trans(Order::DELIVERY_NOVA_POSHTA)] = Order::DELIVERY_NOVA_POSHTA;
        }

        return $choices;
    }
}