<?php

namespace App\Http\Rest\Response\Form\ChoiceBuilder\Payment;

use App\Database\Domain\Entity\Commerce\Order\Order;
use App\Database\Domain\Entity\Globals;
use App\Http\Rest\Response\Form\ChoiceBuilder\AbstractChoiceBuilder;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 */
class PaymentTypeChoiceBuilder extends AbstractChoiceBuilder
{
    /**
     * @return array
     */
    public function types(): array
    {
        $choices = [];
        $choices[$this->translator->trans(Order::PAYMENT_TYPE_CASH)] = Order::PAYMENT_TYPE_CASH;

        $isLiqPayEnabled = $this->globalsService->getByKey(Globals::LIQPAY_IS_ENABLED)->toBoolean();
        if ($isLiqPayEnabled) {
            $choices[$this->translator->trans(Order::PAYMENT_TYPE_ONLINE_LIQPAY)] = Order::PAYMENT_TYPE_ONLINE_LIQPAY;
        }

        return $choices;
    }
}