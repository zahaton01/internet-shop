<?php

namespace App\Http\Rest\Response\Form\ChoiceBuilder\Common;

use App\Http\Rest\Response\Form\ChoiceBuilder\AbstractChoiceBuilder;
use App\Database\Domain\Entity\Commerce\Product\Media\ProductMedia as ProductMedia;

class MediaChoiceBuilder extends AbstractChoiceBuilder
{
    /**
     * @return array
     */
    public function types()
    {
        return [
            $this->translator->trans(ProductMedia::REMOTE) => ProductMedia::REMOTE,
            $this->translator->trans(ProductMedia::LOCAL)  => ProductMedia::LOCAL
        ];
    }
}
