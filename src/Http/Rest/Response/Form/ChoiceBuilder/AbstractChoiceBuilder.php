<?php

namespace App\Http\Rest\Response\Form\ChoiceBuilder;

use App\Application\Service\GlobalsService;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 */
abstract class AbstractChoiceBuilder
{
    /** @var GlobalsService  */
    protected $globalsService;

    /** @var TranslatorInterface  */
    protected $translator;

    public function __construct(GlobalsService $globalsService, TranslatorInterface $translator)
    {
        $this->globalsService = $globalsService;
        $this->translator     = $translator;
    }
}