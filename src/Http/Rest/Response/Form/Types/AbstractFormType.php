<?php

namespace App\Http\Rest\Response\Form\Types;

use Symfony\Component\Form\AbstractType;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 */
abstract class AbstractFormType extends AbstractType
{
    /** @var TranslatorInterface  */
    protected $translator;

    /** @var \Closure */
    protected $groupByFirstLetter;

    public function __construct(TranslatorInterface $translator)
    {
        $this->translator         = $translator;

        $this->groupByFirstLetter = function ($choice, $key, $value) {
            return mb_strtoupper(mb_substr((string) $choice->getName(), 0, 1));
        };
    }
}
