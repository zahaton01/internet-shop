<?php

namespace App\Http\Rest\Response\Form\Types;

use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 */
class MetaType extends AbstractFormType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        return $builder
            ->add('pageTitle', TextType::class, [
                'label'    => $this->translator->trans('Title of the page'),
                'required' => false
            ])
            ->add('metaTitle', TextType::class, [
                'label'    => $this->translator->trans('Meta title of the page'),
                'required' => false
            ])
            ->add('metaDesc', TextType::class, [
                'label'    => $this->translator->trans('Meta description of the page'),
                'required' => false
            ])
            ->add('metaKeywords', TextType::class, [
                'label'    => $this->translator->trans('Meta keywords of the page'),
                'required' => false
            ]);
    }

    /**
     * @param OptionsResolver $resolver
     *
     * @return OptionsResolver|void
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        return $resolver->setDefaults(['allow_extra_fields' => true]);
    }
}
