<?php

namespace App\Http\Rest\Response\Form\Types;

use App\Application\Model\AppEnv\Locale;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 */
class MetaTranslationType extends AbstractFormType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     *
     * @return FormBuilderInterface|void
     */
    protected function extend(FormBuilderInterface $builder, array $options)
    {
        return $builder
            ->add('locale', ChoiceType::class, [
                'label'    => $this->translator->trans('Language of the translation'),
                'choices'  => Locale::allAsChoices()
            ])
            ->add('pageTitle', TextType::class, [
                'label'    => $this->translator->trans('Title of the page'),
                'required' => false
            ])
            ->add('metaTitle', TextType::class, [
                'label'    => $this->translator->trans('Meta title of the page'),
                'required' => false
            ])
            ->add('metaDesc', TextType::class, [
                'label'    => $this->translator->trans('Meta description of the page'),
                'required' => false
            ])
            ->add('metaKeywords', TextType::class, [
                'label'    => $this->translator->trans('Meta keywords of the page'),
                'required' => false
            ])
            ->add('save', SubmitType::class, [
                'label'    => $this->translator->trans('Save')
            ]);
    }

    /**
     * @param OptionsResolver $resolver
     *
     * @return OptionsResolver|void
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        return $resolver->setDefaults(['allow_extra_fields' => true]);
    }
}
