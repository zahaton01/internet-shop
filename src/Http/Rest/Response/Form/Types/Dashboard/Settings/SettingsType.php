<?php

namespace App\Http\Rest\Response\Form\Types\Dashboard\Settings;

use App\Database\Domain\Entity\Globals;
use App\Http\Rest\Response\Form\Types\AbstractFormType;
use App\Application\Model\AppEnv\Locale;
use App\Application\Service\GlobalsService;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 */
class SettingsType extends AbstractFormType
{
    /** @var GlobalsService */
    protected $globals;

    public function __construct(TranslatorInterface $translator, GlobalsService $globalsService)
    {
        parent::__construct($translator);

        $this->globals = $globalsService;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     *
     * @return FormBuilderInterface|void
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        return $builder
            ->add('contact_phone', TextType::class, [
                'label'    => $this->translator->trans('Phone'),
                'data'     => (string) $this->globals->getByKey(Globals::CONTACT_PHONE)
            ])
            ->add('contact_email', TextType::class, [
                'label'    => $this->translator->trans('Email'),
                'data'     => (string) $this->globals->getByKey(Globals::CONTACT_EMAIL)
            ])
            ->add('contact_viber', TextType::class, [
                'label'    => $this->translator->trans('Viber'),
                'data'     => (string) $this->globals->getByKey(Globals::CONTACT_VIBER)
            ])
            ->add('contact_telegram', TextType::class, [
                'label'    => $this->translator->trans('Telegram'),
                'data'     => (string) $this->globals->getByKey(Globals::CONTACT_TELEGRAM)
            ])
            ->add('contact_whats_up', TextType::class, [
                'label'    => $this->translator->trans("What's up"),
                'data'     => (string) $this->globals->getByKey(Globals::CONTACT_WHATS_UP)
            ])
            ->add('host_address', TextType::class, [
                'label'    => $this->translator->trans('Host'),
                'data'     => (string) $this->globals->getByKey(Globals::HOST_ADDRESS),
                'help'     => $this->translator->trans("In some cases the app cannot determine it's host by itself so we have to define it explicitly")
            ])
            ->add('app_name', TextType::class, [
                'label'    => $this->translator->trans('Host'),
                'data'     => (string) $this->globals->getByKey(Globals::APP_NAME)
            ])
            ->add('app_description', TextType::class, [
                'label'    => $this->translator->trans('Host'),
                'data'     => (string) $this->globals->getByKey(Globals::APP_DESCRIPTION)
            ])
            ->add('app_currency', TextType::class, [
                'label'    => $this->translator->trans('Host'),
                'data'     => (string) $this->globals->getByKey(Globals::APP_CURRENCY)
            ])
            ->add('default_locale', ChoiceType::class, [
                'label'    => $this->translator->trans('Default locale'),
                'choices'  => Locale::allAsChoices(),
                'data'     => (string) $this->globals->getByKey(Globals::DEFAULT_LOCALE)
            ])
            ->add('enabled_locales', ChoiceType::class, [
                'label'    => $this->translator->trans('Enabled locales'),
                'multiple' => true,
                'expanded' => true,
                'choices'  => Locale::allAsChoices(),
                'data'     => $this->globals->getByKey(Globals::ENABLED_LOCALES)->toArray()
            ])
            ->add('app_date_format', ChoiceType::class, [
                'label'    => $this->translator->trans('Date displaying format'),
                'help'     => $this->translator->trans("The format of the displaying date time objects around the app"),
                'choices'  => [
                    '15-08-2019'          => 'd-m-Y',
                    '15-08-2019 15:06:49' => 'd-m-Y H:i:s',
                    '15.08.2019'          => 'd.m.Y',
                    '15.08.2019 15:06:49' => 'd.m.Y H:i:s'
                ]
            ])
            ->add('save', SubmitType::class, [
                'label'    => $this->translator->trans('Save')
            ]);
    }

    /**
     * @param OptionsResolver $resolver
     *
     * @return OptionsResolver|void
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        return $resolver->setDefaults(['allow_extra_fields' => true]);
    }
}
