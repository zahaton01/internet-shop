<?php

namespace App\Http\Rest\Response\Form\Types\Dashboard\Settings;

use App\Application\Service\GlobalsService;
use App\Database\Domain\Entity\Globals;
use App\Http\Rest\Response\Form\Types\AbstractFormType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 */
class MetricsType extends AbstractFormType
{
    /** @var GlobalsService */
    protected $globals;

    public function __construct(TranslatorInterface $translator, GlobalsService $globalsService)
    {
        parent::__construct($translator);

        $this->globals = $globalsService;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     *
     * @return FormBuilderInterface|void
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        return $builder
            ->add('metrics_google', TextType::class, [
                'label'    => $this->translator->trans('Google'),
                'data'     => (string) $this->globals->getByKey(Globals::METRICS_GOOGLE)
            ])
            ->add('metrics_google_search_console', TextType::class, [
                'label'    => $this->translator->trans('Google search console'),
                'data'     => (string) $this->globals->getByKey(Globals::METRICS_GOOGLE_SEARCH_CONSOLE)
            ])
            ->add('metrics_yandex', TextType::class, [
                'label'    => $this->translator->trans('Yandex'),
                'data'     => (string) $this->globals->getByKey(Globals::METRICS_YANDEX)
            ])
            ->add('save', SubmitType::class, [
                'label'    => $this->translator->trans('Save')
            ]);
    }

    /**
     * @param OptionsResolver $resolver
     *
     * @return OptionsResolver|void
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        return $resolver->setDefaults(['allow_extra_fields' => true]);
    }
}