<?php

namespace App\Http\Rest\Response\Form\Types;

use App\Application\Model\AppEnv\Locale;
use App\Http\Rest\Response\Form\Types\AbstractFormType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 */
abstract class AbstractCategoryTranslationType extends AbstractFormType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     *
     * @return FormBuilderInterface|void
     */
    public function extend(FormBuilderInterface $builder, array $options = [])
    {
        return $builder
            ->add('locale', ChoiceType::class, [
                'label'    => $this->translator->trans('Language of the translation'),
                'choices'  => Locale::allAsChoices()
            ])
            ->add('name', TextType::class, [
                'label'    => $this->translator->trans('Category name')
            ])
            ->add('description', TextType::class, [
                'label'    => $this->translator->trans('Category description'),
                'required' => false
            ])
            ->add('save', SubmitType::class, [
                'label'    => $this->translator->trans('Save')
            ]);
    }
}