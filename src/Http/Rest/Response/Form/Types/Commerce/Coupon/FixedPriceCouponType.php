<?php

namespace App\Http\Rest\Response\Form\Types\Commerce\Coupon;

use App\Database\Domain\Entity\Globals;
use App\Database\Domain\Entity\Commerce\Order\Coupon;
use App\Http\Rest\Response\Form\Types\AbstractFormType;
use App\Application\Service\GlobalsService;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 */
class FixedPriceCouponType extends AbstractFormType
{
    /** @var string */
    private $currency;

    public function __construct(TranslatorInterface $translator, GlobalsService $globalsService)
    {
        parent::__construct($translator);

        $this->currency = $globalsService->getByKey(Globals::APP_CURRENCY)->getValue();
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     *
     * @return FormBuilderInterface|void
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        return $builder
            ->add('code', TextType::class, [
                'label'    => $this->translator->trans('Code of the coupon')
            ])
            ->add('discountFixed', MoneyType::class, [
                'label'    => $this->translator->trans('Discount amount'),
                'currency' => $this->currency
            ])
            ->add('save', SubmitType::class, [
                'label'    => $this->translator->trans('Save')
            ]);
    }

    /**
     * @param OptionsResolver $resolver
     *
     * @return OptionsResolver|void
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        return $resolver->setDefaults([
            'allow_extra_fields' => true,
            'data_class'         => Coupon::class
        ]);
    }
}
