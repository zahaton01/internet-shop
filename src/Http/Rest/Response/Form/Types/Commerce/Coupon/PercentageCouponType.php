<?php

namespace App\Http\Rest\Response\Form\Types\Commerce\Coupon;

use App\Database\Domain\Entity\Commerce\Order\Coupon;
use App\Http\Rest\Response\Form\Types\AbstractFormType;
use Symfony\Component\Form\Extension\Core\Type\PercentType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 */
class PercentageCouponType extends AbstractFormType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     *
     * @return FormBuilderInterface|void
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        return $builder
            ->add('code', TextType::class, [
                'label' => $this->translator->trans('Code of the coupon')
            ])
            ->add('discountPercent', PercentType::class, [
                'label' => $this->translator->trans('Percentage')
            ])
            ->add('save', SubmitType::class, [
                'label' => $this->translator->trans('Save')
            ]);
    }

    /**
     * @param OptionsResolver $resolver
     *
     * @return OptionsResolver|void
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        return $resolver->setDefaults([
            'allow_extra_fields' => true,
            'data_class'         => Coupon::class
        ]);
    }
}
