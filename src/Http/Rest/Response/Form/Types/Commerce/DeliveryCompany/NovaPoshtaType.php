<?php

namespace App\Http\Rest\Response\Form\Types\Commerce\DeliveryCompany;

use App\Database\Domain\Entity\Globals;
use App\Http\Rest\Response\Form\Types\AbstractFormType;
use App\Application\Service\GlobalsService;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 */
class NovaPoshtaType extends AbstractFormType
{
    /** @var string */
    private $apiHost;
    /** @var bool */
    private $isEnabled;
    /** @var string */
    private $apiKey;
    /** @var string */
    private $currency;
    /** @var integer */
    private $price;

    public function __construct(TranslatorInterface $translator, GlobalsService $globalsService)
    {
        parent::__construct($translator);
        $this->apiHost   = (string) $globalsService->getByKey(Globals::NOVA_POSHTA_HOST);
        $this->isEnabled = $globalsService->getByKey(Globals::NOVA_POSHTA_IS_ENABLED)->toBoolean();
        $this->apiKey    = (string) $globalsService->getByKey(Globals::NOVA_POSHTA_API_KEY);
        $this->currency  = (string) $globalsService->getByKey(Globals::APP_CURRENCY);
        $this->price     = $globalsService->getByKey(Globals::NOVA_POSHTA_PRICE)->toInt();
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     *
     * @return FormBuilderInterface|void
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('is_enabled', CheckboxType::class, [
                'label'    => $this->translator->trans('Is nova poshta enabled in choices of delivery types?'),
                'required' => false,
                'data'     => $this->isEnabled
            ])
            ->add('api_host', TextType::class, [
                'disabled' => true,
                'data'     => $this->apiHost,
                'label'    => $this->translator->trans('Nova poshta api host')
            ])
            ->add('api_key', TextType::class, [
                'label'    => $this->translator->trans('Nova poshta api key'),
                'required' => false,
                'disabled' => true,
                'data'     => '*************************'
            ])
            ->add('price', MoneyType::class, [
                'currency' => $this->currency,
                'label'    => $this->translator->trans('The price of delivery by this company'),
                'data'     => $this->price
            ])
            ->add('save', SubmitType::class, [
                'label'    => $this->translator->trans('Save')
            ]);


        return $builder;
    }

    /**
     * @param OptionsResolver $resolver
     *
     * @return OptionsResolver|void
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        return $resolver->setDefaults(['allow_extra_fields' => true]);
    }
}
