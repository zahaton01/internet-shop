<?php

namespace App\Http\Rest\Response\Form\Types\Commerce\OnlinePayment\Liqpay;

use App\Database\Domain\Entity\Globals;
use App\Database\Domain\Entity\Commerce\Product\Product;
use App\Http\Rest\Response\Form\Types\AbstractFormType;
use App\Application\Service\GlobalsService;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 */
class LiqpaySettingsType extends AbstractFormType
{
    /** @var bool  */
    private $isEnabled;

    /** @var string  */
    private $apiUrl;

    /** @var string  */
    private $checkoutUrl;

    /** @var string  */
    private $publicKey;

    /** @var string  */
    private $privateKey;

    /**
     * LiqpayType constructor.
     * @param TranslatorInterface $translator
     * @param GlobalsService $globalsService
     */
    public function __construct(TranslatorInterface $translator, GlobalsService $globalsService)
    {
        parent::__construct($translator);

        $this->apiUrl      = (string) $globalsService->getByKey(Globals::LIQPAY_API_URL);
        $this->checkoutUrl = (string) $globalsService->getByKey(Globals::LIQPAY_CHECKOUT_URL);
        $this->publicKey   = (string) $globalsService->getByKey(Globals::LIQPAY_PUBLIC_KEY);
        $this->privateKey  = (string) $globalsService->getByKey(Globals::LIQPAY_PRIVATE_KEY);
        $this->isEnabled   = $globalsService->getByKey(Globals::LIQPAY_IS_ENABLED)->toBoolean();
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     *
     * @return FormBuilderInterface|void
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('is_enabled', CheckboxType::class, [
                'label'    => $this->translator->trans('Is LiqPay enabled in choices of payment types?'),
                'required' => false,
                'data'     => $this->isEnabled
            ])
            ->add('api_url', TextType::class, [
                'disabled' => true,
                'data'     => $this->apiUrl,
                'label'    => $this->translator->trans('LiqPay api url')
            ])
            ->add('checkout_url', TextType::class, [
                'disabled' => true,
                'data'     => $this->checkoutUrl,
                'label'    => $this->translator->trans('LiqPay checkout url')
            ])
            ->add('public_key', TextType::class, [
                'label'    => $this->translator->trans('LiqPay public key'),
                'required' => false,
                'disabled' => true,
                'data'     => '*************************'
            ])
            ->add('private_key', TextType::class, [
                'label'    => $this->translator->trans('LiqPay private key'),
                'required' => false,
                'disabled' => true,
                'data'     => '*************************'
            ])
            ->add('save', SubmitType::class, [
                'label'    => $this->translator->trans('Save')
            ]);


        return $builder;
    }

    /**
     * @param OptionsResolver $resolver
     *
     * @return OptionsResolver|void
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        return $resolver->setDefaults(['allow_extra_fields' => true]);
    }
}