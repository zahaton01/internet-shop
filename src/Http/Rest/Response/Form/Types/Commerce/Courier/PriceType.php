<?php

namespace App\Http\Rest\Response\Form\Types\Commerce\Courier;

use App\Database\Domain\Entity\Globals;
use App\Http\Rest\Response\Form\Types\AbstractFormType;
use App\Application\Service\GlobalsService;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 */
class PriceType extends AbstractFormType
{
    /** @var string */
    private $currency;

    /** @var integer */
    private $price;

    public function __construct(TranslatorInterface $translator, GlobalsService $globalsService)
    {
        parent::__construct($translator);

        $this->currency = $globalsService->getByKey(Globals::APP_CURRENCY);
        $this->price    = $globalsService->getByKey(Globals::COURIER_PRICE);
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     *
     * @return FormBuilderInterface|void
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('price', MoneyType::class, [
                'currency' => $this->currency,
                'label'    => $this->translator->trans('The price of delivery by courier'),
                'data'     => $this->price->toInt()
            ])
            ->add('save', SubmitType::class, [
                'label'    => $this->translator->trans('Save')
            ]);


        return $builder;
    }

    /**
     * @param OptionsResolver $resolver
     *
     * @return OptionsResolver|void
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        return $resolver->setDefaults(['allow_extra_fields' => true]);
    }
}