<?php

namespace App\Http\Rest\Response\Form\Types\Commerce\Courier;

use App\Database\Domain\Entity\Commerce\Order\Courier;
use App\Http\Rest\Response\Form\Types\AbstractFormType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 */
class CourierType extends AbstractFormType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     *
     * @return FormBuilderInterface|void
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [
                'label' => $this->translator->trans('Courier name')
            ])
            ->add('save', SubmitType::class, [
                'label' => $this->translator->trans('Save')
            ]);

        return $builder;
    }

    /**
     * @param OptionsResolver $resolver
     *
     * @return OptionsResolver|void
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        return $resolver->setDefaults([
            'allow_extra_fields' => true,
            'data_class'         => Courier::class
        ]);
    }
}