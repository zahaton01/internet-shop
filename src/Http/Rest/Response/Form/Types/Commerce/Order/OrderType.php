<?php

namespace App\Http\Rest\Response\Form\Types\Commerce\Order;

use App\Application\Libs\Api\NovaPoshta\NovaPoshtaApi;
use App\Database\Domain\Entity\Commerce\Order\Order;
use App\Http\Rest\Response\Form\ChoiceBuilder\Delivery\DeliveryChoiceBuilder;
use App\Http\Rest\Response\Form\ChoiceBuilder\Payment\PaymentTypeChoiceBuilder;
use App\Http\Rest\Response\Form\Types\AbstractFormType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 */
class OrderType extends AbstractFormType
{
    /** @var DeliveryChoiceBuilder  */
    private $deliveryChoiceBuilder;

    /** @var PaymentTypeChoiceBuilder  */
    private $paymentTypeChoiceBuilder;

    /** @var NovaPoshtaApi  */
    private $novaPoshtaApi;

    public function __construct(
        TranslatorInterface $translator,
        DeliveryChoiceBuilder $deliveryChoiceBuilder,
        PaymentTypeChoiceBuilder $paymentTypeChoiceBuilder,
        NovaPoshtaApi $novaPoshtaApi
    ) {
        parent::__construct($translator);

        $this->deliveryChoiceBuilder    = $deliveryChoiceBuilder;
        $this->paymentTypeChoiceBuilder = $paymentTypeChoiceBuilder;
        $this->novaPoshtaApi            = $novaPoshtaApi;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     *
     * @return FormBuilderInterface|void
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [
                'label'               => $this->translator->trans('Your name'),
                'label_attr'          => ['class' => 'fw500 text-dark']
            ])
            ->add('surname', TextType::class, [
                'label'               => $this->translator->trans('Your surname'),
                'label_attr'          => ['class' => 'fw500 text-dark'],
                'required'            => false
            ])
            ->add('city', TextType::class, [
                'label'               => $this->translator->trans('Your city'),
                'label_attr'          => ['class' => 'fw500 text-dark'],
                'row_attr'            => ['id' => 'js-city'],
                'required'            => false
            ])
            ->add('address', TextType::class, [
                'label'               => $this->translator->trans('Your address'),
                'label_attr'          => ['class' => 'fw500 text-dark'],
                'row_attr'            => ['id' => 'js-address'],
                'required'            => false
            ])
            ->add('coupon', TextType::class, [
                'label'               => $this->translator->trans('Coupon'),
                'label_attr'          => ['class' => 'fw500 text-dark'],
                'required'            => false,
                'mapped'              => false
            ])
            ->add('phone', TextType::class, [
                'label'               => $this->translator->trans('Your phone number'),
                'label_attr'          => ['class' => 'fw500 text-dark']
            ])
            ->add('email', TextType::class, [
                'label'               => $this->translator->trans('Your contact email'),
                'label_attr'          => ['class' => 'fw500 text-dark'],
                'required'            => false
            ])
            ->add('deliveryType', ChoiceType::class, [
                'choices'             => $this->deliveryChoiceBuilder->types(),
                'label'               => false
            ])
            ->add('paymentType', ChoiceType::class, [
                'choices'             => $this->paymentTypeChoiceBuilder->types(),
                'label'               => false
            ])
            ->add('comment', TextType::class, [
                'label'               => false,
                'required'            => false
            ])
            ->add('nova_city', ChoiceType::class, [
                'choices'             => $this->novaPoshtaApi->getCitiesAsChoices(),
                'required'            => false,
                'label'               => $this->translator->trans('Nova poshta city'),
                'label_attr'          => ['class' => 'fw500 text-dark'],
                'mapped'              => false,
                'row_attr'            => ['id' => 'js-nova-city'],
                'allow_extra_fields'  => true,
                'group_by'            => function ($choice, $key, $value) {
                    return mb_strtoupper(mb_substr((string) $key, 0, 1));
                }
            ])
            ->add('nova_warehouse', ChoiceType::class, [
                'choices'             => [],
                'required'            => false,
                'label'               => $this->translator->trans('Nova poshta warehouse'),
                'label_attr'          => ['class' => 'fw500 text-dark'],
                'mapped'              => false,
                'row_attr'            => ['id' => 'js-nova-warehouse'],
                'allow_extra_fields'  => true
            ])
            ->add('save', SubmitType::class, [
                'label'               => $this->translator->trans('Checkout')
            ]);

        $builder->get('nova_warehouse')->resetViewTransformers();

        return $builder;
    }

    /**
     * @param OptionsResolver $resolver
     *
     * @return OptionsResolver|void
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        return $resolver->setDefaults([
            'allow_extra_fields' => true,
            'data_class'         => Order::class
        ]);
    }
}