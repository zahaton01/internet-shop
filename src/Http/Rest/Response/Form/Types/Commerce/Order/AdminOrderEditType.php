<?php

namespace App\Http\Rest\Response\Form\Types\Commerce\Order;

use App\Application\Libs\Api\NovaPoshta\NovaPoshtaApi;
use App\Database\Domain\Entity\Commerce\Order\Courier;
use App\Database\Domain\Entity\Commerce\Order\Order;
use App\Http\Rest\Response\Form\ChoiceBuilder\Delivery\DeliveryChoiceBuilder;
use App\Http\Rest\Response\Form\ChoiceBuilder\Payment\PaymentTypeChoiceBuilder;
use App\Http\Rest\Response\Form\Types\AbstractFormType;
use App\Database\Domain\Model\EntityState\Order\OrderPaymentStatus;
use App\Database\Domain\Model\EntityState\Order\OrderStatus;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 */
class AdminOrderEditType extends AbstractFormType
{
    /** @var DeliveryChoiceBuilder  */
    private $deliveryChoiceBuilder;

    /** @var PaymentTypeChoiceBuilder  */
    private $paymentTypeChoiceBuilder;

    /** @var NovaPoshtaApi */
    private $novaPoshtaApi;

    /** @var OrderPaymentStatus */
    private $orderPaymentStatus;

    /** @var OrderStatus */
    private $orderStatus;

    public function __construct(
        TranslatorInterface $translator,
        DeliveryChoiceBuilder $deliveryChoiceBuilder,
        PaymentTypeChoiceBuilder $paymentTypeChoiceBuilder,
        NovaPoshtaApi $novaPoshtaApi,
        OrderPaymentStatus $orderPaymentStatus,
        OrderStatus $orderStatus
    ) {
        parent::__construct($translator);

        $this->deliveryChoiceBuilder    = $deliveryChoiceBuilder;
        $this->paymentTypeChoiceBuilder = $paymentTypeChoiceBuilder;
        $this->novaPoshtaApi            = $novaPoshtaApi;
        $this->orderPaymentStatus       = $orderPaymentStatus;
        $this->orderStatus              = $orderStatus;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     *
     * @return FormBuilderInterface|void
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /** @var Order $order */
        $order = $builder->getData();

        $builder
            ->add('paymentStatus', ChoiceType::class, [
                'choices'            => $this->orderPaymentStatus->allAsTranslatedChoices(),
                'label'              => $this->translator->trans('Payment status')
            ])
            ->add('status', ChoiceType::class, [
                'choices'            => $this->orderStatus->allAsTranslatedChoices(),
                'label'              => $this->translator->trans('Order status')
            ])
            ->add('couriers', EntityType::class, [
                'label'              => $this->translator->trans('Set couriers to this order'),
                'class'              => Courier::class,
                'choice_label'       => 'name',
                'multiple'           => true,
                'expanded'           => true
            ])
            ->add('name', TextType::class, [
                'label'              => $this->translator->trans('Name')
            ])
            ->add('surname', TextType::class, [
                'label'              => $this->translator->trans('Surname'),
                'required'           => false
            ])
            ->add('city', TextType::class, [
                'label'              => $this->translator->trans('City'),
                'row_attr'           => ['id' => 'js-city'],
                'required'           => false
            ])
            ->add('address', TextType::class, [
                'label'              => $this->translator->trans('Address'),
                'row_attr'           => ['id' => 'js-address'],
                'required'           => false
            ])
            ->add('phone', TextType::class, [
                'label'              => $this->translator->trans('Phone number')
            ])
            ->add('email', TextType::class, [
                'label'              => $this->translator->trans('Email'),
                'required'           => false
            ])
            ->add('deliveryType', ChoiceType::class, [
                'choices'            => $this->deliveryChoiceBuilder->types(),
                'label'              => false
            ])
            ->add('paymentType', ChoiceType::class, [
                'choices'            => $this->paymentTypeChoiceBuilder->types(),
                'label'              => false
            ])
            ->add('comment', TextType::class, [
                'label'              => $this->translator->trans('Comment'),
                'required'           => false
            ])
            ->add('save', SubmitType::class, [
                'label'              => $this->translator->trans('Save')
            ]);

        if ($order->isDeliveryNovaPoshta()) {
            $builder
                ->add('novaCity', ChoiceType::class, [
                    'choices'            => $this->novaPoshtaApi->getCitiesAsChoices(),
                    'required'           => false,
                    'label'              => $this->translator->trans('Nova poshta city'),
                    'row_attr'           => ['id' => 'js-nova-city'],
                    'allow_extra_fields' => true,
                    'group_by'           => function ($choice, $key, $value) {
                        return mb_strtoupper(mb_substr((string) $key, 0, 1));
                    },
                    'mapped'             => false,
                    'data'               => $order->getDeliveryData()->getCityRef()
                ])
                ->add('novaWarehouse', ChoiceType::class, [
                    'choices'            => $this->novaPoshtaApi->getWarehousesAsChoices($order->getDeliveryData()->getCityRef()),
                    'required'           => false,
                    'label'              => $this->translator->trans('Nova poshta warehouse'),
                    'row_attr'           => ['id' => 'js-nova-warehouse'],
                    'mapped'             => false,
                    'data'               => $order->getDeliveryData()->getWarehouseRef()
                ]);

            $builder->get('novaWarehouse')->resetViewTransformers();
        }

        return $builder;
    }

    /**
     * @param OptionsResolver $resolver
     *
     * @return OptionsResolver|void
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        return $resolver->setDefaults([
            'allow_extra_fields' => true,
            'data_class'         => Order::class
        ]);
    }
}