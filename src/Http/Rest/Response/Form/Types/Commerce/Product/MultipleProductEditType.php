<?php

namespace App\Http\Rest\Response\Form\Types\Commerce\Product;

use App\Database\Domain\Entity\Commerce\Product\Category\ProductCategory;
use App\Database\Domain\Entity\Commerce\Product\SubCategory\ProductSubCategory;
use App\Http\Rest\Response\Form\Types\AbstractFormType;
use App\Database\Domain\Model\EntityState\Availability;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 */
class MultipleProductEditType extends AbstractFormType
{
    /** @var \Closure  */
    private $categoryQuery;

    /** @var Availability */
    private $availability;

    public function __construct(TranslatorInterface $translator, Availability $availability)
    {
        parent::__construct($translator);

        $this->categoryQuery = function (EntityRepository $er) {
            return $er->createQueryBuilder('c')
                ->orderBy('c.name', 'ASC');
        };
        $this->availability  = $availability;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     *
     * @return FormBuilderInterface|void
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('availability', ChoiceType::class, [
                'choices'       => $this->availability->allAsTranslatedChoices(),
                'label'         => $this->translator->trans('Availability'),
                'help'          => $this->translator->trans('Is your product in stock?'),
                'mapped'        => false
            ])
            ->add('isVisible', CheckboxType::class, [
                'label'         => $this->translator->trans('Show this product in catalog?'),
                'help'          => $this->translator->trans('If yes the product will be visible in catalog and people will be able to buy it'),
                'required'      => false,
                'mapped'        => false
            ])
            ->add('category', EntityType::class, [
                'class'         => ProductCategory::class,
                'label'         => $this->translator->trans('Category'),
                'choice_label'  => 'name',
                'query_builder' => $this->categoryQuery,
                'group_by'      => $this->groupByFirstLetter,
                'required'      => false,
                'empty_data'    => null,
                'mapped'        => false
            ])
            ->add('subCategory', EntityType::class, [
                'class'         => ProductSubCategory::class,
                'label'         => $this->translator->trans('Sub category'),
                'choice_label'  => 'name',
                'query_builder' => $this->categoryQuery,
                'group_by'      => $this->groupByFirstLetter,
                'required'      => false,
                'mapped'        => false
            ])
            ->add('save', SubmitType::class, [
                'label'         => $this->translator->trans('Save')
            ]);

        return $builder;
    }

    /**
     * @param OptionsResolver $resolver
     *
     * @return OptionsResolver|void
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        return $resolver->setDefaults(['allow_extra_fields' => true]);
    }
}
