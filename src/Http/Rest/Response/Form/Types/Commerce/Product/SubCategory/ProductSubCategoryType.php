<?php

namespace App\Http\Rest\Response\Form\Types\Commerce\Product\SubCategory;

use App\Database\Domain\Entity\Commerce\Product\Category\ProductCategory;
use App\Database\Domain\Entity\Commerce\Product\SubCategory\Meta\ProductSubCategoryMeta;
use App\Database\Domain\Entity\Commerce\Product\SubCategory\ProductSubCategory;
use App\Http\Rest\Response\Form\DataTransformer\SlugTransformer;
use App\Http\Rest\Response\Form\Types\AbstractFormType;
use App\Http\Rest\Response\Form\Types\MetaType;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 */
class ProductSubCategoryType extends AbstractFormType
{
    /** @var SlugTransformer */
    protected $slugTransformer;

    public function __construct(TranslatorInterface $translator, SlugTransformer $slugTransformer)
    {
        parent::__construct($translator);

        $this->slugTransformer = $slugTransformer;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     *
     * @return FormBuilderInterface|void
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [
                'label'      => $this->translator->trans('Sub category name')
            ])
            ->add('description', TextType::class, [
                'label'      => $this->translator->trans('Sub category description'),
                'required'   => false
            ])
            ->add('slug', TextType::class, [
                'label'      => $this->translator->trans('Sub category url name')
            ])
            ->add('productCategory', EntityType::class, [
                'class'         => ProductCategory::class,
                'label'         => $this->translator->trans('Category'),
                'choice_label'  => 'name',
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('c')
                        ->orderBy('c.name', 'ASC');
                },
                'group_by'      => $this->groupByFirstLetter,
                'required'      => false,
                'empty_data'    => null
            ])
            ->add('meta', MetaType::class, [
                'label'      => $this->translator->trans('Meta data'),
                'data_class' => ProductSubCategoryMeta::class
            ])
            ->add('save', SubmitType::class, [
                'label'      => $this->translator->trans('Save')
            ]);

        $builder->get('slug')
            ->addModelTransformer($this->slugTransformer);

        return $builder;
    }

    /**
     * @param OptionsResolver $resolver
     *
     * @return OptionsResolver|void
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        return $resolver->setDefaults([
            'allow_extra_fields' => true,
            'data_class'         => ProductSubCategory::class
        ]);
    }
}