<?php

namespace App\Http\Rest\Response\Form\Types\Commerce\Product;

use App\Database\Domain\Entity\Globals;
use App\Database\Domain\Entity\Commerce\Product\ProductPromo;
use App\Http\Rest\Response\Form\Types\AbstractFormType;
use App\Application\Service\GlobalsService;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 */
class ProductPromoType extends AbstractFormType
{
    /** @var string */
    private $currency;

    public function __construct(TranslatorInterface $translator, GlobalsService $globalsService)
    {
        parent::__construct($translator);

        $this->currency = (string) $globalsService->getByKey(Globals::APP_CURRENCY);
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     *
     * @return FormBuilderInterface|void
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        return $builder
            ->add('saleStatus', CheckboxType::class, [
                'label'      => $this->translator->trans('Is product on sale?'),
                'required'   => false
            ])
            ->add('salePrice', MoneyType::class, [
                'label'      => $this->translator->trans('Product sale price'),
                'currency'   => $this->currency,
                'required'   => false,
                'help'       => $this->translator->trans('Leave this field empty if the product is not on sale')
            ])
            ->add('isSpecialStatus', CheckboxType::class, [
                'label'      => $this->translator->trans('Has the product special status?'),
                'required'   => false
            ])
            ->add('specialStatus', ChoiceType::class, [
                'label'      => $this->translator->trans('Product special status'),
                'choices'    => [
                    $this->translator->trans(ProductPromo::SPECIAL_STATUS_OUT_SOON) => ProductPromo::SPECIAL_STATUS_OUT_SOON,
                    $this->translator->trans(ProductPromo::SPECIAL_STATUS_TOP_SALE) => ProductPromo::SPECIAL_STATUS_TOP_SALE
                ],
                'required'   => false,
                'empty_data' => null,
                'help'       => $this->translator->trans('Leave this field empty if the product has not special status')
            ]);
    }

    /**
     * @param OptionsResolver $resolver
     *
     * @return OptionsResolver|void
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        return $resolver->setDefaults([
            'allow_extra_fields' => true,
            'data_class'         => ProductPromo::class
        ]);
    }
}
