<?php

namespace App\Http\Rest\Response\Form\Types\Commerce\Product;

use App\Application\Service\GlobalsService;
use App\Database\Domain\Entity\Commerce\Product\Category\ProductCategory;
use App\Database\Domain\Entity\Commerce\Product\Meta\ProductMeta;
use App\Database\Domain\Entity\Commerce\Product\Product;
use App\Database\Domain\Entity\Commerce\Product\SubCategory\ProductSubCategory;
use App\Database\Domain\Entity\Globals;
use App\Database\Domain\Model\EntityState\Availability;
use App\Http\Rest\Response\Form\DataTransformer\SlugTransformer;
use App\Http\Rest\Response\Form\Types\AbstractFormType;
use App\Http\Rest\Response\Form\Types\MetaType;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 */
class ProductType extends AbstractFormType
{
    /** @var \Closure  */
    private $categoryQuery;

    /** @var SlugTransformer */
    private $slugTransformer;

    /** @var string */
    private $currency;

    /** @var Availability */
    private $availability;

    public function __construct(
        TranslatorInterface $translator,
        SlugTransformer $slugTransformer,
        GlobalsService $globalsService,
        Availability $availability
    ) {
        parent::__construct($translator);

        $this->slugTransformer = $slugTransformer;
        $this->categoryQuery   = function (EntityRepository $er) {
            return $er->createQueryBuilder('c')
                ->orderBy('c.name', 'ASC');
        };
        $this->currency        = $globalsService->getByKey(Globals::APP_CURRENCY)->getValue();
        $this->availability    = $availability;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     *
     * @return FormBuilderInterface|void
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [
                'label'         => $this->translator->trans('Product name')
            ])
            ->add('slug', TextType::class, [
                'label'         => $this->translator->trans('Product url name'),
                'help'          => $this->translator->trans('It specifies the url of the product')
            ])
            ->add('availability', ChoiceType::class, [
                'choices'       => $this->availability->allAsTranslatedChoices(),
                'label'         => $this->translator->trans('Availability'),
                'help'          => $this->translator->trans('Is your product in stock?')
            ])
            ->add('isVisible', CheckboxType::class, [
                'label'         => $this->translator->trans('Show this product in catalog?'),
                'help'          => $this->translator->trans('If yes the product will be visible in catalog and people will be able to buy it'),
                'required'      => false
            ])
            ->add('price', MoneyType::class, [
                'label'         => $this->translator->trans('Product price'),
                'currency'      => $this->currency,
                'required'      => false
            ])
            ->add('vendorCode', TextType::class, [
                'label'         => $this->translator->trans('Product vendor code'),
                'required'      => false
            ])
            ->add('productCategory', EntityType::class, [
                'class'         => ProductCategory::class,
                'label'         => $this->translator->trans('Category'),
                'choice_label'  => 'name',
                'query_builder' => $this->categoryQuery,
                'group_by'      => $this->groupByFirstLetter,
                'required'      => false,
                'empty_data'    => null
            ])
            ->add('productSubCategory', EntityType::class, [
                'class'         => ProductSubCategory::class,
                'label'         => $this->translator->trans('Sub category'),
                'choice_label'  => 'name',
                'query_builder' => $this->categoryQuery,
                'group_by'      => $this->groupByFirstLetter,
                'required'      => false
            ])
            ->add('description', TextType::class, [
                'label'         => $this->translator->trans('Product description'),
                'required'      => false
            ])
            ->add('promo', ProductPromoType::class)
            ->add('meta', MetaType::class, [
                'label'         => $this->translator->trans('Meta data'),
                'data_class'    => ProductMeta::class
            ])
            ->add('save', SubmitType::class, [
                'label'         => $this->translator->trans('Save')
            ]);

        $builder->get('slug')
            ->addModelTransformer($this->slugTransformer);

        return $builder;
    }

    /**
     * @param OptionsResolver $resolver
     *
     * @return OptionsResolver|void
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        return $resolver->setDefaults([
            'allow_extra_fields' => true,
            'data_class'         => Product::class
        ]);
    }
}