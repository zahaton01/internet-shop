<?php

namespace App\Http\Rest\Response\Form\Types\Commerce\Product\Category;

use App\Database\Domain\Entity\Commerce\Product\Category\Meta\ProductCategoryMeta;
use App\Database\Domain\Entity\Commerce\Product\Category\ProductCategory;
use App\Http\Rest\Response\Form\DataTransformer\SlugTransformer;
use App\Http\Rest\Response\Form\Types\AbstractFormType;
use App\Http\Rest\Response\Form\Types\MetaType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 */
class ProductCategoryType extends AbstractFormType
{
    /** @var SlugTransformer */
    protected $slugTransformer;

    public function __construct(TranslatorInterface $translator, SlugTransformer $slugTransformer)
    {
        parent::__construct($translator);

        $this->slugTransformer = $slugTransformer;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     *
     * @return FormBuilderInterface|void
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [
                'label'      => $this->translator->trans('Category name')
            ])
            ->add('description', TextType::class, [
                'label'      => $this->translator->trans('Category description'),
                'required'   => false
            ])
            ->add('slug', TextType::class, [
                'label'      => $this->translator->trans('Category url name')
            ])
            ->add('meta', MetaType::class, [
                'label'      => $this->translator->trans('Meta data'),
                'data_class' => ProductCategoryMeta::class
            ])
            ->add('save', SubmitType::class, [
                'label'      => $this->translator->trans('Save')
            ]);

        $builder->get('slug')
            ->addModelTransformer($this->slugTransformer);

        return $builder;
    }

    /**
     * @param OptionsResolver $resolver
     *
     * @return OptionsResolver|void
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        return $resolver->setDefaults([
            'allow_extra_fields' => true,
            'data_class'         => ProductCategory::class
        ]);
    }
}