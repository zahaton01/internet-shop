<?php

namespace App\Http\Rest\Response\Form\Types\Commerce\Product;

use App\Database\Domain\Entity\Commerce\Product\Meta\ProductMetaTranslation;
use App\Http\Rest\Response\Form\Types\MetaTranslationType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 */
class ProductMetaTranslationType extends MetaTranslationType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     *
     * @return FormBuilderInterface|void
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        return $this->extend($builder, $options);
    }

    /**
     * @param OptionsResolver $resolver
     *
     * @return OptionsResolver|void
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        return $resolver->setDefaults([
            'allow_extra_fields' => true,
            'data_class'         => ProductMetaTranslation::class
        ]);
    }
}