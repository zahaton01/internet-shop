<?php

namespace App\Http\Rest\Response\Form\Types\Commerce\Product\Specification;

use App\Database\Domain\Entity\Commerce\Product\Specification\SpecificationValue;
use App\Http\Rest\Response\Form\Types\AbstractFormType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 */
class SpecificationValueType extends AbstractFormType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     *
     * @return FormBuilderInterface|void
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        return $builder
            ->add('value', TextType::class, [
                'label' => $this->translator->trans('Specification value')
            ])
            ->add('save', SubmitType::class, [
                'label' => $this->translator->trans('Save')
            ]);
    }

    /**
     * @param OptionsResolver $resolver
     *
     * @return OptionsResolver|void
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        return $resolver->setDefaults([
            'allow_extra_fields' => true,
            'data_class'         => SpecificationValue::class
        ]);
    }
}
