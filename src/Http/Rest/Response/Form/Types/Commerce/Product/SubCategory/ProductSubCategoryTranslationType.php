<?php

namespace App\Http\Rest\Response\Form\Types\Commerce\Product\SubCategory;

use App\Database\Domain\Entity\Commerce\Product\SubCategory\ProductSubCategoryTranslation;
use App\Http\Rest\Response\Form\Types\AbstractCategoryTranslationType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 */
class ProductSubCategoryTranslationType extends AbstractCategoryTranslationType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     *
     * @return FormBuilderInterface|void
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        return $this->extend($builder, $options);
    }

    /**
     * @param OptionsResolver $resolver
     *
     * @return OptionsResolver|void
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        return $resolver->setDefaults(
            [
                'allow_extra_fields' => true,
                'data_class'         => ProductSubCategoryTranslation::class
            ]
        );
    }
}