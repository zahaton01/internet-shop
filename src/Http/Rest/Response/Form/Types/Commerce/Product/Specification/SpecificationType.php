<?php

namespace App\Http\Rest\Response\Form\Types\Commerce\Product\Specification;

use App\Database\Domain\Entity\Commerce\Product\Category\ProductCategory;
use App\Database\Domain\Entity\Commerce\Product\Specification\Specification;
use App\Database\Domain\Entity\Commerce\Product\SubCategory\ProductSubCategory;
use App\Http\Rest\Response\Form\Types\AbstractFormType;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 */
class SpecificationType extends AbstractFormType
{
    /** @var \Closure  */
    private $categoryQuery;

    public function __construct(TranslatorInterface $translator)
    {
        parent::__construct($translator);

        $this->categoryQuery = function (EntityRepository $er) {
            return $er->createQueryBuilder('c')
                ->orderBy('c.name', 'ASC');
        };
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     *
     * @return FormBuilderInterface|void
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        return $builder
            ->add('name', TextType::class, [
                'label'         => $this->translator->trans('Specification name')
            ])
            ->add('productCategory', EntityType::class, [
                'class'         => ProductCategory::class,
                'label'         => $this->translator->trans('Category'),
                'choice_label'  => 'name',
                'query_builder' => $this->categoryQuery,
                'group_by'      => $this->groupByFirstLetter,
                'required'      => false,
                'empty_data'    => null
            ])
            ->add('productSubCategory', EntityType::class, [
                'class'         => ProductSubCategory::class,
                'label'         => $this->translator->trans('Sub category'),
                'choice_label'  => 'name',
                'query_builder' => $this->categoryQuery,
                'group_by'      => $this->groupByFirstLetter,
                'required'      => false
            ])
            ->add('isEnabled', CheckboxType::class, [
                'label'         => $this->translator->trans('Is enabled for catalog filters?'),
                'required'      => false
            ])
            ->add('save', SubmitType::class, [
                'label'         => $this->translator->trans('Save')
            ]);
    }

    /**
     * @param OptionsResolver $resolver
     *
     * @return OptionsResolver|void
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        return $resolver->setDefaults([
            'allow_extra_fields' => true,
            'data_class'         => Specification::class
        ]);
    }
}
