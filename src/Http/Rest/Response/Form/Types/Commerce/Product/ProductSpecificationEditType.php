<?php

namespace App\Http\Rest\Response\Form\Types\Commerce\Product;

use App\Database\Domain\Entity\Commerce\Product\Category\ProductCategory;
use App\Database\Domain\Entity\Commerce\Product\Specification\ProductSpecification;
use App\Database\Domain\Entity\Commerce\Product\Specification\Specification;
use App\Database\Domain\Entity\Commerce\Product\Specification\SpecificationValue;
use App\Database\Domain\Entity\Commerce\Product\SubCategory\ProductSubCategory;
use App\Http\Rest\Response\Form\Types\AbstractFormType;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 */
class ProductSpecificationEditType extends AbstractFormType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     *
     * @return FormBuilderInterface|void
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('productCategory', EntityType::class, [
                'class'         => ProductCategory::class,
                'label'         => $this->translator->trans('Category'),
                'choice_label'  => 'name',
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('c')
                        ->orderBy('c.name', 'ASC');
                },
                'group_by'      => $this->groupByFirstLetter,
                'required'      => false,
                'empty_data'    => null
            ])
            ->add('productSubCategory', EntityType::class, [
                'class'         => ProductSubCategory::class,
                'label'         => $this->translator->trans('Sub category'),
                'choice_label'  => 'name',
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('sc')
                        ->orderBy('sc.name', 'ASC');
                },
                'group_by'      => $this->groupByFirstLetter,
                'required'      => false
            ])
            ->add('values', EntityType::class, [
                'class'         => SpecificationValue::class,
                'label'         => $this->translator->trans('Specification values'),
                'choice_label'  => 'value',
                'query_builder' => function (EntityRepository $er) use ($options) {
                    return $er->createQueryBuilder('sv')
                        ->leftJoin('sv.specification', 's')
                        ->where("s.id = '{$options['specification_id']}'");
                },
                'multiple'      => true,
                'expanded'      => true
            ])
            ->add('save', SubmitType::class, [
                'label'         => $this->translator->trans('Save')
            ]);

        return $builder;
    }

    /**
     * @param OptionsResolver $resolver
     *
     * @return OptionsResolver|void
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        return $resolver->setDefaults([
            'allow_extra_fields'  => true,
            'data_class'          => ProductSpecification::class,
            'specification_id'    => null,
            'method'              => 'PUT'
        ]);
    }
}
