<?php

namespace App\Http\Rest\Response\Form\Types\Commerce\Product\Category;

use App\Database\Domain\Entity\Commerce\Product\Category\ProductCategoryTranslation;
use App\Http\Rest\Response\Form\Types\AbstractCategoryTranslationType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 */
class ProductCategoryTranslationType extends AbstractCategoryTranslationType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     *
     * @return FormBuilderInterface|void
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        return $this->extend($builder, $options);
    }

    /**
     * @param OptionsResolver $resolver
     *
     * @return OptionsResolver|void
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        return $resolver->setDefaults(
            [
                'allow_extra_fields' => true,
                'data_class'         => ProductCategoryTranslation::class
            ]
        );
    }
}