<?php

namespace App\Http\Rest\Response\Form\Types\Site\Post;

use App\Database\Domain\Entity\Site\Post\Meta\PostMetaTranslation;
use App\Http\Rest\Response\Form\Types\MetaTranslationType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 */
class PostMetaTranslationType extends MetaTranslationType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     *
     * @return FormBuilderInterface|void
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        return $this->extend($builder, $options);
    }

    /**
     * @param OptionsResolver $resolver
     *
     * @return OptionsResolver|void
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        return $resolver->setDefaults([
           'allow_extra_fields' => true,
           'data_class'         => PostMetaTranslation::class
        ]);
    }
}