<?php

namespace App\Http\Rest\Response\Form\Types\Site\Post\Media;

use App\Database\Domain\Entity\Site\Post\Media\PostImage;
use App\Http\Rest\Response\Form\Types\AbstractFormType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 */
class PostImageInternalType extends AbstractFormType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     *
     * @return FormBuilderInterface|void
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        return $builder
            ->add('attachment', FileType::class, [
                'label'  => $this->translator->trans('Image'),
                'mapped' => false
            ])
            ->add('save', SubmitType::class, [
                'label'  => $this->translator->trans('Save')
            ]);
    }

    /**
     * @param OptionsResolver $resolver
     *
     * @return OptionsResolver|void
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        return $resolver->setDefaults([
            'allow_extra_fields' => true,
            'data_class'         => PostImage::class
        ]);
    }
}