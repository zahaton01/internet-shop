<?php

namespace App\Http\Rest\Response\Form\Types\Site\Post;

use App\Application\Model\AppEnv\Locale;
use App\Database\Domain\Entity\Site\Post\PostTranslation;
use App\Http\Rest\Response\Form\Types\AbstractFormType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 */
class PostTranslationType extends AbstractFormType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     *
     * @return FormBuilderInterface|void
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('locale', ChoiceType::class, [
                'label'    => $this->translator->trans('Language of the translation'),
                'choices'  => Locale::allAsChoices()
            ])
            ->add('title', TextType::class, [
                'label'      => $this->translator->trans('Title')
            ])
            ->add('content', TextType::class, [
                'label'      => $this->translator->trans('Content'),
                'required'   => false
            ])
            ->add('save', SubmitType::class, [
                'label'      => $this->translator->trans('Save')
            ]);

        return $builder;
    }

    /**
     * @param OptionsResolver $resolver
     *
     * @return OptionsResolver|void
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        return $resolver->setDefaults([
            'allow_extra_fields' => true,
            'data_class'         => PostTranslation::class
        ]);
    }
}