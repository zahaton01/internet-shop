<?php

namespace App\Http\Rest\Response\Form\Types\Site\Post;

use App\Database\Domain\Entity\Site\Post\Meta\PostMeta;
use App\Database\Domain\Entity\Site\Post\Post;
use App\Http\Rest\Response\Form\DataTransformer\SlugTransformer;
use App\Http\Rest\Response\Form\Types\AbstractFormType;
use App\Http\Rest\Response\Form\Types\MetaType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 */
class PostType extends AbstractFormType
{
    /** @var SlugTransformer */
    protected $slugTransformer;

    public function __construct(TranslatorInterface $translator, SlugTransformer $slugTransformer)
    {
        parent::__construct($translator);

        $this->slugTransformer = $slugTransformer;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     *
     * @return FormBuilderInterface|void
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class, [
                'label'      => $this->translator->trans('Title')
            ])
            ->add('content', TextType::class, [
                'label'      => $this->translator->trans('Content'),
                'required'   => false
            ])
            ->add('type', ChoiceType::class, [
                'label'      => $this->translator->trans('Type'),
                'choices'    => [
                    $this->translator->trans(Post::NEWS)  => Post::NEWS,
                    $this->translator->trans(Post::PROMO) => Post::PROMO
                ]
            ])
            ->add('isVisible', CheckboxType::class, [
                'label'      => $this->translator->trans('Is post visible'),
                'required'   => false
            ])
            ->add('slug', TextType::class, [
                'label'      => $this->translator->trans('Url name')
            ])
            ->add('meta', MetaType::class, [
                'label'      => $this->translator->trans('Meta data'),
                'data_class' => PostMeta::class
            ])
            ->add('save', SubmitType::class, [
                'label'      => $this->translator->trans('Save')
            ]);

        $builder->get('slug')
            ->addModelTransformer($this->slugTransformer);

        return $builder;
    }

    /**
     * @param OptionsResolver $resolver
     *
     * @return OptionsResolver|void
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        return $resolver->setDefaults([
            'allow_extra_fields' => true,
            'data_class'         => Post::class
        ]);
    }
}