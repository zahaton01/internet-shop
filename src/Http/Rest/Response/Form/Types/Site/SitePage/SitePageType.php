<?php

namespace App\Http\Rest\Response\Form\Types\Site\SitePage;

use App\Application\Service\GlobalsService;
use App\Database\Domain\Entity\Site\SitePage\Group\SitePageGroup;
use App\Database\Domain\Entity\Site\SitePage\Meta\SitePageMeta;
use App\Database\Domain\Entity\Site\SitePage\SitePage;
use App\Http\Rest\Response\Form\DataTransformer\SlugTransformer;
use App\Http\Rest\Response\Form\Types\AbstractFormType;
use App\Http\Rest\Response\Form\Types\MetaType;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * @author Anton Zakharuk <anton.zakharuk@cosmonova.net>
 */
class SitePageType extends AbstractFormType
{
    /** @var SlugTransformer */
    protected $slugTransformer;

    /** @var GlobalsService */
    protected $globals;

    public function __construct(TranslatorInterface $translator, GlobalsService $globalsService, SlugTransformer $slugTransformer)
    {
        parent::__construct($translator);

        $this->slugTransformer = $slugTransformer;
        $this->globals         = $globalsService;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     *
     * @return FormBuilderInterface|void
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class, [
                'label'         => $this->translator->trans('Page title')
            ])
            ->add('content', TextType::class, [
                'label'         => $this->translator->trans('Page content'),
                'required'      => false
            ])
            ->add('isEnabled', CheckboxType::class, [
                'label'         => $this->translator->trans('Is enabled'),
                'required'      => false
            ])
            ->add('slug', TextType::class, [
                'label'         => $this->translator->trans('Url')
            ])
            ->add('group', EntityType::class, [
                'label'         => $this->translator->trans('Group'),
                'class'         => SitePageGroup::class,
                'choice_label'  => 'name',
                'group_by'      => function($choice, $key, $value) {
                    return mb_strtoupper(mb_substr((string) $choice->getName(), 0, 1)); // group by first letter
                },
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('g')
                        ->orderBy('g.name', 'ASC'); // in alphabetical order
                },
                'required'      => false,
                'empty_data'    => null
            ])
            ->add('meta', MetaType::class, [
                'label'         => $this->translator->trans('Meta data'),
                'data_class'    => SitePageMeta::class
            ])
            ->add('save', SubmitType::class, [
                'label'         => $this->translator->trans('Save')
            ]);

        $builder->get('slug')
            ->addModelTransformer($this->slugTransformer);

        return $builder;
    }

    /**
     * @param OptionsResolver $resolver
     *
     * @return OptionsResolver|void
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        return $resolver->setDefaults([
            'allow_extra_fields' => true,
            'data_class'         => SitePage::class
        ]);
    }
}