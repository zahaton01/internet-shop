<?php

namespace App\Http\Rest\Response\Form\Types\Site\SitePage;

use App\Database\Domain\Entity\Site\SitePage\Meta\SitePageMeta;
use App\Database\Domain\Entity\Site\SitePage\SitePage;
use App\Http\Rest\Response\Form\Types\AbstractFormType;
use App\Http\Rest\Response\Form\Types\MetaType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 */
class StaticSitePageType extends AbstractFormType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     *
     * @return FormBuilderInterface|void
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        return $builder
            ->add('title', TextType::class, [
                'label'    => $this->translator->trans('Page title')
            ])
            ->add('isEnabled', CheckboxType::class, [
                'label'    => $this->translator->trans('Is enabled'),
                'required' => false
            ])
            ->add('meta', MetaType::class, [
                'label'         => $this->translator->trans('Meta data'),
                'data_class'    => SitePageMeta::class
            ])
            ->add('save', SubmitType::class, [
                'label'    => $this->translator->trans('Save')
            ]);
    }

    /**
     * @param OptionsResolver $resolver
     *
     * @return OptionsResolver|void
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        return $resolver->setDefaults([
            'allow_extra_fields' => true,
            'data_class'         => SitePage::class
        ]);
    }
}