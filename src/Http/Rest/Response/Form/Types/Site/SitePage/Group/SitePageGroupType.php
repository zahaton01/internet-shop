<?php

namespace App\Http\Rest\Response\Form\Types\Site\SitePage\Group;

use App\Application\Service\GlobalsService;
use App\Database\Domain\Entity\Site\SitePage\Group\SitePageGroup;
use App\Http\Rest\Response\Form\DataTransformer\SlugTransformer;
use App\Http\Rest\Response\Form\Types\AbstractFormType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 */
class SitePageGroupType extends AbstractFormType
{
    /** @var SlugTransformer */
    protected $slugTransformer;

    /** @var GlobalsService */
    protected $globals;

    public function __construct(TranslatorInterface $translator, GlobalsService $globalsService, SlugTransformer $slugTransformer)
    {
        parent::__construct($translator);

        $this->slugTransformer = $slugTransformer;
        $this->globals         = $globalsService;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     *
     * @return FormBuilderInterface|void
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [
                'label' => $this->translator->trans('Page title')
            ])
            ->add('slug', TextType::class, [
                'label' => $this->translator->trans('Url')
            ])
            ->add('twigTemplate', ChoiceType::class, [
                'label' => $this->translator->trans('Template')
            ])
            ->add('save', SubmitType::class, [
                'label' => $this->translator->trans('Continue')
            ]);

        $builder->get('slug')
            ->addModelTransformer($this->slugTransformer);

        return $builder;
    }

    /**
     * @param OptionsResolver $resolver
     *
     * @return OptionsResolver|void
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        return $resolver->setDefaults([
            'allow_extra_fields' => true,
            'data_class'         => SitePageGroup::class
        ]);
    }
}