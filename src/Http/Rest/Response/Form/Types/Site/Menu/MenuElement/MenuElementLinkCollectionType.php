<?php

namespace App\Http\Rest\Response\Form\Types\Site\Menu\MenuElement;

use App\Database\Domain\Entity\Site\Menu\MenuElement\MenuElement;
use App\Database\Domain\Entity\Site\Menu\MenuLink\MenuLink;
use App\Http\Rest\Response\Form\Types\AbstractFormType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 */
class MenuElementLinkCollectionType extends AbstractFormType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     *
     * @return FormBuilderInterface|void
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('linkCollectionName', TextType::class, [
                'label'        => $this->translator->trans('Name')
            ])
            ->add('links', EntityType::class, [
                'label'        => $this->translator->trans('Set couriers to this order'),
                'class'        => MenuLink::class,
                'choice_label' => 'name',
                'multiple'     => true,
                'expanded'     => true
            ])
            ->add('save', SubmitType::class, [
                'label' => $this->translator->trans('Save')
            ]);

        return $builder;
    }

    /**
     * @param OptionsResolver $resolver
     *
     * @return OptionsResolver|void
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        return $resolver->setDefaults([
           'allow_extra_fields' => true,
           'data_class'         => MenuElement::class
        ]);
    }
}