<?php

namespace App\Http\Rest\Response\Form\Types\Site\Menu\MenuElement;

use App\Database\Domain\Entity\Site\Menu\MenuElement\MenuElement;
use App\Database\Domain\Entity\Site\Menu\MenuLink\MenuLink;
use App\Http\Rest\Response\Form\Types\AbstractFormType;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 */
class MenuElementLinkType extends AbstractFormType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     *
     * @return FormBuilderInterface|void
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('isEnabled', CheckboxType::class, [
                'label'         => $this->translator->trans('Is enabled'),
                'required'      => false
            ])
            ->add('link', EntityType::class, [
                'class'         => MenuLink::class,
                'label'         => $this->translator->trans('Link'),
                'choice_label'  => 'name',
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('ml')
                        ->orderBy('ml.name', 'ASC');
                },
                'group_by'      => $this->groupByFirstLetter,
                'required'      => false
            ])
            ->add('save', SubmitType::class, [
                'label' => $this->translator->trans('Save')
            ]);

        return $builder;
    }

    /**
     * @param OptionsResolver $resolver
     *
     * @return OptionsResolver|void
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        return $resolver->setDefaults([
           'allow_extra_fields' => true,
           'data_class'         => MenuElement::class
        ]);
    }
}