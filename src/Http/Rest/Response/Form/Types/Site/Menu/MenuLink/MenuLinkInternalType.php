<?php

namespace App\Http\Rest\Response\Form\Types\Site\Menu\MenuLink;

use App\Database\Domain\Entity\Site\Menu\MenuLink\MenuLink;
use App\Database\Domain\Entity\Site\SitePage\SitePage;
use App\Http\Rest\Response\Form\Types\AbstractFormType;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 */
class MenuLinkInternalType extends AbstractFormType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     *
     * @return FormBuilderInterface|void
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [
                'label'         => $this->translator->trans('Name')
            ])
            ->add('sitePage', EntityType::class, [
                'class'         => SitePage::class,
                'label'         => $this->translator->trans('Page'),
                'choice_label'  => 'title',
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('sp')
                        ->orderBy('sp.title', 'ASC');
                },
                'required'      => false
            ])
            ->add('save', SubmitType::class, [
                'label' => $this->translator->trans('Save')
            ]);

        return $builder;
    }

    /**
     * @param OptionsResolver $resolver
     *
     * @return OptionsResolver|void
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        return $resolver->setDefaults([
           'allow_extra_fields' => true,
           'data_class'         => MenuLink::class
        ]);
    }
}