<?php

namespace App\Http\Rest\Response\Ajax\Normalizer\Product\Variety;

use App\Database\Domain\Entity\Commerce\Product\Variety\VarietyValue;
use App\Http\Rest\Response\Ajax\Normalizer\AbstractNormalizer;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 */
class VarietyValueNormalizer extends AbstractNormalizer
{
    /**
     * @param VarietyValue $content
     *
     * @return array
     */
    public function normalize($content): array
    {
        return [
            'id'   => $content->getId(),
            'name' => $content->getValue()
        ];
    }
}