<?php

namespace App\Http\Rest\Response\Ajax\Normalizer\Product\Variety;

use App\Database\Domain\Entity\Commerce\Product\Variety\Variety;
use App\Http\Rest\Response\Ajax\Normalizer\AbstractNormalizer;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 */
class VarietyNormalizer extends AbstractNormalizer
{
    /**
     * @param Variety $content
     *
     * @return array
     */
    public function normalize($content): array
    {
        return [
            'id'   => $content->getId(),
            'name' => $content->getName()
        ];
    }
}