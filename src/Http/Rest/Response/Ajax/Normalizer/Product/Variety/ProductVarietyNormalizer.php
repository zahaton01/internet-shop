<?php

namespace App\Http\Rest\Response\Ajax\Normalizer\Product\Variety;

use App\Database\Domain\Entity\Commerce\Product\Variety\ProductVariety;
use App\Http\Rest\Response\Ajax\Normalizer\AbstractNormalizer;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 */
class ProductVarietyNormalizer extends AbstractNormalizer
{
    /** @var VarietyValueNormalizer */
    private $varietyValueNormalizer;

    public function __construct(VarietyValueNormalizer $varietyValueNormalizer)
    {
        $this->varietyValueNormalizer = $varietyValueNormalizer;
    }

    /**
     * @param ProductVariety $content
     *
     * @return array
     */
    public function normalize($content): array
    {
        $values = [];
        foreach ($content->getValues() as $value) {
            $values[] = [
                'id'           => $value->getAttachedValue()->getId(),
                'name'         => $value->getAttachedValue()->getValue(),
                'availability' => $value->getAvailability()
            ];
        }

        return [
            'variety' => [
                'id'   => $content->getVariety()->getId(),
                'name' => $content->getVariety()->getName()
            ],
            'values'     => $values,
            'all_values' => $this->varietyValueNormalizer->normalizeMultiple($content->getVariety()->getValues())
        ];
    }
}