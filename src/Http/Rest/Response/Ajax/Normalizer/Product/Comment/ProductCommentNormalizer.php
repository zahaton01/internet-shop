<?php

namespace App\Http\Rest\Response\Ajax\Normalizer\Product\Comment;

use App\Database\Domain\Entity\Commerce\Product\Comment\ProductComment;
use App\Database\Domain\Entity\Commerce\Product\Comment\ProductCommentReply;
use App\Http\Rest\Response\Ajax\Normalizer\AbstractNormalizer;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 */
class ProductCommentNormalizer extends AbstractNormalizer
{
    /**
     * @param ProductComment $content
     *
     * @return array
     */
    public function normalize($content): array
    {
        $replies = [];
        /** @var ProductCommentReply $reply */
        foreach ($content->getReplies() as $reply) {
            $replies[] = [
                'id'      => $reply->getId(),
                'name'    => $reply->getName(),
                'text'    => $reply->getText(),
                'date'    => $reply->getCreationDate()->format('d-m-Y'),
                'isAdmin' => $reply->getUser() ? $reply->getUser()->isAdmin() : false
            ];
        }

        return [
            'id'      => $content->getId(),
            'name'    => $content->getName(),
            'text'    => $content->getText(),
            'date'    => $content->getCreationDate()->format('d-m-Y'),
            'replies' => $replies,
            'isAdmin' => $content->getUser() ? $content->getUser()->isAdmin() : false
        ];
    }
}