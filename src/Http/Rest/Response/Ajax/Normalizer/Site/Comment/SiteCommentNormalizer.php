<?php

namespace App\Http\Rest\Response\Ajax\Normalizer\Site\Comment;

use App\Database\Domain\Entity\Site\SiteComment;
use App\Database\Domain\Entity\Site\SiteCommentReply;
use App\Http\Rest\Response\Ajax\Normalizer\AbstractNormalizer;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 */
class SiteCommentNormalizer extends AbstractNormalizer
{
    /**
     * @param SiteComment $content
     *
     * @return array
     */
    public function normalize($content): array
    {
        $replies = [];
        /** @var SiteCommentReply $reply */
        foreach ($content->getReplies() as $reply) {
            $replies[] = [
                'id'      => $reply->getId(),
                'name'    => $reply->getName(),
                'text'    => $reply->getText(),
                'date'    => $reply->getCreationDate()->format('d-m-Y'),
                'isAdmin' => $reply->getUser() ? $reply->getUser()->isAdmin() : false
            ];
        }

        return [
            'id'      => $content->getId(),
            'name'    => $content->getName(),
            'text'    => $content->getText(),
            'date'    => $content->getCreationDate()->format('d-m-Y'),
            'replies' => $replies
        ];
    }
}