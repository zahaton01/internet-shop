<?php

namespace App\Http\Rest\Response\Ajax\Normalizer;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 */
abstract class AbstractNormalizer
{
    /**
     * @param $content
     *
     * @return array
     */
    public function normalizeMultiple($content)
    {
        $res = [];
        foreach ($content as $value) {
            $res[] = $this->normalize($value);
        }

        return $res;
    }

    abstract public function normalize($content): array;
}