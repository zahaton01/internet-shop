<?php

namespace App\Cli\Command\Config;

use App\Application\Util\Identifier;
use App\Cli\Command\AbstractCommand;
use App\Database\Domain\Entity\Site\SitePage\Meta\SitePageMeta;
use App\Database\Domain\Entity\Site\SitePage\SitePage;
use App\Database\Domain\Exception\ModelValidationException;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 */
class CreateSitePagesCommand extends AbstractCommand
{
    protected function configure()
    {
        $this
            ->setName('site:pages:create')
            ->addOption('clear', null, null, 'Removes all previous data and recreates it')
            ->setDescription('Configures app');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     *
     * @return int|void
     *
     * @throws ModelValidationException
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        if ($input->getOption('clear')) {
            $this->clearAllPages();
        }

        $this->createStaticPage('Main page', SitePage::STATIC_MAIN);
        $this->createStaticPage('Cart', SitePage::STATIC_CART);
        $this->createStaticPage('Catalog', SitePage::STATIC_CATALOG);
        $this->createStaticPage('Checkout', SitePage::STATIC_CHECKOUT);
        $this->createStaticPage('Contacts', SitePage::STATIC_CONTACT);
        $this->createStaticPage('Blog', SitePage::STATIC_BLOG);
        $this->createPage('Delivery', 'delivery', '<h1>Your content should be here</h1>');

        return 1;
    }

    private function clearAllPages(): void
    {
        $pages = $this->manager->em()->getRepository(SitePage::class)->findAll();

        $this->manager->remove($pages);
    }

    /**
     * @param string $title
     * @param string $slug
     * @param string $content
     *
     * @return SitePage|null
     *
     * @throws ModelValidationException
     */
    private function createPage(string $title, string $slug, string $content)
    {
        if ($this->manager->em()->getRepository(SitePage::class)->findOneBy(['slug' => $slug])) return null;

        $page = new SitePage();
        $page
            ->setId(Identifier::uuid())
            ->setMeta((new SitePageMeta())->setId(Identifier::uuid()))
            ->setIsEnabled(true)
            ->setSlug($slug)
            ->setTitle($title)
            ->setIsStatic(false)
            ->setContent($content);

        return $this->manager->save($page);
    }

    /**
     * @param string $title
     * @param string $type
     *
     * @return SitePage|null
     *
     * @throws ModelValidationException
     */
    private function createStaticPage(string $title, string $type)
    {
        if ($this->manager->em()->getRepository(SitePage::class)->findOneBy(['staticType' => $type])) return null;

        $page = new SitePage();
        $page
            ->setId(Identifier::uuid())
            ->setMeta((new SitePageMeta())->setId(Identifier::uuid()))
            ->setIsEnabled(true)
            ->setTitle($title)
            ->setSlug(uniqid())
            ->setIsStatic(true)
            ->setStaticType($type);

        return $this->manager->save($page);
    }
}