<?php

namespace App\Cli\Command\Config;

use App\Application\Util\Identifier;
use App\Cli\Command\AbstractCommand;
use App\Database\Domain\Entity\Site\Menu\Menu;
use App\Database\Domain\Entity\Site\Menu\MenuElement\MenuElement;
use App\Database\Domain\Entity\Site\Menu\MenuLink\MenuLink;
use App\Database\Domain\Entity\Site\SitePage\SitePage;
use App\Database\Domain\Exception\ModelValidationException;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 */
class CreateMenuCommand extends AbstractCommand
{
    protected function configure()
    {
        $this
            ->setName('site:menu:create')
            ->addOption('clear', null, null, 'Removes all previous data and recreates it')
            ->setDescription('Configures app');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     *
     * @return int|void
     *
     * @throws ModelValidationException
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        if ($input->getOption('clear')) {
            $this->clearMenus();
            $this->clearMenuElements();
        }

        $footer       = $this->createMenu(Menu::FOOTER);
        $header       = $this->createMenu(Menu::HEADER);

        $blogPage     = $this->manager->em()->getRepository(SitePage::class)->findOneBy(['staticType' => SitePage::STATIC_BLOG]);
        $blogMenuLink = $this->createMenuLink($blogPage);
        $this->createMenuElementLink($blogMenuLink, $header);

        $deliveryPage = $this->manager->em()->getRepository(SitePage::class)->findOneBy(['slug' => 'delivery']);
        if (null !== $deliveryPage) {
            $deliveryMenuLink = $this->createMenuLink($deliveryPage);
            $this->createMenuElementLink($deliveryMenuLink, $header);
        }

        return 1;
    }

    private function clearMenus(): void
    {
        $menus = $this->manager->em()->getRepository(Menu::class)->findAll();
        $this->manager->remove($menus);
    }

    private function clearMenuElements(): void
    {
        $elements = $this->manager->em()->getRepository(MenuElement::class)->findAll();
        $this->manager->remove($elements);
    }

    /**
     * @param string $type
     *
     * @return Menu
     *
     * @throws ModelValidationException
     */
    private function createMenu(string $type): Menu
    {
        /** @var Menu $menu */
        $menu = $this->manager->em()->getRepository(Menu::class)->findOneBy(['type' => $type]);

        if (null !== $menu) {
            return $menu;
        }

        $menu = new Menu();
        $menu
            ->setId(Identifier::uuid())
            ->setType($type);

        return $this->manager->save($menu);
    }

    /**
     * @param MenuLink $link
     * @param Menu     $menu
     *
     * @throws ModelValidationException
     */
    private function createMenuElementLink(MenuLink $link, Menu $menu)
    {
        if ($menu->getElements()) {
            foreach ($menu->getElements() as $element) {
                if ($element->isLink() && $element->getLink()) {
                    if ($element->getLink()->getId() === $link->getId()) {
                        return;
                    }
                }
            }
        }

        $element = (new MenuElement())
            ->setId(Identifier::uuid())
            ->setIsEnabled(true)
            ->setType(MenuElement::LINK)
            ->setLink($link);

        $menu->addElement($element);

        $this->manager->save($element);
    }

    /**
     * @param SitePage $page
     *
     * @return MenuLink
     */
    private function createMenuLink(SitePage $page): MenuLink
    {
        /** @var MenuLink $link */
        $link = $this->manager->em()->getRepository(MenuLink::class)->findOneBy(['sitePage' => $page->getId()]);

        if (null !== $link) {
            return $link;
        }

        $link = (new MenuLink())
            ->setId(Identifier::uuid())
            ->setType(MenuLink::INTERNAL)
            ->setSitePage($page)
            ->setName($page->getTitle());

        return $link;
    }
}