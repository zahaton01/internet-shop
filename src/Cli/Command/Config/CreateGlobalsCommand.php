<?php

namespace App\Cli\Command\Config;

use App\Application\Util\Identifier;
use App\Cli\Command\AbstractCommand;
use App\Database\Domain\Entity\Globals;
use App\Application\Model\AppEnv\Currency;
use App\Application\Model\AppEnv\Locale;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 */
class CreateGlobalsCommand extends AbstractCommand
{
    protected function configure()
    {
        $this
            ->setName('globals:create')
            ->setDescription('Sets globals');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     *
     * @return int|void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('Clearing all globals...');
        $this->clear();

        $output->writeln('Starting the app configuration...');

        $this->createGlobal(Locale::UA, Globals::DEFAULT_LOCALE);
        $this->createGlobal(Locale::all(), Globals::ENABLED_LOCALES);
        $this->createGlobal('d-m-Y H:i:s', Globals::DATE_DISPLAYING_FORMAT);
        $this->createGlobal('http://localhost:8080/index.php', Globals::HOST_ADDRESS);
        $this->createGlobal('My super app', Globals::APP_NAME);
        $this->createGlobal('Hello! This is my super app...', Globals::APP_DESCRIPTION);
        $this->createGlobal(Currency::USD, Globals::APP_CURRENCY);
        $this->createGlobal('', Globals::METRICS_GOOGLE);
        $this->createGlobal('', Globals::METRICS_YANDEX);
        $this->createGlobal('', Globals::METRICS_GOOGLE_SEARCH_CONSOLE);
        $this->createGlobal('Your phone here', Globals::CONTACT_PHONE);
        $this->createGlobal('Your email here', Globals::CONTACT_EMAIL);
        $this->createGlobal('true', Globals::NOVA_POSHTA_IS_ENABLED);
        $this->createGlobal('https://api.novaposhta.ua/v2.0/json/', Globals::NOVA_POSHTA_HOST);
        $this->createGlobal('9f5789101c118fb934f92df6f72ebad2', Globals::NOVA_POSHTA_API_KEY);
        $this->createGlobal('80', Globals::NOVA_POSHTA_PRICE);
        $this->createGlobal('40', Globals::COURIER_PRICE);
        $this->createGlobal('true', Globals::LIQPAY_IS_ENABLED);
        $this->createGlobal('sandbox_i67234337582', Globals::LIQPAY_PUBLIC_KEY);
        $this->createGlobal('sandbox_0s9Exbr1DNoCsODBNIKfNRTLLnR070opn2AbFB6a', Globals::LIQPAY_PRIVATE_KEY);
        $this->createGlobal('https://www.liqpay.ua/api/', Globals::LIQPAY_API_URL);
        $this->createGlobal('https://www.liqpay.ua/api/3/checkout', Globals::LIQPAY_CHECKOUT_URL);
        $this->createGlobal('123', Globals::CONTACT_VIBER);
        $this->createGlobal('123', Globals::CONTACT_TELEGRAM);
        $this->createGlobal('123', Globals::CONTACT_WHATS_UP);

        $output->writeln('Finished');
        return 1;
    }

    private function clear(): void
    {
        $all = $this->manager->em()->getRepository(Globals::class)->findAll();
        $this->manager->remove($all);
    }

    private function createGlobal($value, string $key, $recreate = true): void
    {
        if (!$recreate) return;

        $global = (new Globals())->setId(Identifier::uuid());
        $global->setKey($key);

        if (is_array($value)) {
            $global->setArray($value);
        } else {
            $global->setValue((string) $value);
        }

        $this->manager->save($global);
    }
}
