<?php

namespace App\Cli\Command\User;

use App\Application\Model\AppEnv\Roles;
use App\Application\Util\Identifier;
use App\Cli\Command\AbstractCommand;
use App\Database\Domain\Entity\User\Admin;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoder;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 */
class CreateAdminCommand extends AbstractCommand
{
    protected function configure()
    {
        $this
            ->setName('user:create:admin')
            ->setDescription('Creates new admin')
            ->addArgument('login', InputArgument::REQUIRED, 'Login of the account')
            ->addArgument('password', InputArgument::REQUIRED, 'Password of the account');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     *
     * @return int|void|null
     *
     * @throws \Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('Creating...');

        /** @var UserPasswordEncoder $encoder */
        $encoder = $this->params->container()->get('security.password_encoder');

        $user = (new Admin())->setId(Identifier::uuid());
        $user->setLogin($input->getArgument('login'));
        $user->setRoles([Roles::ADMIN]);
        $user->setPassword($encoder->encodePassword($user, $input->getArgument('password')));

        $this->manager->save($user);

        $output->writeln('Done');

        return 1;
    }
}