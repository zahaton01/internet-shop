<?php

namespace App\Cli\Command;

use App\Application\Service\ParamsService;
use App\Database\Domain\Operation\Manager;
use Symfony\Component\Console\Command\Command;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 */
abstract class AbstractCommand extends Command
{
    /** @var Manager */
    protected $manager;

    /** @var ParamsService */
    protected $params;

    public function __construct(Manager $manager, ParamsService $params)
    {
        parent::__construct();

        $this->manager = $manager;
        $this->params  = $params;
    }
}